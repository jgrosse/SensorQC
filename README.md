This repository contains application software for GPIB and RS232 devices and probestation control (currently only PA300) as used for Sensor QC in Goettingen.

To retrieve the code, run:

git clone https://[your CERN username]@gitlab.cern.ch/jgrosse/SensorQC.git

which delivers the master branch (trunk). For another branch, go to the cloned version and use

git checkout [branch name]

To build on linux, make sure QT and ROOT bin files are in your path. Then call:

qmake -r

make

(or use qmake-qt5 if you have a native QT installation)

All application will be in the respective project directories (e.g. GUIs for QT-based applications).
NB: Applications require ROOT, QT5 and boost as well as linux-gpib packages.

To build on windows, set ROOTSYS and VCDIR environment variables to your ROOT and VisualStudio installations and make sure ROOT and QT are in your PATH. Then call:

setup

qmake -r

nmake

All applications will be in the bin folder.
NB: Applications require ROOT and QT5 packages.

## CV scan
When using Keithley6487 as HV source, must ramp voltage back to 0.

