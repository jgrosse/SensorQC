#include "InfluxDB.h"

#include <influxdb.hpp>

InfluxDB::InfluxDB()
{

}
/*
void InfluxDB::setConfiguration(const nlohmann::json& config){
  
  // Read the DB configuration (port, host and database) from 'config'
  if (config.find("port")   == config.end() ||
      config.find("host")   == config.end() ||
      config.find("database") == config.end() )
      {
        throw std::runtime_error(
        "Please specify 'port' [int], 'host' [str] and 'database' [str] in the \n" \
        "json configuration file under the correspondent sink. See an example \n" \
        "in 'src/configs/input-hw.json'"
        );
      }
  else
  {
    m_port   = config["port"];
    m_host   = config["host"];
    m_dbName = config["database"];
  }

  // Read also the username and password, if provided
  if (config.find("username") != config.end() &&
      config.find("password") != config.end() )
      {
        m_usr = config["username"];
        m_pwd = config["password"];
      }

  // Get the precision, if provided
  if (config.find("precision") != config.end() ) m_prec = config["precision"];
}
*/
void InfluxDB::init(std::string host, int  port, std::string dbName, std::string usr, std::string pwd)
{

  // Initialize the influxdb_cpp object
  m_si = std::make_shared<influxdb_cpp::server_info>(influxdb_cpp::server_info(host, port, dbName, usr, pwd, "ns"));

  // Check whether the provided database exists
  m_retval = influxdb_cpp::query(m_resp, "show databases", *m_si);
  errorCheck(m_retval, m_resp);

  if (m_resp.find(dbName) == std::string::npos)
    throw std::runtime_error("No database \"" + dbName + "\" found on the specified server");
}

void InfluxDB::startMeasurement(const std::string& measurement, std::chrono::time_point<std::chrono::system_clock> time)
{
  m_measName = measurement;

  // Convert the timestamp to ns
  m_timestamp = std::chrono::duration_cast<std::chrono::nanoseconds>(time.time_since_epoch()).count();
}

void InfluxDB::setTag(const std::string& name, const std::string& value){
  m_tagsString[name] = value;
}
void InfluxDB::setTag(const std::string& name, const int8_t value){
  setTag(name, std::to_string(value));
}
void InfluxDB::setTag(const std::string& name, const int32_t value){
  setTag(name, std::to_string(value));
}
void InfluxDB::setTag(const std::string& name, const int64_t value){
  setTag(name, std::to_string(value));
}
void InfluxDB::setTag(const std::string& name, const double value){
  setTag(name, std::to_string(value));
}

void InfluxDB::setField(const std::string& name, const std::string& value){
  m_fieldsString[name] = value;
}
void InfluxDB::setField(const std::string& name, const int8_t value){
  m_fieldsInt[name] = value;
}
void InfluxDB::setField(const std::string& name, const int32_t value){
  m_fieldsInt[name] = value;
}
void InfluxDB::setField(const std::string& name, const int64_t value){
  m_fieldsInt[name] = value;
}
void InfluxDB::setField(const std::string& name, const double value){
  m_fieldsDouble[name] = value;
}

void InfluxDB::setPrecision(int precision)
{
  m_prec = precision;
}

void InfluxDB::recordPoint()
{
  // Create InfluxDB object and useful casts
  influxdb_cpp::builder builder;
  influxdb_cpp::detail::tag_caller  & tag_caller  =static_cast<influxdb_cpp::detail::tag_caller  &>(builder);
  influxdb_cpp::detail::field_caller& field_caller=static_cast<influxdb_cpp::detail::field_caller&>(builder);

  // Set the measurement name
  builder.meas(m_measName);

  // Add tags to the InfluxDB object
  for (const std::pair<std::string, std::string>& x: m_tagsString)
    tag_caller.tag(x.first, x.second);

  bool fieldIsInitialized = false;
  
  // Add fields to the InfluxDB object
  for (const std::pair<std::string, double     >& x: m_fieldsDouble)
    {
      if(!fieldIsInitialized)
	{
	  tag_caller  .field(x.first, x.second, m_prec);
	  fieldIsInitialized = true;	  
	}
      else
	{
	  field_caller.field(x.first, x.second, m_prec);
	}
    }

  for (const std::pair<std::string, int32_t    >& x: m_fieldsInt   )
    {
      if(!fieldIsInitialized)
	{
	  tag_caller  .field(x.first, x.second);
	  fieldIsInitialized = true;
	}
      else
	{
	  field_caller.field(x.first, x.second);
	}
    }

  for (const std::pair<std::string, std::string>& x: m_fieldsString)
    {
      if(!fieldIsInitialized)
	{
	  tag_caller  .field(x.first, x.second);
	  fieldIsInitialized = true;
	}
      else
	{
	  field_caller.field(x.first, x.second);
	}
    }

  // Need fields to upload
  if(!fieldIsInitialized)
    throw std::runtime_error("Unable to record InfluxDB point with no fields.");
  
  // Add the timestamp
  field_caller.timestamp(m_timestamp);

  // Upload everything and store the command result in &m_resp.
  m_retval = field_caller.post_http(*m_si, &m_resp);
  errorCheck(m_retval, m_resp);
}

void InfluxDB::endMeasurement()
{
  m_fieldsString.clear();
  m_fieldsInt.clear();
  m_fieldsDouble.clear();
}

void InfluxDB::query(std::string& m_resp, const std::string& query)
{
  m_retval = influxdb_cpp::query(m_resp, query, *m_si);
  errorCheck(m_retval, m_resp);
}

void InfluxDB::errorCheck(int m_retval, std::string& m_resp)
{
  if (m_retval != 0){
    throw std::runtime_error("Received an error from InfluxDB: " + m_resp);
  }
}
