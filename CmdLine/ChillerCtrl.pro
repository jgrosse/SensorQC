TEMPLATE = app

CONFIG -= qt

include(../build-config.inc)
  
INCLUDEPATH = . ../include

SOURCES += ChillerCtrl.cxx

unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lpthread
        INCLUDEPATH += ../PixRS232
        QMAKE_LFLAGS  +=  -L../PixRS232 -lPixRS232
        QMAKE_RPATHDIR += ../PixRS232
}
win32 {
    DESTDIR = ../bin
    CONFIG += console
    QMAKE_LFLAGS_CONSOLE += /LIBPATH:../PixRS232 /LIBPATH:../bin
    LIBS += PixRS232.lib
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
    INCLUDEPATH += . ../PixRS232
}
