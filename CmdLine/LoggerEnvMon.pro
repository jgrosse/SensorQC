TEMPLATE = app

CONFIG -= qt

include(../build-config.inc)
  
INCLUDEPATH = . ../include

SOURCES += LoggerEnvMon.cxx

unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX
	QMAKE_LFLAGS += -lpthread -lboost_system
}
win32 {
    DESTDIR = ../bin
    CONFIG += console
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
}
