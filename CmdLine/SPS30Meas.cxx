//#include "ComTools.h"
#include "SPS30.h"
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#include <iostream>
#include <string>
#include <string.h>
#include <thread>
#include <chrono>
#include <time.h>
#include <signal.h>

#include <InfluxDB.h>

bool quit=false;
void cleanup(int /*signum*/)
{ quit=true; }

int main(int argc, char* argv[])
{
  int tPortID = atoi(argv[1]);
  if(argc<2){
    printf("USAGE: SPS30Meas [COM PORT NUMBER] <-d dbName> <-m dbMeasName> <-i interval [s]> <-c>\n");
    return -1;
  }

  std::string dbName="";
  std::string host="127.0.0.1";
  uint port=8086;
  std::string DBmeasName = "environment_132";
  uint twait = 10;
  bool runClean = false;
  InfluxDB *db=0;
  FILE *out = 0;

  for(int i=2;i<argc;i++){
    if(strcmp(argv[i],"-c")==0)
	runClean = true;
    else if(strcmp(argv[i],"-i")==0){
      i++;
      twait = (uint)atoi(argv[i]);
    }
    else if(strcmp(argv[i],"-d")==0){
      i++;
      dbName = argv[i];
    }
    else if(strcmp(argv[i],"-m")==0){
      i++;
      DBmeasName = argv[i];
    }
    else if(strcmp(argv[i],"-h")==0){
      i++;
      host = argv[i];
    }
    else if(strcmp(argv[i],"-p")==0){
      i++;
      port = (uint)atoi(argv[i]);
    }
  }
  if(twait<1) twait = 1;

  if(dbName!=""){
    db = new InfluxDB;
    db->init(host, port, dbName);
  } else {
    remove("SPS30_log.txt");
    out = fopen("SPS30_log.txt","w2");
    if(out==0){
      printf("No DB given and can't open out file SPS30_log.txt -> exit!\n");
      return -1;
    }
  }

  SPS30 mySPS(tPortID);
  
  const int npt=10;
  std::string labels[npt];
  float data[npt];
  double avdata[npt];

  if(dbName==""){
    mySPS.getLabels(labels);
    fprintf(out,"Timestamp\t");
    for(uint i=0;i<9;i++) fprintf(out,"%s\t", labels[i].c_str());
    fprintf(out,"%s\n", labels[9].c_str());
  }

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  //mySPS.printInfo();
  //mySPS.resetDev();
  uint myintv;
  mySPS.getIntv(myintv);
  if(myintv==0) printf("WARNING, cleaning interval is zero!\n");

  mySPS.startMeas();
  if(runClean){
    mySPS.cleanFan();
    // wait for fan cleaning to finish
    for(int i=0;i<15;i++){
      printf("Wait %d\n", i);
      std::this_thread::sleep_for (std::chrono::milliseconds(1000));  
    }
  }

  // wait 10 s for SPS30 to clean itself an initialise
  for(uint i=0; i<10; i++){
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));  
    if(quit) break;
  }
  do{
    //float data[npt], avdata[npt];
    for(uint i=0;i<(npt-1);i++) avdata[i] = 0.;
    uint nrep = 1;
    if(twait>10){
      nrep = 5;
      twait -= 5;
    }
    for(uint k=0; k<nrep; k++){
      mySPS.readMeas(data);
      for(uint i=0;i<(npt-1);i++) avdata[i] += (double)data[i];
      std::this_thread::sleep_for (std::chrono::milliseconds(950));  
    }
    for(uint i=0;i<(npt-1);i++) avdata[i] /= (double)nrep;
    time_t mytime = time(NULL);
    if(db!=0){
      try{
	db->setTag("Sensor", "Dust");
	db->startMeasurement(DBmeasName, std::chrono::system_clock::now());
	// write all values that are covered into stream
	db->setField("PartDens0.5", avdata[npt-2]-avdata[4]);// d>0.5
	db->setField("PartDens1",   avdata[npt-2]-avdata[5]);// d>1.0
	db->setField("PartDens5",   avdata[npt-2]-avdata[7]);// d>4.0
	db->recordPoint();
	db->endMeasurement();
      } catch(std::runtime_error &err){
	std::cerr << "Error writing to InfluxDB: " << std::string(err.what()) << std::endl;
      } catch(...){
	std::cerr << "Unknown error writing to InfluxDB" << std::endl;
      }
    } else {
      fprintf(out,"%ld\t",mytime);
      for(uint i=0;i<(npt-1);i++) fprintf(out,"%f\t", avdata[i]);
      fprintf(out,"%f\n", avdata[npt-1]);
    }
    for(uint i=0; i<twait; i++){
      std::this_thread::sleep_for (std::chrono::milliseconds(1000));  
      if(quit) break;
    }
  }while(!quit);

  mySPS.stopMeas();

  if(db!=0) delete db;
  else      fclose(out);
  return 0;
}
