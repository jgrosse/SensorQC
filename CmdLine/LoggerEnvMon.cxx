#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <chrono>
#include <sys/stat.h> 

std::vector<std::string> split_string(const std::string& str,
                                      const std::string& delimiter)
{
    std::vector<std::string> strings;

    std::string::size_type pos = 0;
    std::string::size_type prev = 0;
    while ((pos = str.find(delimiter, prev)) != std::string::npos)
    {
        strings.push_back(str.substr(prev, pos - prev));
        prev = pos + 1;
    }

    // To get the last substring (or only, if delimiter is not found)
    strings.push_back(str.substr(prev));

    return strings;
}

int main(int argc, char** argv)
{

  std::string sensName = "Flex NTC";
  std::string ipadd = "192.168.2.153";
  bool doQuery = false;
  int rport = 7700;
  for(int i=0;i<argc;i++){
    // -i <IP address>
    if(strcmp(argv[i],"-i")==0 && i<(argc-1))
      ipadd = argv[i+1];
    // -p <port>
    if(strcmp(argv[i],"-p")==0 && i<(argc-1))
      rport = atoi(argv[i+1]);
    // -n <sensor name>
    if(strcmp(argv[i],"-n")==0 && i<(argc-1))
      sensName = argv[i+1];
    // -q: just list raw output from logger
    if(strcmp(argv[i],"-q")==0)
      doQuery = true;
    if(strcmp(argv[i],"-h")==0){
      std::cout << "Usage: LoggerEnvMon [-i IP addr.] [-p port] [-n sensor name]" << std::endl;
      std::cout << "   defaults: -i " << ipadd << " -p " << rport << " -n " << sensName << std::endl;
      return 0;
    }
  }
  

  size_t len;
  char bufc[1024], bufr[1024];
  int waitcnt=0, maxwc=10;

  struct stat st;
  int ierr = stat (("/tmp/envMon_"+ipadd+"_"+std::to_string(rport)+".txt").c_str(), &st);
  if (ierr == 0) {
    int file_time = st.st_mtime;
    int now_time = (intmax_t)std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    if((now_time-file_time)<1){
      std::string line;
      std::ifstream tmpfile;
      tmpfile.open ("/tmp/envMon_"+ipadd+"_"+std::to_string(rport)+".txt");//,std::ios::out);
      while ( getline (tmpfile,line) ){
	if(line.substr(0, sensName.length())==sensName){
	  std::string val = line.substr(sensName.length()+1, line.length());
	  size_t lpos = val.find(" ");
	  if(lpos!=std::string::npos) val.erase(lpos, val.length()-lpos);
	  std::cout << val << std::endl;
	  tmpfile.close();
	  return 0;
	}
      }
      tmpfile.close();
    }
  }

  boost::asio::io_service ios, iosc;

  boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(ipadd), rport);
  boost::asio::ip::tcp::socket socket(ios);
  socket.connect(endpoint);

  bufc[0]=127;
  len = socket.send(boost::asio::buffer(bufc, 1));
  std::this_thread::sleep_for (std::chrono::milliseconds(500));
  if(socket.available()!=0){
    len = socket.receive(boost::asio::buffer(bufr));
  } else // something went wrong
    return -1;
  
  bufc[0]='/';
  bufc[1]='E';
  bufc[2]='/';
  bufc[3]='M';
  bufc[4]='/';
  bufc[5]='R';
  bufc[6]='\r';
  len = socket.send(boost::asio::buffer(bufc, 7));
  bool foundStr=false;
  while(waitcnt<maxwc && !foundStr){
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));
    if(socket.available()!=0){
      len = socket.receive(boost::asio::buffer(bufr));
      //std::cout << "Have data of length " << len << "!" << std::endl;
      if(len>0){
	if(doQuery){
	  std::cout << std::string(bufr) << std::endl;
	  foundStr = true;
	} else {
	  std::vector<std::string> lines = split_string(bufr, "\n");
	  for(std::vector<std::string>::iterator it=lines.begin(); it!=lines.end(); it++){
	    if(it->substr(0, sensName.length())==sensName){
	      foundStr = true;
	      std::ofstream tmpfile;
	      tmpfile.open ("/tmp/envMon_"+ipadd+"_"+std::to_string(rport)+".txt",std::ios::out);
	      tmpfile << bufr;
	      tmpfile.close();
	      std::string val = it->substr(sensName.length()+1, it->length());
	      size_t lpos = val.find(" ");
	      if(lpos!=std::string::npos) val.erase(lpos, val.length()-lpos);
	      std::cout << val << std::endl;
	      break;
	    }
	  }
	}
      }
      waitcnt++;
    }
  }

  bufc[0]='/';
  bufc[1]='e';
  bufc[2]='/';
  bufc[3]='m';
  bufc[4]='/';
  bufc[5]='r';
  bufc[6]='\r';
  len = socket.send(boost::asio::buffer(bufc, 7));
  std::this_thread::sleep_for (std::chrono::milliseconds(500));
  if(socket.available()!=0){
    len = socket.receive(boost::asio::buffer(bufr));
  }

  socket.close();

  return 0;
}
