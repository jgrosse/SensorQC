#include <TGraph.h> 
#include <TMultiGraph.h> 
#include <TF1.h>
#include <TApplication.h>
#include <TAxis.h>
#include <TMath.h>
#include <TSystem.h>
#include <TCanvas.h>

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <string.h>
#include <time.h>
#include <signal.h>
#include "upsleep.h"

#include "PixGPIBDevice.h"
#include "ArduinoADC.h"

bool quit=false;
void cleanup(int /*signum*/)
{ quit=true; }

int main(int argc, char **argv){
  int mySleep=2, board=0, PAD=22, PAD2=1, SAD=0, PAD3=68;

  for(int i=0;i<argc;i++){
    if(i<(argc-1) && strcmp(argv[i],"-p1")==0)
      PAD = atoi(argv[i+1]);
    if(i<(argc-1) && strcmp(argv[i],"-p2")==0)
      PAD2 = atoi(argv[i+1]);
    if(i<(argc-1) && strcmp(argv[i],"-p3")==0)
      PAD3 = atoi(argv[i+1]);
    if(i<(argc-1) && strcmp(argv[i],"-w")==0)
      mySleep = atoi(argv[i+1]);
  }
  
  // freq. meas. from humidity sensor
  PixGPIBDevice *m_devH1 = new PixGPIBDevice(board,PAD,SAD,0);
  // I2C to SHT85 humidity sensor
  ArduinoADC *m_devH2 = new ArduinoADC(PAD2);

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  std::cout << "Press ctrl-C to stop" << std::endl << std::endl;

  printf("1/f(1/Hz)\tRH(%%)\tT(degC)\n");

  while(!quit){

    if(m_devH1->measureFrequency(0., true, 0)==0)
      printf("%.4le\t", 1./m_devH1->getFrequency(0));

    // send 2400 to SHT85 to trigger reading
    std::string response = m_devH2->getMeasVal(PAD3, "2400", 6);

    // extract humidity and temperature reading
    std::vector<uint8_t> data(6);
    for(uint32_t i=0;i<response.size()/2;i++)
      {
	std::string byte=response.substr(2*i,2);
	data[i]=std::stoul(byte.c_str(), nullptr, 16);
      }
    
    uint16_t tdata=((data[0])<<8)|data[1];
    uint16_t hdata=((data[3])<<8)|data[4];
    // Parse the data
    double humidity=100*((float)hdata)/(pow(2,16)-1.);
    if(humidity<1.e-3) humidity=1.e-3;
    double temperature=-45+175*((float)tdata)/(pow(2,16)-1.);
    printf("%.2lf\t%.2lf\n", humidity, temperature);

    UPGen::Sleep(1000*mySleep);
  }

  delete m_devH1;
  delete m_devH2;
  printf("\n");
  return 0;
}

