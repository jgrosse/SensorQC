TEMPLATE = app

CONFIG -= qt

include(../build-config.inc)
  
INCLUDEPATH = . ../include ../PixGPIB 

SOURCES += HumidMeas.cxx

unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lgpib -lpthread
        INCLUDEPATH += $${system(root-config --incdir)}
        QMAKE_LFLAGS  +=  $${system(root-config --libs)} -L../PixGPIB -lPixGPIB
        QMAKE_RPATHDIR += ../PixGPIB
}
win32 {
    DESTDIR = ../bin
    CONFIG += console
    QMAKE_LFLAGS_CONSOLE += /LIBPATH:../PixGPIB /LIBPATH:../bin /LIBPATH:$(ROOTSYS)/lib
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
    INCLUDEPATH += . $(ROOTSYS)/include
    LIBS += PixGPIB.lib Gpib-32.obj
    LIBS += libCore.lib
    LIBS += libCint.lib
    LIBS += libRIO.lib
    LIBS += libNet.lib
    LIBS += libHist.lib
    LIBS += libGraf.lib
    LIBS += libGraf3d.lib
    LIBS += libGpad.lib
    LIBS += libTree.lib
    LIBS += libRint.lib
    LIBS += libPostscript.lib
    LIBS += libMatrix.lib
    LIBS += libPhysics.lib
    LIBS += libMathCore.lib
    LIBS += libHistPainter.lib
}
