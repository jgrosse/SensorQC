#include <TGraph.h> 
#include <TMultiGraph.h> 
#include <TF1.h>
#include <TApplication.h>
#include <TAxis.h>
#include <TMath.h>
#include <TSystem.h>
#include <TCanvas.h>

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <string.h>
#include <time.h>
#include "upsleep.h"

#include "PixGPIBDevice.h"

int main(int argc, char **argv){
  int board=0, PAD=22, PAD2=28, PAD3=29, SAD=0, mySleep=2;//, nCycl = 5;

  for(int i=0;i<argc;i++){
    if(i<(argc-1) && strcmp(argv[i],"-p1")==0)
      PAD = atoi(argv[i+1]);
    if(i<(argc-1) && strcmp(argv[i],"-p2")==0)
      PAD2 = atoi(argv[i+1]);
    if(i<(argc-1) && strcmp(argv[i],"-p3")==0)
      PAD3 = atoi(argv[i+1]);
    if(i<(argc-1) && strcmp(argv[i],"-w")==0)
      mySleep = atoi(argv[i+1]);
//     if(i<(argc-1) && strcmp(argv[i],"-c")==0)
//       nCycl = atoi(argv[i+1]);
  }
    
  TApplication app("app",NULL,NULL);

  TCanvas can("mycan","Temperature",1200,600);
  can.Clear();
  can.Divide(3,1);
  gSystem->ProcessEvents();

  TGraph gt, gh, gd, gc;
  gt.SetMarkerStyle(20);
  gh.SetMarkerStyle(20);
  gc.SetMarkerStyle(20);
  gd.SetMarkerStyle(24);
  gd.SetMarkerColor(2);
  TMultiGraph *mg = new TMultiGraph;
  time_t  timev, toffset=0;
  float res, temp, humid, dewpt, Tchuck;
  float cal[4]={0.0020091, 0.000296397, 1.65669e-06, -175.422};
  TF1 fpos("pt1000","1000.*(1+3.9083e-3*x-5.775e-7*x*x)", 0, 100);
  TF1 fneg("pt1000","1000.*(1+3.9083e-3*x-5.775e-7*x*x-4.183e-12*(x-100)*x*x*x)", -100, 0);

  // resistance meas. of PT1000
  PixGPIBDevice *m_devTC = new PixGPIBDevice(board,PAD2,SAD,0);
  // resistance meas. of NTC
  PixGPIBDevice *m_devTA = new PixGPIBDevice(board,PAD3,SAD,0);
  // voltage meas. from humidity sensor
  PixGPIBDevice *m_devH = new PixGPIBDevice(board,PAD,SAD,0);

  std::cout << "Press ctrl-C to stop" << std::endl;
  //for(int i=0;i<nCycl;i++){
  int i = 0;
  while(true){

    time(&timev);
    if(i==0) toffset = timev;
    timev -= toffset;

    // PT1000 on the chuck: calibration from data sheet
    m_devTC->measureResistances();
    res = m_devTC->getResistance(0);
    Tchuck = (res<1000.)?fneg.GetX(res):fpos.GetX(res);

    // NTC for ambient air: constants from fit from table of calib., see TempCheck
    m_devTA->measureResistances();
    res = m_devTA->getResistance(0);
    temp = 1/(cal[0]+cal[1]*TMath::Log(res)+cal[2]*TMath::Power(TMath::Log(res),3))+cal[3];

    // humidity from voltage of HIH4000; calibration from data sheet for 5.0V operation voltage
    m_devH->measureVoltages();
    res = m_devH->getVoltage(0);
    humid = (res/5.0-0.16)/0.0062/(1.0546-0.00216*temp);
    // dew point std. formular from humidity and ambient air temperature
    dewpt = (241.2*TMath::Log(humid/100.)+4222.03716*temp/(241.2+temp)) / 
      (17.5043 - TMath::Log(humid/100.)-17.5043*temp/(241.2+temp));

    //std::cout << "T = " << temp << " degC, humd = " << humid << " rel.% at " << timev << "s" << std::endl;

    // fill graphs and display
    gt.SetPoint(i, timev, temp);
    gh.SetPoint(i, timev, humid);
    gd.SetPoint(i, timev, dewpt);
    gc.SetPoint(i, timev, Tchuck);
    if(i>1){
      can.cd(1);
      gt.Draw("AP");
      gt.GetXaxis()->SetTimeDisplay(1);
      gt.GetXaxis()->SetTimeFormat("%H:%M");
      gt.GetXaxis()->SetTitle("Time [h:m]");
      gt.GetYaxis()->SetTitle("Ambient T [#circ C]");
      gt.GetYaxis()->SetTitleOffset(1.2);
      can.cd(2);
      gh.Draw("AP");
      gh.GetXaxis()->SetTimeDisplay(1);
      gh.GetXaxis()->SetTimeFormat("%H:%M");
      gh.GetXaxis()->SetTitle("Time [h:m]");
      gh.GetYaxis()->SetTitle("Ambient rel. H [%]");
      gh.GetYaxis()->SetTitleOffset(1.2);
      can.cd(3);
      // mg is olverlay of chuck T and dew point
      mg->Delete();
      mg = new TMultiGraph;
      mg->Add(&gc);
      mg->Add(&gd);
      mg->Draw("AP");
      mg->GetXaxis()->SetTimeDisplay(1);
      mg->GetXaxis()->SetTimeFormat("%H:%M");
      mg->GetXaxis()->SetTitle("Time [h:m]");
      mg->GetYaxis()->SetTitle("Chuck T or dewpoint [#circ C]");
      mg->GetYaxis()->SetTitleOffset(1.2);
      can.Update();
      gSystem->ProcessEvents();
    }

    //if(i<(nCycl-1)) 
    UPGen::Sleep(1000*mySleep);
    i++;
  }

  delete m_devTC;
  delete m_devTA;
  delete m_devH;


  //app.Run();

  
  return 0;
}

