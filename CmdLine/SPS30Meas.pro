TEMPLATE = app

CONFIG -= qt

include(../build-config.inc)
  
SOURCES +=  SPS30Meas.cxx

INCLUDEPATH += . ../include
INCLUDEPATH +=  ../InfluxDB
INCLUDEPATH += ../PixRS232

win32 {
  DESTDIR = ../bin
  CONFIG += console
  DEFINES += WIN32 PIX_DLL_EXPORT
}

unix {
  DESTDIR = .
  DEFINES += CF__LINUX
  QMAKE_LFLAGS  +=  -L../PixRS232 -lPixRS232 -L../InfluxDB -lInfluxDB -lpthread
  QMAKE_RPATHDIR += ../PixRS232 ../InfluxDB
}
