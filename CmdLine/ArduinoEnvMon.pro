TEMPLATE = app

CONFIG -= qt

include(../build-config.inc)
  
INCLUDEPATH = . ../include

SOURCES += ArduinoEnvMon.cxx

unix {
    DESTDIR = .
    QMAKE_CXXFLAGS += -fPIC -DCF__LINUX
    INCLUDEPATH += ../PixRS232 $${system(root-config --incdir)}
    QMAKE_LFLAGS  +=  $${system(root-config --libs)} -L../PixRS232 -lPixRS232 -lpthread
    QMAKE_RPATHDIR += ../PixRS232
}
win32 {
    DESTDIR = ../bin
    CONFIG += console
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
}
