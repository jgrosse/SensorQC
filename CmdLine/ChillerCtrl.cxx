#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <string.h>
#include <time.h>
#include "upsleep.h"

#include "PixRs232Device.h"

int main(int argc, char **argv){
  int tPortID = 0;

  for(int i=0;i<argc;i++){
    if(i<(argc-1) && strcmp(argv[i],"-p")==0)
      tPortID = atoi(argv[i+1]);
  }
  tPortID += PixRs232Device::COM1;
  PixRs232Device *chiller = new PixRs232Device((PixRs232Device::Portids)tPortID);

  if(chiller->getStatus()==PixRs232Device::COM_ERROR){
    delete chiller;
    printf("COM port error\n");
    return -1;
  }
  if(chiller->getDeviceFunction()!=CHILLER){
    delete chiller;
    printf("Error: device on COM port %d ios not a chiller\n", tPortID);
    return -2;
  }

  chiller->printDevice();

  char cmd[300];
  sprintf(cmd,"hello");
  while(strcmp(cmd, "quit")!=0){
    printf("Enter a command: (start, stop, status, setT, getSetT, getMeasT, quit)\n");
    if(scanf("%s", cmd)!=1) continue;
    if(strcmp(cmd, "getMeasT")==0)
      printf("Chiller T = %.2lf\n", chiller->getChillerCurrentTemperature(0));
    else if(strcmp(cmd, "getSetT")==0)
      printf("Chiller T = %.2lf\n", chiller->getChillerSetTemperature(0));
    else if(strcmp(cmd, "setT")==0){
      float targT = 20.0;
      printf("Enter new target temperature (in degC): ");
      if(scanf("%f", &targT)==1){
	printf("Setting T to %f\n", targT);
	chiller->setChillerTargetTemperature(0, targT);
      }
    } else if(strcmp(cmd, "start")==0)
      chiller->setPowerStatus(PixRs232Device::COM_ON);
    else if(strcmp(cmd, "stop")==0)
      chiller->setPowerStatus(PixRs232Device::COM_OFF);
    else if(strcmp(cmd, "status")==0)
      chiller->printDevice();
    else if(strcmp(cmd, "quit")!=0)
      printf("Unknown command: please try again\n");
  }

  delete chiller;

  return 0;
}
