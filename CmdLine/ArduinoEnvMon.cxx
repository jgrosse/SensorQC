#include <TF1.h>
#include <TMath.h>
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <thread>
#include <chrono>
#include <sys/stat.h> 
#include <map>
//#include <ComTools.h>
#include <ArduinoADC.h>

double convNTC(double res, int type){
  double cal[4]={0.0020091, 0.000296397, 1.65669e-06, -175.422};
  double cal2[4]={0.0002911, 0.00268, 0.003354,  273.15};
  switch(type){
  default:
  case 0: // "blue drop" NTC
    return 1/(cal[0]+cal[1]*TMath::Log(res)+cal[2]*TMath::Power(TMath::Log(res),3))+cal[3];;
  case 1: // demonstrator module NTC
    return 1/(cal2[0]* TMath::Log(res)-cal2[1]+cal2[2])-cal2[3];
  }
}
double convPT1000(double res){
  static TF1 fpos("pt1000","1000.*(1+3.9083e-3*x-5.775e-7*x*x)", 0, 300);
  static TF1 fneg("pt1000","1000.*(1+3.9083e-3*x-5.775e-7*x*x-4.183e-12*(x-100)*x*x*x)", -200, 0);
  return (res<1000.)?fneg.GetX(res):fpos.GetX(res);
}
double convHIH4000(double measV, double nomV, double ambT){
  return (measV/nomV-0.16)/0.0062/(1.0546-0.00216*ambT);;
}

int main(int argc, char** argv)
{

  std::string sensName = "Flex NTC";
  int rport = 0;
  for(int i=0;i<argc;i++){
    // -p <port>
    if(strcmp(argv[i],"-p")==0 && i<(argc-1))
      rport = atoi(argv[i+1]);
    // -n <sensor name>
    if(strcmp(argv[i],"-n")==0 && i<(argc-1))
      sensName = argv[i+1];
    if(strcmp(argv[i],"-h")==0){
      std::cout << "Usage: ArduinoEnvMon [-p serial port] [-n sensor name]" << std::endl;
      std::cout << "   defaults: -p " << rport << " -n " << sensName << std::endl;
      return 0;
    }
  }

  // channels of sensors
  std::map<std::string, int> sensors;
  sensors.insert(std::make_pair("Ambient T",0));
  sensors.insert(std::make_pair("Flex NTC", 1));
  sensors.insert(std::make_pair("Ambient H",2));
  double nomV = 5.0;
  const int nsens = 3;
  double refRes[nsens] = {1e3, 18e3, 0.};
  double result[nsens];

  if(sensors.find(sensName)==sensors.end()){
    std::cerr << "Can't find sensor " << sensName << " in list -> exit" << std::endl;
      return -1;
  }

  ArduinoADC ar(rport);

  // request data from selected measurement
  for(std::map<std::string, int>::iterator it = sensors.begin(); it!=sensors.end(); it++){
    if(it->first=="Ambient H"){
      // read voltage and convert
      result[it->second] = ar.getMeasVal((uint)it->second);
    } else {
      // read resistance and convert
      double res = ar.getMeasVal((uint)it->second, refRes[it->second], nomV, 0., 5);//refRes[it->second]*((nomV/dresp) - 1.);
      //std::cout << "Resistance: " << res << std::endl;
      if(res>0.){
	if(it->first.find("NTC")!=std::string::npos)
	  result[it->second] = convNTC(res,1);
	else
	  result[it->second] = convPT1000(res);
      } else
	result[it->second] = -273.16;
    }
  }

  // convert hunidty - wait till here, needs ambient T
  result[sensors.find("Ambient H")->second] = convHIH4000(result[sensors.find("Ambient H")->second], nomV, 
							  result[sensors.find("Ambient T")->second]);
  for(std::map<std::string, int>::iterator it = sensors.begin(); it!=sensors.end(); it++){
    if(it->first==sensName) std::cout << "**";
    std::cout << it->first << ": " << result[it->second] << std::endl;
  }
  
  return 0;
}
