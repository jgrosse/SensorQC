/*
This will read three columns of floats from an IVScan output file: voltage, avg. current, std.def. of current
It will then plot avg. current with error bars vs voltage
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <thread>

#include <TGraphErrors.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TAxis.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TROOT.h>

std::vector<double> volts;
std::vector<double> currs, sdcurrs;
bool runEvtLoop;

void ProcRootEvt(){
  // preventing problems with LLVM/OpenGL, see 
  // https://root-forum.cern.ch/t/error-llvm-symbols-exposed-to-cling/23597/2
  gROOT->GetInterpreter();
  while(runEvtLoop){
    gSystem->ProcessEvents();
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
  }
}

void GetData (std::string fname, std::string &waferID, std::string &tile){
	
	std::ifstream infile(fname.c_str());
	std::string dummy;
	double volt;
	double curr, sdcurr;
	
	waferID="";
	tile="";
	while(!infile.eof()){
		std::getline(infile,dummy);
		if(dummy.find("Wafer") != std::string::npos){
				dummy.erase(0, 6);
				waferID = dummy;
		}
		if(dummy.find("Tile") != std::string::npos){
				dummy.erase(0, 9);
				tile = dummy;
		}
		if(dummy.find("voltage") != std::string::npos){ //found "voltage", next line is data
			break;
		}
	}
	
	// now reading data until eof is reached
	while(!infile.eof()){
		infile >> volt >> curr >> sdcurr;
		volts.push_back(volt);
		currs.push_back(curr);
		sdcurrs.push_back(sdcurr);
	}
	volts.pop_back();
	currs.pop_back();
	sdcurrs.pop_back();
}

void PlotIV(std::string ofname, std::string wafer, std::string tile){
	
	TGraphErrors m_graph(volts.size());
	TCanvas c("c","My curve", 800, 800);
	c.Clear();
	c.Show();
	
	for(int i=0; i<(int)volts.size(); i++){
		m_graph.SetPoint(i, volts[i], 1.e6*currs[i]);
		m_graph.SetPointError(i,0,1.e6*sdcurrs[i]);
	}
	
	m_graph.SetTitle(("IV curve for wafer "+wafer+", tile "+tile).c_str());
	m_graph.GetXaxis()->SetTitle("V_{bias} [V]");
	m_graph.GetYaxis()->SetTitle("I_{leak} [#muA]");
	m_graph.SetMarkerStyle(20);
	m_graph.SetMarkerSize(0.5);
	
	m_graph.Draw("AP");
	c.Update();

	std::string response="";
	std::cout << "Enter \"quit\" to quit." << std::endl;
	while(response!="quit"){
	  std::cin >> response;
	  std::this_thread::sleep_for(std::chrono::milliseconds(200));
	}

	c.SaveAs(ofname.append(".root").c_str());
	ofname.erase(ofname.find_last_of("."), 5);
	c.SaveAs(ofname.append(".pdf").c_str());
	
}

int main(int argc, char** argv){
	
	if(argc!=2){
		std::cerr << "No filename provided, can't proceed! " << std::endl;
		std::cerr << "Usage: PlotIV <raw data file name>" << std::endl;
		return -1;
	}

	std::string wafer, tile;
	std::string fname = argv[1];
	GetData(fname, wafer, tile);
  
	std::string ofname = fname;
	ofname.erase(ofname.find_last_of("."), 4);

	TApplication app("app", &argc, argv);
	runEvtLoop = true;
	std::thread thr(ProcRootEvt);
	PlotIV(ofname, wafer, tile);
	runEvtLoop = false;
	thr.join();
  
  return 0;
}
