/* ******************************
   * IVScan 1.0                 *
   * jens.weingarten@cern.ch    *
   * modified by                *
   * jgrosse1@uni-goettingen.de *
   ****************************** */

#include <TGraphErrors.h> 
#include <TApplication.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <iostream>
#include <vector>
#include <fstream>
#include <ctime>
#include <chrono>
#include "upsleep.h"
#include <math.h>
#include <iomanip>
#include <thread>

#include "PixGPIBDevice.h"

void procInput(TApplication *app){
    std::string response="";
    std::cout << "Modify Canvas as needed, then enter \"quit\" to quit." << std::endl;
    while(response!="quit"){
      std::cin >> response;
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    app->Terminate(); 
    std::cout << "Preparing plots for writing to file, please wait..." << std::endl;
    return;
}

void measCurr(PixGPIBDevice &dev, int nr_meas, double &avg_i, double &sigma_i){
  double sum_i=0., sum_i_sqr=0., temp_i;
  for(int n=0; n<nr_meas; n++){		// measure n times and average
    temp_i = dev.getCurrent(0);	//actual measurement
    sum_i += temp_i;
    sum_i_sqr += temp_i*temp_i;
    UPGen::Sleep(100);			//wait for 100ms before the next measurement
  }
  avg_i = sum_i/(double)nr_meas;				//calculate average
  sigma_i = sum_i_sqr/(double)nr_meas;				//calculate sq. average
  sigma_i = sqrt((sigma_i-avg_i*avg_i)/(double)(nr_meas-1));	//calculate std. dev.
}

int main(int argc, const char* argv[]){
	
  if(argc<4){
    std::cerr << "No sensor/tile ID or tester name provided, won't proceed! " << std::endl;
    std::cerr << "Usage: IVScan <wafer-no. tile-no. tester-name> [-p PID for HV] [-t PID for temp. (-1: not availb.)]"
	      << "[-s V-steps] [-n no. meas. per step] [-v max. voltage]" << std::endl;
    return -1;
  }

	int PID=24, PID_T=-1;
	int step_volt = 5;		//step size for voltage increase
	int nr_meas = 10;		//no. of measurements at each voltage
	double max_volt = -250;		//maximum voltage during scan	

	for (int i=4;i<argc;i++){
	  if(std::string(argv[i])=="-h"){
	    std::cout << "Usage:\nCVscan [-p GPIB-ID of HV device (-1: not used)] [-t GPIB-ID of temp. sensor meter (-1: not used)] "
		      << "[-c COM-port-ID of LCR meter] [-s voltage step size] [-v max. voltage] [-n no. of measurements per voltage step] "
		      << "[-f frequence of C-measurement]" << std::endl;
	    return -2;
	  }
		if(std::string(argv[i])=="-p" && i<(argc-1)){
		   PID=atoi(argv[i+1]);
		   i++;
		}
		if(std::string(argv[i])=="-t" && i<(argc-1)){
		   PID_T=atoi(argv[i+1]);
		   i++;
		}
		if(std::string(argv[i])=="-s" && i<(argc-1)){
	       step_volt=atoi(argv[i+1]);
		   i++;
		}
		if(std::string(argv[i])=="-n" && i<(argc-1)){
	       nr_meas=atoi(argv[i+1]);
		   i++;
		}
		if(std::string(argv[i])=="-v" && i<(argc-1)){
	       max_volt=(double)atof(argv[i+1]);
		   i++;
		}
	}

	
	double avg_i, sigma_i;
	std::vector<double> volt;
  
	// don't start from exactly 0V
	double vi=std::copysign(1.0, max_volt);
	volt.push_back(vi);  
	vi = 0.;
	// flexibly set point depending on sign of max_volt
	if(max_volt<0.){
	  while(vi > max_volt){
	    vi-=step_volt;
	    volt.push_back(vi);
	  }
	} else {
	  while(vi < max_volt){
	    vi+=step_volt;
	    volt.push_back(vi);
	  }
	}
	
	//create output file name and open file
	std::ofstream outfile;
	std::stringstream fnamestream;
	std::string fname;
	
	fnamestream<<argv[1]<<"_"<<argv[2]<<"_IV.raw";
	fname = fnamestream.str();
	outfile.open(fname.c_str(), std::ofstream::out | std::ofstream::trunc);
	
	//get date and time
	std::chrono::time_point<std::chrono::system_clock> start;
	start = std::chrono::system_clock::now();
	std::time_t scan_time = std::chrono::system_clock::to_time_t(start);
	
	outfile << "Wafer " << argv[1] << std::endl;
	outfile << "Tile no. " << argv[2] << std::endl;
	outfile << "Scan done " << std::ctime(&scan_time); //std::ctime() seems to put std::endl
	outfile << "by "<<argv[3]<<" at University of Goetingen"<<std::endl;
	outfile << std::endl;
	outfile << "voltage [V] \t avg. current [A] \t std. dev. [A]" << std::endl;
	
	PixGPIBDevice myDevice(0, PID, 1, 0);
	std::cout << "Device is " << myDevice.getDescription() << std::endl;
	std::cout<<"Device has "<<myDevice.getDeviceNumberChannels()<<" channels"<<std::endl;

	if(myDevice.getDeviceFunction()!=SUPPLY_HV){
		std::cout <<"Not an HV power supply. IV scan not possible." << std::endl;
		return 0;
	}

	PixGPIBDevice *tdev = 0;
	if(PID_T>0){
	  tdev = new PixGPIBDevice(0, PID_T, 1, 0);
	  std::cout << "NTC Device is " << tdev->getDescription() << std::endl;
	  std::cout << "NTC Device has "<<tdev->getDeviceNumberChannels()<<" channels"<<std::endl;
	}
	
	// initialize power supply
	if(myDevice.getStatus() == PixGPIBDevice::PGD_ON){	//if the PS is on, turn it off
		myDevice.setState(PixGPIBDevice::PGD_OFF);
		UPGen::Sleep(100);
	}
	
	myDevice.setCurrentLimit(0, 100E-6);
	myDevice.setState(PixGPIBDevice::PGD_ON);
	myDevice.measureCurrents();
	UPGen::Sleep(100);
	
    int m_argc=argc;
    char **m_argv = new char*[argc];
    for(int i=0;i<argc;i++){
      m_argv[i] = new char[100];
      sprintf(m_argv[i], "%s",argv[i]);
    }
	TApplication *app = new TApplication("pixel data viewer", &m_argc, m_argv);
    // preventing problems with LLVM/OpenGL, see 
    // https://root-forum.cern.ch/t/error-llvm-symbols-exposed-to-cling/23597/2
    gROOT->GetInterpreter();
	
	TCanvas can("mycan", "IV curve", 600, 600);
	can.Clear();
	can.cd();
	
	TGraphErrors* m_graph = new TGraphErrors();	//create empty graph
	float cal[4] = {0.0020091, 0.000296397, 1.65669e-06, -175.422}; // NTC calib.

	/* ***** start of measurement loop ***** */
	for(unsigned int j=0; j<volt.size(); j++){
	  std::cout<<"INFO: step no. "<<j<<":\t voltage: "<<volt[j]<<" V";
	  if(tdev!=0){
	    tdev->measureResistances();
	    float res = tdev->getResistance(0);
	    float temp = 1/(cal[0]+cal[1]*TMath::Log(res)+cal[2]*TMath::Power(TMath::Log(res),3))+cal[3];
	    std::cout << ".. T = " << std::setprecision(3) << temp << "°C";
	  }
	    
		myDevice.setVoltage(0, volt[j]);
		UPGen::Sleep(1000);
		
		measCurr(myDevice, nr_meas, avg_i, sigma_i);
		outfile << volt[j] <<"\t"<<std::scientific<< avg_i <<"\t"<< sigma_i<<std::endl;
		std::cout<<"\t current: "<<avg_i<<" A";
		
		m_graph->SetPoint(j, volt[j], avg_i);
		m_graph->SetPointError(j,0,sigma_i);
		
		char gtit[200];
		sprintf(gtit, "IV curve wafer %s tile %s", argv[1], argv[2]);
		m_graph->SetTitle(gtit);
		m_graph->SetMarkerStyle(20);
		m_graph->SetMarkerSize(0.5);
		m_graph->GetXaxis()->SetTitle("bias voltage [V]");
		m_graph->GetYaxis()->SetTitle("current [A]");
		m_graph->Draw("AP");
		can.Update();
		gSystem->ProcessEvents();
		if(std::abs(avg_i)>=50E-6){
			std::cout<<"\t software limit reached"<<std::endl; 
			break;
		} else std::cout<<std::endl;
	}
	/* ***** end of measurement loop ***** */
	
	/* ***** cleaning up ***** */
	delete tdev;
	myDevice.setVoltage(0, 0.0);
	outfile.close();
	
	if(myDevice.getStatus()==PixGPIBDevice::PGD_ON) {
		std::cout << "Found status PGD_ON. Turning off" << std::endl;
		myDevice.setState(PixGPIBDevice::PGD_OFF);
		UPGen::Sleep(100);		//for some reason have to wait after changing status
	}

	std::thread thr(procInput, app);
    app->Run(true);
    app->SetReturnFromRun(false);

    // will continue once separate thread stops TApplication
	fname.erase(fname.find_last_of("."), 4);
	can.SaveAs(fname.append(".root").c_str());
	fname.erase(fname.find_last_of("."), 5);
	can.SaveAs(fname.append(".pdf").c_str());

    app->Terminate();
	delete m_graph;
	delete app;
}
