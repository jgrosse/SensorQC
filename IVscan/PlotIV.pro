TEMPLATE = app

CONFIG -= qt

include(../build-config.inc)
  
INCLUDEPATH += . 

SOURCES += PlotIV.cxx

unix {
  DESTDIR =  .
  INCLUDEPATH  += $${system(root-config --incdir)}
  QMAKE_LFLAGS += $${system(root-config --libs)}
}
win32 {
    DESTDIR =  ../bin
    CONFIG += console
	DEFINES += WIN32 
	DEFINES += _WINDOWS
	DEFINES += _MBCS 
	QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
	INCLUDEPATH += $(ROOTSYS)/include
    QMAKE_LFLAGS_RELEASE = delayimp.lib
    QMAKE_LFLAGS_CONSOLE += /LIBPATH:$(ROOTSYS)/lib
    LIBS += libCore.lib
    LIBS += libCint.lib 
    LIBS += libRIO.lib 
    LIBS += libNet.lib 
    LIBS += libHist.lib 
    LIBS += libGraf.lib 
    LIBS += libGraf3d.lib 
    LIBS += libGpad.lib
    LIBS += libTree.lib
    LIBS += libRint.lib
    LIBS += libPostscript.lib
    LIBS += libMatrix.lib
    LIBS += libPhysics.lib
    LIBS += libMathCore.lib
}
