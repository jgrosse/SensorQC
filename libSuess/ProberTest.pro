CONFIG -= qt

TEMPLATE = app

include(../build-config.inc)
  
SOURCES += ProberTest.cxx

INCLUDEPATH += ../include ../PixGPIB ../PixRS232
unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX
	LIBS += -L. -lSuess -L../PixRS232 -lPixRS232 -L../PixGPIB -lPixGPIB
	QMAKE_RPATHDIR += . ../PixRS232 ../PixGPIB
}
win32 {
    DESTDIR = ../bin
    CONFIG += console
	DEFINES += WIN32 
	DEFINES += _WINDOWS
	DEFINES += _MBCS 
	DEFINES += "_CRT_SECURE_NO_WARNINGS" 
    DEFINES += __VISUALC__ 
	QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
    QMAKE_LFLAGS_RELEASE = delayimp.lib
    QMAKE_LFLAGS_CONSOLE += /LIBPATH:../PixGPIB /LIBPATH:../bin
    LIBS += PixGPIB.lib GPIB-32.obj PixRS232.lib Suess.lib
}

