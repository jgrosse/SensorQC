/////////////////////////////////////////////////////////////////////
// GenericProber.h derived from USBpix's Suess.h
// version 1.0
// Joern Grosse-Knetter, Kevin Kroeninger, University of Goettingen
/////////////////////////////////////////////////////////////////////

//! Class for Suess probe stations

#ifndef _GENERIC_PROBER_H 
#define _GENERIC_PROBER_H 

#include <memory>
#include <string>
#include <map>
#include <mutex>
#include "dllexport.h"

namespace Suess {
  enum Errors
  {
    None = 0,
    END_OF_WAFER = 703,
  };

  class DllExport GenericProber;
  class DllExport Response {

    public:

    class DllExport Parset {
      public:
        Parset();
	Parset(const Parset &p);
	char delimR;
	char delimW;
	std::string eol;
	bool checkId;
	bool haveErrorInfo;
    };

      Response(std::string, Parset);
      Response(const Response & r);
      int client_id;
      int error_code;
      std::string return_data;

      virtual void dump();
      void validate(int client_id);

    protected:
      std::unique_ptr<std::istringstream> data_iss;
      std::string parseString();
      int parseInt();
      float parseFloat();
      Parset p;
  };

  class DllExport StepDieResponse: public Response {
    public:
      StepDieResponse(std::string, Parset);
      StepDieResponse(const StepDieResponse & r);

      virtual void dump();

      int r_column;
      int r_row;
      int r_subdie;
      int r_totalsubdies;
  };

  class DllExport ReadChuckPositionResponse: public Response {
    public:
      ReadChuckPositionResponse(std::string, Parset);
      ReadChuckPositionResponse(const ReadChuckPositionResponse & r);

      virtual void dump();

      int r_x;
      int r_y;
      int r_z;
  };

  class DllExport ReadChuckStatusResponse: public Response {
    public:
      ReadChuckStatusResponse(std::string, Parset);
      ReadChuckStatusResponse(const ReadChuckStatusResponse & r);

      //      virtual void dump();

      int r_move;
      int r_limit;
      std::string r_height;
  };

  class DllExport ReadScopePositionResponse: public Response {
    public:
      ReadScopePositionResponse(std::string, Parset);
      ReadScopePositionResponse(const ReadScopePositionResponse & r);

      virtual void dump();

      int r_x;
      int r_y;
      int r_z;
  };

  class DllExport ReadMapPositionResponse: public Response {
    public:
      ReadMapPositionResponse(std::string, Parset);
      ReadMapPositionResponse(const ReadMapPositionResponse & r);

      virtual void dump();

      int r_column;
      int r_row;
      int r_x_position;
      int r_y_position;
      int r_current_subdie;
      int r_total_subdies;
      int r_current_die;
      int r_total_dies;
      int r_current_cluster;
      int r_total_clusters;
  };

  class DllExport ReadChuckActualTempResponse: public Response {
    public:
      ReadChuckActualTempResponse(std::string, Parset);
      ReadChuckActualTempResponse(const ReadChuckActualTempResponse & r);

      virtual void dump();

      float r_temp;
      std::string r_unit;
      int r_status;
      std::string r_sstatus;

  private:
      std::map<int, std::string> m_statusOpts;
  };
  class DllExport ReadChuckSetTempResponse: public Response {
    public:
      ReadChuckSetTempResponse(std::string, Parset);
      ReadChuckSetTempResponse(const ReadChuckSetTempResponse & r);

      virtual void dump();

      float r_temp;
      std::string r_unit;
  };

  class DllExport GenericProber {
    public:
      GenericProber();
      virtual ~GenericProber();

      std::string clientCommand(std::string cmd, std::string data);

      void MoveChuckContact(float velocity = -1);
      void MoveChuckSeparation(float velocity = -1);
      Suess::StepDieResponse StepFirstDie(
          bool ClearBins = true, bool RecalcRoute = true);
      Suess::StepDieResponse StepNextDie(
          int column = -1, int row = -1, int subdie = -1);
      Suess::ReadMapPositionResponse ReadMapPosition(
          int pos = 0, char from_pos = 'R');
      ReadChuckActualTempResponse ReadChuckActualTemp();
      ReadChuckSetTempResponse ReadChuckSetTemp();

      // MoveChuck: just send move command; MoveChuckWait: wait until movement finished
	  void MoveChuck(float x, float y, char PosRef='H', char Unit='Y', float velocity=1, char comp='D');
	  void MoveChuckWait(float x, float y, char PosRef='H', char Unit='Y', float velocity=1, char comp='D');
	  ReadChuckPositionResponse ReadChuckPosition(char Unit='Y', char PosRef='H', char comp='D');
	  ReadChuckStatusResponse ReadChuckStatus();
          void MoveChuckZ(float z, char PosRef='H', char Unit='Y',float velocity=1, char comp='D');
          void MoveChuckZWait(float z, char PosRef='H', char Unit='Y',float velocity=1, char comp='D');

	  void MoveScope(float x, float y, char PosRef='H', char Unit='Y', float velocity=1, char comp='D');
	  ReadScopePositionResponse ReadScopePosition(char Unit='Y', char PosRef='H', char comp='D');
          void MoveScopeZ(float z, char PosRef='H', char Unit='Y',float velocity=1, char comp='D');


    protected:
      virtual void send(std::string)=0;
      virtual std::string recv()=0;

      int m_client_id;
      Response::Parset p;
      std::mutex m_mutex;

  };

  class DllExport Exception {
    public:
      Exception(std::string text):
        text(text)
      {
      }
      std::string text;
  };

}
#endif // _GENERIC_PROBER_H 
