#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <string.h>
#include <stdlib.h>
#include <chrono>
#include <thread>

#include "VeloxRS232Prober.h"

using namespace Suess;

VeloxRS232Prober::VeloxRS232Prober(int port, int baudr, int parity, int sbit, int dbit) 
  : GenericProber(),
    m_port(port)
{
  p.eol     = "\r";
  p.delimR  = ':';
  p.delimW  = ' ';
  p.checkId = false;
  p.haveErrorInfo = false;

  m_client_id = 0;  

  if(!ComOpen(m_port,baudr,parity,sbit,dbit)){
    std::cerr << "ERROR: Can't open interface!" << std::endl;
    throw Exception("VeloxRS232Prober constructor: Not connected.");
  }
  std::string status = clientCommand("GetStatus","");
  //std::cout << "Status: " << status << std::endl;

}
VeloxRS232Prober::~VeloxRS232Prober(){
  ComClose(m_port);
}
void VeloxRS232Prober::send(std::string out)
{
  if (0) 
  {
    std::cerr << " >>VRP>> '" << out.substr(0, out.length() - p.eol.length()) << "'" << std::endl;
  }

  ComWrite(m_port, (void*)out.c_str(), out.length());
}

std::string VeloxRS232Prober::recv()
{
  std::string response="";
  int iLen = 0;
  int iTimeout;
  const int bflen=65;
  char cBuffer[bflen];
  cBuffer[0]='\0';
  iTimeout=0;

  while (iTimeout<m_timeout){
    iLen = ComRead(m_port,cBuffer,bflen-1);
    if(iLen>0){
      cBuffer[iLen]='\0';
      response += std::string(cBuffer);
      if(response.length()>1 && response.substr(response.length()-p.eol.length(),p.eol.length())==p.eol) break;
    }
    else{
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      iTimeout++;
    }
  }

  if (iTimeout>=m_timeout) response="";
  return response;
}
