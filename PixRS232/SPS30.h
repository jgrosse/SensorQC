#ifndef SPS30_H
#define SPS30_H

#include <dllexport.h>
#include <string>
#include <thread>

class DllExport SPS30{

 public:
  SPS30(int iPort);
  ~SPS30();

  void startMeas();
  void stopMeas();
  void readMeas(float data[]);
  void cleanFan();
  void getIntv(uint &interval);
  void setIntv(uint interval);
  void getLabels(std::string labels[]);
  void printInfo();
  void resetDev();

 private:
  void sendCmd(unsigned char cmd[], uint nbyte);
  void sendCmd(unsigned char cmd[], uint nbyte, float data[]);
  void convert(unsigned char data[], float procData[], uint npd=10);

  int m_iPort;
  std::string m_labels[10];

};
#endif // SPS30_H
