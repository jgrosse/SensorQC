#include "ArduinoADC.h"
#include <ComTools.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>

ArduinoADC::ArduinoADC(int iPort) : m_port(iPort){
  // indicate it's /dev/ttyAMC*
  m_port += 20;
  // open serial port
  if(!ComOpen(m_port, 9600,P_NONE,S_1BIT,D_8BIT)) m_port = -1;

  std::string resp = sendReceive("???", false);
  std::this_thread::sleep_for (std::chrono::milliseconds(500));   
  resp = sendReceive("???");
  if(resp!="ERR unknown command") { // no communication possible -> stop!
    ComClose(m_port);
    std::cerr << "Can't establish communicaion with Arduino on port " << (m_port-20) << std::endl;
    m_port = -1;
  }
}
ArduinoADC::~ArduinoADC(){
  if(m_port>=0) ComClose(m_port);
}

std::string ArduinoADC::sendReceive(std::string command, bool showErr){
  if(m_port<0) return "";
  int iLen = 0;
  unsigned int iTimeout=0, TIMEOUT=50;
  char cBuffer[65];
  cBuffer[0]='\0';
  std::string resp="";

  // request data from selected measurement
  std::string cmd = command+"\r\n";
  iTimeout = 0;
  ComWrite(m_port, (void*)cmd.c_str(), cmd.length(), 0);
  while(ComGetWriteCount(m_port)>0 && iTimeout<TIMEOUT){ 
    iTimeout++;
    std::this_thread::sleep_for (std::chrono::milliseconds(50));   
  }
  iTimeout = 0;
  while(iTimeout<TIMEOUT){
    if(ComGetReadCount(m_port)>0){
      iLen = ComRead(m_port,cBuffer,64,0);
      if(iLen>0){
	cBuffer[iLen]='\0';
	resp += std::string(cBuffer);
	// check for termination with '\r\n' and remove
	if(resp.length()>1 && resp.substr(resp.length()-2,2)=="\r\n"){
	  resp = resp.substr(0,resp.length()-2);
	  break;
	}
      }
    }
    std::this_thread::sleep_for (std::chrono::milliseconds(50));  
    iTimeout++;
  }
  if(iTimeout==TIMEOUT){
    if(showErr){
      std::cerr << "ArduinoADC::sendReceive : timeout reached while waiting for response! " << std::endl;
      // close and re-open port to reset
      ComClose(m_port);
      std::this_thread::sleep_for (std::chrono::milliseconds(100));   
      if(!ComOpen(m_port, 9600,P_NONE,S_1BIT,D_8BIT)) m_port = -1;
      resp = sendReceive("???", false);
      std::this_thread::sleep_for (std::chrono::milliseconds(500));   
      resp = sendReceive("???");
      if(resp!="ERR unknown command") { // no communication possible -> stop!
	ComClose(m_port);
	std::cerr << "Can't establish communicaion with Arduino on port " << (m_port-20) << std::endl;
	m_port = -1;
      }
    }
    return "";
  }
  //std::cout << "Response: " << resp << std::endl;
  return resp;
}

double ArduinoADC::getMeasVal(uint pin, double refRes, double nomV, double offset, uint nrep){
  double dresp = 0.;
  for (uint k=0;k<nrep;k++){
    std::string cmd = "ADC "+std::to_string(pin);
    std::string resp=sendReceive(cmd);
    if(resp=="") return -999.0;
    dresp += std::stod(resp)/1024.*nomV;
  }
  dresp /= (double)nrep - offset;

  if(refRes>0.){
    if(dresp>0.)
      return (refRes*((nomV/dresp) - 1.));
    else
      return -999.;
  } else
    return dresp;
}
std::string ArduinoADC::getMeasVal(uint address, std::string wcmd, uint nblocks){
  std::stringstream cmdw, cmdr;
  cmdw << "I2C WRITE " << std::hex << std::setfill('0') << std::setw(2) << (uint32_t)address << " " << wcmd;

  std::string response =  sendReceive(cmdw.str());
  if(response=="") return "";
  std::this_thread::sleep_for (std::chrono::milliseconds(1000));  

  cmdr << "I2C READ " << std::hex << std::setfill('0') << std::setw(2) << (uint32_t)address << " " << nblocks;
  response =  sendReceive(cmdr.str());

  return response;
}
