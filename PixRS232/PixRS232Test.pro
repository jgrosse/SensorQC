TEMPLATE = app

include(../build-config.inc)
  
SOURCES=\
  PixRs232Device.cxx \
  PixRs232Test.cxx

INCLUDEPATH += . ../include

win32 {
  DESTDIR = ../bin
  CONFIG += console
  SOURCES += ComTools.cxx
  DEFINES += WIN32 PIX_DLL_EXPORT
}

unix {
  DESTDIR = .
  DEFINES += CF__LINUX
  SOURCES += ComToolsLinux.cxx
}
