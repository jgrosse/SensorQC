#include "SPS30.h"
#include "ComTools.h"

SPS30::SPS30(int iPort) : m_iPort(iPort+10){
  if(!ComOpen(m_iPort,115200,P_NONE,S_1BIT,D_8BIT)){
    printf("ERROR: Can't open interface!\n");
    return;
  }  

  m_labels[0] = "Mass Concentration PM1.0 [μg/m3]";
  m_labels[1] = "Mass Concentration PM2.5 [μg/m3]";
  m_labels[2] = "Mass Concentration PM4.0 [μg/m3]";
  m_labels[3] = "Mass Concentration PM10  [μg/m3]";
  m_labels[4] = "Number Concentration PM0.5 [#/cm3]";
  m_labels[5] = "Number Concentration PM1.0 [#/cm3]";
  m_labels[6] = "Number Concentration PM2.5 [#/cm3]";
  m_labels[7] = "Number Concentration PM4.0 [#/cm3]";
  m_labels[8] = "Number Concentration PM10  [#/cm3]";
  m_labels[9] = "Typical Particle Size [μm]";

}
SPS30::~SPS30(){
  ComClose(m_iPort);
}
void SPS30::getLabels(std::string labels[]){
  for(uint i=0;i<10;i++)
    labels[i] = m_labels[i];
  return;
}
void SPS30::printInfo(){
  unsigned char cmd[3];
  cmd[0] = 0xd0;
  cmd[1] = 0x1;
  cmd[2] = 0x0;
  sendCmd(cmd, 3);
  cmd[0] = 0xd0;
  cmd[1] = 0x1;
  cmd[2] = 0x1;
  sendCmd(cmd, 3);
  cmd[0] = 0xd0;
  cmd[1] = 0x1;
  cmd[2] = 0x2;
  sendCmd(cmd, 3);
  return;
}
void SPS30::resetDev(){
  unsigned char cmd[2];
  cmd[0] = 0xd3;
  cmd[1] = 0x0;
  sendCmd(cmd, 2);
  return;
}
void SPS30::startMeas(){
  unsigned char cmd[4];
  cmd[0] = 0x0;
  cmd[1] = 0x2;
  cmd[2] = 0x1;
  cmd[3] = 0x3;
  sendCmd(cmd, 4);
  return;
}
void SPS30::stopMeas(){
  unsigned char cmd[2];
  cmd[0] = 0x1;
  cmd[1] = 0x0;
  sendCmd(cmd, 2);
  return;
}
void SPS30::readMeas(float procData[]){
  unsigned char cmd[2];
  cmd[0] = 0x3;
  cmd[1] = 0x0;
  sendCmd(cmd, 2, procData);
  return;
}
void SPS30::getIntv(uint &interval){
  float procData[1];
  unsigned char cmd[3];
  cmd[0] = 0x80;
  cmd[1] = 0x1;
  cmd[2] = 0x0;
  sendCmd(cmd, 3, procData);
  interval = (uint)procData[0];
  return;
}
void SPS30::setIntv(uint interval){
  unsigned char cmd[7];
  cmd[0] = 0x80;
  cmd[1] = 0x5;
  cmd[2] = 0x0;
  cmd[3] = (0xff&interval);
  cmd[4] = (0xff&(interval>>8));
  cmd[5] = (0xff&(interval>>16));
  cmd[6] = (0xff&(interval>>24));
  sendCmd(cmd, 7);
  interval = 0;
  return;
}
void SPS30::cleanFan(){
  unsigned char cmd[2];
  cmd[0] = 0x56;
  cmd[1] = 0x0;
  sendCmd(cmd,2);
  return;
}
void SPS30::sendCmd(unsigned char cmd[], uint nbyte){
  float data[10];
  sendCmd(cmd, nbyte, data);
}
void SPS30::sendCmd(unsigned char cmd[], uint nbyte, float procData[]){

  // wrap command and sub-command in header, trailer format
  unsigned char sCmd[65];
  uint iLen=2;
  uint cSum = 0;
  sCmd[0] = 0x7e;
  sCmd[1] = 0x00;
  for(uint i=0;i<nbyte;i++){
    sCmd[iLen] = 0xff&cmd[i];
    cSum += (uint)(0xff&cmd[i]);
    iLen++;
  }
  cSum = 0xff-(0xff&cSum);
  sCmd[iLen] = (char)cSum;
  if(sCmd[iLen]==0x7e){
    sCmd[iLen] = 0x7d;
    iLen++;
    sCmd[iLen] = 0x5e;
  }
  iLen++;
  sCmd[iLen] = 0x7e;
  iLen++;
//   for(uint i=0;i<iLen;i++) printf("%d - 0x%x\n", i, 0xff&sCmd[i]);
//   printf("--------\n");
  ComWrite(m_iPort,sCmd,iLen,0);

  // read from COM port until end of message
  unsigned char cBuffer[65];
  cBuffer[0]='\0';
  uint dLen = 0;
  unsigned char dBuffer[65];
  dBuffer[0]='\0';
  int iTimeout=0;
  std::string pReadBack;
  while (iTimeout<10){
    if(ComGetReadCount(m_iPort)>0){
      iLen = ComRead(m_iPort,cBuffer,64, 0);
      // check start and end of frame words
      if(iLen>5 && cBuffer[0]==0x7e && cBuffer[iLen-1]==0x7e &&
	 // check if address and command are repeated correctly
	 cBuffer[1]==sCmd[1] && cBuffer[2]==sCmd[2]){
	if(cBuffer[3]==0){
	  uint rLen = (uint)cBuffer[4];
	  cSum = (uint)cBuffer[iLen-2];
	  for(uint i=5;i<iLen-2;i++){
	    if((0xff&cBuffer[i])==0x7d && i<(iLen-3)){
	      if((0xff&cBuffer[i+1])==0x5e) dBuffer[dLen] = 0x7e;
	      if((0xff&cBuffer[i+1])==0x5d) dBuffer[dLen] = 0x7d;
	      if((0xff&cBuffer[i+1])==0x31) dBuffer[dLen] = 0x11;
	      if((0xff&cBuffer[i+1])==0x33) dBuffer[dLen] = 0x13;
	      i++;
	    } else 
	      dBuffer[dLen] = cBuffer[i];
	    dLen++;
	  }
	  if(dLen!=rLen){
	    printf("Warning: expected %d bytes, got %d\n", rLen, dLen);
	    for(uint i=5;i<iLen-2;i++){
	      printf("%d - 0x%x\n", i-5, 0xff&cBuffer[i]);
	    }
	  }
	  if(cmd[0]==0xd0) printf("Reply: %s\n", dBuffer);
	  else if(cmd[0]==0x03) convert(dBuffer, procData, 10);
	  else if(cmd[0]==0x80) convert(dBuffer, procData, 1);
	  else if(dLen>0) {
	    for(uint i=0;i<dLen;i++)
	      printf("%d: %x\n", i, dBuffer[i]);
	  }
	} else
	  printf("Error with code 0x%x\n", cBuffer[3]);
	break;
      }
    }
    std::this_thread::sleep_for (std::chrono::milliseconds(100));  
    iTimeout++;
  }
  return;
}
void SPS30::convert(unsigned char data[], float procData[], uint npd){
  //combine 4 bytes to int
  const uint nbp=4;
  unsigned char dChunk[nbp];

  for(uint i=0;i<npd;i++){
    for(uint k=0;k<nbp;k++)
      dChunk[k] = data[i*4+(3-k)];
    if(npd>1)
      procData[i] = *(float *)&dChunk;
    else
      procData[i] = (float)(*(int *)&dChunk);
  }
  return;
}
