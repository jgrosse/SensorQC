#include "PixRs232Device.h"
#include "ComTools.h"
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include <upsleep.h>

#define PRD_DEBUG false

PixRs232Device::PixRs232Device(Portids port) : m_port(port){
	debug("DEBUG PixRs232Device::PixRs232Device");
	resetError();
	m_DeviceNumberChannels = 0;
	if(!ComOpen(m_port,9600,P_NONE,S_1BIT,D_8BIT)){
		m_lastErrMsg += "ERROR: Can't open interface!\n\n";
		m_Status = COM_ERROR;
		debug("DEBUG PixRs232Device::PixRs232Device END");
		return;
	}
	m_PowerStatus = COM_OFF;
	m_Status = COM_OK;

	m_TIMEOUT = 10;
	m_writeDelay = 3; // 3ms break time for iseg
	m_readDelay = 0; // 0ms break - no know device that requires it

	identifyDevice();
	if(m_Status == COM_ERROR){
		resetError();
		std::cerr << "Identification on 1st iteration failed, will try with longer timeout" << std::endl;
		// try again with longer timeout and possibly larger wait inbetween char's
		m_TIMEOUT = 50;
		identifyDevice();
	}

	if(m_Status != COM_ERROR){
		updateDeviceFunction();// then it knows position
		updateDeviceNumberChannels();
		initializeDevice();
	
		if (PRD_DEBUG) printDevice();
	}
	debug("DEBUG PixRs232Device::PixRs232Device END");
}


PixRs232Device::~PixRs232Device(){
	debug("DEBUG PixRs232Device::~PixRs232Device is ran");
	ComClose(m_port);
}


void PixRs232Device::identifyDevice(){
	debug("DEBUG PixRs232Device::identifyDevice()");
	std::string response;
	// get ID of device
	m_DeviceType = UNKNOWN;
	m_DeviceFunction = NONE;
	sprintf(m_Description, "UNKNOWN");

	m_writeTerminationString = "\r\n";	//termination string of huber CR LF
	m_readTerminationString = "\r\n";	//termination string of huber CR LF

	writeDevice("STATUS");	//test string used to determine the device from the device answer
	if(readDevice(response)){	//huber answers to STATUS with a number
		bool success = true;
		try{
			std::stoi(response);
		} catch(...){
			success = false;
		}
		if(success){
			m_DeviceType = UNISTAT;
			sprintf(m_Description, "HUBER_UNISTAT");
			std::this_thread::sleep_for (std::chrono::milliseconds(100)); // pause required acc. to manual
			debug("DEBUG PixRs232Device::identifyDevice() END");
			return;
		}
	}

	m_writeTerminationString = "\r\n";	//termination string of iseg CR LF, first try
	m_readTerminationString = "\r\n";	//termination string of iseg CR LF, first try

	writeDevice("W=003"); // set Iseg break time to default of 3ms, might have been changed
	readDevice(response);
	writeDevice("#"); // request SN and FW ver.
	readDevice(response);
	// ISEG replies to # with current specs in mA at end
	if(response.length()>10 && response.substr(6,1)==";" && response.substr(11,1)==";"){
		debug("PixRs232Device::identifyDevice(): ISEQ found");
		writeDevice("U1"); // request SN and FW ver.
		readDevice(response);
		debug("PixRs232Device::identifyDevice: response to \"U1\" is " + response);
		if(response.length()==5){
			m_DeviceType = ISEG_NHQ;
			sprintf(m_Description, "ISEG_NHQ");
		} else {
			m_DeviceType = ISEG_SHQ;
			sprintf(m_Description, "ISEG_SHQ");
		}
		debug("DEBUG PixRs232Device::identifyDevice() END");
		return;
	}

	//reset errors, time out is wanted here to identify device
	if(m_writeDelay==3) m_writeDelay = 0; // if it's not an iseg, no need to delay writing between char's
	resetError();

	m_writeTerminationString = "\r";//termination string CR, first try
	m_readTerminationString = "\r";	//termination string CR, first try
	writeDevice("*RST");	// reset device
	std::this_thread::sleep_for (std::chrono::milliseconds(2000));
	debug("slept for 2000 ms\n");
	writeDevice("*IDN?");	// std. request to retrieve information from device

	if(readDevice(response)){ //no error returned most likely if the termination string is correct
		debug("PixRs232Device::identifyDevice: First termination string fits, checking type from response");
		if(response.find("HM8118")!=response.npos){
			debug("PixRs232Device::identifyDevice: it's a HM8118!");
			m_DeviceType = HM8118;
			sprintf(m_Description, "%s", response.c_str());
			debug("DEBUG PixRs232Device::identifyDevice() END");
			return;
		}
		else{
			debug("PixRs232Device::identifyDevice(): ERROR: in identifying device, maybe device type unknown or not connected to selected port");
			m_lastErrMsg += "ERROR identifying device, but correct termination strings; response: "+response+"\n";
			m_Status = COM_ERROR;
			debug("DEBUG PixRs232Device::identifyDevice() END");
			return;
		}
	}

	m_writeTerminationString = "\r";	//termination string of MICOS CR
	m_readTerminationString = "\r\n\3";	//termination string of MICOS CR LF ETX, second try

	writeDevice(".0VE");	//test string used to determine the device from the device answer
	if(readDevice(response)){	//MICOS MOCO DC answers to ve with identification string Micos GmbH
		if(response.find("Micos GmbH")!=response.npos){
			m_DeviceType = MOCO_DC;
			sprintf(m_Description, "%s", response.c_str());
			debug("DEBUG PixRs232Device::identifyDevice() END");
			return;
		}
		if(response.find("Automation/PI")!=response.npos){
			m_DeviceType = MERCURY;
			sprintf(m_Description, "%s", response.c_str());
			debug("DEBUG PixRs232Device::identifyDevice() END");
			return;
		}
	}

	m_writeTerminationString = "\r";	//input termination string of julabo chiller
	m_readTerminationString = "\r\n";	//output termination string of julabo chiller

	writeDevice("version"); //test string used to determine the device from the device answer
	m_DeviceType = JULABO;
	//std::cout << "recog" << std::endl;
	//return;
	if(readDevice(response)){
		std::string searchexp("J\0U\0L\0A\0B\0O\0",12);
		std::cout << response << std::endl;
		if(response.find(searchexp)!=response.npos){
			m_DeviceType = JULABO;
			sprintf(m_Description, "JULABO CHILLER");
			debug("DEBUG PixRs232Device::identifyDevice() END");
			return;
		}
	}

	debug(std::string("PixRs232Device::identifyDevice(): ERROR: cannot identifiy device, maybe device type unknown"));
	m_lastErrMsg += "ERROR: cannot identifiy device, maybe device type unknown!\n\n";
	m_Status = COM_ERROR;
	debug("DEBUG PixRs232Device::identifyDevice() END");
}


void PixRs232Device::updateDeviceFunction(){
	debug("DEBUG PixRs232Device::updateDeviceFunction()");
	if      (m_DeviceType == ISEG_SHQ) m_DeviceFunction = SUPPLY_HV;
	else if (m_DeviceType == ISEG_NHQ) m_DeviceFunction = SUPPLY_HV;
	else if (m_DeviceType == MOCO_DC)  m_DeviceFunction = POSITION;
	else if (m_DeviceType == MERCURY)  m_DeviceFunction = POSITION;
	else if (m_DeviceType == JULABO)   m_DeviceFunction = CHILLER;
	else if (m_DeviceType == UNISTAT)  m_DeviceFunction = CHILLER;
	else if (m_DeviceType == HM8118)   m_DeviceFunction = LCRMETER;
	else                               m_DeviceFunction = NONE;
	debug("DEBUG PixRs232Device::updateDeviceFunction() END");
}


void PixRs232Device::updateDeviceNumberChannels(){
	debug("DEBUG PixRs232Device::updateDeviceNumberChannels()");
	std::string tResponse = "";
	m_DeviceNumberChannels = 0;
	switch(m_DeviceType){
	case ISEG_SHQ:
	case ISEG_NHQ:
		for(m_DeviceNumberChannels=1; m_DeviceNumberChannels<10; m_DeviceNumberChannels++){
			std::stringstream a;
			a << m_DeviceNumberChannels;
			writeDevice("S"+a.str());	//request status bits of channel i to check if it's available
			readDevice(tResponse);
			if(tResponse.find("?WCN")!=std::string::npos){
				m_DeviceNumberChannels--;
				break;
			}
		}
		break;
	case MOCO_DC:
		for(unsigned int i = 0; i<16; ++i){	// up to 16 different controllers can be addressed via one com port, 
		                                    // ping to find out how many are connected, they have to sorted with the start address 0!
			writeDevice("TS",i);
			if(readDevice(tResponse)){
				m_DeviceNumberChannels++;
			}else{
				writeDevice("TS",0);	//once after wrong read one has to read a working address, device feature...
				readDevice(tResponse);
				resetError();
				break;
			}
		}
		if(m_DeviceNumberChannels == 0){
			m_Status = COM_ERROR;
			m_lastErrMsg += "ERROR: communication errors, cant find a channel\n";
		}
		break;
	case JULABO:
		m_DeviceNumberChannels = 1;
		break;
	case UNISTAT:
		m_DeviceNumberChannels = 1;
		break;
	case MERCURY:
		m_DeviceNumberChannels = 1;
		break;
	case HM8118:
		m_DeviceNumberChannels = 1;
		break;
	default:
		m_DeviceNumberChannels = 0;
	}
	debug("DEBUG PixRs232Device::updateDeviceNumberChannels() END");
}


void PixRs232Device::initializeDevice(int option){
	debug("DEBUG PixRs232Device::initializeDevice()");
	std::string response="";
	switch (m_DeviceType){
		case ISEG_SHQ:
		case ISEG_NHQ:{
			writeDevice("T1");	//request status bits to check if device is on
			readDevice(response);
			debug(" ...response to T1: "+response);
			int tStatus = atoi(response.c_str());
			if ((tStatus & 8) == 8){
				m_PowerStatus= COM_OFF;
			}else if((tStatus & 64) == 64){
				m_PowerStatus= COM_LIM;
			}else{
				m_PowerStatus= COM_ON;
			}
			break;
		}
		case HM8118:{
			bool askSettingOrNot=false;
			bool seeMeas=false;
			bool inAutoRange=true;
			int sleepTime1=200; int sleepTime2=200; int nofAskTime=5;
			int sleepTimeAfterRST=2000;
			writeDevice("*RST"); // reset
			std::this_thread::sleep_for (std::chrono::milliseconds(sleepTimeAfterRST));
			debug("Slept for"+std::to_string(sleepTimeAfterRST)+" ms\n");
			writeDevice("AVGM0"); // no averaging
			writeDevice("OUTP0"); // normal output primary
			writeDevice("OUTS0"); // normal output secondary
			if (askSettingOrNot){askSetting("MMOD?",response," ");}
			writeDevice("MMOD0"); // 0=continuous trigger, 1=manual trigger, 2=external trigger

			if(askSettingOrNot){askSetting("RNGH?",response," ");}
			if(askSettingOrNot){askSetting("RNGE?",response," ");}
			if (inAutoRange){
				writeDevice("RNGH0"); // automatic range ON
			}else{
				writeDevice("RNGH1"); // automatic range OFF
				writeDevice("RNGE6"); // max range=6 for impedance, unless auto is on (see prev line)
			}
			if (seeMeas){measDoneOrNot(response, nofAskTime, sleepTime1, sleepTime2);}

			if (askSettingOrNot){askSetting("PMOD?",response," ");}
			writeDevice("PMOD3"); // C-D measurement
			// NB: this seems to set CIRC0 implicitly, so CIRC-command must be used afterwards
			if (seeMeas){measDoneOrNot(response, nofAskTime, sleepTime1, sleepTime2);}
			break;
		}
		case UNISTAT:
			writeDevice("STATUS");
			readDevice(response);
			if(response=="-1"){ // error status of device
				m_PowerStatus= COM_LIM;
			} else if(response=="0" || response=="1"){
				m_PowerStatus= COM_OFF;
			} else {
				m_PowerStatus= COM_ON;
			}
			std::this_thread::sleep_for (std::chrono::milliseconds(100)); // pause required acc. to manual
			break;
		case UNKNOWN:
			std::cout<<"Unknown device, didn't initialise!";
			break;
		default:
			for (unsigned int i = 0; i < m_DeviceNumberChannels; ++i){
				initializeChannel(i);
			}
	}
	debug("DEBUG PixRs232Device::initializeDevice() END");
}


void PixRs232Device::initializeChannel(unsigned int pChannel){
	std::stringstream tDebug;
	tDebug<<"DEBUG PixRs232Device::initializeChannel(): "<<pChannel;
	debug(tDebug.str());
	switch(m_DeviceType){
	case MOCO_DC:{
		std::string tReadback;
		writeDevice("TS", pChannel);
		readDevice(tReadback);
		int tIntReadback = MOCOstringToInt(tReadback);
		if((tIntReadback & 128) != 128){ //BIT7 shows the motor on/off status, if one channel is on set the device on
			m_PowerStatus = COM_ON;
		}
		writeDevice("DV2000", pChannel); //set standard Speed of 2000/s
		m_maxSpeeds[pChannel] = 2000;
		break;}
	case MERCURY:{
		std::string tReadback;
		char ch [2];
		writeDevice("TS", pChannel);
		readDevice(tReadback);
		//std::cout<<"\n INIT: tReadback whole string "<<tReadback<<"\n";
		std::string refString = tReadback.substr(tReadback.find(":")+1, 2);
		//std::cout<<"\n INIT!! part of string that contains...: "<<refString<<"\n";
		for(int a=0;a<2;a++){ch[a]=refString[a];}
		if(!(ch[0]&0x8)){ // bit set means off, bit 0 means motor on
			std::cout<<"\n MOTOR ON \n";
			m_PowerStatus = COM_ON;
		}else{
			m_PowerStatus = COM_OFF;
		} // otherwise motor off
		
		writeDevice("MN", pChannel); // switch motor on
		writeDevice("SA100000", pChannel); //set standard acceleration
		writeDevice("SV3000", pChannel); //set standard Speed of 2000/s

		m_maxSpeeds[pChannel] = 2000;
		break;}
	default:
		break;
	}
	debug("DEBUG PixRs232Device::initializeChannel END");
}


bool PixRs232Device::readDevice(std::string& pReadBack){ // read response until m_writeTerminationString is received and ignore instruction loop back
	debug("DEBUG PixRs232Device::readDevice()");
	int iLen = 0;
	unsigned int iTimeout=0;
	char cBuffer[65];
	cBuffer[0]='\0';

	pReadBack.clear();

	// read from COM port until end of message
	while (iTimeout<m_TIMEOUT){
		if(ComGetReadCount(m_port)>0){
			iLen = ComRead(m_port,cBuffer,64, m_readDelay);
			if(iLen>0){// && iLen > m_readTerminationString.length() - 1){
				cBuffer[iLen]='\0';
				pReadBack += std::string(cBuffer);
				if(stringIsTerminated(pReadBack)) break;
			}
		}
		std::this_thread::sleep_for (std::chrono::milliseconds(100));
		iTimeout++;
	}

	//a lot of error handling necessary, rather buggy communication for some devices, especially USB->COM port adapters
	if(iTimeout==m_TIMEOUT){ //time out, no (termination string = m_writeTerminationString) found
		if(pReadBack.length() == 0){
			m_Status = COM_ERROR; //nothing read back is a clear error
			m_lastErrMsg += "ERROR: timeout reading from COM port, no response\n";
		}else{
			m_Status = COM_WARNING; //read sth. back is a warning
			m_lastErrMsg += "WARNING: timeout reading from COM port, response so far: \"" + pReadBack +"\"\n";
		}
		debug("PixRs232Device::readDevice: read back time out, abort");
		debug("DEBUG PixRs232Device::readDevice() END");
		return false;
	}
	if (pReadBack.compare(m_readTerminationString) == 0){ //only! termination string found --> read again necessary
		if(m_DeviceType != ISEG_SHQ){ //ISEQ can just return termination string
			debug("PixRs232Device::readDevice: only determination string in read back, read again");
			debug("DEBUG PixRs232Device::readDevice() END");
			return readDevice(pReadBack);
		}
	}
	if (pReadBack.compare(m_lastCommand) == 0){ //some devices loop back the command including termination string --> read again necessary
		debug("PixRs232Device::readDevice: loop back detected, read again");
		debug("DEBUG PixRs232Device::readDevice() END");
		return readDevice(pReadBack);
	}
	if (pReadBack.find(m_lastCommand) != pReadBack.npos){ //some devices loop back the command without termination string --> get rid of loop back command
		debug("PixRs232Device::readDevice: loop back without termination string detected, deleting loop back");
		pReadBack.replace(pReadBack.find(m_lastCommand), m_lastCommand.size(), "");
	}

	pReadBack.replace(pReadBack.find(m_readTerminationString), m_readTerminationString.size(), ""); //get rid of the termination string
	if(pReadBack.length() == 0){
		debug("PixRs232Device::readDevice: only termination string");
	}else{
		debug(std::string("PixRs232Device::readDevice: ").append(pReadBack));
	}

	debug("DEBUG PixRs232Device::readDevice() END");
	return true;
}


void PixRs232Device::setStatus(DeviceStatus state){
	debug("DEBUG PixRs232Device::setStatus() is ran");
	m_Status = state;
}


void PixRs232Device::setPowerStatus(DevicePowerStatus pState){
	debug("DEBUG PixRs232Device::setPowerStatus()");
	std::string response;
	switch(m_DeviceType){
	case MOCO_DC: // turn motors on/off, in off state they do not keep position
		if (pState == COM_ON){
			writeMultiDevice("mn");	//all channels motor on
		}else{
			writeMultiDevice("ab");	//abort all movements
			writeMultiDevice("mf");	//all channels motor off
		}
		m_PowerStatus = pState;
		break;
	case MERCURY:
		if (pState == COM_ON){
			writeMultiDevice("mn");	//all channels motor on
		}else{
			writeMultiDevice("ab");	//abort all movements
			writeMultiDevice("mf");	//all channels motor off
		}
		m_PowerStatus = pState;
		break;
	case JULABO:
		setJulaboChillerStatus(pState);
		m_PowerStatus = pState;
		break;
	case UNISTAT:
		if (pState == COM_ON){
			writeDevice("START");
		}else{
			writeDevice("STOP");
		}
		std::this_thread::sleep_for (std::chrono::milliseconds(100)); // pause required acc. to manual
		m_PowerStatus = pState;
		break;
	case ISEG_NHQ:
	case ISEG_SHQ: // can't turn on/off, but clear limit state if turned off in case limit fired
		if(pState == COM_OFF && m_PowerStatus == COM_LIM){
			writeDevice("S1");
			readDevice(response);
		}
		m_PowerStatus = pState;
	default:
		break; // do nothing, some devices do not have a remote power switch
	}
	debug("DEBUG PixRs232Device::setPowerStatus() END");
}


void PixRs232Device::setVoltage(unsigned int channel, double voltage){
	debug("DEBUG PixRs232Device::setVoltage()");
	std::string response;
	std::stringstream a;
	a<<(channel+1);
	char volts[50];

	if(channel>m_DeviceNumberChannels){
		debug("DEBUG PixRs232Device::setVoltage() END");
		return;
	}

	if(m_DeviceFunction != SUPPLY_HV && m_DeviceFunction != SUPPLY_LV){
		//std::string s; std::stringstream sStream; sStream<<m_Description; sStream>>s;
		//std::string s;
		//for (int idx=0; idx<sizeof(m_Description); idx++){
		//	s.push_back(m_Description[idx]);
		//}
		if(m_DeviceType!=HM8118){//HM8118 can act as V source
			m_lastErrMsg += "ERROR: cannot set a voltage for this device type!\n\n";
			m_Status = COM_ERROR;
			debug("DEBUG PixRs232Device::setVoltage() END");
			return;
		}else{
			//std::cout<<"Seems like you'll use internal bias of HM8118\n";
		}
}

	switch(m_DeviceType){
	case ISEG_SHQ:
		// set voltage
		UPG_sprintf(volts,"%07.2f", fabs(voltage));
		writeDevice("D"+a.str()+"="+std::string(volts));
		if (!readDevice(response)){
			debug("DEBUG PixRs232Device::setVoltage() END");
			return;
		}
		// set ramp speed to maximum
		writeDevice("V"+a.str()+"=010");
		if (!readDevice(response)){
			debug("DEBUG PixRs232Device::setVoltage() END");
			return;
		}
		// start ramp and wait till done
		writeDevice("G"+a.str());
		if (!readDevice(response)){
			debug("DEBUG PixRs232Device::setVoltage() END");
			return;
		}
//		response="S"+a.str()+"=L2H";
//		while(response=="S"+a.str()+"=L2H" || response=="S"+a.str()+"=H2L"){
//			writeDevice("S"+a.str());
//			if (!readDevice(response)){
//				debug("DEBUG PixRs232Device::setVoltage() END");
//				return;
//			}
//		}
		break;
	case ISEG_NHQ:
		// set voltage
		UPG_sprintf(volts,"%04.0f", fabs(voltage));
		writeDevice("D"+a.str()+"="+std::string(volts));
		if (!readDevice(response)){
			debug("DEBUG PixRs232Device::setVoltage() END");
			return;
		}
		// set ramp speed to maximum
		writeDevice("V"+a.str()+"=010");
		if (!readDevice(response)){
			debug("DEBUG PixRs232Device::setVoltage() END");
			return;
		}
		// start ramp and wait till done
		writeDevice("G"+a.str());
		if (!readDevice(response)){
			debug("DEBUG PixRs232Device::setVoltage() END");
			return;
		}
//		response="S"+a.str()+"=L2H";
//		while(response=="S"+a.str()+"=L2H" || response=="S"+a.str()+"=H2L"){
//			writeDevice("S"+a.str());
//			if (!readDevice(response))
//				return;
//		}
		break;
	case HM8118:
		//std::cout<<"HM8118 set voltage: "<<voltage<<" V!\n";
		if(abs(voltage)>5){//this case should not happen
			std::cout<<"HM8118 highest internal bias is +5V, cannot apply "<<*volts<<" V!\n";
			m_lastErrMsg += "ERROR: cannot set a voltage larger than 5V for HM8118!\n\n";
			m_Status = COM_ERROR;
		}else{
			UPG_sprintf(volts,"%.2f", fabs(voltage));
			//std::cout<<"trying to set volts of an HM8118! "<<"VBIA"+std::string(volts)<<"\n";
			writeDevice("VBIA"+std::string(volts));
		}
		break;
	default:
		break; // do nothing
	}
	debug("DEBUG PixRs232Device::setVoltage() END");
}


void PixRs232Device::setCurrentLimit(unsigned int channel, double current){
	// ISEG SHQ: LB in format nnnnnE-07 (-> 0.1uA...1mA)., LS in format nnnnnE-10 (-> 0.1nA...1uA)
	debug("DEBUG PixRs232Device::setCurrentLimit()");
	std::string response;
	std::stringstream a;
	a<<(channel+1);
	char curr[50];

	if(channel>m_DeviceNumberChannels){
		debug("DEBUG PixRs232Device::setCurrentLimit()");
		return;
	}

	if(m_DeviceFunction != SUPPLY_HV && m_DeviceFunction != SUPPLY_LV){
		m_lastErrMsg += "ERROR: cannot set a voltage for this device type!\n\n";
		m_Status = COM_ERROR;
		debug("DEBUG PixRs232Device::setCurrentLimit()");
		return;
	}

	switch(m_DeviceType){
	case ISEG_NHQ:
	case ISEG_SHQ:
		if(current>5.e-11 && current<1.5e-6){
			// set small limit
			UPG_sprintf(curr,"%05.0f", fabs(current*1.e10));
			//printf("Command: LS%s=%s\n", a.str().c_str(), curr);
			writeDevice("LS"+a.str()+"="+std::string(curr));
			if (!readDevice(response)){
				debug("DEBUG PixRs232Device::setCurrentLimit()");
				return;
			}
		}
		if(current>5.e-8 && current<1.5e-3){
			// set large limit
			UPG_sprintf(curr,"%05.0f", fabs(current*1.e7));
			//printf("Command: LB%s=%s\n", a.str().c_str(), curr);
			writeDevice("LB"+a.str()+"="+std::string(curr));
			if (!readDevice(response)){
				debug("DEBUG PixRs232Device::setCurrentLimit()");
				return;
			}
		}
		if(current<5.e-11){
			// set no limit
			writeDevice("LB"+a.str()+"=00000");
			if (!readDevice(response)){return;}
		}
		debug("DEBUG PixRs232Device::setCurrentLimit() END");
		break;
	default:
		debug("DEBUG PixRs232Device::setCurrentLimit() END");
		break; // do nothing
	}
	debug("DEBUG PixRs232Device::setCurrentLimit()");
}


float PixRs232Device::getVoltage(unsigned int channel){
	debug("DEBUG PixRs232Device::getVoltage()");
	//std::cout<<"in getVoltage, device type: "<<m_DeviceType<<"\n";//41 for HM8118
	if(m_DeviceFunction != SUPPLY_HV && m_DeviceFunction != SUPPLY_LV && m_DeviceType!=HM8118){
		return 0.;
	}

	float tVolts = -999999.;

	switch(m_DeviceType){
	case ISEG_NHQ:
	case ISEG_SHQ:
		if(channel < m_DeviceNumberChannels){
			std::string response;
			std::stringstream a;
			a<<(channel+1);
			writeDevice("U"+a.str());	// request read voltage of one channel
			int itry=0;
			const int itmax = 10;
			while(!readDevice(response) && itry<itmax)
				itry++;
			// read-back errors: string still contains \r\n
			if(itry<itmax && response.substr(0,1).find("U")==std::string::npos){
				if(m_DeviceType==ISEG_NHQ)
					tVolts = (float)atoi(response.c_str());
				else
					tVolts = ISEGatof(response.c_str());
			}
			//printf("Voltage: %s - %e\n", response.c_str(), tVolts);
		}
		else{
			m_lastErrMsg += "LIMIT: cannot get a voltage for this channel. Channel does not exist!\n\n";
			m_Status = COM_LIMIT;
		}
		break;
	case HM8118:{
		std::string aRep="init";
		writeDevice("BIAS?");
		readDevice(aRep);
		//std::cout<<"finding out current voltage of HM8118! "<<aRep<<"\n";
		if(aRep!="1"){
			writeDevice("CONV1");
			writeDevice("BIAS1");
			tVolts=0.0;
		}
		//}else{
		//askSetting("VBIA?",aRep, "volt of HM8118: ");
		askSetting("VBIA?",aRep);
		tVolts=std::stof(aRep);
		//}
		break;
		}
	default:
		break; // do nothing
	}
	debug("DEBUG PixRs232Device::getVoltage() END");
	return tVolts;
}


float PixRs232Device::getCurrent(unsigned int channel){
	debug("DEBUG PixRs232Device::getCurrent()");

	if(m_DeviceFunction != SUPPLY_HV && m_DeviceFunction != SUPPLY_LV){
		debug("DEBUG PixRs232Device::getCurrent() END");
		return 0.;
	}

	float tCurr = -999999.;

	switch(m_DeviceType){
	case ISEG_NHQ:
	case ISEG_SHQ:
		if(channel < m_DeviceNumberChannels){
			std::string response;
			std::stringstream a;
			a<<(channel+1);

			writeDevice("T"+a.str());	// request device status
			int itry=0;
			const int itmax = 10;
			while(!readDevice(response) && itry<itmax)
				itry++;
			if(itry>itmax){
				debug("DEBUG PixRs232Device::getCurrent() END");
				return tCurr;
			}
			int status = atoi(response.c_str());
			if(status&64){ //limit reached
				m_PowerStatus = COM_LIM;
			}
			writeDevice("I"+a.str());	// request read current
			itry=0;
			while(!readDevice(response) && itry<itmax)
				itry++;
			if(itry>itmax){
				debug("DEBUG PixRs232Device::getCurrent() END");
				return tCurr;
			}
			// read-back errors: string still contains \r\n
			if(itry<itmax && response.substr(0,1).find("I")==std::string::npos){
				tCurr = ISEGatof(response.c_str());
				if((status&4)==0) tCurr *= -1.;
			}
			//printf("Current: %s - %e\n", response.c_str(), tCurr);
		}
		else{
			m_lastErrMsg += "ERROR: cannot get a current for this channel. Channel does not exist!\n\n";
			m_Status = COM_ERROR;
		}
		break;
	default:
		break; // do nothing
	}
	debug("DEBUG PixRs232Device::getCurrent() END");
	return tCurr;
}


int PixRs232Device::setPosition(unsigned int pChannel, int pPosition){
	std::stringstream tDebug;
	tDebug<<"PixRs232Device::setPosition("<<pPosition<<")";
	debug(tDebug.str());
	if(m_DeviceFunction != POSITION){
		debug("DEBUG PixRs232Device::setPosition END");
		return 0;
	}
	if(m_DeviceType == MOCO_DC){
		if(pChannel < m_DeviceNumberChannels){
			int tDistance = abs(pPosition-getPosition(pChannel)); //calculate the distance to go
			unsigned int tMaxTime = (int)(0.0012*tDistance-0.032*m_maxSpeeds[pChannel]+150); // defines the maximum delay until the motor has to reach the desired position; empiric formular from measurements; otherwise status is set to LIMIT
			int tActualPosition = 0;
			std::stringstream tCommand;
			tCommand<<"MA"<<pPosition; //move to position
			writeDevice(tCommand.str(), pChannel);
			for (unsigned int tActualTime = 0; tActualTime<tMaxTime; ++tActualTime){
				std::string tReadback;
				writeDevice("MS", pChannel);	//tell moving status
				readDevice(tReadback);
				int tMovingStatus = MOCOstringToInt(tReadback); //for tMovingStatus = 4 the trajectory is completed
				writeDevice("TF", pChannel);	//tell error between set/real position
				readDevice(tReadback);
				int tMovingError = MOCOstringToInt(tReadback);
				tActualPosition = getPosition(pChannel); //get actual position
				if((tMovingStatus & 4) == 4 && tMovingError==0 && tActualPosition == pPosition){ //moving successfully competed when tMovingStatus = 4 and the error is 0
//					std::cout<<"MOVING COMPLETE"<<std::endl;
//					std::cout<<"tDistance "<<tDistance<<"\tspeed "<<m_maxSpeeds[pChannel]<<std::endl;
//					std::cout<<"MOVEMENT TOOK "<<tActualTime<<"\tMOVEMENT ALLOWED "<<tMaxTime<<std::endl;
//					std::cout<<"MOVEMENT ALLOWED "<<tMaxTime<<std::endl;
					break;
				}
				if(tActualTime==tMaxTime){
					debug("PixRs232Device::setPosition: LIMIT: position not reached!");
					m_lastErrMsg += "LIMIT: cannot set desired position!\n\n";
					m_Status = COM_LIMIT;
					break;
				}
			}
			debug("DEBUG PixRs232Device::setPosition END");
			return tActualPosition;
		}
		else{
			m_lastErrMsg += "LIMIT: cannot set a position for this channel. Channel does not exist!\n\n";
			m_Status = COM_LIMIT;
		}
	}
	if(m_DeviceType == MERCURY){
		if(pChannel < m_DeviceNumberChannels){
			//int tActualPosition = 0;
			std::stringstream tCommand;

			// new move commands
			double pPositionDouble = pPosition;
			moveAbsolute(pPositionDouble, pChannel);
			
			//tCommand<<"MA"<<pPosition; //move to position
			//writeDevice(tCommand.str(), pChannel);
		}
		else{
			m_lastErrMsg += "LIMIT: cannot set a position for this channel. Channel does not exist!\n\n";
			m_Status = COM_LIMIT;
		}
	}
	debug("DEBUG PixRs232Device::setPosition END");
	return 0;
}


void PixRs232Device::goHome(){
	debug("DEBUG PixRs232Device::goHome()");
	if(m_DeviceFunction != POSITION){
		debug("DEBUG PixRs232Device::goHome END");
		return;
	}
	if(m_DeviceType == MOCO_DC || m_DeviceType == MERCURY){
		writeMultiDevice("GH");
	}
	debug("DEBUG PixRs232Device::goHome END");
}


void PixRs232Device::setHome(){
	debug("DEBUG PixRs232Device::setHome()");
	if(m_DeviceFunction != POSITION){
		debug("DEBUG PixRs232Device::setHome() END");
		return;
	}
	if(m_DeviceType == MOCO_DC || m_DeviceType == MERCURY){
		writeMultiDevice("DH");
	}
	debug("DEBUG PixRs232Device::setHome() END");
}


void PixRs232Device::getError(std::string &errtxt){
	debug("DEBUG PixRs232Device::getError is ran");
	errtxt = m_lastErrMsg;
	m_lastErrMsg = "";
}


void PixRs232Device::writeDevice(std::string pCommand, int pAddress){
	std::stringstream a;
	a<<pAddress;
	debug("DEBUG PixRs232Device::writeDevice("+pCommand+", "+a.str()+")");
	pCommand.append(m_writeTerminationString);
	if(pAddress == -1){
		ComWrite(m_port,(void*)pCommand.c_str(),pCommand.length(), m_writeDelay);
		debug(std::string("Sending command ").append(pCommand));
	}
	else if (m_DeviceType == MOCO_DC || m_DeviceType == MERCURY){
		std::stringstream tCommand;
		tCommand<<"\x01"<<pAddress<<pCommand; //add address identifier 0x1, address and command
		ComWrite(m_port,(void*)tCommand.str().c_str(),tCommand.str().length());
		std::stringstream tDebug;
		tDebug<<"Sending command "<<tCommand.str()<<" for channel address "<<pAddress;
		debug(tDebug.str());
	}
	std::this_thread::sleep_for (std::chrono::milliseconds(500));
	m_lastCommand = pCommand;
	debug("DEBUG PixRs232Device::writeDevice END");
}


void PixRs232Device::writeMultiDevice(std::string pCommand){
	debug("DEBUG PixRs232Device::writeMltiDevice is ran");
	for(unsigned int i=0; i<m_DeviceNumberChannels;++i){
		writeDevice(pCommand, i);
	}
}


void PixRs232Device::printDevice(){
	debug("DEBUG PixRs232Device::printDevice()");
	// print m_Instruments array
	std::cout<<"--- RESULTS OF COM "<<m_port+1<<" INIT ---\n";
	std::cout<<"PORT:\tDevice Type:\tFunction:\tPower:\tStatus:\t";
	for(unsigned int i = 0; i < m_DeviceNumberChannels; ++i){
		std::cout<<"CH"<<i+1<<"\t";
	}

	std::cout<<"\n";

	// get device type and transfer it to a std::string
	std::string deviceTypeDescription;
	if      (getDeviceType() == ISEG_SHQ)deviceTypeDescription = "ISEG_SHQ       ";
	else if (getDeviceType() == ISEG_NHQ)deviceTypeDescription = "ISEG_NHQ       ";
	else if (getDeviceType() == MOCO_DC) deviceTypeDescription = "MOCO_DC        ";
	else if (getDeviceType() == MERCURY) deviceTypeDescription = "MERCURY        ";
	else if (getDeviceType() == JULABO)  deviceTypeDescription = "JULABO CHILLER ";
	else if (getDeviceType() == UNISTAT) deviceTypeDescription = "UNISTAT CHILLER";
	else if (getDeviceType() == HM8118)  deviceTypeDescription = "HM8118         ";
	else                                 deviceTypeDescription = "UNKNOWN        ";

	// get device function and transfer it to a std::string
	std::string deviceFunctionDescription;
	if      (getDeviceFunction() == POSITION) deviceFunctionDescription = "POSITION  ";
	else if (getDeviceFunction() == METER)    deviceFunctionDescription = "METER     ";
	else if (getDeviceFunction() == SUPPLY_LV)deviceFunctionDescription = "SUPPLY_LV ";
	else if (getDeviceFunction() == SUPPLY_HV)deviceFunctionDescription = "SUPPLY_HV ";
	else if (getDeviceFunction() == CHILLER)  deviceFunctionDescription = "CHILLER   ";
	else if (getDeviceFunction() == LCRMETER) deviceFunctionDescription = "LCRMETER  ";
	else                                      deviceFunctionDescription = "NONE      ";

	// get device status and transfer it to a std::string
	std::string devicePowerStatus = "unknown";
	getPowerStatus();
	if(m_PowerStatus == COM_ON)       devicePowerStatus = "ON   ";
	else if(m_PowerStatus == COM_LIM) devicePowerStatus = "LIMIT";
	else if(m_PowerStatus == COM_OFF) devicePowerStatus = "OFF  ";

	std::string deviceStatus = "unknown";
	if(getStatus() == COM_OK) deviceStatus = "OK";
	else if(getStatus() == COM_LIMIT) deviceStatus = "LIMIT";
	else if(getStatus() == COM_WARNING) deviceStatus = "WARNING";
	else if(getStatus() == COM_ERROR){
		deviceStatus = "ERROR";
		devicePowerStatus = "UNKOWN  ";
	}

	std::cout << "COM" << getPortID()+1 << "\t" << deviceTypeDescription << "\t" << deviceFunctionDescription << "\t" << devicePowerStatus<< "\t" << deviceStatus<<"\n";
	for(unsigned int i = 0; i < m_DeviceNumberChannels; ++i){
		if(m_DeviceFunction == SUPPLY_HV || m_DeviceFunction == SUPPLY_LV){
			std::cout << "\t" <<getVoltage(i) << "V /" <<getCurrent(i)<<"A";
		}
		if(m_DeviceFunction == LCRMETER){
			std::cout << "\t" <<getCapacity(i) <<"F ";
		}
		if (m_DeviceFunction == POSITION){
			std::cout << "\t" <<getPosition(i);
		}
		if (m_DeviceFunction == CHILLER){
			std::cout << "\t Temperature: " <<getChillerCurrentTemperature(i) << " and controlled by " << 
			(getChillerRegulation(i)?"external":"internal") << " T-sensor";
		}
	}
	std::cout<<std::endl;
	std::cout<< "----------------------------" <<std::endl;
	debug("DEBUG PixRs232Device::printDevice()");
}


DeviceType PixRs232Device::getDeviceType(){
	return m_DeviceType;
}


PixRs232Device::DeviceStatus PixRs232Device::getStatus(){
	return m_Status;
}


PixRs232Device::DevicePowerStatus PixRs232Device::getPowerStatus(int pChannel){
	debug("DEBUG PixRs232Device::getPowerStatus");
	std::string response;
	std::stringstream a;
	a << (pChannel+1);
	switch(m_DeviceType){
	case ISEG_NHQ:
	case ISEG_SHQ:{
		writeDevice("S"+a.str()); //request status bits to check if device is on
		readDevice(response);
		debug("PixRs232Device::getPowerStatus: response to S"+a.str()+": "+response);
		if(response.find("OFF")!=std::string::npos)
			m_PowerStatus= COM_OFF;
		else if(response.find("TRP")!=std::string::npos || response.find("ERR")!=std::string::npos)
			m_PowerStatus= COM_LIM;
		else
			m_PowerStatus= COM_ON;
		break;}
	case MOCO_DC:
	case MERCURY:{
		if(pChannel == -1){
			for (unsigned int iChannel = 0; iChannel < m_DeviceNumberChannels; ++iChannel){ //return COM_ON if one motor is on
				std::string tReadback;
				writeDevice("TS", iChannel);
				readDevice(tReadback);
				int tIntReadback = MOCOstringToInt(tReadback);
				if((tIntReadback & 128) != 128)	//BIT7 shows the motor on/off status, if one channel is on set the device on
					m_PowerStatus= COM_ON;
			}
		}
		else{
			if((unsigned int)pChannel < m_DeviceNumberChannels){
				std::string tReadback;
				writeDevice("TS", pChannel);
				readDevice(tReadback);
				int tIntReadback = MOCOstringToInt(tReadback);
				if((tIntReadback & 128) != 128)	//BIT7 shows the motor on/off status, if one channel is on set the device on
					m_PowerStatus= COM_ON;
			}
			else{
				m_lastErrMsg += "LIMIT: cannot get power status for this channel. Channel does not exist!\n\n";
				m_Status = COM_LIMIT;
			}
		}
		break;}
	case JULABO:
		m_PowerStatus=getJulaboChillerStatus();
		break;
	case UNISTAT:
		writeDevice("STATUS"); // check if command was successful
		readDevice(response);
		if(response=="-1"){ // error status of device
			m_PowerStatus= COM_LIM;
		} else if(response=="0"){
			m_PowerStatus= COM_OFF;
		} else { // 1, 2, 3
			m_PowerStatus= COM_ON;
		}
		std::this_thread::sleep_for (std::chrono::milliseconds(100)); // pause required acc. to manual
		break;
	default:
		break;
	}
	debug("DEBUG PixRs232Device::getPowerStatus END");
	return m_PowerStatus;
}


DeviceFunction PixRs232Device::getDeviceFunction(){
	return m_DeviceFunction;
}


PixRs232Device::Portids PixRs232Device::getPortID(){
	return m_port;
}

// adjust debugging level by setting debugLevel>x
void PixRs232Device::debug(std::string pDebugString, int debugLevel){
	if (PRD_DEBUG){
		if(debugLevel>0){
			std::cout<<pDebugString<<"\n";
		}
	}
}


bool PixRs232Device::stringIsTerminated(std::string pString){
	debug("DEBUG PixRs232Device::stringIsTerminated is ran");
	if(pString.length() < m_readTerminationString.length()){return false;}
	return (pString.substr(pString.length() - m_readTerminationString.length(), m_readTerminationString.length()).compare(m_readTerminationString) == 0);
}


float PixRs232Device::getJulaboChillerCurrentInternalTemperature() {
	debug("DEBUG PixRs232Device::getJulaboChillerCurrentInternalTemperature() is ran");
	std::string response;
	writeDevice("in_pv_00");
	readDevice(response);
	float tTemperature = (float)atof(response.c_str());
	return tTemperature;
}

float PixRs232Device::getJulaboChillerCurrentExternalTemperature() {
	debug("DEBUG PixRs232Device::getJulaboChillerCurrentExternalTemperature() is ran");
	std::string response;
	writeDevice("in_pv_02");
	readDevice(response);
	float tTemperature = (float)atof(response.c_str());
	return tTemperature;
}

float PixRs232Device::getJulaboChillerHeatingPower() {
	debug("DEBUG PixRs232Device::getJulaboChillerHeatingPower() is ran");
	std::string response;
	writeDevice("in_pv_01");
	readDevice(response);
	float tHeatingPower = (float)atof(response.c_str());
	return tHeatingPower;
}

float PixRs232Device::getJulaboChillerSetTemperature() {
	debug("DEBUG PixRs232Device::getJulaboChillerSetTemperature() is ran");
	std::string response;
	writeDevice("in_sp_00");
	readDevice(response);
	float tSetTemperature = (float)atof(response.c_str());
	return tSetTemperature;
}

PixRs232Device::DeviceStatus PixRs232Device::getJulaboChillerAlarm() {
	debug("DEBUG PixRs232Device::getJulaboChillerSetTemperature() is ran");
	std::string response;
	writeDevice("status");
	readDevice(response);
	int tSetTemperature = (int)(atof(response.c_str())+0.5);
	if ((tSetTemperature==0)||(tSetTemperature==1)){
		return COM_LOCAL;
	} else if((tSetTemperature==2)||(tSetTemperature==3)){
		return COM_OK;
	} else{
		return COM_ERROR;
	}
}

bool PixRs232Device::getJulaboChillerRegulation() {
	debug("DEBUG PixRs232Device::getJulaboChillerRegulation() is ran");
	std::string response;
	writeDevice("in_mode_04");
	readDevice(response);
	int intExt = (int)atoi(response.c_str());
	if (intExt==0) {
		return false;
	} else
		return true;
}

PixRs232Device::DevicePowerStatus PixRs232Device::getJulaboChillerStatus() {
	debug("DEBUG PixRs232Device::getJulaboChillerStatus() is ran");
	std::string response;
	writeDevice("in_mode_05");
	readDevice(response);
	int tStatus = (int)atof(response.c_str());
	if (tStatus==0){
		return COM_OFF;
	} else if(tStatus==1){
		return COM_ON;
	}
	return COM_LIM; // just to return something else - do we need another state here??
}

void PixRs232Device::setJulaboChillerStatus(PixRs232Device::DevicePowerStatus tStatus) {
	debug("DEBUG PixRs232Device::setJulaboChillerStatus is ran");
	if (tStatus==COM_ON){
		writeDevice("out_mode_05 1");
	} else if(tStatus==COM_OFF){
		writeDevice("out_mode_05 0");
	}
}

void PixRs232Device::setJulaboChillerTargetTemperature(float value) {
	debug("DEBUG PixRs232Device::setJulaboChillerTargetTemperature is ran");
	char msg[64];
	UPG_sprintf(msg,"out_sp_00 %.2f", value);
	std::string output;
	output = msg;
	writeDevice(output);
}

void PixRs232Device::setJulaboChillerRegulation(bool extInt) {
	debug("DEBUG PixRs232Device::setJulaboChillerRegulation is ran");
	if(extInt==0){
		writeDevice("out_mode_04 0");
	} else
		writeDevice("out_mode_04 1");
}

float PixRs232Device::getChillerCurrentTemperature(unsigned int ){//pChannel) {
	debug("DEBUG PixRs232Device::getChillerCurrentTemperature is ran");
	std::string response;
	switch(m_DeviceType){
	case JULABO:
		if(getJulaboChillerRegulation()==false){
			return getJulaboChillerCurrentInternalTemperature();
		} else
			return getJulaboChillerCurrentExternalTemperature();
		break;
	case UNISTAT:
		writeDevice("IN_PV_00");
		readDevice(response);
		std::this_thread::sleep_for (std::chrono::milliseconds(100)); // pause required acc. to manual
		return std::stof(response);
		break;
	default:
		return -273.15;
		break; // do nothing beside setting an invalid value
	}
}

float PixRs232Device::getChillerSetTemperature(unsigned int ){//pChannel) {
	debug("DEBUG PixRs232Device::getChillerSetTemperature is ran");
	std::string response;
	switch(m_DeviceType){
	case JULABO:
		return getJulaboChillerSetTemperature();
		break;
	case UNISTAT:
		writeDevice("IN_SP_00");
		readDevice(response);
		std::this_thread::sleep_for (std::chrono::milliseconds(100)); // pause required acc. to manual
		return std::stof(response);
		break;
	default:
		return -273.15;
		break; // do nothing beside setting an invalid value
	}
}

void PixRs232Device::setChillerTargetTemperature(unsigned int , float value) { //pChannel
	switch(m_DeviceType){
	case JULABO:
		setJulaboChillerTargetTemperature(value);
		break;
	case UNISTAT:{
		char cmd[50];
		sprintf(cmd, "OUT_SP_00 %.2f", value);
		writeDevice(cmd);
		std::this_thread::sleep_for (std::chrono::milliseconds(100)); // pause required acc. to manual
		break;}
	default:
		break; // do nothing
	}
}

bool PixRs232Device::getChillerRegulation(unsigned int ){//pChannel) {
	switch(m_DeviceType){
	case JULABO:
		return getJulaboChillerRegulation();
		break;
	default:
		return false;
		break; // do nothing beside setting an invalid value
	}
}

void PixRs232Device::setChillerRegulation(unsigned int , bool choice) { //pChannel
	switch(m_DeviceType){
	case JULABO:
		return setJulaboChillerRegulation(choice);
		break;
	default:
		return;
		break; // do nothing
	}
}


void PixRs232Device::resetError(){
	m_lastErrMsg = "";
	m_Status = COM_OK;
}


int PixRs232Device::getPosition(unsigned int pChannel){
	debug("DEBUG PixRs232Device::getPosition() is ran");
	if(m_DeviceFunction != POSITION)
		return 0;
	if(m_DeviceType == MOCO_DC){
		std::string tReadback;
		writeDevice("TP", pChannel);
		readDevice(tReadback);
		return MOCOstringToInt(tReadback);

	}else if(m_DeviceType == MERCURY){

	std::string buffer;
	writeDevice("TP");
	double position_member;
	readDevice(buffer);
	//MOCOstringToInt(tReadback);
	if (buffer == "0")
		return (0);
	//			return position_member;
	buffer = buffer.substr(3, buffer.length()-2);
	//return (StrToFloat(buffer)/STEPS_PER_MM);
	position_member = (StrToFloat(buffer))/STEPS_PER_MM;
	printf ("Position in getPositionFunction: %f", position_member);
	//return position_member;
	int test = (int)position_member;
	printf ("Position in getPositionFunction as INT : %d", test);
	return test;
	}
	return 0;
}


void PixRs232Device::setMaxSpeed(int pSpeed){
	debug("PixRs232Device::setMaxSpeed(pSpeed)");
	if(m_DeviceFunction != POSITION){
		return;
	}
	if(m_DeviceType == MOCO_DC || m_DeviceType == MERCURY){
		if(pSpeed > 0 && pSpeed <= 1e6){
			std::stringstream tCommand;
			tCommand<<"dv"<<pSpeed;
			writeMultiDevice(tCommand.str());
			for (unsigned int iChannel = 0; iChannel<m_DeviceNumberChannels; ++iChannel)
				m_maxSpeeds[iChannel] = pSpeed;
		}
	}
}


void PixRs232Device::setMaxSpeed(unsigned int pChannel, int pSpeed){
	debug("PixRs232Device::setMaxSpeed(pChannel, pSpeed)");
	if(m_DeviceFunction != POSITION)
		return;
	if(pChannel < m_DeviceNumberChannels){
		std::stringstream tCommand;
		tCommand<<"dv"<<pSpeed;
		writeDevice(tCommand.str(), pChannel);
	}
	else{
		m_lastErrMsg += "LIMIT: cannot set a Speed for this channel. Channel does not exist!\n\n";
		m_Status = COM_LIMIT;
	}
	m_maxSpeeds[pChannel] = pSpeed;
}


int PixRs232Device::getSpeed(unsigned int pChannel){
	debug("PixRs232Device::getMaxSpeed()");
	if(m_DeviceFunction != POSITION)
		return 0;
	if(m_DeviceType == MOCO_DC){
		std::string tReadback;
		writeDevice("TV", pChannel);
		readDevice(tReadback);
		return MOCOstringToInt(tReadback);
	}
	if(m_DeviceType == MERCURY){
		std::string tReadback;
		writeDevice("TY", pChannel);
		readDevice(tReadback);
		return MOCOstringToInt(tReadback);
	}
	return 0;
}


int PixRs232Device::getMaxSpeed(unsigned int pChannel){
	debug("PixRs232Device::getMaxSpeed()");
	if(m_DeviceFunction != POSITION)
		return 0;
	if(m_DeviceType == MOCO_DC || m_DeviceType == MERCURY){
		return m_maxSpeeds[pChannel];
	}
	return 0;
}


float PixRs232Device::ISEGatof(const char *svalue_in){
	float value = 0.;
	std::string sexp="0", svalue = svalue_in;
	int pos = svalue.find_last_of("-");
	if(pos==(int)std::string::npos){pos = svalue.find_last_of("+");}
	if(pos!=(int)std::string::npos){
		sexp = svalue.substr(pos,3);
		svalue = svalue.substr(0,pos);
	}
	value = ((float)atoi(svalue.c_str()))*(float)pow(10.,atoi(sexp.c_str()));
	return value;
}


int PixRs232Device::MOCOstringToInt(std::string pString){
	pString = pString.substr(pString.find(":")+1, pString.length()-pString.find(":"));
	std::stringstream tValue(pString);
	int tIntValue;
	if (!(tValue>>tIntValue)){
		debug(std::string("PixRs232Device::MOCOstringToInt: ").append(pString));
		return -1;
	}
	return tIntValue;
}


bool PixRs232Device::moveAbsolute(double val, unsigned int pChannel){
	char buffer[256];

	UPG_sprintf(buffer, "MA%d", (int)((float)val*STEPS_PER_MM));
	writeDevice(buffer);

	if(isMoveCompleted(pChannel))return true;
	return false;
}


bool PixRs232Device::isMoveCompleted(unsigned int pChannel){
	//unsigned int pChannel = 0;
	std::string tReadback;
	char ch [2];
	bool finished;

	do {
		writeDevice("TS", pChannel);
		readDevice(tReadback);
		//std::cout<<"\n WAIT: tReadback whole string "<<tReadback<<"\n";
		std::string refString = tReadback.substr(tReadback.find(":")+1, 2);
		//std::cout<<"\n WAIT!! part of string that contains...: "<<refString<<"\n";
		for(int a=0;a<2;a++){ch[a]=refString[a];}
		if((ch[1]&0x4)){
			std::cout<<" Trajectory complete \n";
			finished = true;
		}else{
			finished = false;
		}
	}while(finished == false);

	return finished;
}


bool PixRs232Device::moveRelative(double val){
	char buffer[256];

	UPG_sprintf(buffer, "MR%d", (int)((float)val*STEPS_PER_MM));
	writeDevice(buffer);
	if(isMoveCompleted(0)){return true;} // test function only works for channel 0!!!!!!! very bad !!!!!
	return false;
}


double PixRs232Device::getPositionMercury(){
	std::string buffer;
	writeDevice("TP");
	double position_member;
	readDevice(buffer);
	//MOCOstringToInt(tReadback);
	if (buffer == "0"){
		return (0);
		//return position_member;
	}
	buffer = buffer.substr(3, buffer.length()-2);
	//return (StrToFloat(buffer)/STEPS_PER_MM);
	position_member = (StrToFloat(buffer))/STEPS_PER_MM;
	printf ("Position from Function getPositionMercury: %f", position_member);
	return position_member;
}


double PixRs232Device::StrToFloat(std::string stringValue){
	std::stringstream ssStream(stringValue);
	double Return;
	ssStream >> Return;

	return Return; 
}


double PixRs232Device::findReference() // not yet possible
{
	unsigned int LimitRefState = 0;
	unsigned int pChannel = 0;
	std::string tReadback;
	char ch [2];

	// Check if you are already at the reference position
	writeDevice("TS", pChannel);
	readDevice(tReadback);
	std::string refString = tReadback.substr(tReadback.find(":")+13, 2);
	for (int a=0;a<2;a++){
		ch[a]=refString[a];
	}
	if(!(ch[1]&0x1) && (ch[1]&0x2) && !(ch[1]&0x4) && !(ch[1]&0x8)){
		std::cout<<" Condition for reference fulfilled\n";
		LimitRefState = 1;
	}else if (!(ch[1]&0x1) && !(ch[1]&0x2) && (ch[1]&0x4) && !(ch[1]&0x8)){
		std::cout<<" Condition for Positive limit fulfilled\n"; 
		LimitRefState = 2;
	}else if (!(ch[1]&0x1) && !(ch[1]&0x2) && !(ch[1]&0x4) && (ch[1]&0x8)){
		std::cout<<" Condition for Negative limit fulfilled\n";
		LimitRefState = 3;
	}

	if (LimitRefState == 1) return 0; // if you are already at the reference position then immediately return and finish the function

	// if you are not at the reference position....
	do {
		moveRelative(-10);
		writeDevice("TS", pChannel);
		readDevice(tReadback);
		std::cout<<"\n tReadback whole string "<<tReadback<<"\n";
		std::string refString = tReadback.substr(tReadback.find(":")+13, 2);
		std::cout<<"\n FIND REFERENCE!! part of string that contains hex code for limits and reference: "<<refString<<"\n";
		for (int a=0;a<2;a++){
			ch[a]=refString[a];
		}
		if(!(ch[1]&0x1) && (ch[1]&0x2) && !(ch[1]&0x4) && !(ch[1]&0x8)){
			std::cout<<" Condition for reference fulfilled\n";
			LimitRefState = 1;
		}else if (!(ch[1]&0x1) && !(ch[1]&0x2) && (ch[1]&0x4) && !(ch[1]&0x8)){
			std::cout<<" Condition for Positive limit fulfilled\n";
			LimitRefState = 2;
		}else if (!(ch[1]&0x1) && !(ch[1]&0x2) && !(ch[1]&0x4) && (ch[1]&0x8)){
			std::cout<<" Condition for Negative limit fulfilled\n";
			LimitRefState = 3;
		}
		if (LimitRefState==1){break;}
	} while(LimitRefState != 3);

	getPositionMercury();
	writeDevice("ST", pChannel);
	writeDevice("DH", pChannel); // Defines the references switch as the home position
	getPositionMercury();

	return 0;
}


double PixRs232Device::findReference2() // not yet possible
{
	unsigned int pChannel = 0;
	//std::string tReadback;
	//char ch [2];

	//getPositionMercury();

	writeDevice("RT", pChannel); // reset to defaults

	std::this_thread::sleep_for (std::chrono::milliseconds(200));

	writeDevice("FE2", pChannel);
	writeDevice("WS", pChannel);
	writeDevice("DH", pChannel);
	writeDevice("WS", pChannel);
	writeDevice("DH", pChannel);
	//writeDevice("DH", pChannel); // Defines the references switch as the home position

	getPositionMercury();
	return 0;
}


//LCR (HM8118). A function to ask the setting E.g. VOLT, etc.
//if printOut was given a value (that is not default "", print out the reply)
std::string PixRs232Device::askSetting(std::string theCommand, std::string replyStr, std::string printOut){
	debug("DEBUG PixRs232Device::askSetting is ran");
	writeDevice(theCommand);
	readDevice(replyStr);
	if(printOut!=""){
		std::cout<<printOut<<theCommand<<" replyStr="<<replyStr<<"\n";
	}
	return replyStr;
}


//LCR (HM8118). A function to ask whether a measurement is done or not
//sleepTime2 default=0
void PixRs232Device::measDoneOrNot(std::string replyStr, unsigned int maxto, int sleepTime1, int sleepTime2){
	debug("DEBUG PixRs232Device::measDoneOrNot");
	replyStr="";
	unsigned int timeout=0;
	std::this_thread::sleep_for (std::chrono::milliseconds(sleepTime1));
	std::cout<<"Slept for "<<sleepTime1<<" ms\n";
	writeDevice("*OPC?"); // query meas. stat
	for(;replyStr=="" && timeout<maxto;timeout++){
		std::cout<<"timeout/maxto: "<<timeout+1<<"/"<<maxto<<"\n";
		readDevice(replyStr);
		if (sleepTime2!=0){std::this_thread::sleep_for (std::chrono::milliseconds(sleepTime2));}
		if (replyStr=="1"){
			std::cout<<"replyStr: "<<replyStr<<", measurement finished!!!!!!!!!!!!!!!!\n";
		}else if(replyStr=="ERROR"){
			std::cout<<"replyStr: "<<replyStr<<", measurement finished, but LCR replied ERROR...\n";
		}else if(replyStr!=""){
			std::cout<<"replyStr: "<<replyStr<<", weird replyStr!!!!!!!!!!!!!!!!!!!\n";
		}else{
			std::cout<<"replyStr: "<<replyStr<<", meas not finished after "<<timeout+1<<" times :(\n";
		}
		//timeout++;
	}
	if(timeout==maxto){
		std::cerr << "Timeout waiting for measurement to complete (OPC query)!" << std::endl;
	}
	debug("DEBUG PixRs232Device::measDoneOrNot END");
}


// Ran once at the 1st measurement. 
// bool askOrNot=false, bool seeMeas=false
void PixRs232Device::setupHM8118(float freq_in, float veff, double circtype, int biastype, std::string triggeringMode, bool askSettingOrNot, bool seeMeas){
	debug("DEBUG PixRs232Device::setupHM8118()\n");

	std::string cmdstrg="";
	std::string replyStr;
	int sleepTime1=100;//2500
	int sleepTime2=100;
	const int nofAskTime = 10;//the maximum times of trial for *OPC?

	// Set Frequency
	const int npt = 17;
	int lcr_frq[npt]={10,12,15,18,20,24,25,30,36,40,45,50,60,72,75,80,90};

	// find nearest possible frequency and set
	float freq = 20.;
	float fpwrf = 1;
	for(int fpwr = 1; fpwr<5; fpwr++){
		float testf = 20.;
		for(int i=0;i<npt;i++){
			testf = fpwrf*(float)lcr_frq[i];
			if(testf<20.) continue; //min range of meter
			if(fabs(testf-freq_in)<fabs(freq-freq_in))
				freq = testf;
		}
		fpwrf *= 10.;
		if(testf>2.e5) break; //max range of meter
	}
	// extra safeguard
	if(freq<20.) freq = 20.;
	if(freq>2.e5) freq=2.e5;

	std::stringstream a;
	a<<freq;
	cmdstrg = "FREQ"+a.str();
	if(askSettingOrNot){askSetting("FREQ?",replyStr,"  ");}
	writeDevice(cmdstrg);//set FREQ
	//writeDevice("FREQ1000");//set FREQ
	if(seeMeas){measDoneOrNot(replyStr, nofAskTime, sleepTime1, sleepTime2);}

	// Set Meas Volt
	std::stringstream b;
	b<<veff;
	if (askSettingOrNot){askSetting("VOLT?",replyStr,"  ");}
	writeDevice(("VOLT"+b.str()).c_str()); // eff. voltage for measurement
	//writeDevice("VOLT0.05");
	if (seeMeas){measDoneOrNot(replyStr, nofAskTime, sleepTime1, sleepTime2);}

	// Set Circuit Type
	std::stringstream d;
	d<<circtype;
	if(askSettingOrNot){askSetting("CIRC?",replyStr," ");}
	writeDevice(("CIRC"+d.str()).c_str()); // type of circuit for measurement
	//writeDevice("CIRC1");
	if(seeMeas){measDoneOrNot(replyStr, nofAskTime, sleepTime1, sleepTime2);}

	if (biastype==2){//external bias
		writeDevice("CONV1");
		//std::cout<<"sending command BIAS2!";
		writeDevice("BIAS2");
		//askSetting("BIAS?",replyStr);
	}else if (biastype==1){//internal bias
		writeDevice("CONV1");
		writeDevice("BIAS1"); //internal bias
		//askSetting("BIAS?",replyStr);
	}else{
		writeDevice("BIAS0"); //bias off
	}

	// Set Continuous or Manual triggering
	// MMOD0 = cont. trg., MMOD1=manual trg., must be set in init. fct.
	if (triggeringMode=="MMOD1"){
		writeDevice("*TRG"); // issue meas. trigger
		//writeDevice("STRT"); //another command to trigger, not clear in manual what the difference between *TRG and STRT is.
		if(seeMeas){measDoneOrNot(replyStr, nofAskTime, sleepTime1, sleepTime2);}
	}//No need for else because default continuous triggering
	debug("DEBUG PixRs232Device::setupHM8118() END");
}


float PixRs232Device::getCapacity(unsigned int /*channel*/, float freq_in, float veff, double circtype, int biastype, int indexOfMeas){
	debug("DEBUG PixRs232Device::getCapacity");
	debug("Index of meas: "+std::to_string(indexOfMeas));
	if(m_DeviceType == HM8118){
		double cap = -1.;
		std::string cmdstrg;
		std::string replyStr;
		int sleepTime1=100;//2500
		int sleepTime2=100;// sleep time after asking for measured value
		const int maxto = 10;//the maximum times of trial for *OPC?

		bool seeMeas=false;
		std::string triggeringMode="MMOD0";//0=auto triggering; 1=manual triggering
		if(indexOfMeas==0){//TODO this is being ran multiple times when multiple nof repetitions
			setupHM8118(freq_in, veff, circtype, biastype, triggeringMode);
		}

		// Check if setup alright
		if(PRD_DEBUG){
			askSetting("FREQ?",replyStr," ");
			askSetting("VOLT?",replyStr," ");
			askSetting("CIRC?",replyStr," ");
		}
		// Actual Measurment
		writeDevice("*WAI"); // wait, as mentioned in manual
		writeDevice("XMAJ?"); // request meas. value
		std::this_thread::sleep_for (std::chrono::milliseconds(sleepTime2));
		if(readDevice(cmdstrg)){
			// NB: unfinished measurements will return cmdstrg="------"
			debug("HM8118 replied something :)\n");

			if(PRD_DEBUG){std::cout <<indexOfMeas<<" Meas C = "<<cmdstrg << std::endl;}
			if (cmdstrg=="ERROR"){
				std::cerr << "!Error reading meas. cmdstrg==ERROR" << std::endl;
				debug("DEBUG PixRs232Device::getCapacity END");
				return -2.;
			}else if(cmdstrg=="------"){
				std::cerr << "!Error reading meas. cmdstrg==------" << std::endl;
				debug("DEBUG PixRs232Device::getCapacity END");
				return -3;
			}else{
				cap = (double) atof(cmdstrg.c_str());
				if (cap==0){debug("cap==0! possibly a bad measurement!",1);}
			}
		}else{
			if(PRD_DEBUG){std::cout <<indexOfMeas<<" Meas C = "<<cmdstrg << std::endl;}
			getError(cmdstrg);//This isn't useful yet.
			std::cerr << "Error reading meas. result (readDevice==false): " << cmdstrg << std::endl;
			debug("DEBUG PixRs232Device::getCapacity END");
			return -4;
		}

		if(seeMeas){measDoneOrNot(replyStr, maxto, sleepTime1, sleepTime2);}
		debug("DEBUG PixRs232Device::getCapacity END");
		return cap;
	}else{
		std::cout<<"PixRs232Device::getCapacity, Device not an HM8118, returning -1.\n";
		debug("DEBUG PixRs232Device::getCapacity END");
		return -1.;
	}
}

