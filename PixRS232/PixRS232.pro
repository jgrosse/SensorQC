TEMPLATE = lib

include(../build-config.inc)
  
SOURCES = PixRs232Device.cxx \
	  VA18B.cxx \
	  SPS30.cxx \
	  ArduinoADC.cxx

INCLUDEPATH += . ../include

win32 {
  DESTDIR = ../bin
  DLLDESTDIR = ../bin
  SOURCES += ComTools.cxx
  DEFINES += WIN32 PIX_DLL_EXPORT
}

unix {
  DESTDIR = .
  DEFINES += CF__LINUX
  SOURCES += ComToolsLinux.cxx
}

