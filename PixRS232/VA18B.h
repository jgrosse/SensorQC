#ifndef VA18B_H
#define VA18B_H

#include <dllexport.h>
#include <string>
#include <thread>

class DllExport VA18B{

 public:
  VA18B(int iPort);
  ~VA18B();

  double getMeasVal(){return m_measVal;};
  std::string getMeasUnit(){return m_measUnit;};

 private:
  int m_iPort;
  std::thread m_measThr;
  double m_measVal;
  std::string m_measUnit;
  bool m_runMeasLoop;

  void readFromMeter();
  int m_nFailedReads;
};

#endif // VA18B_H
