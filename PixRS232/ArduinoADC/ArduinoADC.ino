#include "Wire.h"

const unsigned int MAXARGS=5;
const unsigned int MAXBYTES=16;
const unsigned int MAXCMDLENGTH=256;
const unsigned int MAXARGLENGTH=16;

// Command parsing
size_t cmdptr=0;
char command[MAXCMDLENGTH];
unsigned int argc=0;
char argv[MAXARGS][MAXARGLENGTH];

// Parse command execute the right function
void runcommand()
{
  // Tokenize string into command and argument
  char* tok=strtok(command," ");
  argc=0;
  while(tok != NULL)
    {
      strcpy(argv[argc++], tok);
      tok = strtok(NULL, " ");
    }

  // Execute the right command                                                                            
  if(strncmp("HELP", argv[0], 4)==0)
    { // Print help menu
      cmdHELP();
    }
    //need to check ADC functionality 
  else if( strncmp("ADC", argv[0], 3)==0)
    { // Read ADC
      if(argc!=2)
        {
          Serial.println("ERR wrong number of arguments to ADC");
          return;
        }
      cmdADC(atoi(argv[1]));
    }
  else if(strncmp("I2C", argv[0], 3)==0)
    { // I2C commands
      if(argc<4)
        {
          Serial.println("ERR wrong number of arguments to I2C");
          return;
        }

      int addr =0;
      sscanf(argv[2], "%02x", &addr);
      if(strncmp("WRITE",argv[1],5)==0)
        {
          cmdI2Cwrite(addr, argv[3]);
        }
      else if(strncmp("READ",argv[1],4)==0)
        {
          cmdI2Cread(addr,atoi(argv[3]));
        }
      else
        {
          Serial.println("ERR unknown I2C command");
        }
    }
                                                                      
 else
    {
      Serial.println("ERR unknown command");
    }
}

//
// The commands
//

//
// Print
void cmdHELP()
{
  Serial.println("Hello World from DevComduino!");
  Serial.println("");
  Serial.println("Available commands:");
  Serial.println("\tHELP - Print this help");
  Serial.println("\tADC ch - Return ADC reading on channel ch");
  Serial.println("\tI2C WRITE addr byte-string - Write a byte-string using I2C to addr, MSB first");
  Serial.println("\tI2C READ addr nbytes - Read number of bytes from addr");
}

//
// Read an analogue pin

void cmdADC(int channel)
{
  float V;
  switch(channel)
    {
    case 0:
      V=analogRead(A0);
      break;
    case 1:
      V=analogRead(A1);
      break;
    case 2:
      V=analogRead(A2);
      break;
    case 3:
      V=analogRead(A3);
      break;
    case 4:
      V=analogRead(A4);
      break;
    case 5:
      V=analogRead(A5);
      break;
    default:
      Serial.println("ERR invalid channel");
      break;
  }

  Serial.println(V);
}

//
// I2C write
void cmdI2Cwrite(int address, char *cmd)
{
  Wire.beginTransmission(address);

  int c =0;
  for(int i=0; i<strlen(cmd); i+=2)
    {
      sscanf(&cmd[i], "%02x", &c);
      Wire.write(c);
    }
  Wire.endTransmission();
  Serial.println("OK");
}

//
// I2C read
void cmdI2Cread(int address, unsigned int nBytes)
{
  Wire.requestFrom(address,nBytes);

  unsigned char c;
  char cstr[4];

  //sprintf(cstr, "%02x:", nBytes);
  //Serial.print(cstr);

  for(unsigned int i=0;i<nBytes;i++)
    {
      if(Wire.available())
	{
	  c=Wire.read();
	  sprintf(cstr, "%02x", c);
	  Serial.print(cstr);
	  //Serial.print(strlen(cstr));
	}
      else
	{
	  Serial.print("ERR");
	}
    }
  Serial.println();
}

//
// The big main loop
//

//
// Setup serial
void setup()
{
  Serial.begin(9600);
  Wire.begin();
}

//
// The main loop looks for commands
void loop()
{
  if(Serial.available()==0)
    return;

  // Read new data
  size_t length=Serial.readBytes(&command[cmdptr], min(Serial.available(), MAXCMDLENGTH-cmdptr));
  if(length==0) return; // No new data...
  cmdptr+=length;

  // Check if command finished (new line)
  if (cmdptr < 2) return;
  if (command[cmdptr-2]!='\r' && command[cmdptr-1]!='\n') {
    if (cmdptr >= MAXCMDLENGTH-1) {
      //overflow command. Clean-up buffer to avoid stalled program
      cmdptr=0;
      Serial.print("ERR Command too long");
    }
    return;
  }

  // There is a command! Process it...
  char *c=command; while(*c) { *c=toupper(*c); c++; }
  command[cmdptr-2]='\0';

  runcommand();
  cmdptr=0; // Reset command

}
