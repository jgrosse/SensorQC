#ifndef ARDUINOADC_H
#define ARDUINOADC_H

#include <dllexport.h>
#include <string>
#include <thread>

class DllExport ArduinoADC{

 public:
  ArduinoADC(int iPort);
  ~ArduinoADC();

  double getMeasVal(uint pin, double refRes=0., double nomV=5., double offset = 0., uint nrep=5);
  std::string getMeasVal(uint address, std::string wcmd, uint nblocks);
  int getPort(){return (m_port-20);};

 private:
  int m_port;
  std::string sendReceive(std::string command, bool showErr=true);
};

#endif // ARDUINOADC_H
