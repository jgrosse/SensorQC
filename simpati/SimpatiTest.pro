CONFIG -= qt

TEMPLATE = app

include(../build-config.inc)

SOURCES += SimpatiTest.cxx

INCLUDEPATH += ../include
unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX
	QMAKE_LFLAGS += -L . -lpthread -lsimpati
	QMAKE_RPATHDIR += ../simpati
}
win32 {
    DESTDIR = ../bin
    CONFIG += console
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
    DEFINES += "_CRT_SECURE_NO_WARNINGS" 
    DEFINES += __VISUALC__ 
    QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
    QMAKE_LFLAGS_RELEASE = delayimp.lib simpati.lib
    QMAKE_LFLAGS_CONSOLE += /LIBPATH:../bin
}

QT += network core
