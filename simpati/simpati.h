#ifndef SIMPATI_H
#define SIMPATI_H

#include "dllexport.h"
#include <map>
#include <vector>
#include <string>
#include <thread>

class DllExport simpati {

 public:
  simpati(std::string ipaddr = "192.168.1.105", int port = 7777, int chid=1, uint dn2id=0);
  ~simpati();

  void setTwarm(double tset);
  void setTcold(double tset);
  void setCage(bool top=true, bool wait=true);
  void setEnabled(bool setOn=true);
  void startCyclingPrg(int pid);
  void stopCycling();
  std::map<int, std::string> getNames(){return m_measNames;};
  std::map<int, std::pair<double, double> > getVals(){return m_measVals;};
  void stopMon(){m_runReadings=false;};
  void updateReadings();
  void updateReadingsLoop();
  int getChamberStatus();

 private:
  std::string getSimAnswer(std::string cmd, std::vector<std::string> args);

  std::map<int, std::string> m_measNames;
  std::map<int, std::pair<double, double> > m_measVals;
  std::string m_ipaddr;
  int m_port;
  int m_chid;
  uint m_dn2id;
  bool m_runReadings;
  std::thread m_roThread;
};

#endif // SIMPATI_H
