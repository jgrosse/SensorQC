TEMPLATE = lib

include(../build-config.inc)
  
SOURCES = simpati.cxx

DESTDIR = .
INCLUDEPATH += ../include

unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX
	QMAKE_LFLAGS += -lpthread
}

win32{
  DLLDESTDIR = ..\bin
  DESTDIR = ..\bin
  CONFIG += dll
  DEFINES += WIN32 PIX_DLL_EXPORT
  QMAKE_LFLAGS_RELEASE = delayimp.lib
  QMAKE_LFLAGS_WINDOWS += /LIBPATH:../bin
}

QT = network core
