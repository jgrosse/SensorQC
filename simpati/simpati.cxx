// #include <boost/asio.hpp>
// #include <boost/array.hpp>
#include <QTcpSocket>
#include <QHostAddress>

#include <iostream>
#include <string>
#include <sstream>
#include <chrono>

#include <simpati.h>

simpati::simpati(std::string ipaddr, int port, int chid, uint dn2id) : m_ipaddr(ipaddr), m_port(port), m_chid(chid){
  std::vector<std::string> args;
  std::string answer = getSimAnswer("11018", args);
  uint nmeas = (uint)atoi(answer.c_str());
  for(uint i=1;i<nmeas+1;i++){
    std::stringstream a;
    a << i;
    args.clear();
    args.push_back(a.str());
    answer = getSimAnswer("11026", args);
    if(answer.substr(0,2)!="  "){
      m_measNames[i] = answer;
      m_measVals[i] = std::make_pair(-9999., -9999.);
    }
  }
  answer = getSimAnswer("14007", args);
  uint ndigi = (uint)atoi(answer.c_str());
  m_dn2id = 0;
  if(dn2id>=1 && dn2id<=ndigi) m_dn2id = dn2id;
  m_runReadings = true;
  m_roThread = std::thread(&simpati::updateReadingsLoop, this);
}
simpati::~simpati(){
  m_runReadings = false;
  if(m_roThread.joinable()) m_roThread.join();
}

void simpati::setTwarm(double tset){
  std::vector<std::string> args;
  std::stringstream a;
  a<< tset;
  args.push_back("3");
  args.push_back(a.str());
  getSimAnswer("11001", args);
}
void simpati::setTcold(double tset){
  std::vector<std::string> args;
  std::stringstream a;
  a<< tset;
  args.push_back("4");
  args.push_back(a.str());
  getSimAnswer("11001", args);
}
void simpati::setCage(bool top, bool wait){
  std::vector<std::string> args;
  args.push_back("1");
  args.push_back(top?"1":"2");
  getSimAnswer("11001", args);
  if(wait){
    double cagepos = top?2:1;
    while(cagepos!=(top?1:2)) {
      std::pair<double, double> vals = getVals()[1];
      cagepos = vals.second;
      std::this_thread::sleep_for (std::chrono::milliseconds(1000));
    }
  }
}
void simpati::setEnabled(bool setOn){
  std::vector<std::string> args;
  // manual operation
  args.push_back("0x20");
  getSimAnswer("19050", args);
  // set main, warm and cold dig. channel
  args.push_back(setOn?"1":"0");
  args[0] = "1";
  getSimAnswer("14001", args);
  args[0] = "2";
  getSimAnswer("14001", args);
  args[0] = "3";
  getSimAnswer("14001", args);
  if(m_dn2id>0){ // set dry air / N2 digital channel to on/off if requested
    std::stringstream a;
    a << m_dn2id;
    args[0] = a.str();
    getSimAnswer("14001", args);
  }
}
void simpati::startCyclingPrg(int pid){
  std::vector<std::string> args;
  // start cycling program no. pid
  std::stringstream a;
  a << pid;
  args.push_back(a.str());
  args.push_back("0");
  std::string answer = getSimAnswer("19014", args);

  // wait unti started
  answer="0";
  uint nwc=0;
  const uint nwcmax=50;
  args.clear();
  while(answer=="0" && nwc<nwcmax){
    answer = getSimAnswer("19062", args);
    nwc++;
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));
  }
  if(nwc<nwcmax)
    ;//    std::cout << "Program started" << std::endl;
  else{
    std::cout << "Error starting program!" << std::endl;
  }
}
void simpati::stopCycling(){
  std::vector<std::string> args;
  getSimAnswer("19015", args);
}

int simpati::getChamberStatus(){
  std::vector<std::string> args;
  std::string answer = getSimAnswer("19062", args);
  if(answer=="1") return 1;
  else return 0;
}

std::string simpati::getSimAnswer(std::string cmd, std::vector<std::string> args){
  QTcpSocket *sock = new QTcpSocket;
  const int myTimeout = 1000;

  size_t len;
  unsigned char bufc[128];
  char cchid[2];
  sprintf(cchid,"%d",m_chid);

  for(uint i=0;i<5;i++)
    bufc[i] = cmd.c_str()[i];
  bufc[5]=182;
  bufc[6]=cchid[0];

  len = 7;
  for(uint i=0;i<args.size();i++){
    bufc[len]=182;
    len++;
    for(uint k=0;k<args[i].length();k++){
      bufc[len]=args[i].c_str()[k];
      len++;
    }
  }
  bufc[len]='\r';
  len++;

  sock->connectToHost(m_ipaddr.data(), m_port);
  if (!sock->waitForConnected(myTimeout)){
    std::cerr << "Timeout connecting to " << m_ipaddr<<std::endl;
    return "";
  }
  if (sock->state() != QAbstractSocket::ConnectedState){
    std::cerr << "Error connecting to " << m_ipaddr<<std::endl;
    return "";
  }
  QByteArray block((const char*)bufc, len);
  size_t len2 = sock->write(block);
  sock->waitForBytesWritten(myTimeout);
  if(len2==len){
    std::this_thread::sleep_for (std::chrono::milliseconds(50));
    QByteArray resp;
    int tries = 0;
    while((!resp.contains('\n')) && (tries++ < 600))
      {
	sock->waitForReadyRead(100);
	resp += sock->readAll();
	if (sock->state() != QAbstractSocket::ConnectedState){
	  sock->close();
	  std::cerr << "Error: lost connection to " << m_ipaddr<<std::endl;
	  return "";
	}
      }
    sock->close();
    int idx = resp.indexOf('\n');
    if (idx < 1)
      return std::string("");
    else{
	char answer[128];
	for(int i=2;i<resp.length();i++){
	  if(resp[i]=='\r'){
	    answer[i-2]='\0';
	    break;
	  } else
	    answer[i-2]=resp[i];
	}
	return std::string(answer);
    }
  } else
    std::cerr << "Sent "<<len2<<" bits, but should have been " << len << std::endl;

  sock->close();
  return "";
}
void simpati::updateReadingsLoop(){
  while(m_runReadings){
    updateReadings();
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));
  }
}
void simpati::updateReadings(){
  for(std::map<int, std::string>::iterator it = m_measNames.begin();
      it!=m_measNames.end();it++){
    std::stringstream a;
    std::vector<std::string> args;
    std::string answer;
    a << it->first;
    args.push_back(a.str());
    answer = getSimAnswer("11002", args);
    double soll = (double)atof(answer.c_str());
    answer = getSimAnswer("11004", args);
    double ist = (double)atof(answer.c_str());
    m_measVals[it->first] = std::make_pair(soll, ist);
  }
}
