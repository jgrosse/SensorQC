#ifndef IVconfiguration_ITkMarketSurvey_h
#define IVconfiguration_ITkMarketSurvey_h

#include <fstream>
#include <iostream>

#include "basicFunctions.h"

// This header file contains the configuration for the analysis of a set of IV curves

void fileExistsOrNot(TString fileName, TString inputPath = "/work1/pixtests/Data/IV_CV_It_rawData/ITkPixBareModules/"){
	std::ifstream ifile;
	std::cout<<inputPath<<fileName<<" ";
	ifile.open(inputPath+fileName);
	if(ifile) {
		std::cout<<"file exists\n";
	} else {
		std::cout<<"file doesn't exist\n";
	}
}

void IVconfiguration( TString IVconfigID, double &multiplyT, double &multiplyV, double &multiplyI, double &multiplyC, double &breakDownCurrent, double &fixedVoltageForCurrentEval, double &fractionOfVBDForCurrentEval, 
	int &NstepsX, int &NstepsY, TString &specifier, TString &inputFileNameBase, TString &outputFileNameBase, 
	vector<TString> &fileName_, vector<TString> &legName_, vector<TString> &graphName_,
	double &Vlo, double &Vhi, int &NbinsV, double &VloLGAD, double &VhiLGAD, double &Ilo, double &Ihi, int &NbinsI, double &IloZoom, double &IhiZoom, double &Clo, double &Chi, double &C2hi){

	multiplyV=-1; multiplyI=-1; // mirror V and I to have positive axis
	multiplyT=1/(60.*60.); multiplyC=1.; //multiplyT=1./60 for IV temperature curve, for old convertToTimeAxis function multiplyT=1./6
	//multiplyC=1E-12;



	if(IVconfigID.Contains("CV")){
		TString graphNameArr[] = {"Cap"}; // if always the same, put just one element
		graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
	}else{
		//TString graphNameArr[] = {"Chuck","","","","","",""}; // if always the same, put just one element
		//TString graphNameArr[] = {"","","","","","_X1_Y1"}; // if always the same, put just one element
		//TString graphNameArr[] = {"_X1_Y1"}; // if always the same, put just one element
		TString graphNameArr[] = {""}; // if always the same, put just one element
		graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
	}

	specifier = IVconfigID;




	if(IVconfigID=="ITkPixV1_BM1_IV"){
		TString fileNameArr[] = { // file in which to find graph
			"V1_BM1_IV_1.txt",
			"V1_BM1_IV_2.txt"
		};
		fileExistsOrNot("V1_BM1_IV_1.txt");
		fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
		TString legNameArr[] = { // corresponding legend name
			"light off",
			"light on"
		};
		legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
	}


	if(IVconfigID=="ITkPixV1_BM1_CV"){
		TString fileNameArr[] = {
			"V1_BM1_CV_1.txt",
			"V1_BM1_CV_3.txt",
			"V1_BM1_CV_4.txt"
		};
		fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
		TString legNameArr[] = {
			"1kHz Veff=1V",
			"1kHz Veff=1.5V",
			"10kHz"
		};
		legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
	}


	if(IVconfigID=="ITkPixV1_3BM_IV"){
		TString fileNameArr[] = { // file in which to find graph
			"V1_BM1_IV_1.txt",
			"V1_BM1_IV_2.txt",
			"V1_BM2_IV_1.txt",
			"V1_BM2_IV_2.txt",
			"V1_BM3_IV_1.txt",
			"V1_BM3_IV_2.txt"
		};
		fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
		TString legNameArr[] = { // corresponding legend name
			"V1_BM1 light off",
			"V1_BM1 light on",
			"V1_BM2 light off",
			"V1_BM2 light on",
			"V1_BM3 light off",
			"V1_BM3 light on"
		};
		legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
	}


	if(IVconfigID=="HPKinKindW5P1_CV"){
		TString fileNameArr[] = {
			"HPKinKindW5P1D1_CV_1.txt",
			"HPKinKindW5P1D2_CV_2.txt",
			"HPKinKindW5P1D3_CV_1.txt"
		};
		fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
		TString legNameArr[] = {
			"Diode 1",
			"Diode 2",
			"Diode 3"
		};
		legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
	}


	breakDownCurrent = 10e-6; // criterion to calculate VBD (threshold current [A])
	fixedVoltageForCurrentEval = 50.; // voltage [V] at which to evaluate the current
	fractionOfVBDForCurrentEval = 0.8; // fraction of breakdown voltage at which to evaluate the current
	NstepsX = 1; // in case of an array
	NstepsY = 1; // in case of an array


	//inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/ITkPixBareModules/";
	//outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITkPixBareModules/";
	inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/ITkPixPreProduction/";
	outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITkPixPreProduction/";

	Vlo=0; Vhi=200; NbinsV=200;
	// Vlo=0.000; Vhi=55.; NbinsV=100; //RHI
	// VloLGAD=0; VhiLGAD=200;
	Ilo=0.0; Ihi=2; // for IV
	//Ilo=0.0; Ihi=0.1; // for IV
	//Ilo=0.0; Ihi=0.02; // for IV zoom
	//Ilo=0.0; Ihi=0.6; // for 2310004
	//Ilo=19.0; Ihi=21; // for T
	//Ilo=0.0; Ihi=55.; // for RH
	//Ilo=0.0; Ihi=0.016; // for RHI

	IloZoom=0; IhiZoom=100e-9; NbinsI=15;
	//Clo=-0.1; Chi=1.e-1; C2hi=0.6e-4; 
	//Clo=0; Chi=2000.e1; C2hi=2.0e-6;
	Clo=0; Chi=30.e1; C2hi=3.0e-3;

	// Colours and markers
	// initialise arrays to 0 if default should be used

	// RD53A 2021-3 plots
	//int colorArr[1000] ={1,2,8,4,5,6,7,9}; 
	//int markerArr[1000]={20,21,22,23,20,21,22,23,33,29}//standard

	// RD53A 2021-10 CV plots
	//int colorArr[] ={1, 2, 3, 4, 5, 6, 7, kOrange-3, kViolet+1, kSpring-7};
	//int markerArr[]={20, 21, 22, 23, 29, 33, 34, 39, 41, 43, 45, 47, 48, 49, 24, 25, 26, 27, 28, 30, 31, 32};

	// RD53A 2021-11 plots
	int colorArr[] ={kBlack, kBlue, kRed, kGreen, kYellow, kMagenta, kCyan, 12, kOrange-3, kSpring-6, kAzure-6, kViolet+1};
	int markerArr[]={20, 21, 22, 23, 20, 21, 22, 23, 33, 34, 29, 43,    24, 25, 26, 32, 27, 28, 30, 42};

	// Basically same as above but for light on/off comparison
	//int colorArr[] ={kBlack, kBlack, kBlue, kBlue, kRed, kRed, kGreen, kGreen, kYellow, kYellow, kMagenta, kMagenta, kCyan, kCyan, 12, 12, kOrange-3, kOrange-3, kSpring-6, kSpring-6, kAzure-6, kAzure-6, kViolet+1, kViolet+1};
	//int markerArr[]={20, 24, 21, 25, 22, 26, 23, 32, 29, 30, 34, 28};

	//int markerArr[1000]    ={20,24,21,25,23,22,26,23,32,33,27}; //{0}
	//IV color scheme
	//int colorArr[1000]     ={4,64,67,38,9,2,95,92,11,12,39,28};//,9,9,11,11,12,12,28,28,38,38,39,39}; //T studies
	//int colorArr[1000]     ={4,64,67,69,2,95,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //all irradiated
	// int colorArr[1000]     ={4,64,2,95,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //all irradiated
	//int colorArr[1000]     ={92,91,67,69,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //sepearate
	//int colorArr[1000]     ={4,64,2,95,4,64,2,95};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3
	//int colorArr[1000]     ={67,69,92,91,4,64,2,95,67,92};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3 and F4
	//int colorArr[1000]     ={67,69,4,1,33};//64,109};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3 and F4 2e15
	//int colorArr[1000]     ={92,91,2,41,43}; //,95,94};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3 and F4 5E15

	//int colorArr[1000]     ={67,69,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //150
	//int markerArr[1000]    ={20,20,24,21,22,26,23,32,29}; //150
	//int markerArr[1000]    ={20,21,22,23,20,21,22,23,33,29}; //standard
	//int markerArr[1000]    ={20,21,22,23,29,3320,21,22,23,33,29}; //standard
	//int markerArr[1000]    ={20,21,22,23,33,29,34,43,45,47,41}; // comparison F3 and F4
	//int markerArr[1000]    ={20,21,22,23,33,29}; // comparison F3 and F4
//	int markerArr[1000]    ={20,24,21,25,22,23,26}; //for light vs no light study
	//int colorArr[1000]     ={8,8,1,1,2,4,4,39,8,9,11,12,28,38,39}; // for light vs no light study

	int lineStyleArr[1000] ={0};
	//do not change last lines:
	std::copy(colorArr, colorArr+1000, color_);
	std::copy(markerArr, markerArr+1000, marker_);
	std::copy(lineStyleArr, lineStyleArr+1000, lineStyle_);


}
#endif

