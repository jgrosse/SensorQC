# HPK single pads
#######################
#root -l -b -q analyseIV.C+'("HPK_W9_SinglePads_Pad")'
#root -l -b -q analyseIV.C+'("HPK_W9_SinglePads_PadGR")'
#root -l -b -q analyseIV.C+'("HPK_W8_SinglePads")'
#root -l -b -q analyseIV.C+'("HPK_W18_SinglePads")'

# HPK arrays
#######################
#enter file name without "IV_" and "_AllXYscan.root"
#root -l -b -q analyseIV.C+'("W2-LG15x15-SE5-IP9-P3_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W8-LG15x15-SE5-IP9-P3_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W8-LG15x15-SE5-IP9-P6_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W10-LG15x15-SE5-IP9-P3_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W8-LG5x5-SE5-IP9-P3_T20C_Phi0")' 
#root -l -b -q analyseIV.C+'("W2-LG5x5-SE5-IP9-P3_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W10-PIN5x5-SE5-IP9-P1_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W10-LG5x5-SE5-IP9-P3_T20C_Phi0")' 
#root -l -b -q analyseIV.C+'("W10-LG5x5-SE5-IP9-P5_T20C_Phi0")' 
#root -l -b -q analyseIV.C+'("W18-LG15x15-SE5-IP9-P1_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W18-LG15x15-SE5-IP9-P2_T20C_Phi0_2nd")'

#root -l -b -q analyseIV.C+'("28995-W7-LG5x5-SE5-IP9-P4_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("28995-W4-LG5x5-SE5-IP9-P4_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("28995-W2-LG5x5-SE5-IP9-P4_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("28995-W3-LG5x5-SE5-IP9-P4_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("28995-W1-LG5x5-SE5-IP9-P5_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("28995-W1-LG15x15-SE5-IP9-P3_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("28995-W17-LG5x5-SE5-IP9-P5_T20C_Phi0")'


#HPK corner
########################
#root -l -b -q analyseIV.C+'("HPK_W8_corner_Pad1")'
#root -l -b -q analyseIV.C+'("HPK_W8_corner_Pad2")'
#root -l -b -q analyseIV.C+'("HPK_W8_corner_Chuck")'
#root -l -b -q analyseIV.C+'("HPK_W8-LG15x15-SE5-IP9-P6_corner_Pad1")'
#root -l -b -q analyseIV.C+'("HPK_W8-LG15x15-SE5-IP9-P6_corner_Pad2")'
#root -l -b -q analyseIV.C+'("HPK_W8-LG15x15-SE5-IP9-P6_corner_Chuck")'
#root -l -b -q analyseIV.C+'("HPK_W18_corner")'
#root -l -b -q analyseIV.C+'("HPK_W18-LG15x15-SE5-IP9-P2_corner")'



# CNM AIDA single pads
#######################
#root -l -b -q analyseIV.C+'("CNM_AIDA_single_W3")'
#root -l -b -q analyseIV.C+'("CNM_AIDA_single_W5")'
#root -l -b -q analyseIV.C+'("CNM_AIDA_single_W7")'
#root -l -b -q analyseIV.C+'("CNM_AIDA_single_W9")'
#root -l -b -q analyseIV.C+'("CNM_AIDA_single_W11")'
#root -l -b -q analyseIV.C+'("CNM_AIDA_single_W14")'

# CNM AIDA arrays
#######################

#root -l -b -q analyseIV.C+'("W11-A210_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W11-A211_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W11-A214_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W11-A227_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W11-A228_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W11-A230_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W11-A244_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W11-A245_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W11-A246_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W5-A201_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W5-A202_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W5-A203_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W5-A217_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W5-A218_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W5-A219_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W5-A235_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W5-A236_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W5-A237_T20C_Phi0")'


#root -l -b -q analyseIV.C+'("W3-A509_T20C_Phi0")' 
#root -l -b -q analyseIV.C+'("W3-A517_T20C_Phi0")' 
#root -l -b -q analyseIV.C+'("W5-A501_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W5-A509_T20C_Phi0")' 
#root -l -b -q analyseIV.C+'("W5-A517_T20C_Phi0")' 
#root -l -b -q analyseIV.C+'("W7-A501_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W7-A509_T20C_Phi0")' 
#root -l -b -q analyseIV.C+'("W7-A517_T20C_Phi0")' 
#root -l -b -q analyseIV.C+'("W9-A501_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W9-A509_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W9-A517_T20C_Phi0")' 
#root -l -b -q analyseIV.C+'("W11-A501_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W11-A509_T20C_Phi0")' 
#root -l -b -q analyseIV.C+'("W11-A517_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W14-A501_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W14-A509_T20C_Phi0")'
#root -l -b -q analyseIV.C+'("W14-A517_T20C_Phi0")'
