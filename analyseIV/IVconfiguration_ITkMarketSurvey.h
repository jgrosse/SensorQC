#ifndef IVconfiguration_ITkMarketSurvey_h
#define IVconfiguration_ITkMarketSurvey_h

#include "basicFunctions.h"

// This header file contains the configuration for the analysis of a set of IV curves

int IVconfiguration( TString IVconfigID, double &multiplyT, double &multiplyV, double &multiplyI, double &multiplyC, double &breakDownCurrent, double &fixedVoltageForCurrentEval, double &fractionOfVBDForCurrentEval, 
                     int &NstepsX, int &NstepsY, TString &specifier, TString &inputFileNameBase, TString &outputFileNameBase, 
                     vector<TString> &fileName_, vector<TString> &legName_, vector<TString> &graphName_,
                     double &Vlo, double &Vhi, int &NbinsV, double &VloLGAD, double &VhiLGAD, double &Ilo, double &Ihi, int &NbinsI, double &IloZoom, double &IhiZoom, double &Clo, double &Chi, double &C2hi)
{
  
  multiplyV=-1; multiplyI=-1; // mirror V and I to have positive axis
  multiplyT=1/(60.*60.); multiplyC=1.; //multiplyT=1./60 for IV temperature curve, for old convertToTimeAxis function multiplyT=1./6
  //multiplyC=1E-12;
  
  
  

  ////////////////////////////////////////////
  if(IVconfigID.Contains("LFoundry")){
    ////////////////////////////////////////////
    
    if(IVconfigID.Contains("CV")){
      TString graphNameArr[] = {"Cap"}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    else{
      //TString graphNameArr[] = {"Chuck","","","","","",""}; // if always the same, put just one element
      TString graphNameArr[] = {"bias","","","","_X1_Y1"}; // if always the same, put just one element
      //TString graphNameArr[] = {"_X1_Y1"}; // if always the same, put just one element     
      //TString graphNameArr[] = {""}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    
    specifier = IVconfigID;



    if(IVconfigID=="LFoundry CV"){
      TString fileNameArr[]    = { // file in which to find graph
	"2_CV_1.txt",
	"2_CV_2.txt",
	//"3_IV_2.txt",
	//"4_IV_9.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"DC 25x100",  
	"DC 50x50", 
	"DC 50x50 2",  
	//"SC 50x50",
	//"SC 25x100",  
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
 }

  
    if(IVconfigID=="LFoundry SC (4) CV"){
      TString fileNameArr[]    = { // file in which to find graph
	"4_CV_18.txt",
	"4_CV_20.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"SC No.4",
	"SC No.4 long",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    if(IVconfigID=="LFoundry"){
      TString fileNameArr[]    = { // file in which to find graph
	//"1_IV_18.txt",
	//"1_IV_18_mod.txt",
	//"2_IV_6.txt",
	//"2_IV_6_mod.txt",
	//"2_IV_7.txt",
	//"2_IV_8.txt",
	//"3_IV_2.txt",
	//"4_IV_12.txt",
	//"4_IV_12_mod.txt",
	//"4_IV_17.txt",
	//"4_IV_17_mod.txt",
	"4_IV_20.txt",
	"4_IV_20_mod.txt",
	"4_IV_10.txt", 
	"4_IV_16.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"DC 25x100 bias+nwell",  
	//"DC 25x100 bias add",
	//"DC 50x50 bias+nwell",  
	//"DC 50x50 bias add",  
	//"SC 50x50",
	"SC 25x100 bias+nwell",  
	"SC 25x100 bias add",  
	"SC 25x100 bias+nwell 1h@300#circC ",  
	"SC 25x100 bias 1h@300#circC add",  
	"SC 25x100 bias+nwell 1h@300#circC", 
	"SC 25x100 bias 1h@300#circC add",
 	"SC 25x100 bias",
	"SC 25x100 bias+nwell 1h@300#circC", 
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry_test"){
      TString fileNameArr[]    = { // file in which to find graph
	"1_IV_18.root",
	"2_IV_6.root",
	"1_IV_18.root",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"DC 25x100 bias+nwell",  
	"DC 25x100 bias",
	"DC 50x50 bias+nwell",  
	"DC 50x50 bias",  
	//"SC 50x50",
	"SC 25x100 bias+nwell",  
	"SC 25x100 bias",  
	"SC 25x100 bias+nwell",  
	"SC 25x100 bias",  
	"SC 25x100 bias+nwell", 
	"SC 25x100 bias",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

 if(IVconfigID=="LFoundry stability measurement @100V"){
      TString fileNameArr[]    = { // file in which to find graph
	"11_1_2_It_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"11_1_2",  
 
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry TS MOS CIRCULAR"){
      TString fileNameArr[]    = { // file in which to find graph
	"9_TS_MOS_CIRCULAR_IV_2.txt",
	"9_TS_MOS_CIRCULAR_IV_3.txt",
	"9_TS_MOS_CIRCULAR_IV_4.txt",
	"9_TS_MOS_CIRCULAR_IV_6.txt",
	"9_TS_MOS_CIRCULAR_IV_7.txt",
	"9_TS_MOS_CIRCULAR_IV_8.txt",
	"9_TS_MOS_CIRCULAR_IV_10.txt",
	"9_TS_MOS_CIRCULAR_IV_11.txt",
	"9_TS_MOS_CIRCULAR_IV_12.txt",
	"9_TS_MOS_CIRCULAR_IV_13.txt",	
	"9_TS_MOS_CIRCULAR_IV_14.txt",
	"9_TS_MOS_CIRCULAR_IV_15.txt",
	"10_TS_MOS_CIRCULAR_IV_1.txt",
	"10_TS_MOS_CIRCULAR_IV_2.txt",
	"10_TS_MOS_CIRCULAR_IV_3.txt",
	"10_TS_MOS_CIRCULAR_IV_4.txt",	
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"9Pad 2",  
	"9Pad 3",
	"9Pad 4",  
	"9Pad 6",
	"9Pad 7",  
	"9Pad 8",
	"9Pad 10",  
	"9Pad 11",
	"9Pad 12",  
	"9Pad 14",  
	"9Pad 15",
	"9Pad 16",   
	"10Pad 2",  
	"10Pad 6",
	"10Pad 10",  
	"10Pad 14",
	
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry TS_LARGE_PIX"){
      TString fileNameArr[]    = { // file in which to find graph
	"9_TS_LARGE_PIX_1_IV_1.txt",
	"9_TS_LARGE_PIX_1_IV_2.txt",
	"9_TS_LARGE_PIX_2_IV_1.txt",
	"9_TS_LARGE_PIX_2_IV_3.txt",
	"9_TS_LARGE_PIX_3_IV_1.txt",
	"9_TS_LARGE_PIX_3_IV_2.txt",
	"9_TS_LARGE_PIX_4_IV_3.txt",
	"9_TS_LARGE_PIX_4_IV_4.txt",
	"10_TS_LARGE_PIX_IV_2.txt",
	"10_TS_LARGE_PIX_IV_3.txt",
	"10_TS_LARGE_PIX_IV_5.txt",	
	"10_TS_LARGE_PIX_2_IV_1.txt",
	"10_TS_LARGE_PIX_2_IV_2.txt",
	"10_TS_LARGE_PIX_2_IV_3.txt",	
	"10_TS_LARGE_PIX_3_IV_1.txt",
	"10_TS_LARGE_PIX_3_IV_2.txt",	
	"10_TS_LARGE_PIX_4_IV_1.txt",
	"10_TS_LARGE_PIX_4_IV_2.txt",	
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"9_1 Pad 1",  
	"9_1 Pad 5",
	"9_2 Pad 1",  
	"9_2 Pad 5",
	"9_3 Pad 2",  
	"9_3 Pad 3",
	"9_4 Pad 2",  
	"9_4 Pad 3",
	"10_1 Pad 1",  
	"10_1 Pad 3",
	"10_1 Pad 5",  
	"10_2 Pad 1",  
	"10_2 Pad 3",
	"10_2 Pad 5", 
	"10_3 Pad 2",  
	"10_3 Pad 3",
	"10_4 Pad 2", 
	"10_4 Pad 3",  
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


if(IVconfigID=="LFoundry TS_F and TS_F_NGR"){
      TString fileNameArr[]    = { // file in which to find graph
	"9_TS_F_IV_1.txt",
	"9_TS_F_IV_2.txt",
	"9_TS_F_IV_3.txt",
	"9_TS_F_NGR_IV_2.txt",
	"9_TS_F_NGR_IV_3.txt",
	"10_TS_F_IV_1.txt",
	"10_TS_F_IV_2.txt",
	"10_TS_F_IV_3.txt",
	"10_TS_F_NGR_IV_1.txt",
	"10_TS_F_NGR_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"9TS_F 1",  
	"9TS_F 2",
	"9TS_F 3",  
	"9TS_F_NGR 2",
	"9TS_F_NGR 3",  
	"10TS_F 1",  
	"10TS_F 2",
	"10TS_F 3",  
	"10TS_F_NGR 2",
	"10TS_F_NGR 3",  
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


if(IVconfigID=="LFoundry TS_DIODE"){
      TString fileNameArr[]    = { // file in which to find graph
	"9_TS_DIODE_IV_1.txt",
	"9_TS_DIODE_IV_3.txt",
	"9_TS_DIODE_2_IV_1.txt",
	"9_TS_DIODE_2_IV_2.txt",
	"9_TS_DIODE_3_IV_4.txt",
	"9_TS_DIODE_3_IV_6.txt",
	"10_TS_DIODE_IV_1.txt",
	"10_TS_DIODE_IV_2.txt",
	"10_TS_DIODE_2_IV_1.txt",
	"10_TS_DIODE_2_IV_2.txt",
	"10_TS_DIODE_3_IV_1.txt",
	"10_TS_DIODE_3_IV_2.txt"      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"9TS_DIODE 1",  
	"9TS_DIODE 3",
	"9TS_DIODE_2 1",  
	"9TS_DIODE_2 3",
	"9TS_DIODE_3 4",  
	"9TS_DIODE_3 6",
	"10TS_DIODE 1",  
	"10TS_DIODE 3",
	"10TS_DIODE_2 1",  
	"10TS_DIODE_2 3",
	"10TS_DIODE_3 4",  
	"10TS_DIODE_3 6",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry TS_RES"){
      TString fileNameArr[]    = { // file in which to find graph
	"9_TS_RES_IV_1.txt",
	"9_TS_RES_IV_3.txt",
	"9_TS_RES_IV_4.txt",
	"9_TS_RES_IV_5.txt",
	"9_TS_RES_2_IV_1.txt",
	"9_TS_RES_2_IV_2.txt",
	"9_TS_RES_2_IV_3.txt",
	"9_TS_RES_2_IV_4.txt",
	"10_TS_RES_IV_1.txt",
	"10_TS_RES_IV_3.txt",
	"10_TS_RES_IV_4.txt",
	"10_TS_RES_IV_5.txt",
	"10_TS_RES_2_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"9TS_RES 1",  
	"9TS_RES 3",
	"9TS_RES 4",  
	"9TS_RES 5",
	"9TS_25_RES 1",  
	"9TS_25_RES 3",
	"9TS_25_RES 4",  
	"9TS_25_RES 5",
	"10TS_RES 1",  
	"10TS_RES 3",
	"10TS_RES 4",  
	"10TS_RES 5",
	"10TS_25_RES 1", 
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

  if(IVconfigID=="LFoundry Teststructures"){
      TString fileNameArr[]    = { // file in which to find graph
	"11_1_1_IV_1.txt",
	"11_1_2_IV_1.txt",
	//"11_1_3_IV_1.txt",
	//"11_1_4_IV_1.txt",
	//"11_1_5_IV_1.txt",
	"11_2_1_IV_1.txt",
	"11_2_2_IV_1.txt",
	//"11_2_4_IV_1.txt",
	//"11_2_5_IV_1.txt",
	"11_3_1_IV_1.txt",
	"11_3_2_IV_1.txt",
	//"11_3_4_IV_1.txt",
	//"11_3_5_IV_1.txt",
	"11_4_1_IV_1.txt",
	"11_4_2_IV_1.txt",
	"11_1_1_IV_2.txt",
	"11_1_2_IV_2.txt",
	"11_2_1_IV_2.txt",
	"11_2_2_IV_2.txt",
	"11_3_1_IV_2.txt",
	"11_3_2_IV_2.txt",
	"11_4_1_IV_2.txt",
	"11_4_2_IV_2.txt",
	"12_1_1_IV_1.txt",
	"12_1_2_IV_1.txt",
	"12_2_1_IV_1.txt",
	"12_2_2_IV_1.txt",
	"12_3_1_IV_1.txt",
	"12_3_2_IV_1.txt",
	"12_4_1_IV_1.txt",
	"12_4_2_IV_1.txt",
	"1_IV_12.txt",
	"1_IV_19.txt",
	"2_IV_1.txt",
	"3_IV_2.txt",
	"4_IV_9.txt",
	"4_IV_10.txt",
	"9_4_4_IV_1.txt",
	"9_4_5_IV_1.txt",
	"10_4_4_IV_1.txt",
	"10_4_5_IV_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"11_1_1", 
	"11_1_2",
	//"11_1_3",
	//"11_1_4",
	//"11_1_5",
	"11_2_1",
	"11_2_2",
	//"11_2_4",
	//"11_2_5",
	"11_3_1",
	"11_3_2",
	//"11_3_4",
	//"11_3_5", 
	"11_4_1",
	"11_4_2",
	"11_1_1 1h@300", 
	"11_1_2 1h@300",
	"11_2_1 1h@300",
	"11_2_2 1h@300",
	"11_3_1 1h@300",
	"11_3_2 1h@300",
	"11_4_1 1h@300",
	"11_4_2 1h@300",
	"12_1_1", 
	"12_1_2",
	"12_2_1",
	"12_2_2",
	"12_3_1",
	"12_3_2",
	"12_4_1",
	"12_4_2",
	"DC 25x100",  
	"DC 25x100 1h@300",  
	"DC 50x50",  
	"SC 50x50",
	"SC 25x100", 
	"SC 25x100 1h@300", 
	"9_4_4",
	"9_4_5",
	"10_4_4",
	"10_4_5",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry_11_1"){
      TString fileNameArr[]    = { // file in which to find graph
	//"11_1_1_IV_1.txt",
	"11_1_2_IV_1.txt",
	//"11_1_1_IV_2.txt",
	"11_1_2_IV_2.txt",
	//"11_1_1_IV_3.txt",
	"11_1_2_IV_3.txt",
	//"11_1_1_IV_4.txt",
	"11_1_2_IV_4.txt",
	//"11_1_1_IV_5.txt",
	"11_1_2_IV_5.txt",
	"11_1_2_IV_6.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"11_1_1", 
	"no annealing",
	//"11_1_1 1h@300", 
	"1h@300C",
	//"11_1_1 1h@300 5d",
	"1h@300C+5d@20C",
	//"11_1_1 1h@300 5.5d",
	"1h@300C+5.5d@20C",
	//"11_1_1 1h@300 5.5 -25#circC",
	"1h@300C+5.5d@20C -25#circC",
	"1h@300C+12d@20C",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry_11_2"){
      TString fileNameArr[]    = { // file in which to find graph
	//"11_2_1_IV_1.txt",
	"11_2_2_IV_1.txt",
	//"11_2_1_IV_2.txt",
	"11_2_2_IV_2.txt",
	//"11_2_1_IV_3.txt",
	"11_2_2_IV_3.txt",
	//"11_2_1_IV_4.txt",
	"11_2_2_IV_4.txt",
	//"11_2_1_IV_5.txt",
	"11_2_2_IV_5.txt",
	"11_2_2_IV_6.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
		//"11_2_1", 
	"no annealing",
	//"11_2_1 1h@300", 
	"1h@300C",
	//"11_2_1 1h@300 5d",
	"1h@300C+5d@20C",
	//"11_2_1 1h@300 5.5d",
	"1h@300C+5.5d@20C",
	//"11_2_1 1h@300 5.5 -25#circC",
	"1h@300C+5.5d@20C -25#circC",
	"1h@300C+12d@20C",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry_11_3"){
      TString fileNameArr[]    = { // file in which to find graph
	//"11_3_1_IV_1.txt",
	"11_3_2_IV_1.txt",
	//"11_3_1_IV_2.txt",
	"11_3_2_IV_2.txt",
	//"11_3_1_IV_3.txt",
	"11_3_2_IV_3.txt",
	//"11_3_1_IV_4.txt",
	"11_3_2_IV_4.txt",
	//"11_3_1_IV_5.txt",
	"11_3_2_IV_5.txt",
	"11_3_2_IV_6.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"11_3_1", 
	"no annealing",
	//"11_3_1 1h@300", 
	"1h@300C",
	//"11_3_1 1h@300 5d",
	"1h@300C+5d@20C",
	//"11_3_1 1h@300 5.5d",
	"1h@300C+5.5d@20C",
	//"11_3_1 1h@300 5.5 -25#circC",
	"1h@300C+5.5d@20C -25#circC",
	"1h@300C+12d@20C",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry_11_4"){
      TString fileNameArr[]    = { // file in which to find graph
	//"11_4_1_IV_1.txt",
	"11_4_2_IV_1.txt",
	//"11_4_1_IV_2.txt",
	"11_4_2_IV_2.txt",
	//"11_4_1_IV_3.txt",
	"11_4_2_IV_3.txt",
	//"11_4_1_IV_4.txt",
	"11_4_2_IV_4.txt",
	//"11_4_1_IV_5.txt",
	"11_4_2_IV_5.txt",
	"11_4_2_IV_7.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"11_1_1", 
	"no annealing",
	//"11_1_1 1h@300", 
	"1h@300C",
	//"11_1_1 1h@300 5d",
	"1h@300C+5d@20C",
	//"11_1_1 1h@300 5.5d",
	"1h@300C+5.5d@20C",
	//"11_1_1 1h@300 5.5 -25#circC",
	"1h@300C+5.5d@20C -25#circC",
	"1h@300C+12d@20C",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry_12"){
      TString fileNameArr[]    = { // file in which to find graph
	"12_1_1_IV_2.txt",
	"12_1_2_IV_2.txt",
	"12_2_1_IV_2.txt",
	"12_2_2_IV_2.txt",
	"12_3_1_IV_1.txt",
	"12_3_2_IV_1.txt",
	"12_4_1_IV_1.txt",
	"12_4_2_IV_1.txt",
	/*"12_1_1_IV_3.txt",
	"12_1_2_IV_3.txt",
	"12_2_1_IV_3.txt",
	"12_2_2_IV_3.txt",
	"12_3_1_IV_3.txt",
	"12_3_2_IV_3.txt",
	"12_4_1_IV_3.txt",
	"12_4_2_IV_3.txt",*/
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"12_1_1", 
	"12_1_2",
	"12_2_1",
	"12_2_2",
	"12_3_1",
	"12_3_2",
	"12_4_1",
	"12_4_2",
	/*"12_1_1 -25#circC", 
	"12_1_2 -25#circC",
	"12_2_1 -25#circC",
	"12_2_2 -25#circC",
	"12_3_1 -25#circC",
	"12_3_2 -25#circC",
	"12_4_1 -25#circC",
	"12_4_2 -25#circC",*/
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry_12_Tstudies"){
      TString fileNameArr[]    = { // file in which to find graph
	//"12_1_1_IV_2.txt",
	"12_1_2_IV_2.txt",
	//"12_2_1_IV_2.txt",
	"12_2_2_IV_2.txt",
	//"12_3_1_IV_1.txt",
	"12_3_2_IV_1.txt",
	//"12_4_1_IV_1.txt",
	"12_4_2_IV_1.txt",
	//"12_1_1_IV_3.txt",
	"12_1_2_IV_3.txt",
	//"12_2_1_IV_3.txt",
	"12_2_2_IV_3.txt",
	//"12_3_1_IV_3.txt",
	"12_3_2_IV_3.txt",
	//"12_4_1_IV_3.txt",
	"12_4_2_IV_3.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"12_1_1", 
	"12_1_2",
	//"12_2_1",
	"12_2_2",
	//"12_3_1",
	"12_3_2",
	//"12_4_1",
	"12_4_2",
	//"12_1_1 -25#circC", 
	"12_1_2 -25#circC",
	//"12_2_1 -25#circC",
	"12_2_2 -25#circC",
	//"12_3_1 -25#circC",
	"12_3_2 -25#circC",
	//"12_4_1 -25#circC",
	"12_4_2 -25#circC",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry_12_11_1and2"){
      TString fileNameArr[]    = { // file in which to find graph
	"12_1_1_IV_2.txt",
	"12_1_2_IV_2.txt",
	"12_2_1_IV_2.txt",
	"12_2_2_IV_2.txt",
	"11_1_1_IV_1.txt",
	"11_1_2_IV_1.txt",
	"11_2_1_IV_1.txt",
	"11_2_2_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"12_1_1", 
	"12_1_2",
	"12_2_1",
	"12_2_2",
	"11_1_1",
	"11_1_2",
	"11_2_1",
	"11_2_2",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry_12_11_3and4"){
      TString fileNameArr[]    = { // file in which to find graph
	"12_3_1_IV_1.txt",
	"12_3_2_IV_1.txt",
	"12_4_1_IV_1.txt",
	"12_4_2_IV_1.txt",
	"11_3_1_IV_1.txt",
	"11_3_2_IV_1.txt",
	"11_4_1_IV_1.txt",
	"11_4_2_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"12_3_1", 
	"12_3_2",
	"12_4_1",
	"12_4_2",
	"11_3_1",
	"11_3_2",
	"11_4_1",
	"11_4_2",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry_12_11(3_4)_9_10"){
      TString fileNameArr[]    = { // file in which to find graph
	"12_3_1_IV_1.txt",
	"12_3_2_IV_1.txt",
	"12_4_1_IV_1.txt",
	"12_4_2_IV_1.txt",
	"11_3_1_IV_1.txt",
	"11_3_2_IV_1.txt",
	"11_4_1_IV_1.txt",
	"11_4_2_IV_1.txt",
     	"9_4_4_IV_1.txt",
	"9_4_5_IV_1.txt",
	"10_4_4_IV_1.txt",
	"10_4_5_IV_2.txt"
 };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"12_3_1", 
	"12_3_2",
	"12_4_1",
	"12_4_2",
	"11_3_1",
	"11_3_2",
	"11_4_1",
	"11_4_2",
	"9_4_4",
	"9_4_5",
	"10_4_4",
	"10_4_5",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry_12_11(1_2)_9_10"){
      TString fileNameArr[]    = { // file in which to find graph
	"12_1_1_IV_1.txt",
	"12_1_2_IV_1.txt",
	"12_2_1_IV_1.txt",
	"12_2_2_IV_1.txt",
	"11_1_1_IV_1.txt",
	"11_1_2_IV_1.txt",
	"11_2_1_IV_1.txt",
	"11_2_2_IV_1.txt",
     	"9_4_4_IV_1.txt",
	"9_4_5_IV_1.txt",
	"10_4_4_IV_1.txt",
	"10_4_5_IV_2.txt"
 };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"12_1_1", 
	"12_1_2",
	"12_2_1",
	"12_2_2",
	"11_1_1",
	"11_1_2",
	"11_2_1",
	"11_2_2",
	"9_4_4",
	"9_4_5",
	"10_4_4",
	"10_4_5",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


if(IVconfigID=="LFoundry_strips"){
      TString fileNameArr[]    = { // file in which to find graph
	//"7_IV_1.txt",
	"7_IV_2.txt",
	"7_IV_3.txt",
	"7_IV_4.txt",
	"7_IV_5.txt",
	"7_IV_6.txt",
	//"7_IV_7.txt",
	"7_IV_8.txt",
	"7_IV_9.txt",
	"7_IV_10.txt",
	"8_IV_1.txt",
	"5_IV_1.txt",
	"6_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"7_LStrip 1",  
	"7_LStrip 1",  
	"7_LStrip 3",
	"7_LStrip 5",  
	"7_LStrip n-1",
	"7_LStrip n",
	//"7_RStrip 1", 
	"7_RStrip 1",  
	"7_RStrip 3",
	"7_RStrip n", 
	"8_LStrip 1", 
	"5_RStrip 1", 
	"6_RStrip 1", 
     };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

   if(IVconfigID=="LFoundry DC 25x100#mum^{2}"){
      TString fileNameArr[]    = { // file in which to find graph
	"1_IV_5.txt",
	//"1_IV_7.txt",
	"1_IV_8.txt",
	//"1_IV_9.txt",
	//"1_IV_10.txt",
	//"1_IV_11.txt",
	//"1_IV_12.txt",
	"1_IV_13.txt",
	//"1_IV_14.txt",
	//"1_IV_15.txt",
	//"1_IV_16.txt",
	"1_IV_17.txt",
	"1_IV_19.txt",
	//"1_IV_20.txt",
	"1_IV_21.txt",
	//"1_IV_22.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"bias",  
	//"bias2ndR",
	"nwell",
	//"bias3rdR",
	//"bias4thR",
	//"nwell5thR",
	//"biasTR 2nd",  
	"bias (HV pad)",
	//"biasTR nwell2nd",
	//"biasTR nwell2nd 2",
	//"biasTR nwell2nd 3",
	"bias+nwell",
	"bias 1h@300#circC",
	//"after annealing 2",
	"nwell 1h@300#circC",
	//"after annealing 2nd bias pad",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="LFoundry DC 25x100#mum^{2} bias separately"){
      TString fileNameArr[]    = { // file in which to find graph
	"1_IV_13.txt",
	"1_IV_17.txt",
	"1_IV_18.txt",
	"1_IV_18.txt",
	"2_IV_6.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"HV on needle",
	"bias and nwell",
	"both",
	"bias",
	"both"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


   if(IVconfigID=="LFoundry SC 25x100#mum^{2}"){
      TString fileNameArr[]    = { // file in which to find graph
	//"4_IV_2.txt",
	//"4_IV_3.txt",
	//"4_IV_4.txt",
	"4_IV_5.txt",
	"4_IV_6.txt",
	//"4_IV_7.txt",
	//"4_IV_8.txt", 
	"4_IV_9.txt", 
	//"4_IV_12.txt", 
	//"4_IV_13.txt", 
 	"4_IV_10.txt", 
	"4_IV_11.txt", 
	"4_IV_14.txt", 
	"4_IV_16.txt", 
	"4_IV_18.txt", 
	"4_IV_19.txt", 
	//"4_IV_20.txt",
	//"4_IV_20_mod.txt",
	"4_IV_21.txt", 
	"4_IV_22.txt", 
     };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"bias",  
	//"bias2ndR",
	"nwell",
	//"biasTR meas2",  
	//"biasTR meas3",  
	"bias (HV pad)",  
	//"biasTR HV on needle 2",  
 	//"biasTR HV on needle 3",  
	//"separately bias",
	//"postCV",
 	"bias 1h@300#circC",  
	"nwell 1h@300#circC",
   	"bias 1h@300#circC (HV pad)",
 	"bias+nwell 1h@300#circC", 
	"bias 1h@300#circC+5d@20#circC",  
	"nwell 1h@300#circC+5d@20#circC",
   	//"bias 1h@300#circC+5d@20#circC (nwell connected)",
 	//"bias+nwell 1h@300#circC+5d@20#circC (sep bias read)", 
	"bias 1h@300#circC+5.5d@20#circC",  
	"bias 1h@300#circC+5.5d@20#circC -25#circC",
 
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


   if(IVconfigID=="LFoundry Fixing CV 1, 68pF"){
      TString fileNameArr[]    = { // file in which to find graph
	"68pF_CV_6.txt", 
     };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
 	"68pF", 
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


    breakDownCurrent = 10e-6; // criterion to calculate VBD (threshold current [A])
    fixedVoltageForCurrentEval = 50.; // voltage [V] at which to evaluate the current
    fractionOfVBDForCurrentEval = 0.8; // fraction of breakdown voltage at which to evaluate the current
    NstepsX = 1; // in case of an array
    NstepsY = 1; // in case of an array
    inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/LFoundry/";
    outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/LFoundry/";	
    Vlo=0; Vhi=100; NbinsV=100; //CV
    //Vlo=0; Vhi=450; NbinsV=500; //IV
    //Vlo=0; Vhi=70; NbinsV=250; //It
    // Vlo=0.000; Vhi=55.; NbinsV=100; //RHI
    VloLGAD=0; VhiLGAD=85;
    Ilo=0.01; Ihi=5000; // for IV
    //Ilo=0.5; Ihi=0.8; // for It
    //   Ilo=-25.6; Ihi=-24.95; // for T
    //   Ilo=0.0; Ihi=55.; // for RH
    //Ilo=0.0; Ihi=0.016; // for RHI    

    IloZoom=0; IhiZoom=10; NbinsI=200;
    Clo=0.; Chi=2e0; C2hi=0.6e-2; 
    // initialise arrays to 0 if default should be used
    //CV color scheme
    //int colorArr[1000]     ={2,2,4,4,8,8,1,1}; 
    //int markerArr[1000]    ={20,24,21,25,22,26,23,32,33,27}; //{0} filled, open
    //IV color scheme
    //int colorArr[1000]     ={2,2,2,4,4,4,8,8,8,1,1,1,1};//,9,9,11,11,12,12,28,28,38,38,39,39}; 
    //int markerArr[1000]    ={20,21,22,23,33,34,29,43,24,37,25,26,35,32,27,28,30,42,29,29,29,29,30}; //{0}
    //int markerArr[1000]    ={20,20,20,20,24,20,24,24,24,25,24,25,24,25,24,25,22,23,22,23,33,34,29,43,24,37,25,26,35,32,27,28,30,42,29,29,29,29,30}; //{0}
    //int markerArr[1000]    ={20,21,20,21,20,21,20,21,24,25,24,25,24,25,24,25,22,23,22,23,33,34,29,43,24,37,25,26,35,32,27,28,30,42,29,29,29,29,30}; //{0}
    int markerArr[1000]    ={20,21,23,24,25,32,26,29,33,30,27,22,34,24,25,26,32,27,28,29,29,29,29,30}; //{0} SC and DC
    //int markerArr[1000]    ={20,21,22,23,29,33,34,43,24,25,26,32,30,27,28,42,20,21,22,23,29,33,34,43,20,24,20,20,20,24,21,22,23,29,32,26,22,24,25,33,34,24,25,26,32,27,28,29,29,29,29,30}; //{0} Teststruc
    //int colorArr[1000]     ={12,12,14,14,16,16,20,20,12,12,14,14,16,16,20,20,30,30,31,31,34,34,29,29,1,1,2,4,8,8,45,45,49,49,9,12,28,29,1,2,4,8,12,28,29};  //Teststruc
    //int colorArr[1000]     ={14,8,14,8,14,8,14,8,14,8,2,4,95,1,2,4,8,14,14,46,46,46,61,61,30,30,39,8,9,11,12,28,38,39}; 
    //int colorArr[1000]     ={1,2,4,8,8,95,1,2,4,8,14,14,46,46,46,61,61,30,30,39,8,9,11,12,28,38,39}; 
    //int colorArr[1000]     ={2,4,1,11,8,9,12,28,38,3,5,7,6,10}; 
    int colorArr[1000]     ={2,1,2,2,1,2,4,2,1,2,2,4,2,2,11,8,9,12,28,38,3,5,7,6,10}; //SC and DC
    //int markerArr[1000]    ={20,21,22,33,23,24}; //{0}
   
    int lineStyleArr[1000] ={0};
    //do not change last lines:
    std::copy(colorArr, colorArr+1000, color_);
    std::copy(markerArr, markerArr+1000, marker_);
    std::copy(lineStyleArr, lineStyleArr+1000, lineStyle_);
  }

  
  


  
  ////////////////////////////////////////////
  if(IVconfigID.Contains("Foundry2")){
    ////////////////////////////////////////////
    
    if(IVconfigID.Contains("CV")){
      TString graphNameArr[] = {"Cap"}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    else{
      //TString graphNameArr[] = {"Chuck","","","","","",""}; // if always the same, put just one element
      //TString graphNameArr[] = {"","","","","","_X1_Y1"}; // if always the same, put just one element
      //TString graphNameArr[] = {"_X1_Y1"}; // if always the same, put just one element     
      TString graphNameArr[] = {""}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    
    specifier = IVconfigID;
    
    if(IVconfigID=="Foundry2 irradiated sensors"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W9-53SPA_IV_1.txt",
	"Foundry2/W12-53SPA_IV_1.txt",
	"Foundry2/W17-54SPA_IV_1.txt",
	"Foundry2/W15-14GPB_IV_1.txt",
	"Foundry2/W3-53SPA_IV_1.txt",
	"Foundry2/W8-53SPA_IV_1.txt",
	"Foundry2/W16-14SPA_IV_1.txt",
	"Foundry2/W16-54SPA_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"S1 2E15 100",
	"S2 2E15 100",
	"S1 2E15 150",
	"S2 2E15 150",
	"S1 5E15 100",
	"S2 5E15 100",
	"S1 5E15 150",
	"S2 5E15 150",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2,3 and 4 irradiated 150#mum sensors comparison"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W17-54SPA_IV_1.txt",
	"Foundry2/W15-14GPB_IV_1.txt",
	"Foundry2/W16-14SPA_IV_1.txt",
	"Foundry2/W16-54SPA_IV_1.txt",
	"Foundry4/SC/IrradiatedSensors/Modules/2E15leftMod_IV_1.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/2E15right_IV_5.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/5E15left_IV_4.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/5E15right_IV_5.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_2.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"F2 S1 2E15",
	"F2 S2 2E15",
	"F2 S1 5E15",
	"F2 S2 5E15",
	"F4 S1 2E15",
	"F4 S2 2E15",
	"F4 S1 5E15",
	"F4 S2 5E15",
	"F3 S1 2E15",
	"F3 S2 5E15",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2,3 and 4 irradiated 150#mum sensors comparison 5E15"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W16-14SPA_IV_1.txt",
	"Foundry2/W16-54SPA_IV_1.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_2.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/5E15left_IV_4.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/5E15right_IV_5.txt",

      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Foundry2 S1 ",
	"Foundry2 S2 ",
	"Foundry3 S2 ",
	"Foundry4 S1 ",
	"Foundry4 S2 ",

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2,3 and 4 irradiated 150#mum sensors comparison 2E15"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W17-54SPA_IV_1.txt",
	"Foundry2/W15-14GPB_IV_1.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_2.txt",
	"Foundry4/SC/IrradiatedSensors/Modules/2E15leftMod_IV_1.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/2E15right_IV_5.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Foundry2 S1",
	"Foundry2 S2",
	"Foundry3 S1",
	"Foundry4 S1",
	"Foundry4 S2",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2 and 3 irradiated 100#mum sensors comparison"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W9-53SPA_IV_1.txt",
	"Foundry2/W12-53SPA_IV_1.txt",
	"Foundry2/W3-53SPA_IV_1.txt",
	"Foundry2/W8-53SPA_IV_1.txt",
	"Foundry3/IrradiatedSensors/432-4_IV_2.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_2.txt",
	"Foundry3/IrradiatedSensors/434-4_IV_2.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_3.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"F2 S1 2E15",
	"F2 S2 2E15",
	"F2 S1 5E15",
	"F2 S2 5E15",
	"F3 S1 2E15",
	"F3 S2 2E15",
	"F3 S1 5E15",
	"F3 S2 5E15",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2 and 3 irradiated 100#mum sensors comparison 5E15"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W3-53SPA_IV_1.txt",
	"Foundry2/W8-53SPA_IV_1.txt",
	"Foundry3/IrradiatedSensors/434-4_IV_2.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_3.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Foundry2 S1 ",
	"Foundry2 S2 ",
	"Foundry3 S1 ",
	"Foundry3 S2 ",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2 and 3 irradiated 100#mum sensors comparison 2E15"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W9-53SPA_IV_1.txt",
	"Foundry2/W12-53SPA_IV_1.txt",
	"Foundry3/IrradiatedSensors/432-4_IV_2.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Foundry2 S1",
	"Foundry2 S2",
	"Foundry3 S1",
	"Foundry3 S2",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2 irradiated 100#mum sensors"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W9-53SPA_IV_1.txt",
	"Foundry2/W12-53SPA_IV_1.txt",
	"Foundry2/W3-53SPA_IV_1.txt",
	"Foundry2/W8-53SPA_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"W9-53SPA",
	"W12-53SPA",
	"W3-53SPA",
	"W8-53SPA",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2 irradiated 100#mum sensors 2E15"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W9-53SPA_IV_1.txt",
	"Foundry2/W12-53SPA_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"W9-53SPA",
	"W12-53SPA",

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
if(IVconfigID=="Foundry2 irradiated 100#mum sensors 5E15"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W3-53SPA_IV_1.txt",
	"Foundry2/W8-53SPA_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"W3-53SPA",
	"W8-53SPA",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2 irradiated 150#mum sensors"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W17-54SPA_IV_1.txt",
	"Foundry2/W15-14GPB_IV_1.txt",
	"Foundry2/W16-14SPA_IV_1.txt",
	"Foundry2/W16-54SPA_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"W17-54SPA",
	"W15-14GPB",
	"W16-14SPA",
	"W14-54SPA",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2 irradiated 150#mum sensors 2E15"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W17-54SPA_IV_1.txt",
	"Foundry2/W15-14GPB_IV_1.txt",
	"Foundry2/W15-14GPB_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"W17-54SPA",
	"W15-14GPB",
	"W15-14GPB (vac)",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2_W15-14GPB"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W15-14GPB_IV_1.txt",
	"Foundry2/W15-14GPB_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Vacuum off",
	"Vacuum on",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry2 irradiated 150#mum sensors 5E15"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry2/W16-14SPA_IV_1.txt",
	"Foundry2/W16-54SPA_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"W16-14SPA",
	"W14-54SPA",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


      
    breakDownCurrent = 10e-6; // criterion to calculate VBD (threshold current [A])
    fixedVoltageForCurrentEval = 50.; // voltage [V] at which to evaluate the current
    fractionOfVBDForCurrentEval = 0.8; // fraction of breakdown voltage at which to evaluate the current
    NstepsX = 1; // in case of an array
    NstepsY = 1; // in case of an array
    inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/ITk_Sensor_MarketSurvey/"; //needed for comparison with foundry4
    // inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/ITk_Sensor_MarketSurvey/Foundry3/IrradiatedSensors/";
    // outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITk_Sensor_MarketSurvey/Foundry4/";
    outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITk_Sensor_MarketSurvey/Foundry2/";	
    Vlo=0; Vhi=700; NbinsV=700;
    // Vlo=0.000; Vhi=55.; NbinsV=100; //RHI
//    VloLGAD=0; VhiLGAD=200;
    Ilo=0.0; Ihi=50.; // for IV
//    Ilo=0.0; Ihi=0.02; // for IV zoom
//    Ilo=0.0; Ihi=0.6; // for 2310004
    //Ilo=19.0; Ihi=21; // for T
    //   Ilo=0.0; Ihi=55.; // for RH
    //Ilo=0.0; Ihi=0.016; // for RHI    

    IloZoom=0; IhiZoom=100e-9; NbinsI=15;
    Clo=-0.1; Chi=1.e-1; C2hi=0.6e-4; 
    // initialise arrays to 0 if default should be used
    //CV color scheme
    //int colorArr[1000]     ={1,2,4,8,8,9}; 
    //int markerArr[1000]    ={20,24,21,25,22,26,23,32,33,27}; //{0}
    //IV color scheme
    //int colorArr[1000]     ={4,64,67,38,9,2,95,92,11,12,39,28};//,9,9,11,11,12,12,28,28,38,38,39,39}; //T studies
    //    int colorArr[1000]     ={4,64,67,69,2,95,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //all irradiated
    // int colorArr[1000]     ={4,64,2,95,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //all irradiated
    //    int colorArr[1000]     ={92,91,67,69,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //sepearate
    //int colorArr[1000]     ={4,64,2,95,4,64,2,95};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3
    //int colorArr[1000]     ={67,69,92,91,4,64,2,95,67,92};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3 and F4
    //int colorArr[1000]     ={67,69,4,1,33};//64,109};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3 and F4 2e15
    //int colorArr[1000]     ={92,91,2,41,43}; //,95,94};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3 and F4 5E15

    int colorArr[1000]     ={67,69,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //150
    //int markerArr[1000]    ={20,20,24,21,22,26,23,32,29}; //150
    int markerArr[1000]    ={20,21,22,23,20,21,22,23,33,29}; //standard
    //int markerArr[1000]    ={20,21,22,23,29,3320,21,22,23,33,29}; //standard
    //int markerArr[1000]    ={20,21,22,23,33,29,34,43,45,47,41}; // comparison F3 and F4
    //int markerArr[1000]    ={20,21,22,23,33,29}; // comparison F3 and F4
//    int markerArr[1000]    ={20,24,21,25,22,23,26}; //for light vs no light study
//   int colorArr[1000]     ={1,1,2,2,4,4,4,39,8,9,11,12,28,38,39}; // for light vs no light study
   
 int lineStyleArr[1000] ={0};
    //do not change last lines:
    std::copy(colorArr, colorArr+1000, color_);
    std::copy(markerArr, markerArr+1000, marker_);
    std::copy(lineStyleArr, lineStyleArr+1000, lineStyle_);

      }

 ////////////////////////////////////////////
  if(IVconfigID.Contains("Foundry1")){
    ////////////////////////////////////////////
    
    if(IVconfigID.Contains("CV")){
      TString graphNameArr[] = {"Cap"}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    else{
      //TString graphNameArr[] = {"Chuck","","","","","",""}; // if always the same, put just one element
      //TString graphNameArr[] = {"","","","","","_X1_Y1"}; // if always the same, put just one element
      //TString graphNameArr[] = {"_X1_Y1"}; // if always the same, put just one element     
      TString graphNameArr[] = {""}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    
    specifier = IVconfigID;

 if(IVconfigID=="Foundry1 irradiated 150#mum sensors"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry1/ASD15-3-A5_IV_1.txt",
	"Foundry1/ASD15-4-A6_IV_1.txt",
	"Foundry1/ASD15-4-A8_IV_1.txt",
	"Foundry1/ASD15-2-A4_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"ASD15-3-A5",
	"ASD15-4-A6",
	"ASD15-4-A8",
	"ASD15-2-A4",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry1,2,3 and 4 irradiated 150#mum sensors comparison 5E15"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry1/ASD15-4-A8_IV_1.txt",
	"Foundry1/ASD15-2-A4_IV_1.txt",
	"Foundry2/W16-14SPA_IV_1.txt",
	"Foundry2/W16-54SPA_IV_1.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_2.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/5E15left_IV_4.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/5E15right_IV_5.txt",

      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Foundry1 S1 ",
	"Foundry1 S2 ",
	"Foundry2 S1 ",
	"Foundry2 S2 ",
	"Foundry3 S2 ",
	"Foundry4 S1 ",
	"Foundry4 S2 ",

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry1,2,3 and 4 irradiated 150#mum sensors comparison 2E15"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry1/ASD15-3-A5_IV_1.txt",
	"Foundry1/ASD15-4-A6_IV_1.txt",
	"Foundry2/W17-54SPA_IV_1.txt",
	"Foundry2/W15-14GPB_IV_1.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_2.txt",
	"Foundry4/SC/IrradiatedSensors/Modules/2E15leftMod_IV_1.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/2E15right_IV_5.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Foundry1 S1",
	"Foundry1 S2",
	"Foundry2 S1",
	"Foundry2 S2",
	"Foundry3 S1",
	"Foundry4 S1",
	"Foundry4 S2",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


  breakDownCurrent = 10e-6; // criterion to calculate VBD (threshold current [A])
    fixedVoltageForCurrentEval = 50.; // voltage [V] at which to evaluate the current
    fractionOfVBDForCurrentEval = 0.8; // fraction of breakdown voltage at which to evaluate the current
    NstepsX = 1; // in case of an array
    NstepsY = 1; // in case of an array
    inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/ITk_Sensor_MarketSurvey/"; //needed for comparison with foundry4
    // inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/ITk_Sensor_MarketSurvey/Foundry3/IrradiatedSensors/";
    // outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITk_Sensor_MarketSurvey/Foundry4/";
    //    outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITk_Sensor_MarketSurvey/Foundry2/";	
    outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITk_Sensor_MarketSurvey/Foundry1/";
    Vlo=0; Vhi=700; NbinsV=700;
    // Vlo=0.000; Vhi=55.; NbinsV=100; //RHI
//    VloLGAD=0; VhiLGAD=200;
    Ilo=0.0; Ihi=50.; // for IV
//    Ilo=0.0; Ihi=0.02; // for IV zoom
//    Ilo=0.0; Ihi=0.6; // for 2310004
    //Ilo=19.0; Ihi=21; // for T
    //   Ilo=0.0; Ihi=55.; // for RH
    //Ilo=0.0; Ihi=0.016; // for RHI    

    IloZoom=0; IhiZoom=100e-9; NbinsI=15;
    Clo=-0.1; Chi=1.e-1; C2hi=0.6e-4; 
    // initialise arrays to 0 if default should be used
    //CV color scheme
    //int colorArr[1000]     ={1,2,4,8,8,9}; 
    int markerArr[1000]    ={20,24,21,25,23,22,26,23,32,33,27}; //{0}
    //IV color scheme
    //int colorArr[1000]     ={4,64,67,38,9,2,95,92,11,12,39,28};//,9,9,11,11,12,12,28,28,38,38,39,39}; //T studies
    //    int colorArr[1000]     ={4,64,67,69,2,95,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //all irradiated
    // int colorArr[1000]     ={4,64,2,95,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //all irradiated
    //    int colorArr[1000]     ={92,91,67,69,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //sepearate
    //int colorArr[1000]     ={4,64,2,95,4,64,2,95};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3
    //int colorArr[1000]     ={67,69,92,91,4,64,2,95,67,92};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3 and F4
    //int colorArr[1000]     ={67,69,4,1,33};//64,109};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3 and F4 2e15
    //int colorArr[1000]     ={92,91,2,41,43}; //,95,94};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F2 and F3 and F4 5E15

    //int colorArr[1000]     ={67,69,92,91};//,9,9,11,11,12,12,28,28,38,38,39,39}; //150
    //int markerArr[1000]    ={20,20,24,21,22,26,23,32,29}; //150
    //int markerArr[1000]    ={20,21,22,23,20,21,22,23,33,29}; //standard
    //int markerArr[1000]    ={20,21,22,23,29,3320,21,22,23,33,29}; //standard
    //int markerArr[1000]    ={20,21,22,23,33,29,34,43,45,47,41}; // comparison F3 and F4
    //int markerArr[1000]    ={20,21,22,23,33,29}; // comparison F3 and F4
//    int markerArr[1000]    ={20,24,21,25,22,23,26}; //for light vs no light study
    int colorArr[1000]     ={8,8,1,1,2,4,4,39,8,9,11,12,28,38,39}; // for light vs no light study
   
 int lineStyleArr[1000] ={0};
    //do not change last lines:
    std::copy(colorArr, colorArr+1000, color_);
    std::copy(markerArr, markerArr+1000, marker_);
    std::copy(lineStyleArr, lineStyleArr+1000, lineStyle_);

      }


  ////////////////////////////////////////////
  if(IVconfigID.Contains("Foundry4")){
    ////////////////////////////////////////////
    
    if(IVconfigID.Contains("CV")){
      TString graphNameArr[] = {"Cap"}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    else{
      //TString graphNameArr[] = {"Chuck","","","","","",""}; // if always the same, put just one element
      //TString graphNameArr[] = {"","","","","","_X1_Y1"}; // if always the same, put just one element
      //TString graphNameArr[] = {"_X1_Y1"}; // if always the same, put just one element     
      TString graphNameArr[] = {""}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    
    specifier = IVconfigID;
    
    if(IVconfigID=="Foundry4_Sensors1-10"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410001_IV_2.txt",
	"DortmundResults/1410001_IV_1.txt",
	"SC/1410002_IV_1.txt",
	"DortmundResults/1410002_IV_1.txt",
	"SC/1410003_IV_3.txt",
	"DortmundResults/1410003_IV_1.txt",
	"SC/1410004_IV_1.txt",
	"DortmundResults/1410004_IV_1.txt"//,
	/*	"SC/1410005_IV_1.txt",
	"DortmundResults/1410005_IV_1.txt",
	"SC/1410006_IV_1.txt",
	"DortmundResults/1410006_IV_1.txt",
	"SC/1410007_IV_1.txt",
	"DortmundResults/1410007_IV_1.txt",
	"SC/1410008_IV_1.txt",
	"DortmundResults/1410008_IV_1.txt",
	"SC/1410009_IV_3.txt",
	"DortmundResults/1410009_IV_1.txt",
	"SC/1410010_IV_1.txt",
	"DortmundResults/1410010_IV_1.txt"*/
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 1 Goe",  
	"Sensor 1 DO",  
	"Sensor 2 Goe",
	"Sensor 2 DO",
	"Sensor 3 Goe",   
	"Sensor 3 DO",
	"Sensor 4 Goe",  
	"Sensor 4 DO"/*,
	"Sensor 5 Goe",
	"Sensor 5 DO",
	"Sensor 6 Goe",
	"Sensor 6 DO",
	"Sensor 7 Goe",
	"Sensor 7 DO",
	"Sensor 8 Goe",
	"Sensor 8 DO",
	"Sensor 9 Goe",
	"Sensor 9 DO",
	"Sensor 10 Goe",   
	"Sensor 10 DO" */
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
 if(IVconfigID=="Foundry4_DCSensors1-20"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420001_IV_1.txt",
	"DC/1420002_IV_1.txt",
	"DC/1420003_IV_1.txt",
	"DC/1420004_IV_1.txt",
	"DC/1420005_IV_1.txt",
	"DC/1420006_IV_1.txt",
	"DC/1420007_IV_1.txt",
	"DC/1420008_IV_1.txt",
	"DC/1420009_IV_1.txt",
	"DC/1420010_IV_1.txt",
	"DC/1420011_IV_1.txt",
	"DC/1420012_IV_1.txt",
	"DC/1420013_IV_1.txt",
	"DC/1420014_IV_1.txt",
	"DC/1420015_IV_1.txt",
	"DC/1420016_IV_1.txt",
	"DC/1420017_IV_1.txt",
	"DC/1420018_IV_1.txt",
	"DC/1420019_IV_1.txt",
	"DC/1420020_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 1 Goe",  
	"Sensor 2 Goe",
	"Sensor 3 Goe",   
	"Sensor 4 Goe",  
	"Sensor 5 Goe",
	"Sensor 6 Goe",
	"Sensor 7 Goe",
	"Sensor 8 Goe",
	"Sensor 9 Goe",
	"Sensor 10 Goe", 
	"Sensor 11 Goe",  
	"Sensor 12 Goe",
	"Sensor 13 Goe",   
	"Sensor 14 Goe",  
	"Sensor 15 Goe",
	"Sensor 16 Goe",
	"Sensor 17 Goe",
	"Sensor 18 Goe",
	"Sensor 19 Goe",
	"Sensor 20 Goe" 
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    } 

 else if(IVconfigID=="Foundry4_1420010"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420010_IV_1.txt",
	"DC/1420010_IV_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"post CV ",   
	"post CV some days later"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    } 

 else if(IVconfigID=="Foundry4_1420011"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420011_IV_1.txt",
	"DC/1420011_IV_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"pre CV",   
	"post CV"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    } 

 else if(IVconfigID=="Foundry4_1420013"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420013_IV_1.txt",
	"DC/1420013_IV_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"pre CV no wait time",   
	"post CV "
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    } 

 else if(IVconfigID=="Foundry4_1420016"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420016_IV_1.txt",
	"DC/1420016_IV_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"pre CV",   
	"post CV "
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    } 

    else if(IVconfigID=="Foundry4_1420008"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420008_IV_1.txt",
	"DC/1420008_IV_2.txt",
	"DC/1420008_IV_3.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV ",   
	"postCV",
	"postCV2"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Foundry4_1420007"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420007_IV_1.txt",
	"DC/1420007_IV_2.txt",
	//	"DC/1420007_IV_3.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV ",   
	"postCV",
	//"postCV2"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1420006"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420006_IV_1.txt",
	"DC/1420006_IV_2.txt",
	//	"DC/1420007_IV_3.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV ",   
	"postCV",
	//"postCV2"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1420005"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420005_IV_1.txt",
	"DC/1420005_IV_2.txt",
	//	"DC/1420007_IV_3.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV ",   
	"postCV",
	//"postCV2"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1420004"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420004_IV_1.txt",
	"DC/1420004_IV_2.txt",
	//	"DC/1420007_IV_3.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV ",   
	"postCV",
	//"postCV2"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1420003"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420003_IV_1.txt",
	"DC/1420003_IV_2.txt",
	//	"DC/1420007_IV_3.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV ",   
	"postCV",
	//"postCV2"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1420002"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420002_IV_1.txt",
	"DC/1420002_IV_2.txt",
	//	"DC/1420007_IV_3.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV ",   
	"postCV",
	//"postCV2"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1420001"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420001_IV_1.txt",
	"DC/1420001_IV_2.txt",
	//	"DC/1420007_IV_3.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV ",   
	"postCV",
	//"postCV2"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1410001"){
      TString fileNameArr[]    = { // file in which to find graph
	//	"SC/1410001_IV_1.txt",
	//	"SC/1410001_IV_1.txt",
	//"SC/1410001_IV_2.txt",
	//"SC/1410001_IV_3.txt",
	"SC/1410001_IV_4.txt",
	//"SC/1410001_IV_5.txt",
	"SC/1410001_IV_6.txt",
	"SC/1410001_IV_7.txt",
	"SC/1410001_IV_8.txt",
	"SC/1410001_IV_9.txt",
	"SC/1410001_IV_10.txt",
	"DortmundResults/1410001_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"K26 RH4 high Acc",  
	//"K24 RH4",  
	//"K24 RH32",
	//"K23 RH38",
	"preCV ",   
	//"K23 RH42 1sec wait",
	"postCV",
	"postCV (2)",
	"postCV bottom pad",
	"RH4 postCV",
	"RH35 postCV",
	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1410002"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410002_IV_1.txt",
	"SC/1410002_IV_2.txt",
	"SC/1410002_IV_3.txt",
	"DortmundResults/1410002_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV",  
	"postCV",
	"postCV (2)",
	"Dortmund"      
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    
    else if(IVconfigID=="Foundry4_1410003"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410003_IV_1.txt",
	"SC/1410003_IV_2.txt",
	"SC/1410003_IV_3.txt"//,
	//"DortmundResults/1410003_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV",  
	"postCV",
	"postCV (2)"//,
	//"Dortmund"      
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Foundry4_1410004"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410004_IV_1.txt",
	"SC/1410004_IV_2.txt",
	"SC/1410004_IV_3.txt",
	"DortmundResults/1410004_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1st try",  
	"2nd try",
	"3rd try",
	"Dortmund"      
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
        
    else if(IVconfigID=="Foundry4_1410005"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410005_IV_1.txt",
	"DortmundResults/1410005_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"RH42",
	"Dortmund"  
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
        
    else if(IVconfigID=="Foundry4_1410006"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410006_IV_1.txt",
	"SC/1410006_IV_2.txt",
	"SC/1410006_IV_3.txt",
	//"SC/1410006_IV_4.txt",
	"SC/1410006_IV_5.txt",
	"SC/1410006_IV_6.txt",
	//"SC/1410006_IV_7.txt",
	"SC/1410006_IV_8.txt"//,
	//"DortmundResults/1410006_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV",  
	"post scratch",
	"postIT",
	//"postIT RH 18-5",
	"postIT RH4",
	"postIT@RH2 RH2",
	//"postITatlowRH RH 13",
	"postIT@RH2 RH33"//,
	//"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Foundry4_1410007"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410007_IV_1.txt",
	"SC/1410007_IV_2.txt",
	"SC/1410007_IV_3.txt",
	"SC/1410007_IV_4.txt",
	"SC/1410007_IV_5.txt",
	"SC/1410007_IV_6.txt",
	"SC/1410007_IV_7.txt",
	"SC/1410007_IV_8.txt",
	"SC/1410007_IV_9.txt",
	"SC/1410007_IV_10.txt",
	"SC/1410007_IV_11.txt",
	"SC/1410007_IV_12.txt",
	"SC/1410007_IV_13.txt",
	"DortmundResults/1410007_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preCV with chiller RH 50",  
	"postCV with chiller RH 50",
	"postCV without chiller RH 50",      
	"postCV without chiller RH 4",
	"postCV without chiller RH 4 (2)",
	"postCV without chiller RH 4 (3)",
	"postCV without chiller RH 45",
	"postCV without chiller RH 45(2) ",
	"postIt with chiller RH 50 ",
	"postIt with chiller RH 50 (2)",
	"postIt with chiller RH 50 (3)",
	"postIt with chiller RH 5",
	"postIt with chiller RH 3",
	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Foundry4_1410008"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410008_IV_1.txt",
	"SC/1410008_IV_2.txt",
	"SC/1410008_IV_3.txt",
	"SC/1410008_IV_4.txt",
	"SC/1410008_IV_5.txt",
	"DortmundResults/1410008_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"pre IT RH 40",  
	"post IT RH 46",
	"post IT RH 20",      
	"post IT RH 4",      
	"post IT RH 3",
	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    
    else if(IVconfigID=="Foundry4_1410009"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410009_IV_1.txt",
	"SC/1410009_IV_2.txt",
	"SC/1410009_IV_3.txt",
	"SC/1410009_IV_4.txt",
	"SC/1410009_IV_5.txt",
	"SC/1410009_IV_6.txt",
	"SC/1410009_IV_7.txt",
	"SC/1410009_IV_8.txt",
	"SC/1410009_IV_9.txt",
	"SC/1410009_IV_10.txt"//,
	//"DortmundResults/1410009_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"RH 40",  
	"RH 34",
	"RH 37 no chiller",      
	"RH 35 some days later",      
	"RH 35 some days later (2)",
	"RH 46 postIT",
	"RH 4 postIT",
	"RH 4 postIT@RH4",
	"RH 32 postIT@RH4",	
	"RH 33 postIT@RH4"//,
	//"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    
    else if(IVconfigID=="Foundry4_1410010"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410010_IV_1.txt",
	"SC/1410010_IV_2.txt",
	"SC/1410010_IV_3.txt",
	"SC/1410010_IV_4.txt",
	"SC/1410010_IV_5.txt",
	"SC/1410010_IV_6.txt"//,
	//"DortmundResults/1410010_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"preIT RH40",
	"preIT RH38",
	"preIT RH39",
  	"postIT RH40",
	"postIT RH7",      
	"postIT RH4"//,
	//"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

 else if(IVconfigID=="Foundry4_SC_irrad_after5months"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/2E15left_IV_3.txt",
	"SC/2E15left_IV_2.txt",
	"SC/2E15right_IV_6.txt",
	"SC/2E15right_IV_2.txt",
	"SC/5E15left_IV_5.txt",
	"SC/5E15left_IV_3.txt",
	"SC/5E15right_IV_6.txt",
	"SC/5E15right_IV_3.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2E15_1 10d@RT+5m -26C",
	"2E15_1 10d@RT -27C",
	"2E15_2 10d@RT+5m -26C",
	"2E15_2 10d@RT -27C",
	"5E15_1 10d@RT+5m -26C",
	"5E15_1 10d@RT -26.5C",
	"5E15_2 10d@RT+5m -26C",
	"5E15_2 10d@RT -27C"
      }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

 else if(IVconfigID=="Foundry4_SC_irrad_bonded"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/2E15left_IV_3.txt",
	"SC/2E15leftMod_IV_1.txt",
	"SC/2E15leftMod_IV_2.txt",
	"SC/2E15right_IV_6.txt",
	"SC/2E15rightMod_IV_1.txt",
	"SC/2E15rightMod_IV_2.txt",
	"SC/5E15left_IV_5.txt",
 	"SC/5E15leftMod_IV_1.txt",
	"SC/5E15leftMod_IV_2.txt",
	"SC/5E15right_IV_6.txt",
	"SC/5E15rightMod_IV_1.txt",
	"SC/5E15rightMod_IV_2.txt",
	"SC/5E15rightMod_IV_3.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2E15_1 10d@RT+5m -26C set",
	"2E15_1 bonded -25C set",
	"2E15_1 bonded -30C set", 
	"2E15_2 10d@RT+5m -26C set",
	"2E15_2 bonded -25C set",
	"2E15_2 bonded -30C set",
	"5E15_1 10d@RT+5m -26C set",
	"5E15_1 bonded -25C set",
	"5E15_1 bonded -30C set",
	"5E15_2 10d@RT+5m -26C set",
	"5E15_2 bonded -25C set",
	"5E15_2 bonded -26C set",
	"5E15_2 bonded -30C set"
      }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4 SC -25C Climate Chamber"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/IrradiatedSensors/2E15leftMod_IV_4.txt",
	"SC/2E15rightMod_IV_4.txt",
	"SC/5E15leftMod_IV_4.txt",
	"SC/5E15rightMod_IV_15.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2E15_1",
	"2E15_2",
	"5E15_1",
	"5E15_2", 
      }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


else if(IVconfigID=="Foundry4 SC -25C CC Power Density"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/2E15leftMod_IV_4.txt",
	"SC/2E15rightMod_IV_4.txt",
	"SC/5E15leftMod_IV_4.txt",
	"SC/5E15rightMod_IV_15.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2E15_1",
	"2E15_2",
	"5E15_1",
	"5E15_2", 
      }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4 SC 2E15_1"){
      TString fileNameArr[]    = { // file in which to find graph
	
	"SC/2E15leftMod_IV_4.txt",
	"SC/2E15leftMod_IV_6.txt",
	//	"SC/2E15left_IV_3_mod.txt",
	"SC/2E15left_IV_3.txt",
	//"SC/2E15leftMod_IV_3.txt",
	//"SC/2E15leftMod_IV_1.txt",
	"SC/2E15leftMod_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-25#circC CC",
	"-26#circC CC",
	//"-25C ProbeSt. extrapol.",
	"-26#circC PS",
	"-30#circC CC",
      }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4 SC 2E15_1 Climate Chamber"){
      TString fileNameArr[]    = { // file in which to find graph
	
	"SC/2E15leftMod_IV_4.txt",
	//	"SC/2E15leftMod_IV_6.txt",
	//	"SC/2E15left_IV_3_mod.txt",
	//"SC/2E15left_IV_3.txt",
	//"SC/2E15leftMod_IV_3.txt",
	//"SC/2E15leftMod_IV_1.txt",
	//"SC/2E15leftMod_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-25#circC CC",
	//"-26#circC CC",
	//"-25C ProbeSt. extrapol.",
	//"-26#circC PS",
	//	"-30#circC CC",
      }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4 SC 2E15_1"){
      TString fileNameArr[]    = { // file in which to find graph
		//	"SC/2E15rightMod_IV_3.txt",
	"SC/2E15rightMod_IV_4.txt",
	"SC/2E15rightMod_IV_6.txt",
	//"SC/2E15right_IV_6_mod.txt",
	"SC/2E15right_IV_6.txt",
	"SC/2E15rightMod_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend nam
	//	"bonded -30C",
	"-25#circC CC",
		"-26#circC CC",
	//"-25C ProbeSt. extrapol.",
	"-26#circC PS",
	"-30#circC CC",
      }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4 SC 2E15_1 Climate Chamber"){
      TString fileNameArr[]    = { // file in which to find graph
		//	"SC/2E15rightMod_IV_3.txt",
	"SC/2E15rightMod_IV_4.txt",
	//"SC/2E15rightMod_IV_6.txt",
	//"SC/2E15right_IV_6_mod.txt",
	//"SC/2E15right_IV_6.txt",
	//"SC/2E15rightMod_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend nam
	//	"bonded -30C",
	"-25#circC CC",
	//	"-26#circC CC",
	//"-25C ProbeSt. extrapol.",
	//"-26#circC PS",
	//"-30#circC CC",
      }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4 SC 5E15_1 Climate Chamber"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15leftMod_IV_4.txt",
	//"SC/5E15leftMod_IV_6.txt",
	//	"SC/5E15left_IV_5_mod.txt",
	//	"SC/5E15left_IV_5.txt",
	//	"SC/5E15leftMod_IV_1.txt",
	//"SC/5E15leftMod_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
       TString legNameArr[]    = { // corresponding legend name
	"-25#circC CC",
	//	"-26#circC CC",
	//"-25C ProbeSt. extrapol.",
	//	"-26#circC PS",
	//"-30#circC CC",
      }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4 SC 5E15_1"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15leftMod_IV_4.txt",
	"SC/5E15leftMod_IV_6.txt",
	//	"SC/5E15left_IV_5_mod.txt",
	"SC/5E15left_IV_5.txt",
	//	"SC/5E15leftMod_IV_1.txt",
	"SC/5E15leftMod_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
       TString legNameArr[]    = { // corresponding legend name
	"-25#circC CC",
	"-26#circC CC",
	//"-25C ProbeSt. extrapol.",
	"-26#circC PS",
	"-30#circC CC",
      }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_all"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15right_IV_6.txt",
	"SC/5E15rightMod_IV_1.txt",
	"SC/5E15rightMod_IV_2.txt",
	"SC/5E15rightMod_IV_3.txt",
	//"SC/5E15rightMod_IV_4.txt",
	//"SC/5E15rightMod_IV_5.txt",
	//"SC/5E15rightMod_IV_6.txt",
 	//"SC/5E15rightMod_IV_7.txt",
 	"SC/5E15rightMod_IV_8.txt",
 	"SC/5E15rightMod_IV_9.txt",
	"SC/5E15rightMod_IV_10.txt",
 	"SC/5E15rightMod_IV_11.txt",
 	"SC/5E15rightMod_IV_12.txt",
 	"SC/5E15rightMod_IV_13.txt"
     };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"10d@RT+5m -26C",
	"bonded -25C",
	"bonded -26C",
	"bonded -30C",
	//"5E15_2 bonded -26C set rep stopped",
	//"5E15_2 bonded -26C set rep",
	//"5E15_2 bonded -26C set 0.5s wait",
	//"5E15_2 bonded -26C set 0.5s wait, 3x",
 	"bonded -26C 0.5s, 3x, 20V steps",
 	"bonded -27C 0.5s, 3x, 20V steps",
	"bonded -28C 0.5s, 3x, 20V steps",
 	"bonded -29C 0.5s, 3x, 20V steps",
	"bonded -24C 0.5s, 3x, 20V steps",
 	"bonded -35C 0.5s, 3x, 20V steps"   
 }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4 5E15 150#mum sensor T studies"){
      TString fileNameArr[]    = { // file in which to find graph
	"5E15rightMod_IV_12.txt",
	"5E15rightMod_IV_1.txt",
 	"5E15rightMod_IV_8.txt",
 	"5E15rightMod_IV_9.txt",
	"5E15rightMod_IV_10.txt",
 	"5E15rightMod_IV_11.txt",
 	"5E15rightMod_IV_3.txt",
 	"5E15rightMod_IV_13.txt"
     };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-24#circC",
	"-25#circC",
 	"-26#circC",
 	"-27#circC",
	"-28#circC",
 	"-29#circC",
	"-30#circC",
 	"-35#circC"   
 }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }



else if(IVconfigID=="Foundry4 SC 5E15_2"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15rightMod_IV_15.txt",
	"SC/5E15rightMod_IV_16.txt",
	"SC/5E15right_IV_6.txt",
	//	"SC/5E15right_IV_6_mod.txt",

	"SC/5E15rightMod_IV_3.txt",
	//"SC/5E15rightMod_IV_4.txt",
	//"SC/5E15rightMod_IV_5.txt",
	//"SC/5E15rightMod_IV_6.txt",
 	//"SC/5E15rightMod_IV_7.txt",
	// 	"SC/5E15rightMod_IV_8.txt",
 	//"SC/5E15rightMod_IV_9.txt",
	//"SC/5E15rightMod_IV_10.txt",
 	//"SC/5E15rightMod_IV_11.txt",
 	//"SC/5E15rightMod_IV_12.txt",
 	//"SC/5E15rightMod_IV_13.txt"
	//"SC/5E15rightMod_IV_1.txt",
     };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-25#circC CC",
	"-26#circC CC",
	//"-25C ProbeSt. extrapol.",
	"-26#circC PS",
	"-30#circC CC",
	//"-26C ProbeSt. (+ calc -25C)",
	//"-26C ClimChamb. (+ calc -25C)",
	//"5E15_2 bonded -30C set",
	//"5E15_2 bonded -26C set rep stopped",
	//"bonded -26C rep",
	//"bonded -26C 0.5s",
	//"bonded -26C 0.5s, 3x",
 	//"-26C ClimChamb. fast (+ calc -25C)",
 	//"bonded -27C 0.5s, 3x, 20V steps",
	//"5E15_2 bonded -28C set 0.5s wait, 3x, 20V steps",
 	//"5E15_2 bonded -29C set 0.5s wait, 3x, 20V steps"
	//"5E15_2 bonded -24C set 0.5s wait, 3x, 20V steps",
 	//"5E15_2 bonded -35C set 0.5s wait, 3x, 20V steps"   
	//	"-25C ClimChamb.",
 }; 
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4_SC_irrad_27"){
      TString fileNameArr[]    = { // file in which to find graph
 	"SC/2E15left_IV_2.txt",
	"SC/2E15right_IV_2.txt",
	"SC/5E15left_IV_3.txt",
	"SC/5E15right_IV_3.txt",
	"SC/1410003_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2E15_1 10d@RT -27C set",
	"2E15_2 10d@RT -27C set",
	"5E15_1 10d@RT -26.5C set",
	"5E15_2 10d@RT -27C set",
	"1410003 before irrad"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

 else if(IVconfigID=="Foundry4_SC_irrad"){
      TString fileNameArr[]    = { // file in which to find graph
 	"SC/2E15left_IV_1.txt",
	"SC/2E15right_IV_1.txt",
	"SC/5E15left_IV_2.txt",
	"SC/5E15right_IV_1.txt",
	"SC/1410003_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2E15_1 -29.2C set",
	"2E15_2 -29.2C set",
	"5E15_1 -29.2C set",
	"5E15_2 -29.2C set",
	"1410003 before irrad"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_SC_irrad_all"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/2E15left_IV_1.txt",
	"SC/2E15left_IV_2.txt",
	"SC/2E15right_IV_1.txt",
	"SC/2E15right_IV_2.txt",
	"SC/2E15right_IV_3.txt",
	"SC/2E15right_IV_4.txt",
	"SC/2E15right_IV_5.txt",
	"SC/5E15left_IV_1.txt",
	"SC/5E15left_IV_2.txt",
	"SC/5E15left_IV_3.txt",
	"SC/5E15right_IV_1.txt",
	"SC/5E15right_IV_2.txt",
	"SC/5E15right_IV_3.txt",
	"SC/5E15right_IV_4.txt",
	"SC/5E15right_IV_5.txt",
	"SC/1410003_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2E15left -29.2C set",
	"2E15left 10d@RT -27C set",
	"2E15right -29.2C set",
	"2E15right 10d@RT -27C set",
	"2E15right 10d@RT -29.5C set",
	"2E15right 10d@RT -29.2C set",
	"2E15right 10d@RT -25C set",
	"5E15left -25.5C set",
	"5E15left -29.2C set",
	"5E15left 10d@RT -26.5C set 3.5V steps",
	"5E15right -29.2C set",
	"5E15right 10d@RT -27C set 3.5V steps",
	"5E15right 10d@RT -27C set",
	"5E15right 10d@RT -29.2C set",
	"5E15right 10d@RT -25C set",
	"1410003 before irrad"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

  else if(IVconfigID=="Foundry4_SC_irrad_5E15_1"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15left_IV_1.txt",
	"SC/5E15left_IV_2.txt",
	"SC/5E15left_IV_3.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-25.5C",
	"-29.2C",
	"10d@RT -26.5C"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

  else if(IVconfigID=="Foundry4_SC_irrad_5E15_2"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15right_IV_1.txt",
	//"SC/5E15right_IV_2.txt",
	"SC/5E15right_IV_3.txt",
	"SC/5E15right_IV_4.txt",
	"SC/5E15right_IV_5.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-29.2C",
	//"10d@RT -27C 3.5Vsteps",
	"10d@RT -27C",
	"10d@RT -29.2C",
	"10d@RT -25C"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

 
    else if(IVconfigID=="Foundry4_SC_irrad_2E15_1"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/2E15left_IV_1.txt",
	"SC/2E15left_IV_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-29.2C",
	"10d@RT -27C"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
     }


  else if(IVconfigID=="Foundry4_SC_irrad_2E15_2"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/2E15right_IV_1.txt",
	"SC/2E15right_IV_2.txt",
	"SC/2E15right_IV_3.txt",
	"SC/2E15right_IV_4.txt",
	"SC/2E15right_IV_5.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-29.2C set",
	"10d@RT -27C",
	"10d@RT -29.5C",
	"10d@RT -29.2C",
	"10d@RT -25C"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
   
    
    /////////////////////////  It ////////////////////////////

  else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_It_all"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15rightMod_It_1_mod.txt",
	"SC/5E15rightMod_It_2_mod.txt",
	"SC/5E15rightMod_It_3_mod.txt",
	"SC/5E15rightMod_It_4_mod.txt", 
	"SC/5E15rightMod_It_5_mod.txt"
     };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2min wait,3x,-26C,125V",
	"10min wait,3x,-26C,125V",
	"500ms wait,3x,-26C,125V",
	"10s wait,10x,-26C,125V",
	"10min wait,10x,-26C,125V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }  

 else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_It_all_600"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15rightMod_It_6.txt",
	"SC/5E15rightMod_It_7_mod.txt",
	"SC/5E15rightMod_It_8_mod.txt",
	"SC/5E15rightMod_It_9_mod.txt", 
	//	"SC/5E15rightMod_It_10_mod.txt"
     };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"500ms wait,3x,-26C,125V",
	"10s wait,10x,-26C,125V",
	"2min wait,3x,-26C,125V",
	"10min wait,3x,-26C,125V",
	//	"10min wait,10x,-26C,125V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
     }

 else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_It_600"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15rightMod_It_6.txt",
	"SC/5E15rightMod_It_7.txt",
	"SC/5E15rightMod_It_8.txt",
	"SC/5E15rightMod_It_9.txt",
	"SC/5E15rightMod_It_10.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"500ms wait,3x,-26C,600V",
	"10s wait,10x,-26C,600V",
	"2min wait,3x,-26C,600V",
	"10min wait,3x,-26C,125V",
	"10min wait,10x,-26C,125V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15rightMod_It_2.txt",
	"SC/5E15rightMod_It_5.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"10min wait,3x,-26C,125V",
	"10min wait,10x,-26C,125V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_It_short"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15rightMod_It_3.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"500ms wait,3x,-26C,125V",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_It_medium"){
      TString fileNameArr[]    = { // file in which to find graph
		"SC/5E15rightMod_It_1.txt",
		"SC/5E15rightMod_It_4.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2min wait, 3x,-26C,125V",
	"10s wait,10x,-26C,125V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
        

    else if(IVconfigID=="Foundry4_1420009_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420009_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 9"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1420016_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420016_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 16"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
  
    else if(IVconfigID=="Foundry4_1410007_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410007_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 7"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Foundry4_1410006_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410006_It_1.txt",
	"SC/1410006_It_3.txt",
	"SC/1410006_It_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 6 ",
	"Sensor 6 RH51",
	"Sensor 6 RH3",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1410008_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410008_It_1.txt",
	"SC/1410008_It_2.txt",
	"SC/1410008_It_3.txt",
	"SC/1410008_It_4.txt",
	"SC/1410008_It_5.txt",	//	"DortmundResults/1410008_It_1.txt"
	"SC/1410008_It_6.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"RH 39-46",
	"RH 3-50 stopped dry air after 2 days",
	"RH 51 5h at 0V before restart",
	"RH 47 1.5 days at 0V before restart flushed 4h",
	"RH 3 stopped flushing after 2 h",
	"RH20-47tried to regulate"
	//	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }   

    else if(IVconfigID=="Foundry4_1410008_Itmod"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410008_It_1.txt",
	"SC/1410008_It_2.txt",
	"SC/1410008_It_3_mod.txt",
	"SC/1410008_It_4_mod.txt",
	"SC/1410008_It_5_mod.txt"	//	"DortmundResults/1410008_It_1.txt"      
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Run 1",
	"Run 2",
	"Run 3",
	"Run 4",
	"Run 5"
	//	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }   


    else if(IVconfigID=="Foundry4_1410009_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410009_It_1.txt",
	"SC/1410009_It_2.txt",
	"DortmundResults/1410009_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 9 ",
	"Sensor 9 RH3",
	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }   

    else if(IVconfigID=="Foundry4_1410010_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410010_It_1.txt",
	//"SC/1410010_It_2.txt",
	"DortmundResults/1410010_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 10 ",
	//	"Sensor 10 RH3",
	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    } 

    else if(IVconfigID=="Foundry4_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410006_It_1.txt",
	"SC/1410006_It_2.txt",
	"SC/1410007_It_1.txt",
	"SC/1410008_It_1.txt",
	"SC/1410009_It_1.txt",
	"SC/1410009_It_2.txt",
	"SC/1410010_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 6",
	"Sensor 6 RH=3%",
	"Sensor 7",
	"Sensor 8",
	"Sensor 9",
	"Sensor 9 RH=3%",
	"Sensor 10"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
 
  /////////////////////////  Tt ////////////////////////////

else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_Tt_all"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15rightMod_It_1_mod.txt",
	"SC/5E15rightMod_It_2_mod.txt",
	"SC/5E15rightMod_It_3_mod.txt",
	"SC/5E15rightMod_It_4_mod.txt", 
	"SC/5E15rightMod_It_5_mod.txt"
     };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2min wait,3x,-26C,125V",
	"10min wait,3x,-26C,125V",
	"500ms wait,3x,-26C,125V",
	"10s wait,10x,-26C,125V",
	"10min wait,10x,-26C,125V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }   

else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_Tt_all_600"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15rightMod_It_6.txt",
	"SC/5E15rightMod_It_7_mod.txt",
	"SC/5E15rightMod_It_8_mod.txt",
	"SC/5E15rightMod_It_9_mod.txt", 
	//	"SC/5E15rightMod_It_10_mod.txt"
     };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"500ms wait,3x,-26C,125V",
	"10s wait,10x,-26C,125V",
	"2min wait,3x,-26C,125V",
	"10min wait,3x,-26C,125V",
	//"10min wait,10x,-26C,125V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }   

else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_Tt_600"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15rightMod_It_6.txt",
	"SC/5E15rightMod_It_7.txt",
	"SC/5E15rightMod_It_8.txt",
	"SC/5E15rightMod_It_9.txt",
	//	"SC/5E15rightMod_It_10.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"500ms wait,3x,-26C,125V",
	"10s wait,10x,-26C,125V",
	"2min wait,3x,-26C,125V",
	"10min wait,3x,-26C,125V",
	//"10min wait,10x,-26C,125V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_Tt"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15rightMod_It_2.txt",
	"SC/5E15rightMod_It_5.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"10 min wait, 3x, -26C, 125V",
	"10 min wait, 10x, -26C, 125V",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_Tt_short"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/5E15rightMod_It_3.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"500ms wait,3x,-26C,125V",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

else if(IVconfigID=="Foundry4_SC_irrad_bonded_5E15_2_Tt_medium"){
      TString fileNameArr[]    = { // file in which to find graph
		"SC/5E15rightMod_It_1.txt",
		"SC/5E15rightMod_It_4.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2min wait, 3x,-26C,125V",
	"10s wait,10x,-26C,125V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Foundry4_Tt_IV"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410001_IV_2.txt",
	"SC/1410002_IV_1.txt",
	"SC/1410003_IV_1.txt",
	"SC/1410004_IV_1.txt",
	"SC/1410005_IV_1.txt",
	"SC/1410006_IV_1.txt",
	"SC/1410007_IV_1.txt",
	"SC/1410008_IV_1.txt",
	"SC/1410009_IV_3.txt",
	"SC/1410010_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 1",  
	"Sensor 2",
	"Sensor 3",   
	"Sensor 4",  
	"Sensor 5",
	"Sensor 6",
	"Sensor 7",
	"Sensor 8",
	"Sensor 9",
	"Sensor 10"    
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    
    
    else if(IVconfigID=="Foundry4_1410007_Tt"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410007_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 7"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Foundry4_1410006_Tt"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410006_It_1.txt",
	"SC/1410006_It_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 6 ",
	"Sensor 6 RH3",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1410008_Tt"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410008_It_1.txt",
	"SC/1410008_It_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"RH 39-46",
	"RH 3-50"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }   

    else if(IVconfigID=="Foundry4_Tt"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410006_It_1.txt",
	"SC/1410006_It_2.txt",
	"SC/1410007_It_1.txt",
	"SC/1410008_It_1.txt",
	"SC/1410009_It_1.txt",
	"SC/1410010_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 6",
	"Sensor 6 RH=3%",
	"Sensor 7",
	"Sensor 8",
	"Sensor 9",
	"Sensor 10"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
  

  /////////////////////////  RHt ////////////////////////////
    
    else if(IVconfigID=="Foundry4_1410007_RHt"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410007_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 7"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Foundry4_1410006_RHt"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410006_It_1.txt",
	"SC/1410006_It_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 6 ",
	"Sensor 6 RH3",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_1410008_RHtmod"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410008_It_1.txt",
	"SC/1410008_It_2.txt",
	"SC/1410008_It_3_mod.txt",
	"SC/1410008_It_4_mod.txt",
	"SC/1410008_It_5_mod.txt"	//	"DortmundResults/1410008_It_1.txt"      
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Run 1",// H 39-46",
	"Run 2", //H 3-50 stopped dry air after 2 days",
	"Run 3", //H 51 5h at 0V before restart",
	"Run 4", //H 47 1.5 days at 0V before restart flushed 4h",
	"Run 5" //H 3 stopped flushing after 2 h"
	//	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }   



    else if(IVconfigID=="Foundry4_1410008_RHt"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410008_It_1.txt",
	"SC/1410008_It_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"RH 39-46",
	"RH 3-50"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }   

    else if(IVconfigID=="Foundry4_RHt"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410006_It_1.txt",
	"SC/1410006_It_2.txt",
	"SC/1410007_It_1.txt",
	"SC/1410008_It_1.txt",
	"SC/1410009_It_1.txt",
	"SC/1410010_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 6",
	"Sensor 6 RH=3%",
	"Sensor 7",
	"Sensor 8",
	"Sensor 9",
	"Sensor 10"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    ////////////////////// RHI //////////////////////
    else if(IVconfigID=="Foundry4_1410008_RHImod"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410008_It_1.txt",
	"SC/1410008_It_2.txt",
	"SC/1410008_It_3_mod.txt",
	"SC/1410008_It_4_mod.txt",
	"SC/1410008_It_5_mod.txt"//,
	//	"SC/1410008_It_6.txt"	//	"DortmundResults/1410008_It_1.txt"      
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Run 1", //H 39-46",
	"Run 2", // H 3-50 stopped dry air after 2 days",
	"Run 3", //H 51 5h at 0V before restart",
	"Run 4", //H 47 1.5 days at 0V before restart flushed 4h",
	"Run 5" //H 3 stopped flushing after 2 h",
	//	"RH20-47 tried to regulate"
	//	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }    

    else if(IVconfigID=="Foundry4_1410006_RHImod"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410006_IV_1_mod.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"9 IVs"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }   

    else if(IVconfigID=="Foundry4_1410008_RHImod"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410008_It_1.txt",
	"SC/1410008_It_2.txt",
	"SC/1410008_It_3_mod.txt",
	"SC/1410008_It_4_mod.txt",
	"SC/1410008_It_5_mod.txt"	//	"DortmundResults/1410008_It_1.txt"      
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"RH 39-46",
	"RH 3-50 stopped dry air after 2 days",
	"RH 51 5h at 0V before restart",
	"RH 47 1.5 days at 0V before restart flushed 4h",
	"RH 3 stopped flushing after 2 h"
	//	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }    

    else if(IVconfigID=="Foundry4_1410008_RHImod1"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410008_It_1.txt",
	//"SC/1410008_It_2.txt",
	//"SC/1410008_It_3_mod.txt",
	//"SC/1410008_It_4_mod.txt",
	//"SC/1410008_It_5_mod.txt"	//	"DortmundResults/1410008_It_1.txt"      
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"RH 39-46",
	//"RH 3-50 stopped dry air after 2 days",
	//"RH 51 5h at 0V before restart",
	//"RH 47 1.5 days at 0V before restart flushed 4h",
	//"RH 3 stopped flushing after 2 h"
	//	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }    

    else if(IVconfigID=="Foundry4_1410008_RHImod2"){
      TString fileNameArr[]    = { // file in which to find graph
	//"SC/1410008_It_1.txt",
	"SC/1410008_It_2.txt",
	//"SC/1410008_It_3_mod.txt",
	//"SC/1410008_It_4_mod.txt",
	//"SC/1410008_It_5_mod.txt"	//	"DortmundResults/1410008_It_1.txt"      
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"RH 39-46",
	"RH 3-50 stopped dry air after 2 days",
	//"RH 51 5h at 0V before restart",
	//"RH 47 1.5 days at 0V before restart flushed 4h",
	//"RH 3 stopped flushing after 2 h"
	//	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }    

    else if(IVconfigID=="Foundry4_1410008_RHImod3"){
      TString fileNameArr[]    = { // file in which to find graph
	//"SC/1410008_It_1.txt",
	//"SC/1410008_It_2.txt",
	"SC/1410008_It_3_mod.txt",
	//"SC/1410008_It_4_mod.txt",
	//"SC/1410008_It_5_mod.txt"	//	"DortmundResults/1410008_It_1.txt"      
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"RH 39-46",
	//"RH 3-50 stopped dry air after 2 days",
	"RH 51 5h at 0V before restart",
	//"RH 47 1.5 days at 0V before restart flushed 4h",
	//"RH 3 stopped flushing after 2 h"
	//	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }  

    else if(IVconfigID=="Foundry4_1410008_RHImod4"){
      TString fileNameArr[]    = { // file in which to find graph
	//"SC/1410008_It_1.txt",
	//"SC/1410008_It_2.txt",
	//"SC/1410008_It_3_mod.txt",
	"SC/1410008_It_4_mod.txt",
	//"SC/1410008_It_5_mod.txt"	//	"DortmundResults/1410008_It_1.txt"      
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"RH 39-46",
	//"RH 3-50 stopped dry air after 2 days",
	//"RH 51 5h at 0V before restart",
	"RH 47 1.5 days at 0V before restart flushed 4h",
	//"RH 3 stopped flushing after 2 h"
	//	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }     

    else if(IVconfigID=="Foundry4_1410008_RHImod5"){
      TString fileNameArr[]    = { // file in which to find graph
	//"SC/1410008_It_1.txt",
	//"SC/1410008_It_2.txt",
	//"SC/1410008_It_3_mod.txt",
	//"SC/1410008_It_4_mod.txt",
	"SC/1410008_It_5_mod.txt"	//	"DortmundResults/1410008_It_1.txt"      
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"RH 39-46",
	//"RH 3-50 stopped dry air after 2 days",
	//"RH 51 5h at 0V before restart",
	//"RH 47 1.5 days at 0V before restart flushed 4h",
	"RH 3 stopped flushing after 2 h"
	//	"Dortmund"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }     
 
   ///////////////////// CV /////////////////////
    
    else if(IVconfigID=="Foundry4_Sensors1-3,7_CV"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410001_CV_1.txt",
	"SC/1410001_CV_2.txt",
	"SC/1410002_CV_2.txt",
	"SC/1410002_CV_3.txt",
	"SC/1410003_CV_1.txt",
	"SC/1410003_CV_2.txt",
	"SC/1410007_CV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 1 1 kHz ",  
	"Sensor 1 10 kHz",
	"Sensor 2 1 kHz ",  
	"Sensor 2 10 kHz",
	"Sensor 3 1 kHz ",  
	"Sensor 3 10 kHz",
	"Sensor 7 10 kHz"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    
    else if(IVconfigID=="Foundry4_1410001_CV"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410001_CV_1.txt",
	"SC/1410001_CV_2.txt"//,
	//"DortmundResults/1410001_CV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1 kHz ",  
	"10 kHz"//,
	//	"Dortmund"      
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Foundry4_1410002_CV"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410002_CV_2.txt",
	"SC/1410002_CV_3.txt" //,
//	"DortmundResults/1410002_CV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1 kHz",  
	"10 kHz"//,
	//	"Dortmund"      
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

 else if(IVconfigID=="Foundry4_1410003_CV"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410003_CV_1.txt",
	"SC/1410003_CV_2.txt",
	//	"DortmundResults/1410002_CV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1 kHz",  
	"10 kHz"//,                              
	//	"Dortmund"      
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
      
 
    else if(IVconfigID=="Foundry4_1410007_CV"){
      TString fileNameArr[]    = { // file in which to find graph
	"SC/1410007_CV_1.txt"//,
	//	"DortmundResults/1410007_CV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name  
	"10 kHz"//,
	//	"Dortmund"      
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    else if(IVconfigID=="Foundry4_DCSensors1-20_CV"){
      TString fileNameArr[]    = { // file in which to find graph
	"DC/1420001_CV_2.txt",
	"DC/1420002_CV_1.txt",
	"DC/1420003_CV_1.txt",
	"DC/1420004_CV_2.txt",
	"DC/1420005_CV_1.txt",
	"DC/1420006_CV_1.txt",
	"DC/1420007_CV_1.txt",
	"DC/1420008_CV_1.txt",
	"DC/1420009_CV_1.txt",
	"DC/1420010_CV_1.txt",
	"DC/1420011_CV_1.txt",
	"DC/1420012_CV_1.txt",
	"DC/1420013_CV_1.txt",
	"DC/1420014_CV_1.txt",
	"DC/1420015_CV_1.txt",
	"DC/1420016_CV_1.txt",
	"DC/1420017_CV_1.txt",/*
	"DC/1420018_CV_1.txt",
	"DC/1420019_CV_1.txt",
	"DC/1420020_CV_1.txt"*/
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sensor 1 1 kHz ",  
	"Sensor 2 1 kHz ",  
	"Sensor 3 1 kHz ",  
	"Sensor 4 1 kHz ",
	"Sensor 5 1 kHz ",  
	"Sensor 6 1 kHz ",  
	"Sensor 7 1 kHz",
	"Sensor 8 1 kHz ",  
	"Sensor 9 1 kHz ",  
	"Sensor 10 1 kHz ",  
	"Sensor 11 1 kHz ",  
	"Sensor 12 1 kHz ",  
	"Sensor 13 1 kHz ",  
	"Sensor 14 1 kHz ",  
	"Sensor 15 1 kHz ",  
	"Sensor 16 1 kHz ",
	"Sensor 17 1 kHz", /* 
	"Sensor 18 1 kHz ",  
	"Sensor 19 1 kHz ",  
	"Sensor 20 1 kHz ",*/  
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    
    breakDownCurrent = 10e-6; // criterion to calculate VBD (threshold current [A])
    fixedVoltageForCurrentEval = 50.; // voltage [V] at which to evaluate the current
    fractionOfVBDForCurrentEval = 0.8; // fraction of breakdown voltage at which to evaluate the current
    NstepsX = 1; // in case of an array
    NstepsY = 1; // in case of an array
    inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/ITk_Sensor_MarketSurvey/Foundry4/SC/IrradiatedSensors/Modules/";
    outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITk_Sensor_MarketSurvey/Foundry4/SC/";	
    Vlo=0; Vhi=40; NbinsV=40; //CV
    //Vlo=0; Vhi=100; NbinsV=200; //IV
    //Vlo=0; Vhi=700; NbinsV=700; //It
    // Vlo=0.000; Vhi=55.; NbinsV=100; //RHI
    // VloLGAD=0; VhiLGAD=200;
    //Ilo=0.0; Ihi=30; // for IV
    Ilo=0.00; Ihi=100; // for It
    //   Ilo=-25.6; Ihi=-24.95; // for T
    //   Ilo=0.0; Ihi=55.; // for RH
    //Ilo=0.0; Ihi=0.016; // for RHI    

    IloZoom=0; IhiZoom=100; NbinsI=200;
    Clo=-0.1; Chi=1.e-1; C2hi=0.6e-4; 
    // initialise arrays to 0 if default should be used
    //CV color scheme
    //int colorArr[1000]     ={2,2,4,4,8,8,1,1}; 
    //int markerArr[1000]    ={20,24,21,25,22,26,23,32,33,27}; //{0}
    //IV color scheme
    //int colorArr[1000]     ={2,2,2,4,4,4,8,8,8,1,1,1,1};//,9,9,11,11,12,12,28,28,38,38,39,39}; 
    //int markerArr[1000]    ={20,26,21,32,23,25,22,21,25,22,26,22,21,32,22,23,25,24,26,23,32}; //,34,28,33,27,29,30,20,24,21,25,22,26}; //{0}
    int colorArr[1000]     ={9,2,95,92,11,39,12,28}; //T studies
    int markerArr[1000]    ={20,21,22,23,33,29,34,43,45,47,41}; // T studies    

    //int colorArr[1000]     ={1,1,2,2,4,8,61,64,66,9,38,39,8,9,11,12,28,38,39}; 
    //int colorArr[1000]     ={2,4,4,1,11,12,28,38}; 
    //int markerArr[1000]    ={20,21,22,33,23,24}; //{0}
   
    int lineStyleArr[1000] ={0};
    //do not change last lines:
    std::copy(colorArr, colorArr+1000, color_);
    std::copy(markerArr, markerArr+1000, marker_);
    std::copy(lineStyleArr, lineStyleArr+1000, lineStyle_);
  }


////////////////////////////////////////////
  if(IVconfigID.Contains("Foundry3")){
    ////////////////////////////////////////////
    
    if(IVconfigID.Contains("CV")){
      TString graphNameArr[] = {"Cap"}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    else{
      //TString graphNameArr[] = {"Chuck","","","","","",""}; // if always the same, put just one element
      //TString graphNameArr[] = {"","","","","","_X1_Y1"}; // if always the same, put just one element
      //TString graphNameArr[] = {"_X1_Y1"}; // if always the same, put just one element     
      TString graphNameArr[] = {""}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    
    specifier = IVconfigID;
    

if(IVconfigID=="Foundry3 irradiated 100#mum sensors"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry3/IrradiatedSensors/432-4_IV_1.txt",
	"Foundry3/IrradiatedSensors/432-4_IV_2.txt",
	//	"432-4_IV_3.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_1.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_2.txt",
	"Foundry3/IrradiatedSensors/434-4_IV_1.txt",
	"Foundry3/IrradiatedSensors/434-4_IV_2.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_1.txt",
	//"Foundry3/IrradiatedSensors/434-14_IV_2.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_3.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"S1 2E15 3d@RT",
	//"S1 100 #mum, 2E15 10d@RT",
	"S1 2E15 10d@RT",
	"S2 2E15 3d@RT",
	"S2 2E15 10d@RT",
	"S1 5E15 3d@RT",
	"S1 5E15 10d@RT",
	"S2 5E15 3d@RT",
	"S2 5E15 10d@RT",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

 if(IVconfigID=="Foundry3 irradiated 150#mum sensors"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry3/IrradiatedSensors/6100-15_IV_1.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_2.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_1.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"S1 2E15 3d@RT",
	"S1 2E15 10d@RT",
	"S2 5E15 3d@RT",
	"S2 5E15 10d@RT",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

 if(IVconfigID=="Foundry3 5E15 150#mum sensor T studies"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry3/IrradiatedSensors/6108-15_IV_4.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_5.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_6.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_11.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_8.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_2.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_3.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_10.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_9.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-20#circC",
	"-21#circC",
	"-22#circC",
	"-23#circC",
	"-24#circC",
	"-25#circC",
	"-26#circC",
	"-27#circC",
	"-28#circC",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry3 5E15 150#mum T studies"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry3/IrradiatedSensors/6108-15Mod_IV_3.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_4.txt",
	"Foundry3/IrradiatedSensors/6108-15Mod_IV_1.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_2.txt",
	"Foundry3/IrradiatedSensors/6108-15Mod_IV_4.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_3.txt",
	"Foundry3/IrradiatedSensors/6108-15Mod_IV_2.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_9.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-20#circC CC",
	"-20#circC PS",
	"-25#circC CC",
	"-25#circC PS",
	"-26#circC CC",
	"-26#circC PS",
	"-28#circC CC",
	"-28#circC PS",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry3 2E15 150#mum sensor T studies"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry3/IrradiatedSensors/6100-15_IV_7.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_6.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_5.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_4.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_3.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_2.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_8.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_9.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_10.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-20#circC",
	"-21#circC",
	"-22#circC",
	"-23#circC",
	"-24#circC",
	"-25#circC",
	"-26#circC",
	"-27#circC",
	"-28#circC",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry3 2E15 150#mum T studies"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry3/IrradiatedSensors/6100-15Mod_IV_6.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_7.txt",
	"Foundry3/IrradiatedSensors/6100-15Mod_IV_4.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_2.txt",
	"Foundry3/IrradiatedSensors/6100-15Mod_IV_7.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_8.txt",
	"Foundry3/IrradiatedSensors/6100-15Mod_IV_5.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_10.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-20#circC CC",
	"-20#circC PS",
	"-25#circC CC",
	"-25#circC PS",
	"-26#circC CC",
	"-26#circC PS",
	"-28#circC CC",
	"-28#circC PS",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


if(IVconfigID=="Foundry3 5E15 100#mum sensor T studies"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry3/IrradiatedSensors/434-14_IV_11.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_10.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_9.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_8.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_7.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_3.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_6.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_4.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_5.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-20#circC",
	"-21#circC",
	"-22#circC",
	"-23#circC",
	"-24#circC",
	"-25#circC",
	"-26#circC",
	"-27#circC",
	"-28#circC",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

if(IVconfigID=="Foundry3 2E15 100#mum sensor T studies"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry3/IrradiatedSensors/432-14_IV_3.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_4.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_5.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_6.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_7.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_2.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_8.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_9.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_10.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"-20#circC",
	"-21#circC",
	"-22#circC",
	"-23#circC",
	"-24#circC",
	"-25#circC",
	"-26#circC",
	"-27#circC",
	"-28#circC",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }



if(IVconfigID=="Foundry3 Foundry4 irradiated 150#mum sensors comparison"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry4/SC/IrradiatedSensors/Modules/2E15leftMod_IV_1.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/2E15right_IV_5.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/5E15left_IV_4.txt",
	"Foundry4/SC/IrradiatedSensors/BareSensors/5E15right_IV_5.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_2.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"F4 S1 2E15",
	"F4 S2 2E15",
	"F4 S1 5E15",
	"F4 S2 5E15",
	"F3 S1 2E15",
	"F3 S2 5E15",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


if(IVconfigID=="Foundry3 irradiated sensors"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry3/IrradiatedSensors/432-4_IV_1.txt",
	"Foundry3/IrradiatedSensors/432-4_IV_2.txt",
	//	"432-4_IV_3.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_1.txt",
	"Foundry3/IrradiatedSensors/432-14_IV_2.txt",
	"Foundry3/IrradiatedSensors/434-4_IV_1.txt",
	"Foundry3/IrradiatedSensors/434-4_IV_2.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_1.txt",
	"Foundry3/IrradiatedSensors/434-14_IV_2.txt",
	"Foundry3/IrradiatedSensors/6100-15_IV_1.txt",
	//"6100-15_IV_2.txt",
	"Foundry3/IrradiatedSensors/6108-15_IV_1.txt",
	//"6108-15_IV_2.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"S1 100 #mum, 2E15 3d@RT",
	//"S1 100 #mum, 2E15 10d@RT",
	"S1 100 #mum, 2E15 10d@RT",
	"S2 100 #mum, 2E15 3d@RT",
	"S2 100 #mum, 2E15 10d@RT",
	"S1 100 #mum, 5E15 3d@RT",
	"S1 100 #mum, 5E15 10d@RT",
	"S2 100 #mum, 5E15 3d@RT",
	"S2 100 #mum, 5E15 10d@RT",
	"S1 150 #mum, 2E15 3d@RT",
	//"S1 150 #mum, 2E15 10d@RT",
	"S2 150 #mum, 5E15 3d@RT",
	//"S2 150 #mum, 5E15 10d@RT",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

    if(IVconfigID=="Foundry3_W6097_SC"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1310001_IV_1.txt",
"W6097/1310002_IV_1.txt",
"W6097/1310003_IV_1.txt",
"W6097/1310004_IV_2.txt",
"W6097/1310005_IV_1.txt",
"W6097/1310006_IV_1.txt",
"W6097/1310007_IV_1.txt",
"W6097/1310008_IV_1.txt",
"W6097/1310009_IV_1.txt",
"W6097/1310010_IV_1.txt",
"W6097/1310011_IV_1.txt",
"W6097/1310012_IV_1.txt",
"W6097/1310013_IV_1.txt",
"W6097/1310014_IV_1.txt",
"W6097/1310015_IV_1.txt",
"W6097/1310016_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1310001", 
	"1310002",
	"1310003",
	"1310004",
	"1310005",
	"1310006",
	"1310007",
	"1310008",
	"1310009",
	"1310010",
	"1310011",
	"1310012",
	"1310013",
	"1310014",
	"1310015",
	"1310016"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

   if(IVconfigID=="Foundry3_W6097_DC"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1320001_IV_2.txt",
"W6097/1320002_IV_1.txt",
"W6097/1320003_IV_1.txt",
"W6097/1320004_IV_1.txt",
"W6097/1320005_IV_1.txt",
"W6097/1320006_IV_1.txt",
"W6097/1320007_IV_1.txt",
"W6097/1320008_IV_1.txt",
"W6097/1320009_IV_1.txt",
"W6097/1320010_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1320001", 
	"1320002",
	"1320003",
	"1320004",
	"1320005",
	"1320006",
	"1320007",
	"1320008",
	"1320009",
	"1320010"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

   if(IVconfigID=="Foundry3_W6097_QC"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1340001_IV_1.txt",
"W6097/1340002_IV_1.txt",
"W6097/1340003_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1340001", 
	"1340002",
	"1340003"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }


    if(IVconfigID=="Foundry3_W0416_SC"){
      TString fileNameArr[]    = { // file in which to find graph
	"W0416/2310001_IV_1.txt",
"W0416/2310002_IV_1.txt",
"W0416/2310003_IV_1.txt",
"W0416/2310004_IV_6.txt",
"W0416/2310005_IV_10.txt",
"W0416/2310006_IV_2.txt",
"W0416/2310007_IV_1.txt",
"W0416/2310008_IV_1.txt",
"W0416/2310009_IV_1.txt",
"W0416/2310010_IV_1.txt",
"W0416/2310011_IV_1.txt",
"W0416/2310012_IV_1.txt",
"W0416/2310013_IV_1.txt",
"W0416/2310014_IV_1.txt",
"W0416/2310015_IV_1.txt",
"W0416/2310016_IV_1.txt",
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2310001", 
	"2310002",
	"2310003",
	"2310004",
	"2310005",
	"2310006",
	"2310007",
	"2310008",
	"2310009",
	"2310010",
	"2310011",
	"2310012",
	"2310013",
	"2310014",
	"2310015",
	"2310016"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

   if(IVconfigID=="Foundry3_W0416_DC"){
      TString fileNameArr[]    = { // file in which to find graph
	"W0416/2320001_IV_1.txt",
"W0416/2320002_IV_2.txt",
"W0416/2320003_IV_3.txt",
"W0416/2320004_IV_2.txt",
"W0416/2320005_IV_1.txt",
"W0416/2320006_IV_1.txt",
"W0416/2320007_IV_1.txt",
"W0416/2320008_IV_1.txt",
"W0416/2320009_IV_1.txt",
"W0416/2320010_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2320001", 
	"2320002",
	"2320003",
	"2320004",
	"2320005",
	"2320006",
	"2320007",
	"2320008",
	"2320009",
	"2320010"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }

   if(IVconfigID=="Foundry3_W0416_QC"){
      TString fileNameArr[]    = { // file in which to find graph
	"W0416/2340001_IV_2.txt",
"W0416/2340002_IV_2.txt",
"W0416/2340003_IV_5.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2340001", 
	"2340002",
	"2340003"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
///////////////////////////////
/// It
//////////////////////////////
  if(IVconfigID=="Foundry3_1310006_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"Foundry3/W6097/1310006_It_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1310006_It_2 80V",
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
      Ilo=0.000; Ihi=0.035; // for It
      Vlo=0; Vhi=60; NbinsV=800; //It
    }
    if(IVconfigID=="Foundry3_1310004_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1310004_It_1.txt",
	"W6097/1310004_It_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1310004_It_1 80V",
	"1310004_It_2 80V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
      Ilo=0.000; Ihi=0.035; // for It
      Vlo=0; Vhi=60; NbinsV=800; //It
    }
    if(IVconfigID=="Foundry3_1310005_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1310005_It_1.txt",
	"W6097/1310005_It_2.txt"
	//"W6097/1310005_It_3.txt",
	//"W6097/1310005_It_4.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1310005_It_1 65V",
	"1310005_It_2 80V"
	//"1310005_It_3 80V RH up",
	//"1310005_It_4 80V RH dn"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
      Ilo=0.000; Ihi=0.01; // for It
//     Ilo=0.000; Ihi=0.4; // for It largeRange
      Vlo=0; Vhi=60; NbinsV=800; //It
    }
    if(IVconfigID=="Foundry3_2310004_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"W0416/2310004_It_1.txt",
	"W0416/2310004_It_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2310004_1 65V",
	"2310004_2 65V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
      Ilo=0.000; Ihi=0.11; // for It
      Vlo=0; Vhi=60; NbinsV=800; //It
    }
    if(IVconfigID=="Foundry3_2310005_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"W0416/2310005_It_1.txt",
	"W0416/2310005_It_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2310005_1 65V",
	"2310005_2 65V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
      Ilo=0.000; Ihi=0.003; // for It
      Vlo=0; Vhi=60; NbinsV=800; //It
    }
    if(IVconfigID=="Foundry3_2310006_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"W0416/2310006_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2310006_1 65V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
      Ilo=0.000; Ihi=0.003; // for It
      Vlo=0; Vhi=60; NbinsV=800; //It
    }
    if(IVconfigID=="Foundry3_All_It"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1310004_It_1.txt",
	"W6097/1310004_It_2.txt",
	"W6097/1310005_It_1.txt",
	"W6097/1310005_It_2.txt",
	//"W6097/1310005_It_3.txt",
	//"W6097/1310005_It_4.txt",
	"W0416/2310004_It_1.txt",
	"W0416/2310004_It_2.txt",
	"W0416/2310005_It_1.txt",
	"W0416/2310005_It_2.txt",
	"W0416/2310006_It_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1310004_1 80V",
	"1310004_2 80V",
	"1310005_1 65V",
	"1310005_2 80V",
	"2310004_1 65V",
	"2310004_2 65V",
	"2310005_1 65V",
	"2310005_2 65V",
	"2310006_1 65V"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
        Vlo=0; Vhi=90; NbinsV=800; //It
//    Ilo=0.000; Ihi=0.01; // for It zoom
    Ilo=0.000; Ihi=0.15; // for It
    }

/// special looks
   if(IVconfigID=="Foundry3_1310001"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1310001_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1310001_1"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
Ilo=0.0; Ihi=6.; // for IV
    }
   if(IVconfigID=="Foundry3_1310003"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1310003_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1310003_1"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
Ilo=0.0; Ihi=0.015; // for IV
    }
   if(IVconfigID=="Foundry3_1310004"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1310004_IV_1.txt",
"W6097/1310004_IV_2.txt",
"W6097/1310004_IV_3.txt",
"W6097/1310004_IV_4.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1310004_1", 
	"1310004_2 pre It2",
	"1310004_3 post It2",
	"1310004_4 post It2"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
Ilo=0.0; Ihi=0.06; // for IV
    }

   if(IVconfigID=="Foundry3_1310005"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1310005_IV_1.txt",
"W6097/1310005_IV_2.txt",
"W6097/1310005_IV_3.txt",
"W6097/1310005_IV_4.txt",
"W6097/1310005_IV_5.txt",
"W6097/1310005_IV_6.txt",
"W6097/1310005_IV_7.txt",
"W6097/1310005_IV_8.txt"
//"W6097/1310005_IV_9.txt",
//"W6097/1310005_IV_10.txt",
//"W6097/1310005_IV_11.txt",
//"W6097/1310005_IV_12.txt",
//"W6097/1310005_IV_13.txt",
//"W6097/1310005_IV_14.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1310005_1", 
	"1310005_2 pre It1",
	"1310005_3 post It1",
	"1310005_4 post It1",
	"1310005_5 post It1",
	"1310005_6 pre It2",
	"1310005_7 pre It2",
	"1310005_8 post It2"
	//"1310005_9 pre It3",
	//"1310005_10 post It3 RH49",
	//"1310005_11 post It4",
	//"1310005_12 post It4",
	//"1310005_13 after 6h",
	//"1310005_14 after night"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
Ilo=0.0; Ihi=4.; // for IV
    }
   if(IVconfigID=="Foundry3_1320001"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1320001_IV_2.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1320001_1"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
Ilo=0.0; Ihi=0.13; // for IV
    }
   if(IVconfigID=="Foundry3_1320004"){
      TString fileNameArr[]    = { // file in which to find graph
	"W6097/1320004_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"1320004_1"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
Ilo=0.0; Ihi=0.1; // for IV
    }

   if(IVconfigID=="Foundry3_2320007"){
      TString fileNameArr[]    = { // file in which to find graph
	"W0416/2320007_IV_1.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2320007_1"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
Ilo=0.0; Ihi=0.022; // for IV
    }

   if(IVconfigID=="Foundry3_2310004"){
      TString fileNameArr[]    = { // file in which to find graph
	"W0416/2310004_IV_1.txt",
"W0416/2310004_IV_2.txt",
"W0416/2310004_IV_3.txt",
"W0416/2310004_IV_4.txt",
"W0416/2310004_IV_5.txt",
"W0416/2310004_IV_6.txt",
"W0416/2310004_IV_7.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2310004_1", 
	"2310004_2 pre It1",
	"2310004_3 post It1",
	"2310004_4",
	"2310004_5",
	"2310004_6 pre It2",
	"2310004_7 post It2"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
Ilo=0.0; Ihi=1.; // for IV
    }

  if(IVconfigID=="Foundry3_2310005"){
      TString fileNameArr[]    = { // file in which to find graph
	"W0416/2310005_IV_1.txt",
"W0416/2310005_IV_2.txt",
"W0416/2310005_IV_3.txt",
"W0416/2310005_IV_4.txt",
"W0416/2310005_IV_5.txt",
"W0416/2310005_IV_6.txt",
"W0416/2310005_IV_7.txt",
"W0416/2310005_IV_8.txt",
"W0416/2310005_IV_9.txt",
"W0416/2310005_IV_10.txt",
"W0416/2310005_IV_11.txt",
"W0416/2310005_IV_12.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2310005_1", 
	"2310005_2",
	"2310005_3",
	"2310005_4",
	"2310005_5",
	"2310005_6",
	"2310005_7",
	"2310005_8 pre It1",
	"2310005_9 post It1",
	"2310005_10 no light",
	"2310005_11 pre It2",
	"2310005_12 post It2"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
Ilo=0.0; Ihi=10.; // for IV
    }

   if(IVconfigID=="Foundry3_2310006"){
      TString fileNameArr[]    = { // file in which to find graph
	"W0416/2310006_IV_1.txt",
"W0416/2310006_IV_2.txt",
"W0416/2310006_IV_3.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2310006_1", 
	"2310006_2 pre It1",
	"2310006_3 post It1"

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
Ilo=0.0; Ihi=10.; // for IV
    }

   if(IVconfigID=="Foundry3_light_vs_noLight"){
      TString fileNameArr[]    = { // file in which to find graph
	"W0416/2320002_IV_1.txt",
"W0416/2320002_IV_2.txt",
//"W0416/2340003_IV_1.txt",
"W0416/2340003_IV_3.txt",
"W0416/2340003_IV_5.txt",
"W0416/2310005_IV_1.txt",
"W0416/2310005_IV_7.txt",
"W0416/2310005_IV_10.txt"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"2320002_1 much light", 
	"2320002_2 less light",
//	"2340003_1 much light",
	"2340003_3 much light",
	"2340003_5 no light",
	"2310005_1 much light",
	"2310005_7 much light",
	"2310005_10 no light",

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
     
    
    breakDownCurrent = 10e-6; // criterion to calculate VBD (threshold current [A])
    fixedVoltageForCurrentEval = 50.; // voltage [V] at which to evaluate the current
    fractionOfVBDForCurrentEval = 0.8; // fraction of breakdown voltage at which to evaluate the current
    NstepsX = 1; // in case of an array
    NstepsY = 1; // in case of an array
    inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/ITk_Sensor_MarketSurvey/"; //needed for comparison with foundry4
    // inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/ITk_Sensor_MarketSurvey/Foundry3/IrradiatedSensors/";
    // outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITk_Sensor_MarketSurvey/Foundry4/";
    outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITk_Sensor_MarketSurvey/Foundry3/";	
    //Vlo=0; Vhi=700; NbinsV=700;
     Vlo=0.000; Vhi=55.; NbinsV=100; //RHI
//    VloLGAD=0; VhiLGAD=200;
    // Ilo=0.0; Ihi=100.; // for IV
//    Ilo=0.0; Ihi=0.02; // for IV zoom
    Ilo=0.0; Ihi=0.005; // for It
    //Ilo=19.0; Ihi=21; // for T
    //   Ilo=0.0; Ihi=55.; // for RH
    //Ilo=0.0; Ihi=0.016; // for RHI    

    IloZoom=0; IhiZoom=100e-9; NbinsI=15;
    Clo=-0.1; Chi=1.e-1; C2hi=0.6e-4; 
    // initialise arrays to 0 if default should be used
    //CV color scheme
    //int colorArr[1000]     ={2,2,4,4,8,8,9}; 
    //int markerArr[1000]    ={20,24,21,25,22,26,23,32,33,27}; //{0}
    //IV color scheme
    //    int colorArr[1000]     ={4,64,67,38,9,2,95,92,11,12,39,28};//,9,9,11,11,12,12,28,28,38,38,39,39}; //T studies
    int colorArr[1000]     ={4,4,2,2,95,95,11,11,12,39,28};//,9,9,11,11,12,12,28,28,38,38,39,39}; //T studies modules
    //int colorArr[1000]     ={4,4,64,64,2,2,95,95,67,92};//,9,9,11,11,12,12,28,28,38,38,39,39}; //all irradiated
    //int colorArr[1000]     ={4,64,2,95,67,92};//,9,9,11,11,12,12,28,28,38,38,39,39}; //comparison F3 and F4
    // int colorArr[1000]     ={67,67,92,92};//,9,9,11,11,12,12,28,28,38,38,39,39}; //150
    //int markerArr[1000]    ={20,24,21,25}; //150
    //int markerArr[1000]    ={20,24,21,25,22,26,23,32,33,29}; //standard
    int markerArr[1000]    ={20,24,21,25,22,26,23,32,33,29,34,43,45,47,41}; // comparison F3 and F4
    //int markerArr[1000]    ={20,21,22,23,33,29}; // comparison F3 and F4
//    int markerArr[1000]    ={20,24,21,25,22,23,26}; //for light vs no light study
//   int colorArr[1000]     ={1,1,2,2,4,4,4,39,8,9,11,12,28,38,39}; // for light vs no light study
   
 int lineStyleArr[1000] ={0};
    //do not change last lines:
    std::copy(colorArr, colorArr+1000, color_);
    std::copy(markerArr, markerArr+1000, marker_);
    std::copy(lineStyleArr, lineStyleArr+1000, lineStyle_);
  }
  
  
  
  
  
  
  ////////////////////////////////////////////
  if(IVconfigID.Contains("HLL_calibration")){
    ////////////////////////////////////////////
    
    if(IVconfigID.Contains("CV")){
      TString graphNameArr[] = {"Cap"}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    else{
      TString graphNameArr[] = {"Chuck"}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    
    specifier = IVconfigID;
    
    if(IVconfigID=="HLL_calibration_20C_2.3RH"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_W8_S2spA_T20C_Phi_RH2.3_BR1.root",
	"IV_W8_S2spA_T20C_Phi_RH2.3_BR2.root",
	"IV_W8_S2spA_T20C_Phi_RH2.3_BR3.root",
	"IV_W8_S2spA_T20C_Phi_RH2.3_BL1.root",
	"IV_W8_S2spA_T20C_Phi_RH2.3_BL2.root",
	"IV_W8_S2spA_T20C_Phi_RH2.3_BL3.root",
	"IV_W8_S2spA_T20C_Phi_RH2.3_TR1.root",
	"IV_W8_S2spA_T20C_Phi_RH2.3_TR2.root",
	"IV_W8_S2spA_T20C_Phi_RH2.3_TR3.root",
	"IV_W8_S2spA_T20C_Phi_RH2.3_TL1.root",
	"IV_W8_S2spA_T20C_Phi_RH2.3_TL2.root",
	"IV_W8_S2spA_T20C_Phi_RH2.3_TL3.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"W8S2spA BR1",
	"W8S2spA BR2",
	"W8S2spA BR3",
	"W8S2spA BL1",
	"W8S2spA BL2",
	"W8S2spA BL3",
	"W8S2spA TR1",
	"W8S2spA TR2",
	"W8S2spA TR3",
	"W8S2spA TL1",
	"W8S2spA TL2",
	"W8S2spA TL3"  
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    
    else if(IVconfigID=="HLL_calibration_sensors"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_W8S2spA_T20C_Phi0.root",
 	"IV_W8S2spB_T20C_Phi0.root",
	"IV_W6_SLIMSP_T20C_Phi0.root",
	"IV_W6_SLIMSP_T20C_Phi0_postlongTerm_RH44.root",
	"IV_W4_S2spB_T20C_Phi0.root",
	"IV_W3_S9_T20C_Phi0_RH26.root",
	"IV_W6_S10_T20C_Phi0_RH26.root",
	"IV_W6_S10_T20C_Phi0_RH34_new.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	/*	"W8S2spB",
	"W8S2spA",
	"W6_SLIMSP",
	"PLT W6_SLIMSP",
	"W4_S2spB",
	"W3_S9",
	"W6_S10",
	"PLT W6_S10" */
	"Sample A",
	"Sample B",
	"Sample C",
	"Sample C PIt",
	"Sample D",
	"Sample E",
	"Sample F",
	"Sample F PIt",

      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="HLL_calibration_20C_RH2_W6_SLIMSP"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_W6_SLIMSP_T20_RH2C_passivationopening_SE.root",
	"IV_W6_SLIMSP_T20_RH2C_passivationopening2_SE.root",
	"IV_W6_SLIMSP_T20_RH2C_passivationopening3_SE.root",
	"IV_W6_SLIMSP_T20_RH2C_passivationopening_LE.root",
	"IV_W6_SLIMSP_T20_RH2C_passivationopening2_LE.root",
	"IV_W6_SLIMSP_T20_RH2C_passivation_LE.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"W6_SLIMSP SE pass open",
	"W6_SLIMSP SE2 pass open",
	"W6_SLIMSP SE3 pass open",
	"W6_SLIMSP LE pass open",
	"W6_SLIMSP LE2 pass open",
	"W6_SLIMSP LE"    
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="HLL_calibration_20C_RH2_W4_S2spB"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_W4_S2spB_T20_RH2C_passivationopening_SE.root",
	"IV_W4_S2spB_T20_RH2C_passivationopening2_SE.root",
	"IV_W4_S2spB_T20_RH2C_passivationopening_LE.root",
	"IV_W4_S2spB_T20_RH2C_passivationopening2_LE.root",
	"IV_W4_S2spB_T20_RH2C_passivation_LE.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"W4_S2spB SE pass open",
	"W4_S2spB SE2 pass open",
	"W4_S2spB LE pass open",
	"W4_S2spB LE2 pass open",
	"W4_S2spB LE"    
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="HLL_calibration_20C_RH2_W3_S9"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_W3_S9_T20_RH2C_passivationopening_SE.root",
	"IV_W3_S9_T20_RH2C_passivation_SE.root",
	"IV_W3_S9_T20_RH2C_passivation_LE.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"W3_S9 SE pass open",
	"W3_S9 SE",
	"W3_S9 LE"    
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="HLL_calibration_20C_RH2_W6_S10"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_W6_S10_T20_RH2C_passivationopening_SE.root",
	"IV_W6_S10_T20_RH2C_passivation_SE.root",
	"IV_W6_S10_T20_RH2C_passivation_LE.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"W6_S10 SE pass open",
	"W6_S10 SE",
	"W6_S10 LE"    
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="HLL_calibration_W6_SLIMSP_T20C_differentRH"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_W6_SLIMSP_T20C_Phi0.root",
	"IV_W6_SLIMSP_T20C_Phi0_RH7.root",
	"IV_W6_SLIMSP_T20C_Phi0_RH4.root",
	"IV_W6_SLIMSP_T20C_Phi0_postlongTerm_RH44.root",
	"IV_W6_SLIMSP_T20C_Phi0_postlongTerm_RH4.9.root",
	"IV_W6_SLIMSP_T20C_Phi0_postlongTerm_RH3.9.root",
	"IV_W6_SLIMSP_T20C_Phi0_postlongTerm_RH3.6.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"RH 26%",  
	"RH 7%",  
	"RH 4%",
	"PLT RH 44%",
	"PLT RH 4.9%",
	"PLT RH 3.9%",
	"PLT RH 3.6%"       
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="HLL_calibration_W6_S10_T20C_differentRH"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_W6_S10_Phi0_RH26.root",
	"IV_W6_S10_Phi0_RH4.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"RH 26%",  
	"RH 4%"         
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="HLL_calibration_W6_SLIMSP_differentT"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_W6_SLIMSP_T20C_Phi0.root",
	"IV_W6_SLIMSP_T10C_Phi0.root",
	"IV_W6_SLIMSP_T0C_Phi0.root",
	"IV_W6_SLIMSP_T-25C_Phi0.root",
	"IV_W6_SLIMSP_T10C_Phi0_postlongTerm_RH3.5.root",
	"IV_W6_SLIMSP_T20C_Phi0_postlongTerm_RH3.6.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"T 20C",  
	"T 10C",  
	"T 0C",
	"T -25C",
	"PLT T 10C",
	"PLT T 20C"         
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="HLL_calibration_W6_S10_differentT"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_W6_S10_T20C_Phi0_RH6.root",
	"IV_W6_S10_T10C_Phi0.root",
	"IV_W6_S10_T0C_Phi0.root",
	"IV_W6_S10_T-25C_Phi0.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"T 20C",  
	"T 10C",  
	"T 0C",
	"T -25C"         
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="HLL_calibration_longtermIV"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_W6_S10_T20C_Phi0_longTermTestEvery10min14h_RH34.root",
	"IV_W6_SLIMSP_T20C_Phi0_longTermTestEvery10min24h_2nd.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"Sample F",  
	"Sample C"       
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="HLL_calibration_sensors_CV"){
      TString fileNameArr[]    = { // file in which to find graph
	//"CV_W6_SLIMSP_T20C_Phi0_f1kHz_RH26_3rdMeas.root",
	//"CV_W6_SLIMSP_T20C_Phi0_f10kHz_RH26.root",
	//"CV_W6_10_T20C_Phi0_f10kHz_RH28.root",
	//"CV_W6_S10_T20C_Phi0_f1kHz_RH28.root",
	"CV_W6_SLIMSP_T20C_Phi0_f1kHz_postlongTerm_RH34.root",
	"CV_W6_SLIMSP_T20C_Phi0_f10kHz_postlongTerm_RH34.root",
	//"CV_W6_SLIMSP_T20C_Phi0_f1kHz_postlongTerm_RH3.root",
	//"CV_W6_SLIMSP_T20C_Phi0_f10kHz_postlongTerm_RH3.root",
	//"CV_W6_S10_T20C_Phi0_f10kHz_RH34_new.root",
	//"CV_W6_S10_T20C_Phi0_f1kHz_RH34_new.root",
	//"CV_W6_S10_T20C_Phi0_f10kHz_RH4_new.root",
	//"CV_W6_S10_T20C_Phi0_f1kHz_RH3.6_new.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	//"W6_SLIMSP 1kHz old RH26",
	//"W6_SLIMSP 10kHz old RH26",
	//"W6_S10 10kHz old RH28",     
	//"W6_S10 1kHz old RH28", 
	"Sample C 1 kHz",
	"Sample C 10 kHz",
	//"W6_SLIMSP 1kHz RH3",
	//"W6_SLIMSP 10kHz RH3",
	//"W6_S10 10kHz RH34",     
	//"W6_S10 1kHz RH34",
	//"W6_S10 10kHz RH4",     
	//"W6_S10 1kHz RH4"    
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    breakDownCurrent = 10e-6; // criterion to calculate VBD (threshold current [A])
    fixedVoltageForCurrentEval = 50.; // voltage [V] at which to evaluate the current
    fractionOfVBDForCurrentEval = 0.8; // fraction of breakdown voltage at which to evaluate the current
    NstepsX = 1; // in case of an array
    NstepsY = 1; // in case of an array
    inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/ITk_Sensor_MarketSurvey/Calibration/";
    outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITk_Sensor_MarketSurvey/Calibration/";	
    //Vlo=0; Vhi=150; NbinsV=150; //200 and 200
    Vlo=0; Vhi=60; NbinsV=200; //200 and 200
    VloLGAD=0; VhiLGAD=200;
    Ilo=0; Ihi=6; //1e-6;
    IloZoom=0; IhiZoom=100e-9; NbinsI=15;
    //Clo=0; Chi=1.2e-7; C2hi=6e19; 
    Clo=0; Chi=2000.e1; C2hi=.6e-4;
    // initialise arrays to 0 if default should be used
    //CV color scheme
    int colorArr[1000]    ={2,4,2,2,8,9,4,4}; 
    int markerArr[1000]    ={20,21,21,25,20,24,23,27,22,26};
    int lineStyleArr[1000] ={0};
    //IV color scheme
    //int colorArr[1000]    ={1,39,4,4,8,9,2,2}; 
    //int markerArr[1000]    ={27,21,22,26,23,29,20,24}; //     25,20,24,23,27,22,26};
    //int lineStyleArr[1000] ={0};
    //do not change last lines:
    std::copy(colorArr, colorArr+1000, color_);
    std::copy(markerArr, markerArr+1000, marker_);
    std::copy(lineStyleArr, lineStyleArr+1000, lineStyle_);
  }
  
  ////////////////////////////////////////////
  else if(IVconfigID.Contains("Diode_calibration")){
    ////////////////////////////////////////////
    
    if(IVconfigID.Contains("CV")){
      TString graphNameArr[] = {"Cap"}; // if always the same, put just one element
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    else{
      //TString graphNameArr[] = {"GR","GR","Pad"}; // if always the same, put just one element -> adjust for every plot!
      TString graphNameArr[] = {"GR"}; // if always the same, put just one element 
      graphName_.insert(graphName_.end(), &graphNameArr[0], &graphNameArr[sizeof(graphNameArr)/sizeof(TString)]);
    }
    
    specifier = IVconfigID;
    
    
    if(IVconfigID=="Diode_calibration_20C_RH2_allIV_D6"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_CiS-350259-W19-0D6_1_T20_RH2C_GR.root",
	"IV_CiS-350259-W19-0D6_2_T20_RH2C_GR.root",
	"IV_CiS-350259-W19-0D6_3_T20_RH2C_GR.root",
	"IV_CiS-350259-W19-0D6_4_T20_RH2C_GR.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"D6_1",
	"D6_2",
	"D6_3",
	"D6_4"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Diode_calibration_20C_RH2_allIV_D3"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_CiS-350259-W19-0D3_1_T20_RH2C_GR.root",
	"IV_CiS-350259-W19-0D3_2_T20_RH2C_GR.root",
	"IV_CiS-350259-W19-0D3_3_T20_RH2C_GR.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"D3_1",
	"D3_2",
	"D3_3"  
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Diode_calibration_20C_RH2_allCV_D6"){
      TString fileNameArr[]    = { // file in which to find graph
	"CV_10kHz_CiS-350259-W19-0D6_1_T20_RH2C_GR.root",
	"CV_1kHz_CiS-350259-W19-0D6_1_T20_RH2C_GR.root",
	"CV_10kHz_CiS-350259-W19-0D6_2_T20_RH2C_GR.root",
	"CV_1kHz_CiS-350259-W19-0D6_2_T20_RH2C_GR.root",
	"CV_10kHz_CiS-350259-W19-0D6_3_T20_RH2C_GR.root",
	"CV_1kHz_CiS-350259-W19-0D6_3_T20_RH2C_GR.root",
	"CV_10kHz_CiS-350259-W19-0D6_4_T20_RH2C_GR.root",
	"CV_1kHz_CiS-350259-W19-0D6_4_T20_RH2C_GR.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"D6_1 10kHz",
	"D6_1 1kHz",
	"D6_2 10kHz",
	"D6_2 1kHz",
	"D6_3 10kHz",
	"D6_3 1kHz",
	"D6_4 10kHz",
	"D6_4 1kHz"     
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Diode_calibration_20C_RH2_allCV_D3"){
      TString fileNameArr[]    = { // file in which to find graph
	"CV_10kHz_CiS-350259-W19-0D3_1_T20_RH2C_GR.root",
	"CV_1kHz_CiS-350259-W19-0D3_1_T20_RH2C_GR.root",
	"CV_10kHz_CiS-350259-W19-0D3_2_T20_RH2C_GR.root",
	"CV_1kHz_CiS-350259-W19-0D3_2_T20_RH2C_GR.root",
	"CV_10kHz_CiS-350259-W19-0D3_3_T20_RH2C_GR.root",
	"CV_1kHz_CiS-350259-W19-0D3_3_T20_RH2C_GR.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"D3_1 10kHz",
	"D3_1 1kHz",
	"D3_2 10kHz",
	"D3_2 1kHz",
	"D3_3 10kHz",
	"D3_3 1kHz"    
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Diode_calibration_20C_RH2_CVcomp_D3_1"){
      TString fileNameArr[]    = { // file in which to find graph
	"CV_10kHz_CiS-350259-W19-0D3_1_T20_RH2C_GR.root",
	"CV_1kHz_CiS-350259-W19-0D3_1_T20_RH2C_GR.root",
	"CV_10kHz_CiS-350259-W19-0D3_1_T20_RH2C_Pad.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"D3_1 10kHz GR",
	"D3_1 1kHz GR",
	"D3_1 10kHz Pad"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    else if(IVconfigID=="Diode_calibration_20C_RH2_IVcomp_D3_1"){
      TString fileNameArr[]    = { // file in which to find graph
	"IV_CiS-350259-W19-0D3_1_T20_RH2C_Pad.root",
	"IV_CiS-350259-W19-0D3_1_T20_RH2C_GR.root",
	"IV_CiS-350259-W19-0D3_1_T20_RH2C_PadGR.root",
	"IV_CiS-350259-W19-0D3_1_T20_RH2C_PadGR.root"
      };
      fileName_.insert(fileName_.end(), &fileNameArr[0], &fileNameArr[sizeof(fileNameArr)/sizeof(TString)]);
      TString legNameArr[]    = { // corresponding legend name
	"D3_1 Pad only connected",
	"D3_1 GR only connected",
	"D3_1 Pad (with GR contacted)",
	"D3_1 GR (with Pad contacted)"
      };
      legName_.insert(legName_.end(), &legNameArr[0], &legNameArr[sizeof(legNameArr)/sizeof(TString)]);
    }
    
    breakDownCurrent = 10e-6; // criterion to calculate VBD (threshold current [A])
    fixedVoltageForCurrentEval = 50.; // voltage [V] at which to evaluate the current
    fractionOfVBDForCurrentEval = 0.8; // fraction of breakdown voltage at which to evaluate the current
    NstepsX = 1; // in case of an array
    NstepsY = 1; // in case of an array
    inputFileNameBase = "/work1/pixtests/Data/IV_CV_It_rawData/ITk_Sensor_MarketSurvey/";
    outputFileNameBase = "/work1/pixtests/Data/plotIV_CV_Itanalysis/ITk_Sensor_MarketSurvey/";	
    Vlo=0; Vhi=200; NbinsV=40; //200 and 200
    VloLGAD=0; VhiLGAD=200;
    Ilo=3E-6; Ihi=.2; //1e-6;
    IloZoom=0; IhiZoom=100e-9; NbinsI=15;
    //Clo=0; Chi=1.2e-7; C2hi=.06;
    Clo=0; Chi=2000.e1; C2hi=2.0e-4; 
    // initialise arrays to 0 if default should be used
    // CV colour and marker scheme
    int colorArr[1000]     ={4,4,2,2,8,8}; 
    int markerArr[1000]    ={20,24,21,25,22,26};
    // IV colour and marker scheme
    //int colorArr[1000]     ={4,2,8}; 
    //int markerArr[1000]    ={0};
    // IV comp colour and marker scheme
    //int colorArr[1000]     ={4,2,8,1}; 
    //int markerArr[1000]    ={20,21,22,23};
    
    int lineStyleArr[1000] ={0};
    //do not change last lines:
    std::copy(colorArr, colorArr+1000, color_);
    std::copy(markerArr, markerArr+1000, marker_);
    std::copy(lineStyleArr, lineStyleArr+1000, lineStyle_);
  }
  
}
#endif

