#ifndef myRootStyle_h
#define myRootStyle_h

// ========================================================
//  style
//
// ========================================================

#include "TROOT.h"
#include "TStyle.h"
#include "TSystem.h"

const int fontstyle=42;

void setMyRootStyle(TStyle& myStyle)
{
  myStyle.SetPalette(1);
    
  // ==============
  //  Canvas
  // ==============
	
  myStyle.SetCanvasBorderMode(0);
  myStyle.SetCanvasColor(kWhite);
  myStyle.SetCanvasDefH(600); //Height of canvas
  myStyle.SetCanvasDefW(600); //Width of canvas
  myStyle.SetCanvasDefX(0);   //Position on screen
  myStyle.SetCanvasDefY(0);
	
  // ==============
  //  Pad
  // ==============
	
  myStyle.SetPadBorderMode(0);
  // myStyle.SetPadBorderSize(Width_t size = 1);
  myStyle.SetPadColor(kWhite);
  myStyle.SetPadGridX(false);
  myStyle.SetPadGridY(false);
  myStyle.SetGridColor(0);
  myStyle.SetGridStyle(3);
  myStyle.SetGridWidth(1);
	
  // ==============
  //  Frame
  // ==============
	
  myStyle.SetFrameBorderMode(0);
  myStyle.SetFrameBorderSize(1);
  myStyle.SetFrameFillColor(0);
  myStyle.SetFrameFillStyle(0);
  myStyle.SetFrameLineColor(1);
  myStyle.SetFrameLineStyle(1);
  myStyle.SetFrameLineWidth(1);
	
  // ==============
  //  Histo
  // ==============

  myStyle.SetErrorX(0.0);
  myStyle.SetEndErrorSize(8);
	
  // myStyle.SetHistFillColor(1);
  // myStyle.SetHistFillStyle(0);
  // myStyle.SetHistLineColor(1);
  myStyle.SetHistLineStyle(0);
  myStyle.SetHistLineWidth(2);
  // myStyle.SetLegoInnerR(Float_t rad = 0.5);
  // myStyle.SetNumberContours(Int_t number = 20);

  // myStyle.SetErrorMarker(20);
	
  myStyle.SetMarkerStyle(20);
	
  // ==============
  //  Fit/function
  // ==============
	
  myStyle.SetOptFit(1);
  myStyle.SetFitFormat("5.4g");
  myStyle.SetFuncColor(2);
  myStyle.SetFuncStyle(1);
  myStyle.SetFuncWidth(1);
	
  // ==============
  //  Date
  // ============== 
	
  myStyle.SetOptDate(0);
  // myStyle.SetDateX(Float_t x = 0.01);
  // myStyle.SetDateY(Float_t y = 0.01);
	
  // =====================
  //  Statistics Box
  // =====================
	
  myStyle.SetOptFile(0);
  myStyle.SetOptStat(0); // To display the mean and RMS:   SetOptStat("mr");
  myStyle.SetStatColor(kWhite);
  myStyle.SetStatFont(fontstyle);
  myStyle.SetStatFontSize(0.025);
  myStyle.SetStatTextColor(1);
  myStyle.SetStatFormat("6.4g");
  myStyle.SetStatBorderSize(1);
  myStyle.SetStatH(0.1);
  myStyle.SetStatW(0.15);
  // myStyle.SetStatStyle(Style_t style = 1001);
  // myStyle.SetStatX(Float_t x = 0);
  // myStyle.SetStatY(Float_t y = 0);
	
  // ==============
  //  Margins
  // ==============

  myStyle.SetPadTopMargin(0.1);
  myStyle.SetPadBottomMargin(0.1);
  myStyle.SetPadLeftMargin(0.12);
  myStyle.SetPadRightMargin(0.13);
	
  // ==============
  //  Global Title
  // ==============
	
  myStyle.SetOptTitle(0);
  myStyle.SetTitleFont(fontstyle);
  myStyle.SetTitleColor(1);
  myStyle.SetTitleTextColor(1);
  myStyle.SetTitleFillColor(0);
  myStyle.SetTitleFontSize(0.05);
  // myStyle.SetTitleH(0); // Set the height of the title box
  // myStyle.SetTitleW(0); // Set the width of the title box
  // myStyle.SetTitleX(0); // Set the position of the title box
  // myStyle.SetTitleY(0.985); // Set the position of the title box
  // myStyle.SetTitleStyle(Style_t style = 1001);
   myStyle.SetTitleBorderSize(0);
   myStyle.SetTitleAlign(22);

   // ==============
   //  Legend
   // ==============

   myStyle.SetLegendBorderSize(0);
   //myStyle.SetLegendFillColor(-1);  // not available in 5.17
	
  // ==============
  //  Axis titles
  // ==============
	
  myStyle.SetTitleColor(1, "XYZ");
  myStyle.SetTitleFont(fontstyle, "XYZ");
  myStyle.SetTitleSize(0.05, "XYZ");
  // myStyle.SetTitleXSize(Float_t size = 0.02); // Another way to set the size?
  // myStyle.SetTitleYSize(Float_t size = 0.02);
  myStyle.SetTitleXOffset(0.9);
  myStyle.SetTitleYOffset(1.2);
  // myStyle.SetTitleOffset(1.1, "Y"); // Another way to set the Offset
	
  // ==============
  //  Axis Label
  // ==============
	
  //myStyle.SetLabelColor(1, "XYZ");
  myStyle.SetLabelFont(fontstyle, "XYZ");
  myStyle.SetLabelOffset(0.007, "XYZ");
  myStyle.SetLabelSize(0.04, "XYZ");
	
  // ==============
  //  Axis
  // ==============
	
  myStyle.SetAxisColor(1, "XYZ");
  myStyle.SetStripDecimals(kTRUE);
  myStyle.SetTickLength(0.03, "XYZ");
  myStyle.SetNdivisions(510, "XYZ");
  myStyle.SetPadTickX(1);  // To get tick marks on the opposite side of the frame
  myStyle.SetPadTickY(1);
	
  // Change for log plots:
  myStyle.SetOptLogx(0);
  myStyle.SetOptLogy(0);
  myStyle.SetOptLogz(0);
	
  // ==============
  //  Text
  // ==============
	
  myStyle.SetTextAlign(11);
  myStyle.SetTextAngle(0);
  myStyle.SetTextColor(1);
  myStyle.SetTextFont(fontstyle);
  myStyle.SetTextSize(0.05);
	
  // =====================
  //  Postscript options:
  // =====================
	
  myStyle.SetPaperSize(20.,20.);
  // myStyle.SetLineScalePS(Float_t scale = 3);
  // myStyle.SetLineStyleString(Int_t i, const char* text);
  // myStyle.SetHeaderPS(const char* header);
  // myStyle.SetTitlePS(const char* pstitle);
	
  // myStyle.SetBarOffset(Float_t baroff = 0.5);
  // myStyle.SetBarWidth(Float_t barwidth = 0.5);
  // myStyle.SetPaintTextFormat(const char* format = "g");
  // myStyle.SetPalette(Int_t ncolors = 0, Int_t* colors = 0);
  // myStyle.SetTimeOffset(Double_t toffset);
  // myStyle.SetHistMinimumZero(kTRUE);
}

#endif
