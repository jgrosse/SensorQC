#Calibration run
#########################
#RD53A bare modules
#############################
#root -l -b -q analyseIV.C++'("RD53A bare modules comparison")'
#root -l -b -q analyseIV.C++'("RD53A bare module 3 comparison with vendor QC")'
#root -l -b -q analyseIV.C++'("RD53A bare module 1 comparison with vendor QC")'
#root -l -b -q analyseIV.C++'("RD53A bare module 2 comparison with vendor QC")'
 
#root -l -b -q analyseIV.C++'("RD53A bare module 4 FE comparison")'
#root -l -b -q analyseIV.C++'("RD53A bare module 4 FE comparison with last meas")'

#root -l -b -q analyseIV.C++'("RD53A bare module thin")'
#root -l -b -q analyseIV.C++'("RD53A bare module light off or on comparison")'

#root -l -b -q analyseCV.C++'("RD53A 3379-10-Q3 CV V_DP")'
#root -l -b -q analyseCV.C++'("RD53A 3378-1-Q1 CV V_DP")'
#root -l -b -q analyseCV.C++'("RD53A 3377-7-Q1 CV V_DP")'
#root -l -b -q analyseCV.C++'("RD53A 3377-6-Q1 CV V_DP")'
#root -l -b -q analyseCV.C++'("RD53A 3377-2-Q5 CV V_DP")'
#root -l -b -q analyseCV.C++'("RD53A 3377-5-Q2 CV V_DP")'
#root -l -b -q analyseCV.C++'("RD53A 3377-5-Q3 CV V_DP")'
#root -l -b -q analyseCV.C++'("RD53A HPK201218-3 CV V_DP")'

#root -l -b -q analyseCV.C++'("RD53A bare modules CV")'
#root -l -b -q analyseIV_works.C++'("RD53A BM")'

root -l -b -q analyseIV_works.C++'("RD53A BM IV try")'
root -l -b -q analyseCV.C++'("RD53A BM CV try")'
