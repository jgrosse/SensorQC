CONFIG += qt
equals(QT_MAJOR_VERSION, 5) {
  QT+= widgets
}
unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, 1[0-9].[0-9].[0-9]) {
     CONFIG += c++17
  } else {
    contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
       CONFIG += c++14
    } else {
       contains(GCC_VERSION, 4.[8-9].[0-9]) {
         CONFIG += c++11
       } else {
         message( "unknown g++ version: ")
         message($$GCC_VERSION)
       }
    }
  }
} else {
  CONFIG += c++11
}
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release
