#ifndef basicFunctions_h
#define basicFunctions_h

/// This header file contains very general functions that can be useful for any root macro

#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <map>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <typeinfo>

#include <TROOT.h>
#include <TSystem.h>

#include <TH1F.h>
#include <TH2F.h>
#include <TGraphErrors.h>
#include <THStack.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TKey.h>
#include <TGraphAsymmErrors.h>
#include <TString.h>

#include <TLine.h>
#include <TBox.h>
#include <TPaveLabel.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TStyle.h>
#include <TF1.h>
#include <TBox.h>
#include <TGaxis.h>
#include <TError.h>
#include <TMath.h>
#include <TVector3.h>

//Fabian testing to include matrix inversion
#include <TMatrixD.h>


using namespace std;

#include "myRootStyle.h"

int color_[]     ={1, 2, 4, 6, 7, 8, 9, 11, 12, 13, 14, 15, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15};
int marker_[]    ={20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
int lineStyle_[] ={1,2,3,4,1,1,1,1,1,1,1,1,1,1,1,1,1};

/// structure to contain cut of two different variables
struct cut_struct {
	TString var1;
	TString var2;
	double cutValue1Lo;
	double cutValue1Hi;
	double cutValue2Lo;
	double cutValue2Hi;
	/// constructor
	cut_struct(TString var1Ini="", double cutValue1LoIni=-11111, double cutValue1HiIni=11111,
			 TString var2Ini="", double cutValue2LoIni=-11111, double cutValue2HiIni=11111)
	        : var1(var1Ini), cutValue1Lo(cutValue1LoIni), cutValue1Hi(cutValue1HiIni),
	          var2(var2Ini), cutValue2Lo(cutValue2LoIni), cutValue2Hi(cutValue2HiIni){}
};

TH1 *histFromFile(const char *fileId, const char *histName)
{
  TH1 *result = 0;

  // open file, but first check whether already open
  const TString fileName(fileId);
  TFile *file = static_cast<TFile*>(gROOT->GetListOfFiles()->FindObject(fileName));
  if (!file) file = TFile::Open(fileName);

  // get TH1
  if (file) file->GetObject(histName, result);
  else std::cout << "histFromFile: no file " << std::endl;

  if (!result) std::cout << "histFromFile: no '" << histName << "' in file."
			 << std::endl;
  return result;
}

// this function is meant to provide a dummy graph to be able to setRangeUser as desired
//   between x1,y1 and x2,y2
TGraph* dummyGraph( double x1, double y1, double x2, double y2, TString title=""){
	TGraph* dummy = new TGraph(2);
	dummy->SetPoint(0,x1,y1);
	dummy->SetPoint(1,x2,y2);
	dummy->SetTitle(title);
	dummy->SetLineColor(0);
	dummy->SetMarkerColor(0);
	dummy->SetMarkerSize(0);
	dummy->GetXaxis()->SetRangeUser(x1,x2);
	dummy->GetYaxis()->SetRangeUser(y1,y2);
	return dummy;
}

// this function is meant to provide a dummy HISTO to be able to setRangeUser as desired
//   between x1,y1 and x2,y2
TH1F* dummyHisto( double x1, double y1, double x2, double y2, TString title=""){
	TH1F* dummy = new TH1F("dummy", "dummy", 1000, 0, 1000);
	dummy->SetTitle(title);
	dummy->SetTitleFont(42);
	dummy->SetLineColor(0);
	dummy->SetMarkerColor(0);
	dummy->SetMarkerSize(0);
	dummy->GetXaxis()->SetRangeUser(x1,x2);
	dummy->GetYaxis()->SetRangeUser(y1,y2);
	return dummy;
}

TPaveText * DrawLabel(TString text, const double x1, const double y1, const double x2, const double y2, int centering=12, double textSize=0.04)
  {
    // function to directly draw a label into the active canvas
    // the coordinates of the window are: "x1", "y1", "x2", "y2"
    // the label will contain the text "text" and the textsize is "textSize"
    // modified quantities: the active canvas
    // used functions: NONE
    // used enumerators: NONE

    TPaveText *label = new TPaveText(x1, y1, x2, y2, "br NDC");
    label->AddText(text);
    label->SetFillStyle(0);
    label->SetBorderSize(0);
    if(textSize!=0) label->SetTextSize(textSize);
    label->SetTextAlign(centering);
    label->Draw("same");
	return label;
  }


void PlotHistosInOneCanvas(int nHis, vector<TString> files, vector<TString> hisNames, TCanvas *Canv=0, TLegend* leg=0,
		                   vector<TString> legText=vector<TString>(), TString legOpt="l", bool norm=false, TString drawOpt="hist",
		                   double xRangeLo=-11, double xRangeHi=-11, double yRangeLo=-11, double yRangeHi=-11,
		                   vector<int> markers=vector<int>(), vector<int> colors=vector<int>(), vector<int> lineStyles=vector<int>(),
		                   int verbose=1)
{
	/// Function to plot several histos in one canvas;
	/// Histos are loaded from vector of TFiles
	/// Legend is created optionally if passed as argument

	/// Maybe also pass vector by reference? E.g. vector<TString>& files

	/// initialise style parameters if not passed to function
	if(markers.size()==0)    markers.assign(marker_, marker_ + sizeof(marker_)/sizeof(int));
	if(colors.size()==0)     colors.assign(color_, color_ + sizeof(color_)/sizeof(int));
	if(lineStyles.size()==0) lineStyles.assign(lineStyle_, lineStyle_ + sizeof(lineStyle_)/sizeof(int));

	/// check valid length of arrays
	if( files.size() != nHis || hisNames.size() != nHis){
		std::cout << "ERROR! Vector size of files or hisNames != nHis" << std::endl;
		return;
	}
	if(leg!=0) {
		if(legText.size() != nHis){
			std::cout << "ERROR! Vector size of legText != nHis" << std::endl;
			return;
		}
	}
	if(markers.size() < nHis || colors.size() < nHis || lineStyles.size() < nHis){
		std::cout << "ERROR! Vector size of markers, colors or lineSyles < nHis" << std::endl;
		return;
	}

	TString drawOptSame = "";
	if(Canv==0) Canv=new TCanvas("c", "c", 1);
	Canv->cd();

	vector<TH1*> his_= vector<TH1*>();
	double maxY=0; /// maximum y range

	for(int i=0; i<nHis; i++){
		his_.push_back((TH1*)histFromFile(files[i], hisNames[i])->Clone(hisNames[i]+legText[i]));
		if(norm ) {
			if(his_[i]->Integral()!=0){
				his_[i]->Scale(1/his_[i]->Integral());
			}
			else {
				std::cout<< "ERROR Histo Integral ==0. No normalisation possible." << std::endl;
				exit(0);
			}
			his_[i]->GetYaxis()->SetTitle( TString(his_[i]->GetYaxis()->GetTitle()) + " (norm.)");
		}
		if(i==1) drawOptSame = " same";
		his_[i]->SetMarkerStyle(markers[i]);
		his_[i]->SetMarkerColor(colors[i]);
		his_[i]->SetLineColor(colors[i]);
		his_[i]->SetLineStyle(lineStyles[i]);

		his_[i]->Draw(drawOpt+drawOptSame);
		if(leg!=0) leg->AddEntry(his_[i], legText[i], legOpt);

		maxY = TMath::Max(maxY, his_[i]->GetMaximum());
	}
	/// set axis range
	if(xRangeLo!=-11 && xRangeHi!=-11) his_[0]->GetXaxis()->SetRangeUser(xRangeLo, xRangeHi);
	if(yRangeLo!=-11 && yRangeHi!=-11) his_[0]->GetYaxis()->SetRangeUser(yRangeLo, yRangeHi);
	else               	               his_[0]->GetYaxis()->SetRangeUser(0, 1.1*maxY);

	if(leg!=0) leg->Draw();

}



void multiplyGraph(TGraph* graph, double multX, double multY){
  double* Xarr=graph->GetX();
  double* Yarr=graph->GetY();
  for(int i=0; i<graph->GetN(); i++){
    graph->SetPoint(i, Xarr[i]*multX, Yarr[i]*multY);
  }
}

void setupCV(TGraph* graph){
  double* Xarr=graph->GetX();
  double* Yarr=graph->GetY();
  for(int i=0; i<graph->GetN(); i++){
    graph->SetPoint(i,Xarr[i], 1/pow(Yarr[i],2));
  }
}
double getMeanTemp(const char *fileID){
  double result=0;
  const TString fileName1(fileID);
  FILE *file1 = fopen(fileName1, "r");
  int cfl = 0;
  int count = 0;
  int head = 6;
  char line1[1000] = {0};
  double value1[6];

  while(fgets(line1,1000, file1) != NULL){
    if(cfl >= head){
      sscanf(&line1[0]  ,"%lg %lg %lg %lg %lg %lg",&value1[0],&value1[1],&value1[2],&value1[3],&value1[4],&value1[5]);
      result += value1[4];
      count++;
    }
    cfl++;
  }
  return result/count;
}

//function to fit CV curve to
Double_t myfunc (Double_t *x, Double_t *par){
	  Double_t xx=x[0];
	  return -par[0]*abs(xx-par[1])+par[0]*xx+par[2];
}

Double_t myfunc2 (Double_t *x, Double_t *par){
  Double_t xx=x[0];
  if(xx<par[0]) return par[1]*xx;
  else return par[1]*par[0];
}

Double_t myfunc3 (Double_t *x, Double_t *par){ //worked with right initialisation
  Double_t xx=x[0];
  if(xx<par[0]) return par[1]*xx+par[2];
  else return par[3];
}

TGraph *smoothTemperature(const char *fileID, const double temp){
  TGraph *result = new TGraph();
  const TString fileName1(fileID);
  FILE *file1 = fopen(fileName1, "r");
  int cfl = 0;
  int cgl = 0;
  int head = 6;
  char line1[1000] = {0};
  double value1[6];
  double T1=temp+273.15;
  double T2;
  double Eg = 1.2;
  double kt = 8.617e-5;

  while(fgets(line1,1000, file1) != NULL){
    if(cfl >= head){
      sscanf(&line1[0]  ,"%lg %lg %lg %lg %lg %lg",&value1[0],&value1[1],&value1[2],&value1[3],&value1[4],&value1[5]);
      T2 = value1[4]+273.15;
      result->SetPoint(cgl, value1[1],value1[2]*pow(T1/T2, 2)*exp(Eg*(T1-T2)/(2*kt*T1*T2)));
      cgl++;
    }
    cfl++;
  }
  multiplyGraph(result, -1, -1);
  fclose(file1);
  return result;
}

double tempFromCurrentRatio(double ratio,double tempcomp, double starttemp){//ratio = I(want temp)/I(compare)
  double Eg = 1.2;
  double kt = 8.617e-5;
  double step = 0.1;
  double T1 = starttemp+273.15;
  double T2 = tempcomp+273.15;
  double dislast;
  double disnext;
  bool switched=false;
  bool contin= true;
  for(int i=0; i<100; i++){
    dislast = pow(T1/T2, 2)*exp(Eg*(T1-T2)/(2*kt*T1*T2))-ratio;
    T1 += step;
    disnext = pow(T1/T2, 2)*exp(Eg*(T1-T2)/(2*kt*T1*T2))-ratio;
    if(abs(disnext)>abs(dislast)){
      if(switched){
	return T1-step-273.15;
      }
      else{
	step *= -1;
	switched = true;
	T1 += step;
      }
    }
    else{
      switched = false;
    }
  }
  return -11;
}

TGraph *compareTemperature(const char *file1ID, const char *file2ID, double Eg){
  TGraph *result = new TGraph();
  const TString fileName1(file1ID);
  const TString fileName2(file2ID);
  FILE *file1 = fopen(fileName1, "r");
  FILE *file2 = fopen(fileName2, "r");
  int cfl = 0;
  int cgl = 0;
  int head = 6;
  char line1[1000] = {0};
  char line2[1000] = {0};
  double value1[6];
  double value2[6];
  double T1;
  double T2;
  //double Eg = 1.137;
  double kt = 8.617e-5;

  while((fgets(line1,1000, file1) != NULL)&&(fgets(line2,1000,file2) != NULL)){
    if(cfl >= head){
      sscanf(&line1[0]  ,"%lg %lg %lg %lg %lg %lg",&value1[0],&value1[1],&value1[2],&value1[3],&value1[4],&value1[5]);
      sscanf(&line2[0],"%lg %lg %lg %lg %lg %lg",&value2[0],&value2[1],&value2[2],&value2[3],&value2[4],&value2[5]);
      T1 = value1[4]+273.15;
      T2 = value2[4]+273.15;
      result->SetPoint(cgl, value1[1], pow(T1/T2, 2)*exp(Eg*(T1-T2)/(2*kt*T1*T2)));
      cgl++;
    }
    cfl++;
  }
  multiplyGraph(result, -1, 1);
  fclose(file1);
  fclose(file2);
  return result;
}

double findMin(double arr[], int n){
  double temp=abs(arr[0]);
  for(int i=0; i<n; i++){
    if (abs(arr[i])<temp) temp=abs(arr[i]);
  }
  return temp;
}

double findMax(double arr[], int n){
  double temp=abs(arr[0]);
  for(int i=0; i<n; i++){
    if (abs(arr[i])>temp) temp=abs(arr[i]);
  }
  return temp;
}

TGraph *scaleToTemperature(const char *fileId, double temp){
  TGraph *result = new TGraph();
  const TString fileName(fileId);
  int cfl = 0; //current file line
  int cgl = 0; //current graph line
  int head = 6; //number of lines to skip
  char line[1000] = {0};
  double value[6];
  FILE *file = fopen(fileName, "r");
  double T1=temp+273.15;
  double T2;
  double Eg = 1.2;
  double kt = 8.617e-5;
    
  if (!file) {
    cout <<"No File" << endl;
  }
    
  while(fgets(line,1000, file) != NULL){
    if(cfl >= head){
      scanf(&line[0]  ,"%lg %lg %lg %lg %lg %lg",&value[0],&value[1],&value[2],&value[3],&value[4],&value[5]);
      T2 = value[4]+273.15;
      result->SetPoint(cgl,-value[1], value[2]*pow(T1/T2, 2)*exp(Eg*(T1-T2)/(2*kt*T1*T2)));
      cgl++;
      cout << "works" << endl;
    }
    cfl++;
  }
  fclose(file);
  return result;
}


TGraph *graphFromFile(const char *fileId, const char *histName)
{
  TGraphErrors *result = new TGraphErrors();

      // open file, but first check whether already open
      const TString fileName(fileId);
  if (fileName.Contains(".root")){
      TFile *file = static_cast<TFile*>(gROOT->GetListOfFiles()->FindObject(fileName));
      if (!file) file = TFile::Open(fileName);

      // get TH1
      if (file) file->GetObject(histName, result);
      else std::cout << "histFromFile: no file " << std::endl;

      if (!result) std::cout << "histFromFile: no '" << histName << "' in file."
			     << std::endl;
  }
  else if (fileName.Contains(".txt")){
    int cfl = 0; //current file line
    int cgl = 0; //current graph line
    int head = 6; //number of lines to skip
    char line[1000] = {0};
    double value[6];
    FILE *file = fopen(fileName, "r");
    
    if (!file) {
      cout <<"No File" << endl;
    }
    
    while(fgets(line,1000, file) != NULL){
      if(cfl >= head){
	sscanf(&line[0]  ,"%lg %lg %lg %lg %lg %lg",&value[0],&value[1],&value[2],&value[3],&value[4],&value[5]); 
	if(fileName.Contains("_IV_")){
	  result->SetPoint(cgl, value[1], value[2]);//value4->value2
        }

	else if(fileName.Contains("_CV_")){
	    result->SetPoint(cgl, value[1], value[4]);
        }
	cgl++;
      }
      cfl++;
    }
    fclose(file);
  }
  if (fileName.Contains("_IV_")){
    multiplyGraph(result, -1.0, -1.0);//1->-1
  }
  else if (fileName.Contains("_CV_")){
    multiplyGraph(result, -1.0, 1.0);
    setupCV(result);
      
  }
  double x=-11;
  double y=-11;
  for (int i=0; i < result->GetN(); i++){
    result ->GetPoint(i, x, y);
    // cout << "line" << i  << " x=" << x << " y=" << y  << endl;
  }
 
  return result;
}




      

TGraph *plotTempFromFile(const char *fileID){
  TGraph *result = new TGraph();
  const TString fileName(fileID);
  int cfl = 0; //current file line
  int cgl = 0; //current graph line
  int head = 6; //number of lines to skip
  char line[1000] = {0};
  double value[6];
  FILE *file = fopen(fileName, "r");
    
  if (!file) {
    cout <<"No File here" << endl;
  }
    
  while(fgets(line,1000, file)){
    if(cfl >= head){
      sscanf(&line[0]  ,"%lg %lg %lg %lg %lg %lg",&value[0],&value[1],&value[2],&value[3],&value[4],&value[5]); 
      result->SetPoint(cgl, value[1], value[4]);
      cgl++;
      // cout <<"temp point " << cgl << "set"<< endl;
    }
    cfl++;
    // cout << "cfl= " << cfl << endl;
  }
  multiplyGraph(result, -1, 1);
  fclose(file);
  return result;
}

TGraph *tempDiffGraph(const char *unirrad, const char *irrad){
  TGraph *unirrad_graph=plotTempFromFile(unirrad);
  TGraph *irrad_graph=plotTempFromFile(irrad);
  TGraph *diff=new TGraph();
  double *Xarr=unirrad_graph->GetX();
  double *Yunirrad=unirrad_graph->GetY();
  double *Yirrad=irrad_graph->GetY();
  for(int i=0; i<unirrad_graph->GetN(); i++){
    diff->SetPoint(i, Xarr[i], Yirrad[i]-Yunirrad[i]);
  }
  return diff;
}

double theoryDeltaI(double fluence, double volume, double alpha){
  return fluence*volume*alpha;
}

double scaleFactor(double from, double to, double Eg){
  if (Eg==0) Eg = 1.21;
  double kt = 8.617e-5;
  double T1 = to + 273.15;
  double T2 = from +273.15;
  return pow(T1/T2, 2)*exp(Eg*(T1-T2)/(2*kt*T1*T2));
}

TGraph *realTempGraph(const char *compareFile, const char *toFile){
  TGraph *comparetemp = plotTempFromFile(compareFile);
  TGraph *comparecurrent = graphFromFile(compareFile, "wurst");
  TGraph *tocurrent = graphFromFile(toFile, "wurst");
  TGraph *realTemp= new TGraph();
  double *Xarr = comparetemp->GetX();
  double *Ycomptemp = comparetemp->GetY();
  double *Ycompcurr = comparecurrent->GetY();
  double *Ytocurr = tocurrent->GetY();
  for(int i=0; i<comparetemp->GetN();i++){
    realTemp->SetPoint(i, Xarr[i], tempFromCurrentRatio(Ytocurr[4*i]/Ycompcurr[i],Ycomptemp[i],-25));
  }
  return realTemp;
}





void PlotGraphsInOneCanvas(int nHis, vector<TString> files, vector<TString> hisNames, TCanvas *Canv=0, TLegend* leg=0,
		                   vector<TString> legText=vector<TString>(), TString legOpt="l", bool norm=false, TString drawOpt="LP",
		                   double xRangeLo=-11, double xRangeHi=-11, double yRangeLo=-11, double yRangeHi=-11,
		                   vector<int> markers=vector<int>(), vector<int> colors=vector<int>(), vector<int> lineStyles=vector<int>(),
		                   int verbose=1)
{
	/// Function to plot several histos in one canvas;
	/// Histos are loaded from vector of TFiles
	/// Legend is created optionally if passed as argument

	/// Maybe also pass vector by reference? E.g. vector<TString>& files

	/// initialise style parameters if not passed to function
	if(markers.size()==0)    markers.assign(marker_, marker_ + sizeof(marker_)/sizeof(int));
	if(colors.size()==0)     colors.assign(color_, color_ + sizeof(color_)/sizeof(int));
	if(lineStyles.size()==0) lineStyles.assign(lineStyle_, lineStyle_ + sizeof(lineStyle_)/sizeof(int));

	/// check valid length of arrays
	if( files.size() != nHis || hisNames.size() != nHis){
		std::cout << "ERROR! Vector size of files or hisNames != nHis" << std::endl;
		return;
	}
	if(leg!=0) {
		if(legText.size() != nHis){
			std::cout << "ERROR! Vector size of legText != nHis" << std::endl;
			return;
		}
	}
	if(markers.size() < nHis || colors.size() < nHis || lineStyles.size() < nHis){
		std::cout << "ERROR! Vector size of markers, colors or lineSyles < nHis" << std::endl;
		return;
	}

	TString drawOptAddition = "";
	if(Canv==0) Canv=new TCanvas("c", "c", 1);
	Canv->cd();

	vector<TGraph*> his_= vector<TGraph*>();
	
        TGraph * dummy=0;
        

	for(int i=0; i<nHis; i++){
		his_.push_back((TGraph*)graphFromFile(files[i], hisNames[i])->Clone(hisNames[i]+legText[i]));
		if(i==0) {
                    
                    if(xRangeLo!=-11 && xRangeHi!=-11 && yRangeLo!=-11 && yRangeHi!=-11){ // need dummy if axis range should be set new
                        dummy=dummyGraph( xRangeLo, yRangeLo, xRangeHi, yRangeHi, his_[i]->GetTitle());
                        dummy->GetXaxis()->SetTitle( his_[i]->GetXaxis()->GetTitle()  );
                        dummy->GetYaxis()->SetTitle( his_[i]->GetYaxis()->GetTitle()  );
                        dummy->Draw(drawOpt+"A");
                        drawOptAddition = " same";
                    }
                    else drawOptAddition = " A";
                }
                else     drawOptAddition = " same";
		his_[i]->SetMarkerStyle(markers[i]);
		his_[i]->SetMarkerColor(colors[i]);
		his_[i]->SetLineColor(colors[i]);
		his_[i]->SetLineStyle(lineStyles[i]);

		his_[i]->Draw(drawOpt+drawOptAddition);
		if(leg!=0) leg->AddEntry(his_[i], legText[i], legOpt);

		//maxY = TMath::Max(maxY, his_[i]->GetMaximum());
	}
	/// set axis range
	if(xRangeLo!=-11 && xRangeHi!=-11) his_[0]->GetXaxis()->SetRangeUser(xRangeLo, xRangeHi);
	if(yRangeLo!=-11 && yRangeHi!=-11) his_[0]->GetYaxis()->SetRangeUser(yRangeLo, yRangeHi);
	//else               	               his_[0]->GetYaxis()->SetRangeUser(0, 1.1*maxY);

	if(leg!=0) leg->Draw();

}

//From $ROOTSYS/tutorials/fit/langaus.C
//-----------------------------------------------------------------------
//
// Convoluted Landau and Gaussian Fitting Function
//         (using ROOT's Landau and Gauss functions)
//
//  Based on a Fortran code by R.Fruehwirth (fruhwirth@hephy.oeaw.ac.at)
//  Adapted for C++/ROOT by H.Pernegger (Heinz.Pernegger@cern.ch) and
//   Markus Friedl (Markus.Friedl@cern.ch)
//
//  to execute this example, do:
//  root > .x langaus.C
// or
//  root > .x langaus.C++
//
//-----------------------------------------------------------------------

Double_t langaufun(Double_t *x, Double_t *par) {

   //Fit parameters:
   //par[0]=Width (scale) parameter of Landau density
   //par[1]=Most Probable (MP, location) parameter of Landau density
   //par[2]=Total area (integral -inf to inf, normalization constant)
   //par[3]=Width (sigma) of convoluted Gaussian function
   //
   //In the Landau distribution (represented by the CERNLIB approximation),
   //the maximum is located at x=-0.22278298 with the location parameter=0.
   //This shift is corrected within this function, so that the actual
   //maximum is identical to the MP parameter.

      // Numeric constants
      Double_t invsq2pi = 0.3989422804014;   // (2 pi)^(-1/2)
      Double_t mpshift  = -0.22278298;       // Landau maximum location

      // Control constants
      Double_t np = 100.0;      // number of convolution steps
      Double_t sc =   5.0;      // convolution extends to +-sc Gaussian sigmas

      // Variables
      Double_t xx;
      Double_t mpc;
      Double_t fland;
      Double_t sum = 0.0;
      Double_t xlow,xupp;
      Double_t step;
      Double_t i;


      // MP shift correction
      mpc = par[1] - mpshift * par[0];

      // Range of convolution integral
      xlow = x[0] - sc * par[3];
      xupp = x[0] + sc * par[3];

      step = (xupp-xlow) / np;

      // Convolution integral of Landau and Gaussian by sum
      for(i=1.0; i<=np/2; i++) {
         xx = xlow + (i-.5) * step;
         fland = TMath::Landau(xx,mpc,par[0]) / par[0];
         sum += fland * TMath::Gaus(x[0],xx,par[3]);

         xx = xupp - (i-.5) * step;
         fland = TMath::Landau(xx,mpc,par[0]) / par[0];
         sum += fland * TMath::Gaus(x[0],xx,par[3]);
      }

      return (par[2] * step * sum * invsq2pi / par[3]);
}



TF1 *langaufit(TH1 *his, Double_t *fitrange, Double_t *startvalues, Double_t *parlimitslo, Double_t *parlimitshi, Double_t *fitparams, Double_t *fiterrors, Double_t *ChiSqr, Int_t *NDF)
{
   // Once again, here are the Landau * Gaussian parameters:
   //   par[0]=Width (scale) parameter of Landau density
   //   par[1]=Most Probable (MP, location) parameter of Landau density
   //   par[2]=Total area (integral -inf to inf, normalization constant)
   //   par[3]=Width (sigma) of convoluted Gaussian function
   //
   // Variables for langaufit call:
   //   his             histogram to fit
   //   fitrange[2]     lo and hi boundaries of fit range
   //   startvalues[4]  reasonable start values for the fit
   //   parlimitslo[4]  lower parameter limits
   //   parlimitshi[4]  upper parameter limits
   //   fitparams[4]    returns the final fit parameters
   //   fiterrors[4]    returns the final fit errors
   //   ChiSqr          returns the chi square
   //   NDF             returns ndf

   Int_t i;
   Char_t FunName[100];

   sprintf(FunName,"Fitfcn_%s",his->GetName());

   TF1 *ffitold = (TF1*)gROOT->GetListOfFunctions()->FindObject(FunName);
   if (ffitold) delete ffitold;

   TF1 *ffit = new TF1(FunName,langaufun,fitrange[0],fitrange[1],4);
   ffit->SetParameters(startvalues);
   ffit->SetParNames("LandauWidth","LandauMPV","Area","GaussSigma");

   for (i=0; i<4; i++) {
      ffit->SetParLimits(i, parlimitslo[i], parlimitshi[i]);
   }

   //his->Fit(FunName,"RB0");   // fit within specified range, use ParLimits, do not plot
   his->Fit(FunName,"RBI", "same");   // fit within specified range, use ParLimits, do plot

   ffit->GetParameters(fitparams);    // obtain fit parameters
   for (i=0; i<4; i++) {
      fiterrors[i] = ffit->GetParError(i);     // obtain fit parameter errors
   }
   ChiSqr[0] = ffit->GetChisquare();  // obtain chi^2
   NDF[0] = ffit->GetNDF();           // obtain ndf

   return (ffit);              // return fit function

}

Int_t langaupro(Double_t *params, Double_t &maxx, Double_t &FWHM) {

   // Seaches for the location (x value) at the maximum of the
   // Landau-Gaussian convolute and its full width at half-maximum.
   //
   // The search is probably not very efficient, but it's a first try.

   Double_t p,x,fy,fxr,fxl;
   Double_t step;
   Double_t l,lold;
   Int_t i = 0;
   Int_t MAXCALLS = 10000;


   // Search for maximum

   p = params[1] - 0.1 * params[0];
   step = 0.05 * params[0];
   lold = -2.0;
   l    = -1.0;


   while ( (l != lold) && (i < MAXCALLS) ) {
      i++;

      lold = l;
      x = p + step;
      l = langaufun(&x,params);

      if (l < lold)
         step = -step/10;

      p += step;
   }

   if (i == MAXCALLS)
      return (-1);

   maxx = x;

   fy = l/2;


   // Search for right x location of fy

   p = maxx + params[0];
   step = params[0];
   lold = -2.0;
   l    = -1e300;
   i    = 0;


   while ( (l != lold) && (i < MAXCALLS) ) {
      i++;

      lold = l;
      x = p + step;
      l = TMath::Abs(langaufun(&x,params) - fy);

      if (l > lold)
         step = -step/10;

      p += step;
   }

   if (i == MAXCALLS)
      return (-2);

   fxr = x;


   // Search for left x location of fy

   p = maxx - 0.5 * params[0];
   step = -params[0];
   lold = -2.0;
   l    = -1e300;
   i    = 0;

   while ( (l != lold) && (i < MAXCALLS) ) {
      i++;

      lold = l;
      x = p + step;
      l = TMath::Abs(langaufun(&x,params) - fy);

      if (l > lold)
         step = -step/10;

      p += step;
   }

   if (i == MAXCALLS)
      return (-3);


   fxl = x;

   FWHM = fxr - fxl;
   return (0);
}

// function for pairing channels
void pairGen(int pair, int nItems, int *iCh1, int *iCh2)
{	
	int paircount=0, iPair1=0, iPair2=0;
	
	if(pair<0)
	{
		std::cerr << "Warning: pairGen(): paircount<0." << std::endl;
		*iCh1 = *iCh2 = 0;
		return;
	}
	
	for(iPair1=0; iPair1<nItems && paircount<=pair; ++iPair1)
	{
		for(iPair2=iPair1+1; iPair2<nItems && paircount<=pair; ++iPair2)
		{
			++paircount;
		}
	}
	if(iPair1 == iPair2) std::cerr << "Warning: pairGen(): paircount higher than number of combinations." << std::endl;
	
	*iCh1 = iPair1-1;
	*iCh2 = iPair2-1;
	
}

// function to get back the full histo sigma (assuming it has been corrected/subtracted before by timeResolutionCorrection)
double getFullHistoSigma(double timeResolutionCorrected, double timeResolutionCorrection)
{
    if      (timeResolutionCorrection == -1.41) return timeResolutionCorrected * TMath::Sqrt(2);
    else if (timeResolutionCorrection == 0    ) return timeResolutionCorrected;
    else if (timeResolutionCorrection > 0     ) return ( TMath::Sqrt(timeResolutionCorrected*timeResolutionCorrected + timeResolutionCorrection*timeResolutionCorrection) );
    else return -1;
}

// function to fit a series of full histo sigmas (timeResolutionCombination), assuming it is a convolution of two separate components (timeResolutionChannel)
double getPerChannelResolution(vector<double> timeResolutionCombination, vector<double> timeResolutionCombinationErr, vector<double>& timeResolutionChannel, vector<double>& timeResolutionChannelErr)
{
    uint nCombinations = timeResolutionCombination.size();
    
    cout << "nCombinations= " << nCombinations << endl;
    
    double chi2=0;
    
    
    // distinguish between channel/combinations number
    
    if(nCombinations==1) { // if there is only one combination, there are only 2 channels and the resolution is divided by sqrt(2), which is assigned to both channels
        
        // pass back time resolution per channel (ordered by channel index/number)
        timeResolutionChannel.push_back( timeResolutionCombination[0]/TMath::Sqrt(2) ); 
        timeResolutionChannel.push_back( timeResolutionCombination[0]/TMath::Sqrt(2) ); 
        
        timeResolutionChannelErr.push_back( timeResolutionCombinationErr[0]/TMath::Sqrt(2) ); 
        timeResolutionChannelErr.push_back( timeResolutionCombinationErr[0]/TMath::Sqrt(2) ); 
        
         cout << "nComb=1; timeResolutionChannel[0]= " << timeResolutionChannel[0] << "; timeResolutionChannel[1]= " << timeResolutionChannel[1] << endl;
        
    }
    else if(nCombinations==3) { // for 3 combinations, i.e. 3 channels, the time resolution of each can be calulated analytically. But in fact this might be a special case of the more general when fitting is used -> check
        
        // dummy return values so far
        timeResolutionChannel.push_back(0); // channel 0
        timeResolutionChannel.push_back(1); // channel 1
        timeResolutionChannel.push_back(2); // channel 2
        timeResolutionChannel.push_back(3); // channel 3
        
        timeResolutionChannelErr.push_back(0); // channel 0
        timeResolutionChannelErr.push_back(1); // channel 1
        timeResolutionChannelErr.push_back(2); // channel 2
        timeResolutionChannelErr.push_back(3); // channel 3
        
        cout << "nComb=3; timeResolutionChannel[0]= " << timeResolutionChannel[0] << "; timeResolutionChannel[1]= " << timeResolutionChannel[1] << "; timeResolutionChannel[2]= " << timeResolutionChannel[2] << "; timeResolutionChannel[3]= " << timeResolutionChannel[3] << endl;
        
        
    }
    else if(nCombinations==6) { // for 6 combinations, i.e. 4 channels, the time resolution of each is fitted with a chi2 minimisation

	printf("Input: \n");
	for(unsigned int i=0;i<nCombinations;i++){
		printf("%f \n",timeResolutionCombination[i]);
	}

        
	TMatrixD sigma(4,4);
	sigma.Zero();
	TMatrixD sigmaerr(4,4);
	sigmaerr.Zero();


	
	//filling the sigma matrix between 2 channels ..
	sigma[1][0]=timeResolutionCombination[0];
	sigma[0][1]=sigma[1][0];
	sigma[2][0]=timeResolutionCombination[1];
	sigma[0][2]=sigma[2][0];	
	sigma[3][0]=timeResolutionCombination[2];
	sigma[0][3]=sigma[3][0];
	sigma[2][1]=timeResolutionCombination[3];
	sigma[1][2]=sigma[2][1];
	sigma[3][1]=timeResolutionCombination[4];
	sigma[1][3]=sigma[3][1];
	sigma[3][2]=timeResolutionCombination[5];
	sigma[2][3]=sigma[3][2];

	//fillin test values
/*	sigma[1][0]=sqrt(20*20+30*30);
	sigma[0][1]=sigma[1][0];
	sigma[2][0]=sqrt(20*20+40*40);
	sigma[0][2]=sigma[2][0];	
	sigma[3][0]=sqrt(20*20+50*50);
	sigma[0][3]=sigma[3][0];
	sigma[2][1]=sqrt(30*30+40*40);
	sigma[1][2]=sigma[2][1];
	sigma[3][1]=sqrt(30*30+50*50);
	sigma[1][3]=sigma[3][1];
	sigma[3][2]=sqrt(40*40+50*50);
	sigma[2][3]=sigma[3][2];*/

/*	printf("sigma:\n");
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			printf("%f \t",sigma[i][j]);
		}
		printf("\n");
	}
*/
	//and their errors
	sigmaerr[1][0]=timeResolutionCombinationErr[0];
	sigmaerr[0][1]=sigmaerr[1][0];
	sigmaerr[2][0]=timeResolutionCombinationErr[1];
	sigmaerr[0][2]=sigmaerr[2][0];	
	sigmaerr[3][0]=timeResolutionCombinationErr[2];
	sigmaerr[0][3]=sigmaerr[3][0];
	sigmaerr[2][1]=timeResolutionCombinationErr[3];
	sigmaerr[1][2]=sigmaerr[2][1];
	sigmaerr[3][1]=timeResolutionCombinationErr[4];
	sigmaerr[1][3]=sigmaerr[3][1];
	sigmaerr[3][2]=timeResolutionCombinationErr[5];
	sigmaerr[2][3]=sigmaerr[3][2];


/*	printf("sigmaerr\n");
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			printf("%f \t",sigmaerr[i][j]);
		}
		printf("\n");
	}
*/

//	printf("sigma[1][0] %f sigmaerr[1][0]: %f \n", sigma[1][0],sigmaerr[1][0]);

	//getting beta Matrix

	TMatrixD beta(4,4);
//	beta = sigma*sigma;
	
	//printf("beta matrix: \n");
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			beta[i][j]=sigma[i][j]*sigma[i][j];
			//printf("%f \t",beta[i][j]);
		}
		//printf("\n");
	}
	
	//multiplying by 2 somehow didnt work, so I am doing it by hand. Usually 2*sigma*sigmaerr
	TMatrixD betaerr(4,4);
	//betaerr.Mult(sigma,sigmaerr);
	//betaerr = betaerr + betaerr;


	//printf("beta error matrix: \n");
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			betaerr[i][j]=2*sigma[i][j]*sigmaerr[i][j];
			//printf("%f \t",betaerr[i][j]);
		}
		//printf("\n");
	}

//	betaerr = sigma*sigmaerr;
//	betaerr +=betaerr;

	//b vector
	TMatrixD bVec(4,4);
	bVec.Zero();

	for (unsigned int i=0;i<4;i++){
		for(unsigned int j=0;j<4;j++){
			if(betaerr[i][j]!=0)	bVec[i][0]+=beta[i][j]/(betaerr[i][j]*betaerr[i][j]);			
		}
	}

	/*printf("betavec: \n");
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			printf("%f \t",bVec[i][j]);
		}
		printf("\n");
	}
*/

	//U matrix
	TMatrixD U(4,4);
	U.Zero();
	
	for (unsigned int i=0;i<4;i++){
		for(unsigned int j=0;j<4;j++){
			if(i==j){
				for(unsigned int k=0;k<4;k++){
					if(betaerr[i][k]!=0) U[i][j]+=1/(betaerr[i][k]*betaerr[i][k]);
				}
			}
			else if(betaerr[i][j]!=0) U[i][j]=1/(betaerr[i][j]*betaerr[i][j]);	
		}
	}


/*	printf("Umatrix: \n");
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			printf("%f \t",U[i][j]);
		}
		printf("\n");
	}
*/

	TMatrixD UInv(TMatrixD::kInverted,U);
//	UInv.Invert(U);
	

	/*printf("U INV matrix: \n");
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			printf("%f \t",UInv[i][j]);//(U.Invert())[i][j]);
		}
		printf("\n");
	}*/

	//testing inversion:
	//printf("testing inversion: \n");
	
	/*TMatrixD TestInv(4,4);
	TestInv.Zero();
	TestInv.Mult(U,UInv);

	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			printf("%f \t",TestInv[i][j]);
		}
		printf("\n");
	}*/	

	//A Matrix
	TMatrixD A(4,4);
	A.Zero();
	
	A.Mult(UInv,bVec);
	//A=(U.Invert())*bVec;

	/*printf("A matrix: \n");
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			printf("%f \t",A[i][j]);
		}
		printf("\n");
	}
*/
	//A error Matrix
	TMatrixD Aerr(4,4);
	Aerr.Zero();

	for(int i=0;i<4;i++){
		Aerr[i][0]=2*sqrt(UInv[i][i]);
	}

/*	printf("A Err matrix: \n");
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			printf("%f \t",Aerr[i][j]);
		}
		printf("\n");
	}
*/
	TMatrixD OutErr(4,4);
	OutErr.Zero();
	
	for(int i=0;i<4;i++){
		OutErr[i][0]=1/sqrt(2)*Aerr[i][0]/(A[i][0]);
	}

	/*printf("Out Err matrix: \n");
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			printf("%f \t",OutErr[i][j]);
		}
		printf("\n");
	}
*/
        // construct a chi2 here and minimise
 
        
	//checking if the algorithm succeeded
	if(A[3][0]>0){
		timeResolutionChannel.push_back(sqrt(A[0][0])); // channel 0
		timeResolutionChannel.push_back(sqrt(A[1][0])); // channel 1
		timeResolutionChannel.push_back(sqrt(A[2][0])); // channel 2
		timeResolutionChannel.push_back(sqrt(A[3][0])); // channel 3
		
		timeResolutionChannelErr.push_back(OutErr[0][0]); // channel 0
		timeResolutionChannelErr.push_back(OutErr[1][0]); // channel 1
		timeResolutionChannelErr.push_back(OutErr[2][0]); // channel 2
		timeResolutionChannelErr.push_back(OutErr[3][0]); // channel 3
	}
	// if not give back the initial values
	else {
		timeResolutionChannel.push_back(sqrt(timeResolutionCombination[0]*timeResolutionCombination[0]-11*11)); // channel 0
		timeResolutionChannel.push_back(sqrt(timeResolutionCombination[1]*timeResolutionCombination[1]-11*11)); // channel 1
		timeResolutionChannel.push_back(11); // channel 2
		timeResolutionChannel.push_back(11); // channel 3
		
		timeResolutionChannelErr.push_back(timeResolutionCombinationErr[0]); // channel 0
		timeResolutionChannelErr.push_back(timeResolutionCombinationErr[1]); // channel 1
		timeResolutionChannelErr.push_back(timeResolutionCombinationErr[2]); // channel 2
		timeResolutionChannelErr.push_back(timeResolutionCombinationErr[3]); // channel 3
	}

	for(int i=0;i<4;i++){
		printf("Ch %i timeRes: \t %f \t orig: \t %f \n",i,timeResolutionChannel[i], i<2 ? sqrt(timeResolutionCombination[i]*timeResolutionCombination[i]-11*11) : 11);
	}
//        cout << "nComb=6; timeResolutionChannel[0]= " << timeResolutionChannel[0] << "; timeResolutionChannel[1]= " << timeResolutionChannel[1] << "; timeResolutionChannel[2]= " << timeResolutionChannel[2] << "; timeResolutionChannel[3]= " << timeResolutionChannel[3] << endl;
        
        
    }
    else{ // so far no use case for other nCombinations -> set to 0
        
        timeResolutionChannel.push_back(0); // channel 0
        timeResolutionChannel.push_back(0); // channel 1
        timeResolutionChannel.push_back(0); // channel 2
        timeResolutionChannel.push_back(0); // channel 3
        
        timeResolutionChannelErr.push_back(0); // channel 0
        timeResolutionChannelErr.push_back(0); // channel 1
        timeResolutionChannelErr.push_back(0); // channel 2
        timeResolutionChannelErr.push_back(0); // channel 3
        
        cout << "other nComb; timeResolutionChannel[0]= " << timeResolutionChannel[0] << "; timeResolutionChannel[1]= " << timeResolutionChannel[1] << "; timeResolutionChannel[2]= " << timeResolutionChannel[2] << "; timeResolutionChannel[3]= " << timeResolutionChannel[3] << endl;
        
    }
    
    cout << "chi2=" << chi2 << endl;
    
    return chi2;
    
    
    
}

#endif
