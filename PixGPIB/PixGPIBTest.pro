CONFIG -= qt

TEMPLATE = app

include(../build-config.inc)
  
SOURCES += PixGPIBTest.cxx
SOURCES += PixGPIB.cxx
SOURCES += PixGPIBDevice.cxx
SOURCES += PixGPIBError.cxx

INCLUDEPATH = . ../include

unix {
    DESTDIR = .
    QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
    QMAKE_LFLAGS += -lgpib -lpthread
}
win32 {
    CONFIG += console
    DESTDIR = ../bin
    LIBS += Gpib-32.obj
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
}
