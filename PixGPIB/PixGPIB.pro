CONFIG -= qt

TEMPLATE = lib

include(../build-config.inc)
  
SOURCES += PixGPIBDevice.cxx
SOURCES += PixGPIBError.cxx

INCLUDEPATH = ../include

unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lgpib -lpthread
}
win32 {
	LIBS += Gpib-32.obj
    DESTDIR = ../bin
	DLLDESTDIR = ../bin
	DEFINES += PIX_DLL_EXPORT
	DEFINES += WIN32 
	DEFINES += _WINDOWS
	DEFINES += _MBCS 
}
