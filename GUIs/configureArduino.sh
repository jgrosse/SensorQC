#! /bin/bash
CWD=`pwd`
cd ../PixRS232/ArduinoADC
SENSORQC=`pwd`
ARDUINO=/usr/local/arduino-1.8.13/arduino
cd $CWD
MYHOST=`hostname -s`
BOARD="arduino:avr:mega:cpu=atmega2560"
if [ ${MYHOST}=="lab17" ]
then
  BOARD="arduino:avr:uno"
fi
# flash arduino                                                                 
${ARDUINO} --board ${BOARD} --port /dev/ttyACM0 --upload ${SENSORQC}/ArduinoADC.ino
# wait for arduino to be ready and clear output                                 
sleep 2 
