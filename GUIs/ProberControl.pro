TEMPLATE = app

CONFIG += qt
equals(QT_MAJOR_VERSION, 5) {
  QT+= widgets
}

include(../build-config.inc)
  
FORMS += ProberControl.ui

HEADERS += ProberControl.h

SOURCES += ProberControl.cxx ProberControlMain.cxx

INCLUDEPATH += ../include ../PixGPIB ../PixRS232 ../libSuess
unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX
	LIBS += -L ../libSuess -lSuess -L../PixRS232 -lPixRS232 -L../PixGPIB -lPixGPIB
	QMAKE_RPATHDIR += . ../PixRS232 ../PixGPIB ../libSuess
}
win32 {
    DESTDIR = ../bin
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
    QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
    INCLUDEPATH += $(ROOTSYS)/include
    QMAKE_LFLAGS_RELEASE = delayimp.lib
    QMAKE_LFLAGS_WINDOWS += /LIBPATH:../PixGPIB /LIBPATH:../bin
    LIBS += Suess.lib PixRS232.lib PixGPIB.lib GPIB-32.obj
}

