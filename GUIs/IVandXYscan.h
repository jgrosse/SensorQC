#ifndef IVANDXYSCAN_H
#define IVANDXYSCAN_H

#include <QMainWindow>
#include <vector>
#include <QSignalMapper>
#include <QtWidgets/QHBoxLayout>

class TCanvas;
class TGraph;
class PixGPIBDevice;
class QTimer;
class IVscan;
class ProberControl;
class TempMon;

class IVandXYscan : public QMainWindow {

	Q_OBJECT

	public:
		IVandXYscan(QWidget * parent = 0, Qt::WindowFlags flags = Qt::Window);
		~IVandXYscan();
		IVscan *ivscan;
		ProberControl *probercontrol;
		TempMon* tempmon;
		QHBoxLayout *horizontalLayout;

	public slots:
		void startIVandXYscan();
		void enableIVandXYscan();
		void flagAbortScanToTrue();

	private:
		bool flagAbortScan;
};

#endif // IVANDXYSCAN_H
