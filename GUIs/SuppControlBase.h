#ifndef SUPPCONTROLBASE_H
#define SUPPCONTROLBASE_H

#include "ui_SuppControlBase.h"
#include <QWidget>
#include <QEvent>
#include <vector>
#include <thread>
#include <mutex>

class SuppControlBase : public QWidget, public Ui::SuppControlBase {

    Q_OBJECT

 public:
    SuppControlBase(QWidget * parent = 0, Qt::WindowFlags flags = Qt::Window, bool isStandalone=true);
  ~SuppControlBase(){};

  void setChan(int nChan);
  void connectDev();
  void disconnectDev();
  // needed for handling of emits from thread
  void customEvent( QEvent * e );
  bool readChans(bool);
  void readChansThr();

class setNewRdgEvent : public QEvent
{
 public:
  setNewRdgEvent(QMap<QString, double> vals)
    : QEvent((QEvent::Type)2001){ m_vals=vals;};
  
  QMap<QString, double> getVals(){return m_vals;};

 private:
  QMap<QString, double> m_vals;
};

 public slots:
  void turnOn();
  void turnOff();
  void turnOff(bool);
  void typeChg(QString label);
  void updateIV();
  void padChanged(int pad);

 signals:
  void newSuppply(QList<QString>);
  void obsSuppply(QList<QString>);
  void newSuppRdg(QMap<QString, double>);

 private:
  std::vector<QLCDNumber*> vDisp;
  std::vector<QLCDNumber*> aDisp;
  std::vector<QLabel*> vLabel;
  std::vector<QComboBox*> aLabel;
  std::vector<QDoubleSpinBox*> vSet;
  std::vector<QDoubleSpinBox*> aSet;
  std::vector<QLabel*> vSlabel;
  std::vector<QComboBox*> aSlabel;
  void* m_dev;
  int m_nChan;
  std::thread m_thread;
  bool m_thrRuns;
  std::mutex m_mutex;
};
#endif // SUPPCONTROLBASE_H
