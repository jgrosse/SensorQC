#ifndef INTERLOCK_H
#define INTERLOCK_H

#include "ui_Interlock.h"
#include <QWidget>
#include <vector>

class InterlockCond;

class Interlock : public QWidget, public Ui::Interlock {

    Q_OBJECT

 public:
    Interlock(QWidget * parent = 0, Qt::WindowFlags flags = Qt::Window);
    ~Interlock();

 signals:
    void interlockFired(int stopCond);

 public slots:
   void setNcond(int nc);
   void newValues(QMap<QString, double> valList);
   void newNames(QList<QString> nameList);
   void obsNames(QList<QString> nameList);
   void setActive();

 private:
   std::vector<InterlockCond*> m_conds;
   int m_active;
};

#endif // INTERLOCK_H
