#include "SuppControl.h"
#include "QRootApplication.h"

#include <iostream>
#include <sstream>
#include <exception>

int main( int argc, char** argv )
{

  // start root and QT application
  QRootApplication app( argc, argv);

  // create main data viewer window
  SuppControl *Win = new SuppControl(0, Qt::Window);
  Win->show();

  // executing our application
  int ret = 0;
  std::stringstream msg;
  try{
    ret  = app.exec();
  } catch(std::exception& s){
    msg << "Std-lib exception \"";
    msg << s.what();
  } catch(...){
    msg << "Unknown exception \"";
  }
  if(msg.str()!="")
    std::cerr << msg.str() << "\" not caught during execution of main window." << std::endl;
  // cleaning up
  delete Win;

  return ret;
}
