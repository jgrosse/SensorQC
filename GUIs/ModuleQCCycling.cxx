#include "Cycling.h"
#include "TempMon.h"
#include "DataLogging.h"
#include "Interlock.h"
#include "QRootApplication.h"
#include <QMainWindow>
#include <QDockWidget>

#include <iostream>
#include <sstream>
#include <exception>

int main( int argc, char** argv )
{

  std::string cfgfile="";
  for(int i=1; i<argc; i++){
    if(strcmp(argv[i], "-h")==0){
      std::cout << "Usage:" << std::endl;
      std::cout << "ModuleQCCycling [-f file]" << std::endl;
      std::cout << "   -f file loads pre-set of channels etc. and calibration constants of sensors" << std::endl;
      return 0;
    }
    if(strcmp(argv[i], "-f")==0 && i<(argc-1)){
      cfgfile = argv[i+1];
      i++;
    }
  }

  // start root and QT application
  QRootApplication app( argc, argv);

  // create main data viewer window
  QMainWindow *Win = new QMainWindow(0);
  Win->setWindowTitle("Module QC Cycling");

  QDockWidget* dockWidget_1 = new QDockWidget(Win);
  dockWidget_1->setObjectName(QStringLiteral("dockWidget_1"));
  dockWidget_1->setWindowTitle("T,RH Devices");
  QDockWidget* dockWidget_2 = new QDockWidget(Win);
  dockWidget_2->setObjectName(QStringLiteral("dockWidget_2"));
  dockWidget_2->setWindowTitle("Data Logging");
  QDockWidget* dockWidget_3 = new QDockWidget(Win);
  dockWidget_3->setObjectName(QStringLiteral("dockWidget_3"));
  dockWidget_3->setWindowTitle("Cycling");
  QDockWidget* dockWidget_4 = new QDockWidget(Win);
  dockWidget_4->setObjectName(QStringLiteral("dockWidget_4"));
  dockWidget_4->setWindowTitle("Interlock");

  // monitoring
  QString hname = qgetenv("HOSTNAME");
  hname = hname.split(".").at(0);
  TempMon *tm = new TempMon(Win, Qt::Window, true, cfgfile);
  dockWidget_1->setWidget(tm);
  // data logging
  DataLogging *dl = new DataLogging(Win, Qt::Window);
  dockWidget_2->setWidget(dl);
  // dynamically alter some pre-sets in logger GUI
  dl->dbHostPort->setText("192.168.1.100:8086");
  dl->dbMeasName->setText(dl->dbMeasName->text()+"_"+hname);
  int idbid = 2;
  dl->logComboBox->setCurrentIndex(idbid);
  dl->logChecked(idbid);
  dl->logIntervalBox->setValue(10);
  // shock chamber control
  Cycling *cy = new Cycling(0, Qt::Window, true);
  dockWidget_3->setWidget(cy);
  // interlock panel
  Interlock *il = new Interlock(0, Qt::Window);
  dockWidget_4->setWidget(il);

  // connect data from temp and cycling widget to logger
  QWidget::connect(tm, SIGNAL(newTempVal(QMap<QString, double>)), dl, SLOT(newValues(QMap<QString, double>)) );
  QWidget::connect(tm, SIGNAL(obsTempDev(QList<QString>)), dl, SLOT(obsValues(QList<QString>)) );
  QWidget::connect(cy, SIGNAL(newCycVal(QMap<QString, double>)), dl, SLOT(newValues(QMap<QString, double>)) );
  QWidget::connect(cy, SIGNAL(obsCycDev(QList<QString>)), dl, SLOT(obsValues(QList<QString>)) );
  QWidget::connect(tm, SIGNAL(newTempDev(QList<QString>)), il, SLOT(newNames(QList<QString>)) );
  QWidget::connect(cy, SIGNAL(newCycDev(QList<QString>)), il, SLOT(newNames(QList<QString>)) );
  QWidget::connect(tm, SIGNAL(obsTempDev(QList<QString>)), il, SLOT(obsNames(QList<QString>)) );
  QWidget::connect(cy, SIGNAL(obsCycDev(QList<QString>)), il, SLOT(obsNames(QList<QString>)) );
  QWidget::connect(tm, SIGNAL(newTempVal(QMap<QString, double>)), il, SLOT(newValues(QMap<QString, double>)) );
  QWidget::connect(cy, SIGNAL(newCycVal (QMap<QString, double>)), il, SLOT(newValues(QMap<QString, double>)) );
  QWidget::connect(tm, SIGNAL(newTempDev(QList<QString>)), cy, SLOT(newNames(QList<QString>)) );
  QWidget::connect(tm, SIGNAL(obsTempDev(QList<QString>)), cy, SLOT(obsNames(QList<QString>)) );
  QWidget::connect(tm, SIGNAL(newTempVal(QMap<QString, double>)), cy, SLOT(newVals(QMap<QString, double>)) );
  QWidget::connect(il, SIGNAL(interlockFired(int)), cy, SLOT(stopCycling(int)));

  Win->addDockWidget(Qt::LeftDockWidgetArea,  dockWidget_1);
  Win->addDockWidget(Qt::LeftDockWidgetArea,  dockWidget_2);
  Win->addDockWidget(Qt::RightDockWidgetArea,  dockWidget_3);
  Win->addDockWidget(Qt::RightDockWidgetArea,  dockWidget_4);
  Win->show();

  // executing our application
  int ret = 0;
  std::stringstream msg;
  try{
    ret  = app.exec();
  } catch(std::exception& s){
    msg << "Std-lib exception \"";
    msg << s.what();
  } catch(...){
    msg << "Unknown exception \"";
  }
  if(msg.str()!="")
    std::cerr << msg.str() << "\" not caught during execution of main window." << std::endl;
  // cleaning up
  delete Win;

  return ret;
}
