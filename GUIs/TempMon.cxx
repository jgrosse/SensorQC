#include <TSystem.h>
#include <TF1.h>
#include <TMath.h>

#include <TempMon.h>
#include <QFileDialog>
#include <QDateTime>
#include <QTcpSocket>
#include <QHostAddress>
#include <QCoreApplication>

#include "PixGPIBDevice.h"
#include "PixRs232Device.h"
#include "VA18B.h"
#include "ArduinoADC.h"

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>

TempMon::TempMon(QWidget * parent, Qt::WindowFlags flags, bool cycling, std::string cfgfile)
  : QWidget(parent, flags), m_cycling(cycling) {
  setupUi(this);

  m_meterReadings[0] = meterReading_0; m_meterReadings[1] = meterReading_1; m_meterReadings[2] = meterReading_2; m_meterReadings[3] = meterReading_3; m_meterReadings[4] = meterReading_4; m_meterReadings[5] = meterReading_5; 
     m_meterReadings[6] = meterReading_6; m_meterReadings[7] = meterReading_7; m_meterReadings[8] = meterReading_8;
  m_checkBox[0] = checkBox_0; m_checkBox[1] = checkBox_1; m_checkBox[2] = checkBox_2; m_checkBox[3] = checkBox_3; m_checkBox[4] = checkBox_4; m_checkBox[5] = checkBox_5; 
     m_checkBox[6] = checkBox_6; m_checkBox[7] = checkBox_7; m_checkBox[8] = checkBox_8; 
  m_gpibLabels[0]=gpibLabel_0; m_gpibLabels[1]=gpibLabel_1; m_gpibLabels[2]=gpibLabel_2; m_gpibLabels[3]=gpibLabel_3; m_gpibLabels[4]=gpibLabel_4; m_gpibLabels[5]=gpibLabel_5;
     m_gpibLabels[6]=gpibLabel_6;  m_gpibLabels[7]=gpibLabel_7; m_gpibLabels[8]=gpibLabel_8;
  m_gpibPADs[0]=gpibPAD_0; m_gpibPADs[1]=gpibPAD_1; m_gpibPADs[2]=gpibPAD_2; m_gpibPADs[3]=gpibPAD_3; m_gpibPADs[4]=gpibPAD_4; m_gpibPADs[5]=gpibPAD_5;
     m_gpibPADs[6]=gpibPAD_6; m_gpibPADs[7]=gpibPAD_7; m_gpibPADs[8]=gpibPAD_8;
  m_calibBox[0]=calibBox_0; m_calibBox[1]=calibBox_1; m_calibBox[2]=calibBox_2; m_calibBox[3]=calibBox_3; m_calibBox[4]=calibBox_4; m_calibBox[5]=calibBox_5;
     m_calibBox[6]=calibBox_6; m_calibBox[7]=calibBox_7; m_calibBox[8]=calibBox_8;
  m_gpibComboBox[0]=gpib_ComboBox_0; m_gpibComboBox[1]=gpib_ComboBox_1; m_gpibComboBox[2]=gpib_ComboBox_2; m_gpibComboBox[3]=gpib_ComboBox_3; m_gpibComboBox[4]=gpib_ComboBox_4; m_gpibComboBox[5]=gpib_ComboBox_5;
     m_gpibComboBox[6]=gpib_ComboBox_6; m_gpibComboBox[7]=gpib_ComboBox_7; m_gpibComboBox[8]=gpib_ComboBox_8;
  m_ipAdds[0] = ipAdd_0; m_ipAdds[1] = ipAdd_1; m_ipAdds[2] = ipAdd_2; m_ipAdds[3] = ipAdd_3; m_ipAdds[4] = ipAdd_4; m_ipAdds[5] = ipAdd_5;
     m_ipAdds[6] = ipAdd_6; m_ipAdds[7] = ipAdd_7; m_ipAdds[8] = ipAdd_8;
  m_unitLabels[0] = setVLabel_0; m_unitLabels[1] = setVLabel_1; m_unitLabels[2] = setVLabel_2; m_unitLabels[3] = setVLabel_3; m_unitLabels[4] = setVLabel_4; m_unitLabels[5] = setVLabel_5;
     m_unitLabels[6] = setVLabel_6; m_unitLabels[7] = setVLabel_7; m_unitLabels[8] = setVLabel_8;
  m_chanLabels[0] = chanLabel_0, m_chanLabels[1] = chanLabel_1, m_chanLabels[2] = chanLabel_2, m_chanLabels[3] = chanLabel_3; m_chanLabels[4] = chanLabel_4; m_chanLabels[5] = chanLabel_5;
     m_chanLabels[6] = chanLabel_6; m_chanLabels[7] = chanLabel_7; m_chanLabels[8] = chanLabel_8;
  m_devChans[0] = devChan_0, m_devChans[1] = devChan_1, m_devChans[2] = devChan_2, m_devChans[3] = devChan_3; m_devChans[4] = devChan_4; m_devChans[5] = devChan_5;
     m_devChans[6] = devChan_6; m_devChans[7] = devChan_7; m_devChans[8] = devChan_8;

  for(uint i=0;i<nitems;i++){
    m_dev[i] = NULL;
    m_com[i] = NULL;
    m_va[i]  = NULL;
    m_ar[i]  = NULL;
    m_gpibComboBox[i]->addItem(QString("RS232"));
    m_gpibComboBox[i]->addItem(QString("Arduino"));
    m_gpibComboBox[i]->addItem(QString("DataLogger"));
    m_gpibComboBox[i]->addItem(QString("VA18B"));
    m_gpibComboBox[i]->addItem(QString("CustTCP"));
    m_gpibComboBox[i]->setCurrentText("Arduino");
    typeChg(m_gpibComboBox[i]->currentText(), i);
    connect(m_checkBox[i], SIGNAL(toggled(bool)), this, SLOT(devSelected(bool)));
  }

  // usually hide 2nd set of ambient sensors
  if(cycling){
    m_checkBox[1]->setText("Arduino T");
  } else {
    for(int i=ni_cyc; i<nitems;i++){
      m_checkBox[i]->setChecked(false);
      m_checkBox[i]->hide();
      m_meterReadings[i]->hide();
      m_gpibPADs[i]->hide();
      m_calibBox[i]->hide();
      m_gpibLabels[i]->hide();
      m_unitLabels[i]->hide();
      m_gpibComboBox[i]->hide();
      m_devChans[i]->hide();
      m_chanLabels[i]->hide();
    }
    checkBox_DP2->setChecked(false);
    checkBox_DP2->hide();
    meterReading_DP2->hide();
    setVLabel_DP2->hide();
    horizontalSpacer_8->changeSize(0,0);
  }

  connect(checkBox_DP, SIGNAL(toggled(bool)), meterReading_DP, SLOT(setEnabled(bool)));
  connect(checkBox_DP2, SIGNAL(toggled(bool)), meterReading_DP2, SLOT(setEnabled(bool)));

  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDev()));
  connect(nDUTbox, SIGNAL(valueChanged(int)), this, SLOT(setNDUT(int)));
  connect(m_gpibComboBox[0], SIGNAL(activated(QString)), this, SLOT(typeChg0(QString)));
  connect(m_gpibComboBox[1], SIGNAL(activated(QString)), this, SLOT(typeChg1(QString)));
  connect(m_gpibComboBox[2], SIGNAL(activated(QString)), this, SLOT(typeChg2(QString)));
  connect(m_gpibComboBox[3], SIGNAL(activated(QString)), this, SLOT(typeChg3(QString)));
  connect(m_gpibComboBox[4], SIGNAL(activated(QString)), this, SLOT(typeChg4(QString)));
  connect(m_gpibComboBox[5], SIGNAL(activated(QString)), this, SLOT(typeChg5(QString)));
  connect(m_gpibComboBox[6], SIGNAL(activated(QString)), this, SLOT(typeChg6(QString)));
  connect(m_gpibComboBox[7], SIGNAL(activated(QString)), this, SLOT(typeChg7(QString)));
  connect(m_gpibComboBox[8], SIGNAL(activated(QString)), this, SLOT(typeChg8(QString)));

  // store calibration if provided
  if(cfgfile!=""){
    FILE *cfg = fopen(cfgfile.c_str(), "r");
    if(cfg==0) std::cerr << "Can't open config. file " << cfgfile << std::endl;
    else {
      char line[2001];
      bool newsens=true;
      int currID=-1;
      while(fgets(line, 2000, cfg)!=0){
	if(line[strlen(line)-1]=='\n') line[strlen(line)-1]='\0';//strip of CR at end
	if(newsens){
	  QString slbl = line;
	  for(int i=0; i<nitems;i++){
	    if(m_checkBox[i]->text()==slbl){
	      currID=i;
	      break;
	    }
	  }
	  if(currID<0) std::cerr << "Can't find sensor " << std::string(line) << std::endl;
	  else {
	    m_checkBox[currID]->setChecked(true);
	    if(m_checkBox[currID]->text().left(3)=="DUT")
	      nDUTbox->setValue(nDUTbox->value()+1);
	  }
	  newsens = false;
	} else if(line[0]=='-' && line[1]=='-'){
	  newsens = true;
	  currID = -1;
	} else if(currID>=0) { // parameter from previously declared sensor
	  char label[100];
	  double value;
	  sscanf(line, "%s\t%lf", label, &value);
	  std::string slabel = label;
	  if(slabel=="Type")
	    m_gpibComboBox[currID]->setCurrentIndex((int)value);
	  else if(slabel=="PAD")
	    m_gpibPADs[currID]->setValue((int)value);
	  else if(slabel=="Chan")
	    m_devChans[currID]->setValue((int)value);
	  else if(slabel=="SensType")
	    m_calibBox[currID]->setCurrentIndex((int)value);
	  else {
// 	    std::cout << "Storing par. " << slabel << " for sensor \"" << m_checkBox[currID]->text().toStdString() << 
// 	      "\" with value " << value << std::endl;
	    m_calib[currID].insert(std::make_pair(slabel, value));
	  }
	}
      }
      fclose(cfg);
    }
  }

  setNDUT(nDUTbox->value());

}

TempMon::~TempMon(){
  disconnDev();
}
void TempMon::connDev(){
  QList<QString> myTypes;
  m_readLogger.clear();

  for(uint i=0;i<nitems;i++){

    if(m_checkBox[i]->isChecked()){
      if(m_gpibComboBox[i]->currentText()=="GPIB"){
	m_dev[i] = NULL;
	// check if same device (with other channel) is used already -> of so, just copy pointer
	for(uint k=0;k<i;k++) {
	  if(m_dev[k] != NULL && m_gpibPADs[i]->value()==m_gpibPADs[k]->value() && 
	     m_gpibComboBox[k]->currentText()=="GPIB") {
	    m_dev[i] = m_dev[k];
	    break;
	  }
	}
	if(m_dev[i] == NULL)
	  m_dev[i] = new PixGPIBDevice(0, m_gpibPADs[i]->value(), 0, false); 
	if(m_dev[i]->getStatus()==PixGPIBDevice::PGD_ERROR){
	  delete m_dev[i]; m_dev[i] = NULL;
	  m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	  std::cerr << "Error contacting GPIB device on " << m_gpibPADs[i]->value() << std::endl;
	} else{
	  m_gpibComboBox[i]->setEnabled(false);
	  m_checkBox[i]->setEnabled(false);
	  m_gpibPADs[i]->setEnabled(false);
	  int nch = m_dev[i]->getDeviceNumberChannels();
	  if(nch>1){
	    m_devChans[i]->setMaximum(nch-1);
	  } else {
	    m_devChans[i]->hide();
	    m_chanLabels[i]->hide();
	  }
	  myTypes.push_back(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")");
	  if(m_checkBox[i]->text()=="Rel. Humidity") checkBox_DP->setEnabled(false);
	  if(m_checkBox[i]->text()=="Rel. Humidity 2") checkBox_DP2->setEnabled(false);
	}
      } else if(m_gpibComboBox[i]->currentText()=="Arduino"){
	// check if Arduino already exists
	for(uint k=0;k<nitems;k++){
	  if(m_ar[k]!=NULL && m_ar[k]->getPort()==m_gpibPADs[i]->value()){
	    m_ar[i] = m_ar[k];
	    break;
	  }
	}
	// create new Arduino instance
	if(m_ar[i] == NULL) m_ar[i] = new ArduinoADC(m_gpibPADs[i]->value());
	m_gpibComboBox[i]->setEnabled(false);
	m_checkBox[i]->setEnabled(false);
	m_gpibPADs[i]->setEnabled(false);
	myTypes.push_back(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")");
	if(m_checkBox[i]->text()=="Rel. Humidity") checkBox_DP->setEnabled(false);
	if(m_checkBox[i]->text()=="Rel. Humidity 2") checkBox_DP2->setEnabled(false);
      } else if(m_gpibComboBox[i]->currentText()=="RS232"){
	// to do 
      } else if(m_gpibComboBox[i]->currentText()=="VA18B"){
	m_va[i] = new VA18B(m_gpibPADs[i]->value());
	QCoreApplication::processEvents();
	// wait for device to get ready
	int ium=0;
	while(m_va[i]->getMeasUnit()=="unknown" && ium<10){
	  ium++;
	  std::this_thread::sleep_for (std::chrono::milliseconds(500));  
	}
	if(ium>10){
	  delete m_va[i]; m_va[i] = NULL;
	  m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	  std::cerr << "Error contacting VA18B device on " << m_gpibPADs[i]->value() << std::endl;	  
	} else {
	  m_gpibComboBox[i]->setEnabled(false);
	  m_checkBox[i]->setEnabled(false);
	  m_gpibPADs[i]->setEnabled(false);
	  m_devChans[i]->hide();
	  m_chanLabels[i]->hide();
	  myTypes.push_back(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")");
	}
      } else if(m_gpibComboBox[i]->currentText()=="CustTCP"){
	// dealt with on the fly, nothing to do
	m_gpibComboBox[i]->setEnabled(false);
	m_checkBox[i]->setEnabled(false);
	m_gpibPADs[i]->setEnabled(false);
	myTypes.push_back(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")");
      } else {
	QStringList sl = m_ipAdds[i]->text().split(":");
	QString ipadd = sl.at(0);// = "192.168.2.153";
	int port = sl.at(1).toInt();// = 7700;
	if(m_readLogger.find(ipadd)==m_readLogger.end() || !m_readLogger[ipadd]) {
	  //std::cout << "connecting IP " << std::string(ipadd.toLatin1().data()) << " on port " << port << std::endl;
	  m_readLogger[ipadd] = false;
	  m_thrDL.push_back(std::thread(&TempMon::readLogger, this, ipadd, port));
	}
	QCoreApplication::processEvents();
	std::this_thread::sleep_for (std::chrono::milliseconds(1000));
	if(m_readLogger[ipadd]){
	  m_gpibComboBox[i]->setEnabled(false);
	  m_checkBox[i]->setEnabled(false);
	  m_gpibPADs[i]->setEnabled(false);
	  m_devChans[i]->hide();
	  m_chanLabels[i]->hide();
	  myTypes.push_back(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")");
	} else {
	  m_gpibLabels[i]->setText("Error");
	  m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	}
      }
    } else { // hide unused elements
      m_checkBox[i]->hide();
      m_meterReadings[i]->hide();
      m_calibBox[i]->hide();
      m_gpibLabels[i]->hide();
      m_unitLabels[i]->hide();
      m_gpibPADs[i]->hide();
      m_gpibComboBox[i]->hide();
      m_ipAdds[i]->hide();
      m_devChans[i]->hide();
      m_chanLabels[i]->hide();
    }
    QCoreApplication::processEvents();
  }

  checkBox_DP->setEnabled(false);
  if(checkBox_DP->isChecked()) myTypes.push_back(checkBox_DP->text()+"("+setVLabel_DP->text()+")");
  checkBox_DP2->setEnabled(false);
  if(checkBox_DP2->isChecked()) myTypes.push_back(checkBox_DP2->text()+"("+setVLabel_DP2->text()+")");

//   readMeters();
  m_thrRuns = true;
  m_thread = std::thread(&TempMon::readMeters, this);
  emit newTempDev(myTypes);

  gpibConnectButton->setText("Disconnect Selected");
  disconnect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDev()));
  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(disconnDev()));
}
void TempMon::disconnDev(){
  m_thrRuns = false;
  if(m_thread.joinable()) m_thread.join();
  QList<QString> myTypes;
  for(QMap<QString,bool>::iterator it = m_readLogger.begin(); it!=m_readLogger.end(); it++)
    it.value() = false;
  for(std::vector<std::thread>::iterator it = m_thrDL.begin(); it!=m_thrDL.end(); it++)
    if(it->joinable()) it->join();
  m_readLogger.clear();
  
  for(uint i=0;i<nitems;i++){
    if(m_checkBox[i]->isChecked()){
      m_gpibLabels[i]->setStyleSheet("QLabel {background-color: #efebe7}");
      typeChg(m_gpibComboBox[i]->currentText(), i);
      m_gpibComboBox[i]->setEnabled(true);
      // if this device was already deleted elsewhere, don't delete again
      for(uint k=0;k<i;k++) {
	if(m_gpibPADs[i]->value()==m_gpibPADs[k]->value()){
	  if(m_gpibComboBox[k]->currentText()=="GPIB") 
	    if(m_dev[k] == NULL) m_dev[i] = NULL;
	  if(m_gpibComboBox[k]->currentText()=="Arduino")
	    if(m_ar[k] == NULL) m_ar[i] = NULL;
	}
      }
      m_checkBox[i]->setEnabled(true);
      delete m_dev[i]; m_dev[i] = NULL;
      delete m_com[i]; m_com[i] = NULL;
      delete m_va[i]; m_va[i] = NULL;
      myTypes.push_back(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")");
    } else { // show prev. unused elements
      if(m_cycling || i<ni_cyc){
	m_checkBox[i]->show();
	m_meterReadings[i]->show();
	m_calibBox[i]->show();
	m_gpibLabels[i]->show();
	m_unitLabels[i]->show();
	m_gpibComboBox[i]->show();
	typeChg(m_gpibComboBox[i]->currentText(), i);
      }
    }
    if(m_cycling || i<ni_cyc){
      m_devChans[i]->show();
      m_chanLabels[i]->show();
    }
    QCoreApplication::processEvents();
  }
  for(uint i=0;i<nitems;i++)
    emit m_checkBox[i]->toggled(m_checkBox[i]->isChecked());
  
  checkBox_DP->setEnabled(true);
  if(checkBox_DP->isChecked()) myTypes.push_back(checkBox_DP->text()+"("+setVLabel_DP->text()+")");
  if(m_cycling){
    checkBox_DP2->setEnabled(true);
    if(checkBox_DP2->isChecked()) myTypes.push_back(checkBox_DP2->text()+"("+setVLabel_DP2->text()+")");
  }
  setNDUT(nDUTbox->value());

  emit obsTempDev(myTypes);
  gpibConnectButton->setText("Connect Selected");
  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDev()));
  disconnect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(disconnDev()));
}
void TempMon::readMeters(){

  while(m_thrRuns){

    double rawMeas, calMeas=0., Tamb=20., Tamb2=20., Tard=25.;
    QMap<QString, double> Tmap;
    
    // Cstray for HS1101 changed on the fly
    FILE *recal = fopen("/tmp/RelHum2_Cstray.txt","r");
    if(recal!=0){
      int sensID=-1;
      for(int i=0; i<nitems;i++){
	if(m_checkBox[i]->text()=="Rel. Humidity 2"){
	  sensID=i;
	  break;
	}
      }
      double Cstray;
      if(fscanf(recal, "%lf\n", &Cstray)==1 && sensID>=0){
       if(m_calib[sensID].find("Cstray")!=m_calib[sensID].end())
         m_calib[sensID]["Cstray"] = Cstray;
       else
         m_calib[sensID].insert(std::make_pair("Cstray", Cstray));
      }
      fclose(recal);
      remove("/tmp/RelHum2_Cstray.txt");
    }

    for(uint i=0;i<nitems;i++){
      
      if(m_checkBox[i]->isChecked() && !m_checkBox[i]->isEnabled()){
	rawMeas = -1.;
	if(m_gpibComboBox[i]->currentText()=="GPIB"){
	  if(m_dev[i]!=NULL){
	    if(m_checkBox[i]->text().left(8)=="Rel. Hum" && m_calibBox[i]->currentText()=="HIH4000"){
	      m_dev[i]->measureVoltages(0., true, m_devChans[i]->value());
	      rawMeas = m_dev[i]->getVoltage(m_devChans[i]->value());
	    } else if(m_checkBox[i]->text().left(8)=="Rel. Hum" && m_calibBox[i]->currentText()=="HS1101LF"){
	      m_dev[i]->measureFrequency(0., true, m_devChans[i]->value());
	      rawMeas = m_dev[i]->getFrequency(m_devChans[i]->value());
	    } else {
	      m_dev[i]->measureResistances(0., true, m_devChans[i]->value());
	      rawMeas = m_dev[i]->getResistance(m_devChans[i]->value());
	    }
	  }
	  if(m_dev[i]->getStatus()==PixGPIBDevice::PGD_ERROR){
	    m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	    std::cerr << "Error reading from GPIB device on " << m_gpibPADs[i]->value() << std::endl;
	  } else
	    m_gpibLabels[i]->setStyleSheet("QLabel {background-color: #efebe7}");
	} else if(m_gpibComboBox[i]->currentText()=="Arduino"){
	  if(m_ar[i]!=NULL){
	    if(m_calibBox[i]->currentText()=="SHT85"){
	      // send 2400 to SHT85 to trigger reading
	      std::string response = m_ar[i]->getMeasVal(m_devChans[i]->value(), "2400", 6);
	      // extract humidity and temperature reading
	      std::vector<uint8_t> data(6);
	      for(uint32_t i=0;i<response.size()/2;i++)
		{
		  std::string byte=response.substr(2*i,2);
		  data[i]=std::stoul(byte.c_str(), nullptr, 16);
		}
	      uint16_t tdata=((data[0])<<8)|data[1];
	      uint16_t hdata=((data[3])<<8)|data[4];
	      // Parse the data
	      if(m_checkBox[i]->text().left(8)=="Rel. Hum"){
		rawMeas = 100*((float)hdata)/(pow(2,16)-1.);
		if(rawMeas<1.e-3) rawMeas=1.e-3;
	      } else { // must be temperature
		rawMeas = -45+175*((float)tdata)/(pow(2,16)-1.);
	      }
	    } else {
	      double nomV = 5.;
	      double offset = 0.;
	      int nrep = 5;
	      if(m_calib[i].find("Vnom")!=m_calib[i].end())
		nomV = m_calib[i]["Vnom"];
	      if(m_calib[i].find("offset")!=m_calib[i].end())
		offset = m_calib[i]["offset"];
	      if(m_checkBox[i]->text().left(8)=="Rel. Hum"){
		rawMeas = m_ar[i]->getMeasVal(m_devChans[i]->value(), 0., nomV, offset, nrep)/nomV;
	      } else {
		// to do: store ref. resistor value somewhere
		double refRes = 990.;
		if(m_calibBox[i]->currentText()=="NTC"){
		  refRes = 16.8e3;
		} else
		  nrep = 10;
		if(m_calib[i].find("Rdiv")!=m_calib[i].end())
		  refRes = m_calib[i]["Rdiv"];
		rawMeas = m_ar[i]->getMeasVal(m_devChans[i]->value(), refRes, nomV, offset, nrep);
	      }
	    }
	  }
	} else if(m_gpibComboBox[i]->currentText()=="RS232"){
	  // to do
	} else if(m_gpibComboBox[i]->currentText()=="VA18B"){
	  std::string unit = m_va[i]->getMeasUnit();
	  if((m_checkBox[i]->text().left(8)=="Rel. Hum" && unit=="V") || 
	     unit=="Ohm" || unit=="°C")
	    rawMeas = m_va[i]->getMeasVal();
	  else 
	    rawMeas = -1.;
	  if(unit=="unknown"){
	    m_gpibLabels[i]->setText("Error");
	    m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	    std::cerr << "Error reading from VA18B:device on " << m_gpibPADs[i]->value() << ": no recent data available!"<< std::endl;	  
	  } else{
	    m_gpibLabels[i]->setText("");
	    m_gpibLabels[i]->setStyleSheet("QLabel {background-color: #efebe7}");
	  }
	} else if(m_gpibComboBox[i]->currentText()=="DataLogger"){
	  if(m_loggerVals.find(m_checkBox[i]->text())!=m_loggerVals.end()) {
	    calMeas = m_loggerVals[m_checkBox[i]->text()];
	    rawMeas = 0.;
	    m_gpibLabels[i]->setText("   ");
	    m_gpibLabels[i]->setStyleSheet("QLabel {background-color: #efebe7}");
	  } else {
	    m_gpibLabels[i]->setText("not fd.");
	    m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	  }
	} else if(m_gpibComboBox[i]->currentText()=="CustTCP"){
	  QStringList sl = m_ipAdds[i]->text().split(":");
	  if(sl.size()==3){
	    QString ipadd = sl.at(0);
	    int port = sl.at(1).toInt();
	    QString dcsName = sl.at(2);
	    if(readEth(ipadd, port,dcsName,m_devChans[i]->value(),calMeas)){
	      m_gpibLabels[i]->setText("   ");
	      m_gpibLabels[i]->setStyleSheet("QLabel {background-color: #efebe7}");
	    } else {
	      m_gpibLabels[i]->setText("Error");
	      m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	    }
	  } else
	    calMeas = -999999.;
	  rawMeas = 0.;
	}
	if(m_calibBox[i]->currentText()=="direct" || m_calibBox[i]->currentText()=="SHT85")
	  calMeas = rawMeas;
	else if(rawMeas>0.){
	  if(m_calibBox[i]->currentText()=="PT1000"){
	    double nomR = 1000.;
	    if(m_calib[i].find("Rnom")!=m_calib[i].end())
	      nomR = m_calib[i]["Rnom"];
	    calMeas = convPT(rawMeas, nomR);
	  } else if(m_calibBox[i]->currentText()=="PT100") {
	    double nomR = 100.;
	    if(m_calib[i].find("Rnom")!=m_calib[i].end())
	      nomR = m_calib[i]["Rnom"];
	    calMeas = convPT(rawMeas, nomR);
	  } else if(m_calibBox[i]->currentText()=="NTC") {
	    calMeas = convNTC(rawMeas,0);
	  } else if(m_calibBox[i]->currentText()=="Mod-NTC") {
 	    double nomR = 10000.;
 	    if(m_calib[i].find("Rnom")!=m_calib[i].end())
 	      nomR = m_calib[i]["Rnom"];
 	    double nomT = 298.15;
 	    if(m_calib[i].find("Tnom")!=m_calib[i].end())
 	      nomT = m_calib[i]["Tnom"];
 	    double nomB = 3435.;
 	    if(m_calib[i].find("Bnom")!=m_calib[i].end())
 	      nomB = m_calib[i]["Bnom"];
	    calMeas = convNTC(rawMeas,1, nomT, nomR, nomB);
	  } else if(m_calibBox[i]->currentText()=="HIH4000")
	    calMeas = convHIH4000(rawMeas, Tamb);
	  else if(m_calibBox[i]->currentText()=="HS1101LF") {
	    double R22 = 468.8e3, R4 = 50.e3;
	    double Cnom = 180.e-12, Cstray = 20.e-12;
	    double gradT = 2.e-4;
	    if(m_calib[i].find("R22")!=m_calib[i].end())
	      R22 = m_calib[i]["R22"];
	    if(m_calib[i].find("R4")!=m_calib[i].end())
	      R4 = m_calib[i]["R4"];
	    if(m_calib[i].find("Cnom")!=m_calib[i].end())
	      Cnom = m_calib[i]["Cnom"];
	    if(m_calib[i].find("Cstray")!=m_calib[i].end())
	      Cstray = m_calib[i]["Cstray"];
	    if(m_calib[i].find("gradT")!=m_calib[i].end())
	      gradT = m_calib[i]["gradT"];
	    calMeas = convHS1101LF(rawMeas, R22, R4, Cnom, Cstray, Tard, gradT);
	  } else{
	    calMeas = 0.;
	    std::cerr << "Unknown calibration method: " << m_calibBox[i]->currentText().toLatin1().data() << std::endl;
	  }
	}
	if(rawMeas>=0. || m_calibBox[i]->currentText()=="direct" || m_calibBox[i]->currentText()=="SHT85"){
	  if(m_checkBox[i]->text()=="Arduino T") Tard = calMeas;
	  if(m_checkBox[i]->text()=="Ambient T") Tamb = calMeas;
	  if(m_checkBox[i]->text()=="Ambient T 2") Tamb2 = calMeas;
	  if(m_checkBox[i]->text()=="Rel. Humidity" && checkBox_DP->isChecked()){ // calculate dew point
	    double dewpt = (241.2*TMath::Log(calMeas/100.)+4222.03716*Tamb/(241.2+Tamb)) / 
	      (17.5043 - TMath::Log(calMeas/100.)-17.5043*Tamb/(241.2+Tamb));
	    meterReading_DP->display(convDispl(dewpt));
	    Tmap.insert(checkBox_DP->text()+"("+setVLabel_DP->text()+")", dewpt);
	  }
	  if(m_checkBox[i]->text()=="Rel. Humidity 2" && checkBox_DP2->isChecked()){ // calculate 2nd dew point
	    double dewpt = (241.2*TMath::Log(calMeas/100.)+4222.03716*Tamb2/(241.2+Tamb2)) / 
	      (17.5043 - TMath::Log(calMeas/100.)-17.5043*Tamb2/(241.2+Tamb2));
	    meterReading_DP2->display(convDispl(dewpt));
	    Tmap.insert(checkBox_DP2->text()+"("+setVLabel_DP2->text()+")", dewpt);
	  }
	  m_meterReadings[i]->display(convDispl(calMeas));
	  Tmap.insert(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")", calMeas);
	} else {
	  m_meterReadings[i]->display("-");
	}
      }
    }
    //emit newTempVal(Tmap);
    setNewRdgEvent *nre = new setNewRdgEvent(Tmap);
    QCoreApplication::postEvent(this, nre);
    std::this_thread::sleep_for (std::chrono::milliseconds(500));  
  }

}
double TempMon::convNTC(double res, int type, double Tref, double Rref, double Bntc){
  double cal[4]={0.0020091, 0.000296397, 1.65669e-06, -175.422};
  //double cal2[4]={0.0002911, 0.00268, 0.003354,  273.15};
  if(res<0) return -273.16;
  switch(type){
  default:
  case 0: // "blue drop" NTC
    return 1/(cal[0]+cal[1]*TMath::Log(res)+cal[2]*TMath::Power(TMath::Log(res),3))+cal[3];;
  case 1:{ // demonstrator module NTC
    //return 1/(cal2[0]* TMath::Log(res)-cal2[1]+cal2[2])-cal2[3];
    if (res >= 3.e+38) return -273.15;
    return Bntc*Tref/(Tref*std::log(res/Rref)+Bntc) - 273.15;
  }
  }
}
double TempMon::convPT(double res, double nomR){
  char formula[100];
  if(res<nomR)
    sprintf(formula, "%lf*(1+3.9083e-3*x-5.775e-7*x*x-4.183e-12*(x-100)*x*x*x)", nomR);
  else
    sprintf(formula, "%lf*(1+3.9083e-3*x-5.775e-7*x*x)", nomR);
  static TF1 fcal("ptcal",formula, -100, 100);
  if(res<0) return -273.16;
  return fcal.GetX(res);
}
double TempMon::convHIH4000(double measVrat, double ambT){
  double RH = (measVrat-0.16)/0.0062/(1.0546-0.00216*ambT);;
  // sanity checks
  if(RH>99.999) RH = 99.999;
  if(RH<1.e-3) RH = 1.e-3;
  return RH;
}
double TempMon::convHS1101LF(double measF, double R22, double R4, double Cnom, double Cstray, double ambT, double gradT){
  // to do: make this constants configurable
//   double R22 = 468.8e3, R4 = 50.e3;
//   double Cnom = 1.1203*180.e-12, Cstray = 19.95e-12;
  // calculate capacity from frequency and correct for stray capacity
  double X = (1./measF/(R4+2.*R22)/TMath::Log(2.)-Cstray)/Cnom;
  // TLC555 resistors' temperature correction
  X -= (ambT - 25.0) * gradT;
  double RH = -3.4656e3*TMath::Power(X,3)+1.0732e4*TMath::Power(X,2)-1.0457e4*X+3.2459e3;
  // sanity checks
  if(RH>99.999) RH = 99.999;
  if(RH<1.e-3) RH = 1.e-3;
  return RH;
}
QString TempMon::convDispl(double value){
  char cstrg[50];
  sprintf(cstrg, "%.1lf", value);
  return QString(cstrg);
  //return QString::number((int)value) + "." + QString("%1").arg(qRound((value-(double)(int)value) * 10), 1, 10, QChar('0'));
}
void TempMon::typeChg(QString type, uint item){
  if(item>=nitems) return;
  if(type=="GPIB"){
    m_gpibLabels[item]->setText("PAD");
    m_gpibPADs[item]->show();
    m_calibBox[item]->show();
    m_ipAdds[item]->hide();
  } else if(type=="RS232"){
    m_gpibLabels[item]->setText("COM");
    m_gpibPADs[item]->show();
    m_calibBox[item]->show();
    m_ipAdds[item]->hide();
  } else if(type=="Arduino"){
    m_gpibLabels[item]->setText("USB");
    m_gpibPADs[item]->show();
    m_calibBox[item]->show();
    m_ipAdds[item]->hide();
  } else if(type=="VA18B"){
    m_gpibLabels[item]->setText("USB");
    m_gpibPADs[item]->show();
    m_calibBox[item]->show();
    m_ipAdds[item]->hide();
  } else {
    m_gpibLabels[item]->setText("   ");
    m_gpibPADs[item]->hide();
    m_calibBox[item]->hide();
    m_ipAdds[item]->show();
    if(type=="CustTCP") m_ipAdds[item]->setText("localhost:7700:USB_regulators_0");
    else m_ipAdds[item]->setText("small-logger:7700");
  }
}
bool TempMon::readEth(QString ipadd, int port, QString dcsName, int chan, double &val){
  val = -99999.;
  uint len_in, len_out;

  QTcpSocket *sock = new QTcpSocket;
  const int myTimeout = 1000;

  sock->connectToHost(ipadd, port);
  if (!sock->waitForConnected(myTimeout)){
    std::cerr << "Timeout connecting to " << ipadd.toStdString() << std::endl;
    delete sock;
    return false;
  }
  if (sock->state() != QAbstractSocket::ConnectedState){
    std::cerr << "Error connecting to " << ipadd.toStdString() << std::endl;
    delete sock;
    return false;
  }

  QByteArray block;
  QString cmdstr = "DCS req "+dcsName+":"+QString::number(chan)+":temperature"+"\n";
  len_in = cmdstr.length();
  block.append(cmdstr);

  len_out = sock->write(block);
  sock->waitForBytesWritten(myTimeout);
  if(len_out==len_in){
    std::this_thread::sleep_for (std::chrono::milliseconds(50));
    QByteArray resp;
    int tries = 0;
    while((!resp.contains('\n')) && (tries++ < 600)) {
      sock->waitForReadyRead(100);
      if (sock->state() != QAbstractSocket::ConnectedState){
	std::cerr << "Error: lost connection to " << ipadd.toStdString() << " - " << resp.toStdString() << std::endl;
	sock->close();
	delete sock;
	return false;
      }
      resp += sock->readAll();
    }
    int idx = resp.indexOf('\n');
    if (idx < 1){
      std::cerr << "Error: lost connection(2) to " << ipadd.toStdString() << " - " << resp.toStdString()<< std::endl;
      sock->close();
      delete sock;
      return false;
    }
    val = resp.left(idx).toDouble();
    sock->close();
    delete sock;
    return true;
  } else
    std::cerr << "Error sending request to " << ipadd.toStdString() << std::endl;
    
  return false;
}
void TempMon::readLogger(QString ipadd, int port){
  uint len_in, len_out;
  //char bufc[1024], bufr[1024];

  m_loggerVals.clear();

  QTcpSocket *sock = new QTcpSocket;
  const int myTimeout = 1000;

  sock->connectToHost(ipadd, port);
  if (!sock->waitForConnected(myTimeout)){
    std::cerr << "Timeout connecting to " << ipadd.toStdString() << std::endl;
    delete sock;
    return;
  }
  if (sock->state() != QAbstractSocket::ConnectedState){
    std::cerr << "Error connecting to " << ipadd.toStdString() << std::endl;
    delete sock;
    return;
  }

  QByteArray block;
  block[0] = 127;
  len_in = 1;

  len_out = sock->write(block);
  sock->waitForBytesWritten(myTimeout);
  if(len_out==len_in){
    std::this_thread::sleep_for (std::chrono::milliseconds(50));
    QByteArray resp;
    int tries = 0;
    while((!resp.contains('\n')) && (tries++ < 600)) {
      sock->waitForReadyRead(100);
      resp += sock->readAll();
      if (sock->state() != QAbstractSocket::ConnectedState){
	std::cerr << "Error: lost connection to " << ipadd.toStdString() << std::endl;
	sock->close();
	delete sock;
	return;
      }
    }
    int idx = resp.indexOf('\n');
    if (idx < 1 || resp.left(2)!="<<"){
      std::cerr << "Error: lost connection to " << ipadd.toStdString() << " - " << resp.toStdString()<< std::endl;
      sock->close();
      delete sock;
      return;
    }
  }

  m_readLogger[ipadd] = true;

  block.clear();
  block[0]='/';
  block[1]='E';
  block[2]='/';
  block[3]='M';
  block[4]='/';
  block[5]='R';
  block[6]='\r';
  len_in = 7;

  len_out = sock->write(block);
  sock->waitForBytesWritten(myTimeout);
  if(len_out==len_in){
    std::this_thread::sleep_for (std::chrono::milliseconds(50));
    QByteArray resp;
    int tries = 0;
    while((!resp.contains('\n')) && (tries++ < 600)) {
      sock->waitForReadyRead(100);
      resp += sock->readAll();
      if (sock->state() != QAbstractSocket::ConnectedState){
	sock->close();
	delete sock;
	std::cerr << "Error: lost connection to " << ipadd.toStdString() << std::endl;
	return;
      }
    }
  }

  while(m_readLogger[ipadd]){
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));
    if (sock->state() == QAbstractSocket::ConnectedState){
      QByteArray resp;
      int tries = 0;
      while((!resp.contains('\n')) && (tries++ < 600)) {
	sock->waitForReadyRead(100);
	resp += sock->readAll();
	if (sock->state() != QAbstractSocket::ConnectedState){
	  sock->close();
	  delete sock;
	  std::cerr << "Error: lost connection to " << ipadd.toStdString() << std::endl;
	  return;
	}
      }
      int idx = resp.indexOf('\n');
      if (idx < 1){
	std::cerr << "Error: lost connection to " << ipadd.toStdString() << std::endl;
	sock->close();
	delete sock;
	return;
      }
      QString str(resp);//bufr);
      QStringList list = str.split('\n', QString::SkipEmptyParts);
      for(int i=0; i<list.size();i++){
	QString listel = list.at(i);
	//std::cout << "Found item " << listel.toStdString() << std::endl;
	if(listel.left(11)=="Amb Temp PT"){
	  listel.remove(0,12);
	  if(m_loggerVals.find("Ambient T")==m_loggerVals.end())
	    m_loggerVals.insert("Ambient T", listel.split(" ").at(0).toDouble());
	  else
	    m_loggerVals["Ambient T"] = listel.split(" ").at(0).toDouble();
	}
	if(listel.left(17)=="Chuck Temperature"){
	  listel.remove(0,18);
	  if(m_loggerVals.find("Chuck T")==m_loggerVals.end())
	    m_loggerVals.insert("Chuck T", listel.split(" ").at(0).toDouble());
	  else
	    m_loggerVals["Chuck T"] = listel.split(" ").at(0).toDouble();
	}
	if(listel.left(20)=="VacChuck Temperature"){
	  listel.remove(0,21);
	  if(m_loggerVals.find("Chuck T")==m_loggerVals.end())
	    m_loggerVals.insert("Chuck T", listel.split(" ").at(0).toDouble());
	  else
	    m_loggerVals["Chuck T"] = listel.split(" ").at(0).toDouble();
	}
	if(listel.left(16)=="Ambient Humidity" || listel.left(8)=="Humidity"){
	  if(listel.left(16)=="Ambient Humidity") listel.remove(0,17);
	  else listel.remove(0,9);
	  if(m_loggerVals.find("Rel. Humidity")==m_loggerVals.end())
	    m_loggerVals.insert("Rel. Humidity", listel.split(" ").at(0).toDouble());
	  else
	    m_loggerVals["Rel. Humidity"] = listel.split(" ").at(0).toDouble();
	}
      }
      //std::cout << std::endl;
    }
  }

  block.clear();
  block[0]='/';
  block[1]='e';
  block[2]='/';
  block[3]='m';
  block[4]='/';
  block[5]='r';
  block[6]='\r';
  len_in = 7;

  len_out = sock->write(block);
  sock->waitForBytesWritten(myTimeout);
  if(len_out==len_in){
    std::this_thread::sleep_for (std::chrono::milliseconds(50));
    QByteArray resp;
    int tries = 0;
    while((!resp.contains('\n')) && (tries++ < 600)) {
      sock->waitForReadyRead(100);
      resp += sock->readAll();
      if (sock->state() != QAbstractSocket::ConnectedState){
	sock->close();
	std::cerr << "Error: lost connection to " << ipadd.toStdString() << std::endl;
	return;
      }
    }
  }

  m_loggerVals.clear();
  sock->close();
  delete sock;

}
void TempMon::devSelected(bool isSel){
  int icb = -1;
  for(int i=0; i<nitems;i++){
    if(m_checkBox[i]==QObject::sender()){
      icb = i;
      break;
    }
  }
  if(icb>=0 && icb<nitems){
    m_gpibLabels[icb]->setEnabled(isSel);
    m_gpibPADs[icb]->setEnabled(isSel);
    m_calibBox[icb]->setEnabled(isSel);
    m_gpibComboBox[icb]->setEnabled(isSel);
    m_ipAdds[icb]->setEnabled(isSel);
    m_unitLabels[icb]->setEnabled(isSel);
    m_chanLabels[icb]->setEnabled(isSel);
    m_devChans[icb]->setEnabled(isSel);
    m_meterReadings[icb]->setEnabled(isSel);
  }
}
void TempMon::customEvent( QEvent * event )
{
  if(event==0){
    std::cerr << "SuppControlBase::customEvent called with a NULL-pointer";
    return;
  }
  
  if(event->type()==2011){
    QMap<QString, double> valmap = dynamic_cast<setNewRdgEvent*>(event)->getVals();
    emit newTempVal(valmap);
  }
}
void TempMon::setNDUT(int ndut){
  for(uint i=0;i<nitems-3;i++){
    int idut = i-2;
    if(idut<ndut){
      m_checkBox[i]->show();
      m_meterReadings[i]->show();
      m_gpibPADs[i]->show();
      m_calibBox[i]->show();
      m_gpibLabels[i]->show();
      m_unitLabels[i]->show();
      m_gpibComboBox[i]->show();
      m_devChans[i]->show();
      m_chanLabels[i]->show();
      typeChg(m_gpibComboBox[i]->currentText(), i);
    } else{
      m_checkBox[i]->hide();
      m_meterReadings[i]->hide();
      m_gpibPADs[i]->hide();
      m_calibBox[i]->hide();
      m_gpibLabels[i]->hide();
      m_unitLabels[i]->hide();
      m_gpibComboBox[i]->hide();
      m_devChans[i]->hide();
      m_chanLabels[i]->hide();
    }
  }
}
