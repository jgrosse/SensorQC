#include "InterlockCond.h"

double noloadval = -99999999.;

InterlockCond::InterlockCond(QWidget * parent, Qt::WindowFlags flags)
 : QWidget(parent, flags) {
  setupUi(this);
  m_valA=  noloadval;
  m_valB = noloadval;
}
InterlockCond::~InterlockCond(){
}
void InterlockCond::addItem(QString name){
  int itid = itemAbox->findText(name);
  if(itid<0) itemAbox->addItem(name);
  itid = itemBbox->findText(name);
  if(itid<0) itemBbox->addItem(name);
}
void InterlockCond::removeItem(QString name){
  int itid = itemAbox->findText(name);
  if(itid>=0) itemAbox->removeItem(itid);
  itid = itemBbox->findText(name);
  if(itid>=0) itemBbox->removeItem(itid);
  resetState();
}
void InterlockCond::resetState(){
  compBox->setPalette(QPalette(Qt::lightGray));
  m_valA=  noloadval;
  m_valB = noloadval;
}
bool InterlockCond::checkState(QMap<QString, double> valList){

  if(itemBbox->currentText()=="nothing") m_valB = 0.;

  for(QMap<QString, double>::iterator it = valList.begin(); it != valList.end(); it++){
    if(it.key()==itemAbox->currentText()) m_valA = it.value();
    if(it.key()==itemBbox->currentText() && itemBbox->currentText()!="nothing") m_valB = it.value();
  }

  // check if values have arrived to make a decision, and if not just return
  if(m_valA==noloadval || m_valB==noloadval) return false;

  double checkVal = m_valA - m_valB;
  if(compBox->currentText()==">"){ 
    if(checkVal<cutvalSpinBox->value()) {
      compBox->setPalette(QPalette(Qt::red));
      return true;
    } else {
      compBox->setPalette(QPalette(Qt::green));
    }
  }else if(compBox->currentText()=="<"){
    if(checkVal>cutvalSpinBox->value()) {
      compBox->setPalette(QPalette(Qt::red));
      return true;
    } else {
      compBox->setPalette(QPalette(Qt::green));
    }
  } else { // unknown comparison
    compBox->setPalette(QPalette(Qt::blue));
    return true;
  }
  return false;
}
QList<QString> InterlockCond::listItems(){
  QList<QString> boxNames;
  for(int i=0;i<itemAbox->count();i++)
    boxNames.push_back(itemAbox->itemText(i));
  return boxNames;
}
