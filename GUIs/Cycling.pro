TEMPLATE = app

CONFIG += qt
equals(QT_MAJOR_VERSION, 5) {
  QT+= widgets
}

include(../build-config.inc)
  
INCLUDEPATH += . ../include ../simpati

FORMS += Cycling.ui

SOURCES += Cycling.cxx \
	   CyclingMain.cxx

HEADERS += Cycling.h

unix {
    DESTDIR =  .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lpthread -lboost_system
        QMAKE_LFLAGS  +=  -L../simpati -l simpati
        QMAKE_RPATHDIR += ../simpati
}

win32 {
    DESTDIR =  ../bin
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
    QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
    QMAKE_LFLAGS_RELEASE = delayimp.lib simpati.lib
    QMAKE_LFLAGS_WINDOWS += /LIBPATH:../bin
}

