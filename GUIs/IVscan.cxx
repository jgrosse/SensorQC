#ifdef WIN32
#include <Windows4Root.h>
#endif
#include "QRootApplication.h"
#include "IVscan.h"
#include "PixGPIBDevice.h"
#include "PixRs232Device.h"

#include <iostream>
#include <sstream>
#include <exception>
#include <string>
#include <ctime>
#include <chrono>
#include <fstream>

#include <QString>
#include <QApplication>
#include <QFileDialog>

#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TFile.h>
#include <TROOT.h>

#define DEBUG false

IVscan::IVscan(QWidget * parent, Qt::WindowFlags flags) : QWidget(parent, flags) {
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::IVscan\n";}
	setupUi(this);
	flagAbortScan = false;
	m_can = NULL;
	m_canAllXYscan = NULL;
	legAllXYscan = NULL;
	maxNumInstr = 6;
	numInstrEnabled = 0;

	stepX=-1;
	stepY=-1;
	posX=-1;
	posY=-1;
	iScan=0;
	NScan=1;

	m_ChuckT=-9999;
	m_AmbientT=-9999;
	m_RH=-9999;

	// fill vectors of instrument parameters
	sensorPart_.push_back(sensorPart_0); sensorPart_.push_back(sensorPart_1); sensorPart_.push_back(sensorPart_2); sensorPart_.push_back(sensorPart_3); sensorPart_.push_back(sensorPart_4); sensorPart_.push_back(sensorPart_5);
	checkBoxHV_.push_back(checkBoxHV_0); checkBoxHV_.push_back(checkBoxHV_1); checkBoxHV_.push_back(checkBoxHV_2); checkBoxHV_.push_back(checkBoxHV_3); checkBoxHV_.push_back(checkBoxHV_4); checkBoxHV_.push_back(checkBoxHV_5);
	checkBoxImeas_.push_back(checkBoxImeas_0); checkBoxImeas_.push_back(checkBoxImeas_1); checkBoxImeas_.push_back(checkBoxImeas_2); checkBoxImeas_.push_back(checkBoxImeas_3); checkBoxImeas_.push_back(checkBoxImeas_4); checkBoxImeas_.push_back(checkBoxCmeas_5);
	gpibPAD_.push_back(gpibPAD_0); gpibPAD_.push_back(gpibPAD_1); gpibPAD_.push_back(gpibPAD_2); gpibPAD_.push_back(gpibPAD_3); gpibPAD_.push_back(gpibPAD_4); gpibPAD_.push_back(gpibPAD_5);
	instrumentChan_.push_back(instrumentChan_0); instrumentChan_.push_back(instrumentChan_1); instrumentChan_.push_back(instrumentChan_2); instrumentChan_.push_back(instrumentChan_3); instrumentChan_.push_back(instrumentChan_4); instrumentChan_.push_back(instrumentChan_5);
	setVolt_.push_back(setVolt_0); setVolt_.push_back(setVolt_1); setVolt_.push_back(setVolt_2); setVolt_.push_back(setVolt_3); setVolt_.push_back(setVolt_4); setVolt_.push_back(setVolt_5);
	measCurr_.push_back(measCurr_0); measCurr_.push_back(measCurr_1); measCurr_.push_back(measCurr_2); measCurr_.push_back(measCurr_3); measCurr_.push_back(measCurr_4); measCurr_.push_back(measCap_5);
	gpibMessage_.push_back(gpibMessage_0); gpibMessage_.push_back(gpibMessage_1); gpibMessage_.push_back(gpibMessage_2); gpibMessage_.push_back(gpibMessage_3); gpibMessage_.push_back(gpibMessage_4); gpibMessage_.push_back(gpibMessage_5);
	setVLabel_.push_back(setVLabel_0); setVLabel_.push_back(setVLabel_1); setVLabel_.push_back(setVLabel_2); setVLabel_.push_back(setVLabel_3); setVLabel_.push_back(setVLabel_4); setVLabel_.push_back(setVLabel_5);
	measALabel_.push_back(measALabel_0); measALabel_.push_back(measALabel_1); measALabel_.push_back(measALabel_2); measALabel_.push_back(measALabel_3); measALabel_.push_back(measALabel_4); measALabel_.push_back(measCLabel_5);
	chanLabel_.push_back(chanLabel_0); chanLabel_.push_back(chanLabel_1); chanLabel_.push_back(chanLabel_2); chanLabel_.push_back(chanLabel_3); chanLabel_.push_back(chanLabel_4); chanLabel_.push_back(chanLabel_5);
	instrumentLabel_.push_back(instrumentLabel_0); instrumentLabel_.push_back(instrumentLabel_1); instrumentLabel_.push_back(instrumentLabel_2); instrumentLabel_.push_back(instrumentLabel_3); instrumentLabel_.push_back(instrumentLabel_4); instrumentLabel_.push_back(instrumentLabel_5);
	gpibLabel_.push_back(gpibLabel_0); gpibLabel_.push_back(gpibLabel_1); gpibLabel_.push_back(gpibLabel_2); gpibLabel_.push_back(gpibLabel_3); gpibLabel_.push_back(gpibLabel_4); gpibLabel_.push_back(gpibLabel_5);
	idLabel_.push_back(idLabel_0); idLabel_.push_back(idLabel_1); idLabel_.push_back(idLabel_2); idLabel_.push_back(idLabel_3); idLabel_.push_back(idLabel_4); idLabel_.push_back(idLabel_5);
	for(int i=0; i< maxNumInstr; i++) {
		m_ConnInstr_.push_back(NULL); // initialise connected instruments with NULL
		m_grIV_.push_back(NULL); // initialise TGraphs with NULL
		isHVsrc_.push_back(false); // initialise HVsrc with false
		isImeter_.push_back(false); // initialise Imeter with false
	} 

	for(int i=0; i<maxNumInstr; i++){
		gpibMessage_[i]->setText("");
		QObject::connect(gpibLabel_[i], SIGNAL(activated(int)), idLabel_[i], SLOT(setCurrentIndex(int)));
		idLabel_[i]->setEnabled(false);
		//setVolt_[i]->hide();
		//measCurr_[i]->hide();
		//setVLabel_[i]->hide();
		// measALabel_[i]->hide();
		// instrumentChan_[i]->hide();
		// chanLabel_[i]->hide();
	}

	// specific for LCR meter: hide HV and V items
	checkBoxHV_5->hide();
	setVolt_5->hide();
	setVLabel_5->hide();

	extTemperBox->clear();
	extTemperBox->addItem("none");
	m_currT = -273.16;

	// Yusong's note: connect all devices, start scan, not sure what saveFileDialog() is
	// connect(Object1, signal1, Object2, slot1)
	// connDevAll() uses connDev()
	connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDevAll()));
	connect(startButton, SIGNAL(clicked()), this, SLOT(startScan()));
	connect(saveButtonBox, SIGNAL(clicked()), this, SLOT(saveFileDialog()));
	progressBar->hide();
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::IVscan END\n";}
}


IVscan::~IVscan(){
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::~IVscan()\n";}
	//deleteIVgraphs();
	if(m_can!=NULL)          delete m_can;
	if(m_canAllXYscan!=NULL) delete m_canAllXYscan;
	//for(int i=0; i<maxNumInstr; i++) disconnDev(i);
	disconnDevAll();
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::~IVscan() END\n";}
}


/*void IVscan::deleteIVgraphs(){
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::deleteIVgraphs()\n";}
	for(int i=0; i<maxNumInstr; i++) {
		if(m_grIV_[i]!=NULL) m_grIV_[i]->Delete();
	}
	for(uint i=0; i<m_grIVallXYscan_.size(); i++){
		if(m_grIV_[i]!=NULL) m_grIVallXYscan_[i]->Delete();
	}
	if(legAllXYscan!=NULL) legAllXYscan->Delete();
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::deleteIVgraphs() END\n";}
}*/


void IVscan::startScan(){
	if(DEBUG){std::cout<<"In IVscan.cxx. startScan() is ran!!!!!! \n\n";}
	int verbose = 0;

	// stop background meter reading thread, taken care of here
	m_thrRuns = false;
	if(m_thread.joinable()) m_thread.join();

	disconnect(startButton, SIGNAL(clicked()), this, SLOT(startScan()));
	connect(startButton, SIGNAL(clicked()), this, SLOT(flagAbortScanToTrue()));
	startButton->setText("Abort");
	extTemperBox->setEnabled(false);

	double Vstart = startVolts->value();
	double Vend   = endVolts->value();
	int nsteps = nSteps->value();
	double stepsize = (Vend-Vstart)/(double)(nsteps-1);
	int Nmeas = spinBoxNmeas->value(); // number of repeated measurements to calculate average
	double waitTimeRepetition = 10.; // waiting time before repeated measurement [ms]

	double rampVoltStep        = qRampVoltStep->value(); // [V]
	double rampVoltStepSpeed   = (double) qRampVoltStepSpeed->value(); // [ms]
	std::cout<<"step: "<<rampVoltStep<<" speed: "<<rampVoltStepSpeed<<"\n";
	double measurementWaitTime = qMeasurementWaitTime->value(); // waiting time before each measurement after V ramp [ms]
	double rampBackToValue     = qRampBackToValue->value(); // ramp back to this value after scan

	double currentLimit = compliance->value()*1E-6;

	for(int i=0; i<maxNumInstr; i++) gpibConnectButton->setEnabled(false);
	//startButton->setEnabled(false);
	progressBar->setRange(0,nsteps);
	progressBar->show();

	//determine which instrument is HVsrc or Imeter; check if all HVsrc start from same HV value and if there are at least one HV source and one I Meter connected
	std::vector<int> idxHVsrc_;//Yusong: index of HV source
	std::vector<int> idxImeter_;//Yusong: index of I meter(s)
	std::vector<int> idxCmeter_;
	std::vector<int> idxMeters_;
	double initialVolt=-99999.;
	bool haveCap=false; 
	bool capAsV=false;
	//for HM8118 as HV
	//if (idxConnInstr_.size()==1){
	//	if (idxConnInstr_[0]==5){initialVolt=0}
	//}

	//idxConnInstr_ is defined in IVscan.h. vector of instrument indices of connected instruments, defined in connDev(int instrIdx)
	for(uint i=0; i<idxConnInstr_.size(); i++){
		if(DEBUG){std::cout<<"in for loop of startScan. idxConnInstr_.size(): "<<idxConnInstr_.size()<<"Loop idx "<<i<<"; Start scan with connected instr. with idx " << idxConnInstr_[i] <<"; HV: "<<isHVsrc_[idxConnInstr_[i]] <<"; Imeter: "<<isImeter_[idxConnInstr_[i]]<<std::endl;}
		//deals with I or C meter, and in case of INT bias for HM8118, add to idxHVsrc_
		if(isImeter_[idxConnInstr_[i]]){
			idxImeter_.push_back(idxConnInstr_[i]);
			if(checkBoxImeas_[idxConnInstr_[i]]->text()=="C Meter"){
				haveCap=true;
				idxCmeter_.push_back(idxConnInstr_[i]);
				QString biastypestring = capBiasBox->currentText();
				//std::cout<<"biastypestring.toStdString(): "<<biastypestring.toStdString()<<"\n";
				PixRs232Device* dev = (PixRs232Device*) m_ConnInstr_[idxConnInstr_[i]];
				if(biastypestring == "INT"){
					//idxHVsrc_.push_back(idxConnInstr_[i]);
					isHVsrc_[idxConnInstr_[i]]  = true;
					capAsV=true;
					//std::cout<<"idxHVsrc_[0]: "<<idxHVsrc_[0]<<"\n";//5
					if(initialVolt==-99999) initialVolt = dev->getVoltage(instrumentChan_[idxConnInstr_[i]]->value());
				}
				//std::cout<<"idxHVsrc_.size(): "<<idxHVsrc_.size()<<"\n";
			}
		}
		std::cout<<"initialVolt: "<<initialVolt<<"\n";
		if(isHVsrc_[idxConnInstr_[i]]){
			idxHVsrc_.push_back(idxConnInstr_[i]);
			if(gpibLabel_[idxConnInstr_[i]]->currentText().contains("GPIB")){
				PixGPIBDevice* dev = (PixGPIBDevice*)m_ConnInstr_[idxConnInstr_[i]];
				dev->measureVoltages();
				if(initialVolt==-99999){initialVolt = dev->getVoltage(instrumentChan_[idxConnInstr_[i]]->value());}
				else if(initialVolt != dev->getVoltage(instrumentChan_[idxConnInstr_[i]]->value())){ 
					std::cout<<"HV sources must have same initial applied voltage! ABORT scan!" <<std::endl;
					gpibConnectButton->setEnabled(true);
					disconnect(startButton, SIGNAL(clicked()), this, SLOT(startScan()));
					connect(startButton, SIGNAL(clicked()), this, SLOT(flagAbortScanToTrue()));
					startButton->setEnabled(true);
					startButton->setText("Start IV");
					extTemperBox->setEnabled(true);
					progressBar->hide();
					idxHVsrc_.clear();
					idxImeter_.clear();
					idxCmeter_.clear();
					idxMeters_.clear();
					if(DEBUG){std::cout<<"In IVscan.cxx. startScan() END\n";}
					return;
				}
			} else {
				PixRs232Device* dev = (PixRs232Device*) m_ConnInstr_[idxConnInstr_[i]];
				//special case HM8118: the getVoltage should already set BIAS1
				if(initialVolt==-99999) initialVolt = dev->getVoltage(instrumentChan_[idxConnInstr_[i]]->value());
				else if(initialVolt != dev->getVoltage(instrumentChan_[idxConnInstr_[i]]->value())){
					std::cout<<"HV sources must have same initial applied voltage! ABORT scan!" <<std::endl;
					gpibConnectButton->setEnabled(true);
					disconnect(startButton, SIGNAL(clicked()), this, SLOT(startScan()));
					connect(startButton, SIGNAL(clicked()), this, SLOT(flagAbortScanToTrue()));
					startButton->setEnabled(true);
					startButton->setText("Start IV");
					extTemperBox->setEnabled(true);
					progressBar->hide();
					idxHVsrc_.clear();
					idxImeter_.clear();
					idxCmeter_.clear();
					idxMeters_.clear();
					if(DEBUG){std::cout<<"In IVscan.cxx. startScan() END\n";}
					return;
				}
			}
		}
	}
	//Yusong: add meters together, then remove duplicate
	for(unsigned int i=0;i<idxImeter_.size();i++){
		idxMeters_.push_back(idxImeter_[i]);
	}
	for(unsigned int i=0;i<idxCmeter_.size();i++){
		idxMeters_.push_back(idxCmeter_[i]);
	}
	//remove duplicate elements in vector
	unsigned int originalSize=idxMeters_.size();
	unsigned int newSize=originalSize;
	for(unsigned int i=0;i<idxMeters_.size();i++){
		if(i<newSize){  
			std::vector<int> duplicateIndices;
			for(unsigned int j=i+1;j<idxMeters_.size();j++){
				if (idxMeters_[i]==idxMeters_[j]){
					duplicateIndices.push_back(j);
				}
			}
			for(int j=duplicateIndices.size()-1;j>=0;j--){//loop from the back so that the indices don't change
				idxMeters_.erase(idxMeters_.begin() + duplicateIndices[j]);
			}
			newSize=idxMeters_.size();
		}
	}
	if(DEBUG){std::cout<<"Indices of connected meters: ";
		for (unsigned int i=0;i<idxMeters_.size();i++){
			std::cout<<idxMeters_[i]<<" ";
		}
		std::cout<<"\n";
	}

	if(DEBUG){std::cout<<"haveCap = "<<haveCap<<"\n";}
	if(idxHVsrc_.size()==0 || idxMeters_.size()==0){
		std::cout<<"Must have at least one HV source and one I or C Meter! ABORT scan!" <<std::endl;
		gpibConnectButton->setEnabled(true);
		disconnect(startButton, SIGNAL(clicked()), this, SLOT(startScan()));
		connect(startButton, SIGNAL(clicked()), this, SLOT(flagAbortScanToTrue()));
		startButton->setEnabled(true);
		startButton->setText("Start IV");
		extTemperBox->setEnabled(true);
		progressBar->hide();
		idxHVsrc_.clear();
		idxImeter_.clear();
		idxCmeter_.clear();
		idxMeters_.clear();
		return;
	}
	double circtype=2;//automatic circuit type
	int biastype=0;
	if(haveCap){
		QString circtypestring = capCircuitBox->currentText();
		if(circtypestring == "Par")
			circtype=1;
		else if(circtypestring == "Ser")
			circtype=0;
		else circtype=2;

		QString biastypestring = capBiasBox->currentText();
		if(biastypestring == "EXT")
			biastype=2;
		else if(biastypestring == "INT")
			biastype=1;
		else biastype=0;
		if(capAsV){
			Vstart=abs(Vstart);
			Vend=abs(Vend);
			rampBackToValue=abs(rampBackToValue);
			if(Vend>5.0){
				std::cout<<"Using HM8118 as HV, can only ramp up to +5V! You specified"<<Vend<<"V. Aborting...\n";
				flagAbortScan=true;
			}
		}
	}

	// create file name and text file header
	std::string homedir = getenv("HOME");
	if(DEBUG){std::cout << "Home directory: "<<homedir << "\n";}
	QString fileNameQ = folderText->text();
	if (!fileNameQ.endsWith("/")){
		fileNameQ=fileNameQ+"/";
	}
	if (fileNameQ.contains("~")){
		fileNameQ.replace("~",QString::fromStdString(homedir));
	}
	std::string fileName;
	std::string fileNameAllXYscan;

	if(deviceNameFolderBox->isChecked()) fileNameQ+=deviceNameText->text()+"/";
	if(deviceNameBox->isChecked()) fileNameQ+=deviceNameText->text();
	fileNameQ+="_"+fileNameBaseText->text();
	if(measIndexBox->isChecked()) fileNameQ+="_"+measIndexText->text();
	if(irradiationBox->isChecked()) fileNameQ+="_Phi"+irradiationText->text();
	if(annealingBox->isChecked()) fileNameQ+="_"+annealingText->text();
	if(userBox->isChecked()) fileNameQ+="_"+userText->text();
	if(commentBox->isChecked()) fileNameQ+="_"+commentText->text();

	if(stepX!=-1 &&stepY!=-1) {
		fileNameAllXYscan = fileNameQ.toStdString()+"_AllXYscan";
		//std::cout<<"File name All XYscan: " << fileNameAllXYscan << std::endl;
		fileNameQ+="_X"+QString::number( stepX)+"_Y"+QString::number(stepY);
	}
	fileName = fileNameQ.toStdString(); 
	if(DEBUG){std::cout<<"Output file name: " << fileName << std::endl;}
	std::ofstream outTxtFile;
	outTxtFile.open((fileName+".txt").c_str(), std::ofstream::out | std::ofstream::trunc);
	if(outTxtFile.fail()){std::cout<<"outTxtFile somehow failed! Not writing anything in .txt !\n";}
	std::ofstream outTxtFileRepetitions;
	if (Nmeas>1) {
		outTxtFileRepetitions.open((fileName+"_repetitions.txt").c_str(), std::ofstream::out | std::ofstream::trunc);
		if(outTxtFileRepetitions.fail()){std::cout<<"outTxtFileRepetitions somehow failed! Not writing all repetitions in .txt !\n";}
	}
	//get date and time
	std::chrono::time_point<std::chrono::system_clock> startIV = std::chrono::system_clock::now();
	std::time_t scan_time = std::chrono::system_clock::to_time_t(startIV);
	std::tm* timeinfo;
	char timeChar [80];
	std::time(&scan_time);
	timeinfo = std::localtime(&scan_time);
	std::strftime(timeChar,80,"%Y-%m-%d_%H:%M",timeinfo); //custom date format
	// write header in data file
	outTxtFile << deviceNameText->text().toStdString()<<" " << fileNameBaseText->text().toStdString()<<std::endl;
	outTxtFile << "Index: " << measIndexText->text().toStdString() << ", Irr.: " << irradiationText->text().toStdString() << ", Ann.: " << annealingText->text().toStdString( ) << ", Comments: " << commentText->text().toStdString();
	outTxtFile << ", Ccorr. [pF]: " << Ccorrection->value();
	if(stepX!=-1 && stepY!=-1){outTxtFile << ", Step X index: " <<  stepX << ", Rel. pos. X: " << posX << ", Step Y index: " <<  stepY << ", Relative position Y: " << posY;}
	outTxtFile<< std::endl;
	outTxtFile << "UniGoe\t" <<userText->text().toStdString()<< "\t" << timeChar <<std::endl;//std::ctime(&scan_time);
	outTxtFile << "Stepsize or Vstart: "<<(abs(stepsize) ? abs(stepsize) : Vstart) << "\t"<<"measurementWaitTime[s]: "<<measurementWaitTime*1e-3 << "\t" <<"Nof repetitions: "<< Nmeas << "\t"; // Voltage step, delay, measurements per step,
	if(haveCap){ // compliance for IV, frequency for CV
		outTxtFile << "\ncirc: "<<(capCircuitBox->currentText()).toStdString()<<"\tfreq[Hz]: "<<capFreqBox->currentText().toDouble()*1.e3 <<"\tVeff: "<<boxCVeff->value()<<"\tbias: "<<(capBiasBox->currentText()).toStdString()<<"\n";
	}else{outTxtFile <<"compliance: "<< currentLimit <<std::endl; // compliance for IV, frequency for CV
	}
	outTxtFile << "chuckT: " << m_ChuckT << "\tRH: " << m_RH << std::endl;
	outTxtFile << "t/s\tU/V";
	if (Nmeas>1) {outTxtFileRepetitions << "t/s\tU/V";}

	// if no env monitoring, printout warning
	if(m_ChuckT==-9999 || m_RH==-9999) {
		std::cout << std::endl << "********************************" << std::endl;
		std::cout << "WARNING! No temperature or RH logging for environment!!!"  << std::endl;
		std::cout << "********************************\n" << std::endl;
	}
	if(m_chillerTmeas_tr==-9999){std::cout << "WARNING! No temperature from chiller!!!!!!\n"  << std::endl;}
	std::cout<<"initialVolt: "<<initialVolt<<"\n";

	// create canvas for IV and CV
	m_can = (TCanvas*) gROOT->FindObject("ivcan");
	if(m_can==NULL) m_can = new TCanvas("ivcan","IV scan reslut",10,10,800,600);
	m_can->Clear();
	if(haveCap){
		m_can->Divide(1,2);
		m_can->cd(1);
	}
	gPad->SetRightMargin(0.3);
	// create canvas for all XY steps in case of XY scan
	if(stepX!=-1 &&stepY!=-1){
		m_canAllXYscan = (TCanvas*) gROOT->FindObject("ivcanAllXYscan");
		if(m_canAllXYscan==NULL) m_canAllXYscan = new TCanvas("ivcanAllXYscan","IV scan reslut all XY",810,10,800,600);
		if(stepX==1 && stepY==1) m_canAllXYscan->Clear();
		gPad->SetRightMargin(0.3);
	}

	// clean up before doing completely new IV scan (but not during XY scan)
	if((stepX==-1 && stepY==-1) || (stepX==1 && stepY==1)){
		m_grIVallXYscan_.clear();
		/*for(uint i=0; i<m_grIVallXYscan_.size(); i++){
			std::cout << "Here1 before deleting m_grIVallXYscan" << std::endl;
			if(m_grIVallXYscan_[i]!=NULL) m_grIVallXYscan_[i]->Delete();
			std::cout << "Here2 after deleting m_grIVallXYscan" << std::endl;
		}
		for(int i=0; i<maxNumInstr; i++){
			if(m_grIV_[i]!=NULL ) m_grIV_[i]->Delete();
		}*/
	}

	// set range, title, legend for the canvas
	TGraph *oldg = (TGraph*) gROOT->FindObject("ivcurves");
	if(oldg!=NULL) oldg->Delete();
	oldg = (TGraph*) gROOT->FindObject("cvcurves");
	if(oldg!=NULL) oldg->Delete();
	oldg = NULL;
	TGraph * dummy = new TGraph(2);
	dummy->SetPoint(0,Vstart,-1);
	dummy->SetPoint(1,Vend,1);
	TGraph * dummy2 = new TGraph(2);
	dummy2->SetPoint(0,Vstart,-1);
	dummy2->SetPoint(1,Vend,1);
	if(haveCap) m_can->cd(1);
	else m_can->cd();
	dummy->Draw("AP");
	dummy->SetTitle("IV Curves");
	dummy->SetName("ivcurves");
	gROOT->GetList()->Add(dummy); // TGraphs must be added explicitly, otherwise not found later
	dummy->GetXaxis()->SetTitle("Voltage [V]");
	dummy->GetYaxis()->SetTitle("Current [A]");
	dummy->GetYaxis()->SetTitleOffset(1.1);
	if(haveCap){
		m_can->cd(2);
		dummy2->Draw("AP");
		dummy2->SetTitle("CV Curves");
		dummy2->SetName("cvcurves");
		gROOT->GetList()->Add(dummy2); // TGraphs must be added explicitly, otherwise not found later
		dummy2->GetXaxis()->SetTitle("Voltage [V]");
		dummy2->GetYaxis()->SetTitle("Capacity [pF]");
		dummy2->GetYaxis()->SetTitleOffset(1.1);
	}
	if(stepX==1 && stepY==1) {
		m_canAllXYscan->cd();
		dummy->Draw("AP");
		//if(legAllXYscan!=NULL) legAllXYscan->Delete();
		legAllXYscan = new TLegend(0.72,0.1,0.98,0.9);
		if(NScan>50) legAllXYscan->SetNColumns(2);
	}

	TLegend* leg=new TLegend(0.72,0.5,0.98,0.9);
	for(uint i=0; i<idxMeters_.size(); i++){
		std::string graphName = sensorPart_[idxMeters_[i]]->text().toStdString();
		std::stringstream a,b;
		a << stepX;
		b << stepY;
		if(stepX!=-1 &&stepY!=-1) graphName=graphName+"_X"+a.str()+"_Y"+b.str();
		m_grIV_[i] = new TGraphErrors();
		m_grIV_[i]->SetName( graphName.c_str() );
		m_grIV_[i]->SetTitle(sensorPart_[idxMeters_[i]]->text().toStdString().c_str());
		m_grIV_[i]->SetMarkerStyle(20+i);
		m_grIV_[i]->SetMarkerColor(1+i+iScan);
		m_grIV_[i]->SetLineColor(1+i+iScan);
		leg->AddEntry(m_grIV_[i], graphName.c_str(),"p");
		if(stepX!=-1 && stepY!=-1) legAllXYscan->AddEntry(m_grIV_[i], graphName.c_str(),"p");

		if(checkBoxImeas_[idxMeters_[i]]->text()=="C Meter"){
			outTxtFile <<"\t"<<sensorPart_[idxMeters_[i]]->text().toStdString()<<"_Cavg/pF";
			outTxtFile <<"\t"<<sensorPart_[idxMeters_[i]]->text().toStdString()<<"_Cunc/pF";//still has unc column even if no repetitions
			//if(Nmeas>1) outTxtFile <<"\t"<<sensorPart_[idxMeters_[i]]->text().toStdString()<<"_Cunc/pF";
			outTxtFileRepetitions <<"\t"<<sensorPart_[idxMeters_[i]]->text().toStdString()<<"_Cavg/pF";
			if(Nmeas>1) outTxtFileRepetitions <<"\t"<<sensorPart_[idxMeters_[i]]->text().toStdString()<<"_Cunc/pF\n";
		} else {
			outTxtFile <<"\t"<<sensorPart_[idxMeters_[i]]->text().toStdString()<<"_Iavg/uA";
			outTxtFile <<"\t"<<sensorPart_[idxMeters_[i]]->text().toStdString()<<"_Iunc/uA";//still has unc column even if no repetitions
			//if(Nmeas>1) outTxtFile <<"\t"<<sensorPart_[idxMeters_[i]]->text().toStdString()<<"_Iunc/uA";
			outTxtFileRepetitions <<"\t"<<sensorPart_[idxMeters_[i]]->text().toStdString()<<"_Iavg/uA";
			if(Nmeas>1) outTxtFileRepetitions <<"\t"<<sensorPart_[idxMeters_[i]]->text().toStdString()<<"_Iunc/uA\n";
		}
		//if(extTemperBox->currentText()!="none") outTxtFile << "\t "+std::string(extTemperBox->currentText().toLatin1().data())+" [C]";
	}
	outTxtFile << "\tT/°C\tRH/%\tchillerT/°C" <<std::endl;
	double yRangeMin = 0., yRangeMax = 0.;
	double cRangeMin = 0., cRangeMax = 0.;
	
	// start real scan
	std::chrono::time_point<std::chrono::high_resolution_clock> measureCurrentStep0 = std::chrono::high_resolution_clock::now();
	double actualVolt = initialVolt;
	for(int i=0;i<nsteps;i++){
		double volt = Vstart+stepsize*(double)i;
		double rslt;//stores result (curr or cap) of 1 measurement (only 1 val)
		int NmeasSucceeded=0; // number of succeeded measurements in this (volt) step

		// ramp to next voltage step
		//std::cout << "step: " <<i <<"; next voltage step: " <<volt<<"; actualVolt: " <<actualVolt<<std::endl;
		actualVolt=rampVolt(idxHVsrc_, actualVolt, volt, rampVoltStep, rampVoltStepSpeed);
		if(flagAbortScan){
			std::cout << "ABORT scan clicked! Ramp down to "<< rampBackToValue << " V and exit!" <<std::endl;
			flagAbortScan=false;
			break;
		}
		std::this_thread::sleep_for (std::chrono::milliseconds((int)measurementWaitTime));

		// measure current or capacitance
		double sum_i[10]={0.};// sum_i[k][iMeas], k different devices, iMeas nof already measured repetitions
		double rslt_[10][100]={0.};//can handel 10 devices, each measure 100 repetitions
		std::vector<int> NmeasSucceeded_list;
		std::chrono::time_point<std::chrono::high_resolution_clock> measureCurrentStart = std::chrono::high_resolution_clock::now();
		if(i==0){measureCurrentStep0 = measureCurrentStart;}
		outTxtFile << std::chrono::duration_cast<std::chrono::seconds>(measureCurrentStart - measureCurrentStep0).count();
		outTxtFile << "\t" << actualVolt;
		if (Nmeas>1) {outTxtFileRepetitions << std::chrono::duration_cast<std::chrono::seconds>(measureCurrentStart - measureCurrentStep0).count();}
		if (Nmeas>1) {outTxtFileRepetitions << "\t" << actualVolt;}

		for(int iMeas=0; iMeas<Nmeas; iMeas++){ // measurement repetitions for average. If only meas once, iMeas=0, Nmeas=1
			if(iMeas>0){std::this_thread::sleep_for (std::chrono::milliseconds((int)waitTimeRepetition));}
			for(uint k=0; k<idxMeters_.size(); k++){
				std::string theString=(gpibLabel_[idxMeters_[k]]->currentText()).toStdString();//QString
				//if(DEBUG){std::cout<<"idxMeters_.size()"<<idxMeters_.size()<<", k = "<<k<<"gpibLabel_[idxMeters_[k]]->currentText()"<<theString<<"\n";}
				rslt = 0.;

				if(gpibLabel_[idxMeters_[k]]->currentText().contains("GPIB")){//Yusong: if the text box in GUI currently showstext that contains "GPIB"
					if(DEBUG){std::cout<<"currentText().contains(\"GPIB\")\n";}
					PixGPIBDevice* dev = (PixGPIBDevice*)m_ConnInstr_[idxMeters_[k]];
					dev->measureCurrents();
					rslt = dev->getCurrent(instrumentChan_[idxConnInstr_[k]]->value()); //actual measurement
					if(std::abs(rslt) > 0.99*compliance->value()*1E-6) flagAbortScan=true; // abort if current is reaching compliance
				} else {
					if(DEBUG){std::cout<<"currentText() does not contain \"GPIB\"\n";}
					PixRs232Device* dev = (PixRs232Device*)m_ConnInstr_[idxMeters_[k]];
					theString=(checkBoxImeas_[idxConnInstr_[k]]->text()).toStdString();
					if(DEBUG){std::cout<<"Index of connected instrument "<<k<<": "<<idxConnInstr_[k]<<" checkBoxImeas_[idxConnInstr_[k]]->text(): "<<theString<<"\n";}
					if(checkBoxImeas_[idxMeters_[k]]->text()=="C Meter") { // LCR meter
						double freq = capFreqBox->currentText().toDouble()*1.e3;
						double veff = boxCVeff->value();
						rslt = dev->getCapacity(instrumentChan_[idxConnInstr_[k]]->value(), freq, veff, circtype, biastype, i)*1.e12 - Ccorrection->value(); //converted to pF, corrected with user defined correction
					} else {
						rslt = dev->getCurrent(instrumentChan_[idxConnInstr_[k]]->value());  //actual measurement
					}
				}
				rslt_[k][iMeas]=rslt;
				if(DEBUG){std::cout<<"In IVscan.cxx::startScan(). rslt: "<<rslt<<"\n";}
				if (rslt!=-4e+12 and rslt!=-3e+12 and rslt!=-2e+12 and rslt!=-1e+12){//4 possible error values from getCapacity
					sum_i[k]+=rslt;
					NmeasSucceeded+=1;
					if(DEBUG){std::cout<<"Repetition "<<iMeas<<" rslt!=-4 or -3 or -2 or -1e+12 rslt: "<<rslt<<"\n";}
				}else{std::cout<<"Repetition "<<iMeas<<" rslt="<<rslt<<"!!!!!! One measurement not good\n";}
			}
		}
		NmeasSucceeded_list.push_back(NmeasSucceeded);

		// 1 meas point for all devices and repetitions done, calculate and display measured average current
		for(uint k=0; k<idxMeters_.size(); k++){
			// calculate average
			double avg_i=-9999.;
			if (NmeasSucceeded_list[k]!=0){
				avg_i = sum_i[k]/NmeasSucceeded_list[k];
			}else{//if 0 measurements succeeded (all -4e+12 or -3e+12 or -1e+12), use the last value as average instead of averaging
				avg_i = rslt_[k][Nmeas-1];
			}
			if (NmeasSucceeded_list[k]!=Nmeas){
				std::cout<<"Not all repetitions good! Only "<<NmeasSucceeded_list[k]<<" out of "<<Nmeas<<" good\n";
			}else{std::cout<<"All "<<NmeasSucceeded_list[k]<<" repetitions good\n";}
			// calculate uncertainty
			double diff_i_sqr = 0;
			for(int iMeas=0; iMeas<Nmeas; iMeas++){//add to diff sqr if meas good
				if(rslt_[k][iMeas]!=-4e+12 and rslt_[k][iMeas]!=-3e+12 and rslt_[k][iMeas]!=-2e+12 and rslt_[k][iMeas]!=-1e+12){
					diff_i_sqr += std::pow((rslt_[k][iMeas]-avg_i),2);
				}
			}
			double unc_i = 0.0;
			if(NmeasSucceeded_list[k]>1){//calculate std. dev. of mean if at least 2 meas good
				unc_i = sqrt( diff_i_sqr/ ((NmeasSucceeded_list[k])*((NmeasSucceeded_list[k])-1)) );
			}//otherwise unc_i remains 0
			m_grIV_[k]->SetPoint(i, volt, avg_i);
			m_grIV_[k]->SetPointError(i, 0, unc_i);
			double conversionFactor=1; // to convert to uA
			if(checkBoxImeas_[idxMeters_[k]]->text()=="C Meter"){
				if(cRangeMin>1.2*avg_i) cRangeMin = 1.2*avg_i;
				if(cRangeMax<1.2*avg_i) cRangeMax = 1.2*avg_i;
				m_can->cd(2);
				m_grIV_[k]->Draw("PL same");
			} else {
				if(yRangeMin>1.2*avg_i) yRangeMin = 1.2*avg_i;
				if(yRangeMax<1.2*avg_i) yRangeMax = 1.2*avg_i;
				if(haveCap)
					m_can->cd(1);
				else
					m_can->cd();
				m_grIV_[k]->Draw("PL same");
				leg->Draw();
				conversionFactor=1e6;
			}
			// even if only 1 succeeded, write uncertainty 0.0
			outTxtFile << "\t"<<avg_i*conversionFactor;
			outTxtFile << "\t"<<unc_i*conversionFactor;
			//if(NmeasSucceeded_list[k]>1) outTxtFile << "\t"<<unc_i*conversionFactor;
			// has a txt file for repetitions only if have repetitions
			if(Nmeas>1){
				outTxtFileRepetitions << "\t"<<avg_i*conversionFactor;
				outTxtFileRepetitions << "\t"<<unc_i*conversionFactor;
				for (int iMeas=0; iMeas<Nmeas; iMeas++){
					outTxtFileRepetitions << "\t"<<rslt_[k][iMeas];
				}
			}
			measCurr_[idxMeters_[k]]->display(avg_i);
			QApplication::processEvents(); // update GUI
		}

		std::chrono::time_point<std::chrono::high_resolution_clock> measureCurrentEnd = std::chrono::high_resolution_clock::now();
		dummy->GetYaxis()->SetRangeUser(yRangeMin, yRangeMax);
		if(haveCap) dummy2->GetYaxis()->SetRangeUser(cRangeMin, cRangeMax);
		m_can->Update();
		//if(extTemperBox->currentText()!="none") outTxtFile << "\t"<<m_currT;
		outTxtFile << "\t" << m_ChuckT << "\t" << m_RH << "\t" << m_chillerTmeas_tr << std::endl;
		//if (labelMeasT->isVisible()){ outTxtFile << "\t" << (labelMeasT->currentText()) << std::endl;}#Yusong: doesn't work.  error: ‘labelMeasT’ was not declared in this scope
		if(Nmeas>1){outTxtFileRepetitions << "\n";}
		progressBar->setValue(i);
		QApplication::processEvents();
		
		if(flagAbortScan){
			std::cout << "Compliance reached! Ramp down to "<< rampBackToValue << " V and exit!" <<std::endl;
			flagAbortScan=false;
			break;
		}
		if(verbose){
			std::chrono::time_point<std::chrono::high_resolution_clock> endOfMeasurementPoint = std::chrono::high_resolution_clock::now();
			std::cout << "  I meas. time [ms]: " << std::chrono::duration_cast<std::chrono::milliseconds>(measureCurrentEnd - measureCurrentStart).count();
			std::cout << "  Update+Process [ms]: " << std::chrono::duration_cast<std::chrono::milliseconds>(endOfMeasurementPoint - measureCurrentEnd).count() << std::endl;
		}
	}

	// draw full graph at end of each IV measurement
	std::chrono::time_point<std::chrono::high_resolution_clock> beforeDrawToAll = std::chrono::high_resolution_clock::now();
	for(uint k=0; k<idxMeters_.size(); k++){
		if(stepX!=-1 && stepY!=-1) {
					m_grIVallXYscan_.push_back(m_grIV_[k]);
					m_canAllXYscan->cd(); 
					//m_grIV_[k]->DrawClone("PL same");
					m_grIV_[k]->Draw("PL same");
					legAllXYscan->Draw();
		}
	}
	if(verbose){
		std::chrono::time_point<std::chrono::high_resolution_clock> afterDrawToAll = std::chrono::high_resolution_clock::now();
		std::cout << "  Draw to All XY [us]: " << std::chrono::duration_cast<std::chrono::microseconds>(afterDrawToAll - beforeDrawToAll).count() << std::endl;
	}

	// ramp voltage to desired final value at end
	//std::cout<<"ramping back voltage... actualVolt, rampBackToValue, rampVoltStep, rampVoltStepSpeed/10.:\n"<<actualVolt<<" "<<rampBackToValue<<" "<<rampVoltStep<<" "<<(rampVoltStepSpeed/10.0)<<"\n";//2 0 1 100
	rampVolt(idxHVsrc_, actualVolt, rampBackToValue, rampVoltStep, rampVoltStepSpeed/10.);
	//std::cout<<"ramp back finished!";

	progressBar->setValue(nsteps);

	// save data
	std::string rootFileOption = "RECREATE";
	TCanvas * canvToSave = m_can;
	std::string fileNameToSave = fileName;
	if(stepX!=-1 &&stepY!=-1) {
		canvToSave=m_canAllXYscan;
		fileNameToSave=fileNameAllXYscan;
		if(iScan!=0) rootFileOption = "UPDATE";
	}
	if(checkBoxPng->isChecked()) canvToSave->Print((fileNameToSave+".png").c_str());
	if(checkBoxTGraph->isChecked()){
		TFile rootFile((fileNameToSave+".root").c_str(),rootFileOption.c_str());
		for(uint i=0; i<idxMeters_.size(); i++) m_grIV_[i]->Write();
		rootFile.Close();
	}
	outTxtFile.close();
	if(Nmeas>1){outTxtFileRepetitions.close();}
	if(DEBUG){std::cout<<"Output txt file closed!\n";}

	gpibConnectButton->setEnabled(true);
	progressBar->hide();

	disconnect(startButton, SIGNAL(clicked()), this, SLOT(flagAbortScanToTrue() ));
	connect(startButton, SIGNAL(clicked()), this, SLOT(startScan()));
	startButton->setText("Start IV");
	extTemperBox->setEnabled(true);
	flagAbortScan = false;

	idxHVsrc_.clear();
	idxMeters_.clear();
	idxImeter_.clear();
	idxCmeter_.clear();

	// re-start backround meter reading thread
	m_thrRuns = true;
	m_thread = std::thread(&IVscan::readDevThread, this);

	std::chrono::time_point<std::chrono::system_clock> endIV = std::chrono::system_clock::now();
	std::cout << "Duration IV scan [s]: "<< (std::chrono::duration_cast<std::chrono::seconds>(endIV - startIV)).count()	<< std::endl;
	if(DEBUG){std::cout<<"In IVscan.cxx. startScan() END \n"<<std::endl;}
}


double IVscan::rampVolt(std::vector<int> idxHVsrc_, double start, double end, double rampVoltStep, double rampVoltStepSpeed){
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::rampVolt\n";}
	double actualVolt = start;
	double rampUpOrDown = 0; // scan up or down?

	while( std::abs(actualVolt - end) > rampVoltStep){
		if(flagAbortScan){
			if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::rampVolt END, scan aborted\n";}
			return actualVolt; // abort if required
		}
		rampUpOrDown = (actualVolt < end) ? 1 : -1; // scan up or down?
		actualVolt+=rampUpOrDown*rampVoltStep;
		for(uint j=0; j<idxHVsrc_.size(); j++) {
			if(gpibLabel_[idxHVsrc_[j]]->currentText().contains("GPIB")){
				PixGPIBDevice* dev = (PixGPIBDevice*)m_ConnInstr_[idxHVsrc_[j]];
				dev->setVoltage(instrumentChan_[idxConnInstr_[j]]->value(), actualVolt);
				setVolt_[idxHVsrc_[j]]->display(actualVolt);
				//std::cout <<" actualVolt: " <<actualVolt<<std::endl; 
			} else {
				PixRs232Device* dev = (PixRs232Device*) m_ConnInstr_[idxConnInstr_[j]];
				//std::cout<<"dev->getDescription(): "<<dev->getDescription()<<"\n";//HAMEG Instruments, HM8118,053400088,1.57
				if(dev->getDeviceType()==HM8118){
					if(end>5.0 or end<-5.0){//this should not happen since there is already a flagAbort... in startScan()
						std::cout<<"Using HM8118 as HV, can only ramp up to +5V! You specified"<<end<<"V. Aborting...\n";
					}
				}
				dev->setVoltage(instrumentChan_[idxConnInstr_[j]]->value(), actualVolt);
				setVolt_[idxHVsrc_[j]]->display(actualVolt);//desplay volt at the box in GUI
			}
		}
		if(flagAbortScan){
			if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::rampVolt END\n";}
			return actualVolt; // abort if required
		}
		QApplication::processEvents();
		std::this_thread::sleep_for (std::chrono::milliseconds((int)rampVoltStepSpeed));
		//std::cout <<" actualVolt: " <<actualVolt<<std::endl;
	}
	for(uint j=0; j<idxHVsrc_.size(); j++) {
		actualVolt=end;
		if(gpibLabel_[idxHVsrc_[j]]->currentText().contains("GPIB")){
			PixGPIBDevice* dev = (PixGPIBDevice*)m_ConnInstr_[idxHVsrc_[j]];
			dev->setVoltage(instrumentChan_[idxConnInstr_[j]]->value(), actualVolt);
			setVolt_[idxHVsrc_[j]]->display(actualVolt);
		} else {
			PixRs232Device* dev = (PixRs232Device*) m_ConnInstr_[idxConnInstr_[j]];
			dev->setVoltage(instrumentChan_[idxConnInstr_[j]]->value(), actualVolt);
			setVolt_[idxHVsrc_[j]]->display(actualVolt);
		}
	}
	QApplication::processEvents();
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::rampVolt END\n";}
	return actualVolt;
}


void IVscan::flagAbortScanToTrue(){
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::flagAbortScanToTrue ran\n";}
	flagAbortScan = true;
}


void IVscan::connDevAll() {
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::connDevAll()\n";}
	bool allDevicesGood = true;
	bool nHVsrcGood = true;
	int nHVsrc = 0;
	// connect all devices selected as source or meter
	for(int i=0; i<maxNumInstr; i++) {
		int returnValue = connDev(i);
		if(isHVsrc_[i]) nHVsrc++;
		if(nHVsrc > spinBoxTotalNoHVsrc->value() ){
			nHVsrcGood=false;
			break;
		}
		if(returnValue==-1) {
			allDevicesGood=false;
			break;
		}
	}
	gpibConnectButton->setText("Disconnect Selected");
	disconnect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDevAll()));
	connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(disconnDevAll()));
	startButton->setEnabled(true);
	m_thrRuns = true;
	m_thread = std::thread(&IVscan::readDevThread, this);
	if(!idxConnInstr_.size()) {
		std::cout <<"No HV source or I meter connected. Try again!"<<std::endl;
		disconnDevAll();
	}else if(!allDevicesGood) {
		std::cout << "Not all devices good. Aborting connection of all instruments!"<<std::endl;
		disconnDevAll();
	}else if(!nHVsrcGood) {
		std::cout << "Number of HV Src checked exceeds desired total no. of HV Src. Aborting connection of all instruments!"<<std::endl;
		disconnDevAll();
	}
	else std::cout <<"Connected "<<idxConnInstr_.size()<<" instruments."<<std::endl;
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::connDevAll END\n";}
}


int IVscan::connDev(int instrIdx) {
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::connDev\n";}
	if(checkBoxHV_[instrIdx]->isChecked()){
		isHVsrc_[instrIdx]  = true;
	}else {isHVsrc_[instrIdx]  = false;}
	//if((checkBoxImeas_[idxConnInstr_[instrIdx]]->text()=="C Meter")){
	//	QString theCheckBoxImeasText=checkBoxImeas_[idxConnInstr_[instrIdx]]->text();
	//	std::cout<<"!!! !!! test for C meter: "<<theCheckBoxImeasText.toStdString()<<"\n";
	//	isHVsrc_[instrIdx]  = true;
	//}
	if(checkBoxImeas_[instrIdx]->isChecked()) isImeter_[instrIdx] = true;
	else isImeter_[instrIdx] = false;
	bool forceMeter = false;

	if(!isHVsrc_[instrIdx] && !isImeter_[instrIdx]) {
		// disable not connected channels
		// Yusong's note: I think setEnabled(false) grays the button/option window
		instrumentLabel_[instrIdx]->setEnabled(false);
		sensorPart_[instrIdx]->setEnabled(false);
		checkBoxHV_[instrIdx]->setEnabled(false);
		checkBoxImeas_[instrIdx]->setEnabled(false);
		gpibLabel_[instrIdx]->setEnabled(false);
		gpibPAD_[instrIdx]->setEnabled(false);
		setVolt_[instrIdx]->setEnabled(false);
		measCurr_[instrIdx]->setEnabled(false);
		setVLabel_[instrIdx]->setEnabled(false);
		measALabel_[instrIdx]->setEnabled(false);
		instrumentChan_[instrIdx]->setEnabled(false);
		chanLabel_[instrIdx]->setEnabled(false);
		return 0; // only connect device if selected as source or meter
	}else if(!isHVsrc_[instrIdx] && isImeter_[instrIdx]){forceMeter = true;} // if device should only act as meter (and not as source) [DOES NOT WORK FOR NOW]
	complianceLabel->setEnabled(false);
	compliance->setEnabled(false);
	totalNoHVsrcLabel->setEnabled(false);
	spinBoxTotalNoHVsrc->setEnabled(false);

	//check if instrument has been already connected
	if(m_ConnInstr_[instrIdx]!=NULL){
		std::cout << "Instrument with index "<<instrIdx<<" already connected (NOT NULL). Disconnect first before re-connecting!!"<<std::endl;
		return 0;
	}
	for (uint i=0; i<idxConnInstr_.size(); i++){
		if(instrIdx==idxConnInstr_[i]){
			std::cout << "Instrument with index "<<instrIdx<<" already connected. Disconnect first before re-connecting!!"<<std::endl;
			return 0;
		}
		if(gpibPAD_[instrIdx]==gpibPAD_[idxConnInstr_[i]]){
			std::cout << "Istrument with GPIB PAD "<<gpibPAD_[instrIdx]<<" already connected. Disconnect first before re-connecting!!"<<std::endl;
			return 0;
		}
	}

	double currentLimit = compliance->value()*1E-6;
	
	idxConnInstr_.push_back(instrIdx);

	if(gpibLabel_[instrIdx]->currentText().contains("GPIB")){ // if it is a GPIB device
		PixGPIBDevice* thisInstr = new PixGPIBDevice(0, gpibPAD_[instrIdx]->value(), 1, forceMeter);
		if(thisInstr->getDeviceFunction()==SUPPLY_HV || thisInstr->getDeviceFunction()==METER){ // check if instrument is the proper one
			m_ConnInstr_[instrIdx] = (void*)thisInstr;
			gpibMessage_[instrIdx]->setText("Conn. device is "+QString(thisInstr->getDescription())+ " with "+
											QString::number(thisInstr->getDeviceNumberChannels())+ " ch.");
			instrumentChan_[instrIdx]->setValue(0);
			instrumentChan_[instrIdx]->setMaximum(thisInstr->getDeviceNumberChannels()-1);
			instrumentChan_[instrIdx]->setMinimum(0);
			
			if(thisInstr->getDeviceFunction()==SUPPLY_HV){
				thisInstr->setVoltage(0, 0.0); //Set voltage 0.0 when not doing a scan
				thisInstr->setCurrentLimit(0, currentLimit);
			}
			thisInstr->setState(PixGPIBDevice::PGD_ON);
			std::this_thread::sleep_for (std::chrono::milliseconds(100));
			
			gpibPAD_[instrIdx]->setEnabled(false);
			checkBoxHV_[instrIdx]->setEnabled(false);
			checkBoxImeas_[instrIdx]->setEnabled(false);
			
			setVolt_[instrIdx]->display(0.);
			measCurr_[instrIdx]->display(0.);
			
			std::cout<<"Connected instrument with index " << instrIdx <<": "<<thisInstr->getDescription();
			if      (thisInstr->getDeviceFunction()==SUPPLY_HV) std::cout << "  Function: HV source." << std::endl;
			else if (thisInstr->getDeviceFunction()==METER)     std::cout << "  Function: AMPERE METER." << std::endl;
		} else {
			delete thisInstr;
			m_ConnInstr_[instrIdx] = NULL;
		}
	} else { // asume it's a RS232 device
		PixRs232Device* thisInstr = new PixRs232Device((PixRs232Device::Portids)(gpibPAD_[instrIdx]->value()-1+PixRs232Device::COM1)); 
		if(thisInstr->getDeviceFunction()==LCRMETER || thisInstr->getDeviceFunction()==SUPPLY_HV){ // check if instrument is the proper one
			m_ConnInstr_[instrIdx] = (void*)thisInstr;
			gpibMessage_[instrIdx]->setText("Conn. device is "+QString(thisInstr->getDescription())+ " with "+
											QString::number(thisInstr->getDeviceNumberChannels())+ " ch.");
			instrumentChan_[instrIdx]->setValue(0);
			instrumentChan_[instrIdx]->setMaximum(thisInstr->getDeviceNumberChannels()-1);
			instrumentChan_[instrIdx]->setMinimum(0);

			gpibPAD_[instrIdx]->setEnabled(false);
			checkBoxHV_[instrIdx]->setEnabled(false);
			checkBoxImeas_[instrIdx]->setEnabled(false);
			
			setVolt_[instrIdx]->display(0.);
			measCurr_[instrIdx]->display(0.);
			
			std::cout<<"Connected instrument with index " << instrIdx <<": "<<thisInstr->getDescription()<<std::endl;
		} else {
			delete thisInstr;
			m_ConnInstr_[instrIdx] = NULL;
		}
	}
	
	if(m_ConnInstr_[instrIdx] == NULL) {
		gpibMessage_[instrIdx]->setText("ERROR connecting");
		std::cout<<"ERROR connecting instrument with index " << instrIdx <<std::endl;
		instrumentLabel_[instrIdx]->setEnabled(false);
		sensorPart_[instrIdx]->setEnabled(false);
		checkBoxHV_[instrIdx]->setEnabled(false);
		checkBoxImeas_[instrIdx]->setEnabled(false);
		gpibLabel_[instrIdx]->setEnabled(false);
		gpibPAD_[instrIdx]->setEnabled(false);
		setVolt_[instrIdx]->setEnabled(false);
		measCurr_[instrIdx]->setEnabled(false);
		setVLabel_[instrIdx]->setEnabled(false);
		measALabel_[instrIdx]->setEnabled(false);
		instrumentChan_[instrIdx]->setEnabled(false);
		chanLabel_[instrIdx]->setEnabled(false);
		if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::connDev END\n";}
		return -1;
	}else{
		if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::connDev END\n";}
		return 1;
	}
	if(DEBUG){std::cout<<"In IVscan.cxx. IVscan::connDev END\n";}
}


void IVscan::disconnDevAll() {
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::disconnDevAll\n";}
	// disconnect all devices selected as source or meter
	m_thrRuns = false;
	if(m_thread.joinable()) m_thread.join();
	for(int i=0; i<maxNumInstr; i++) disconnDev(i);
	gpibConnectButton->setText("Connect Selected");
	disconnect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(disconnDevAll()));
	connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDevAll()));
	startButton->setEnabled(false);
	complianceLabel->setEnabled(true);
	compliance->setEnabled(true);
	totalNoHVsrcLabel->setEnabled(true);
	spinBoxTotalNoHVsrc->setEnabled(true);
	idxConnInstr_.clear();
	isHVsrc_.clear();
	isImeter_.clear();
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::disconnDevAll END\n";}
}


void IVscan::disconnDev(int instrIdx) {
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::disconnDev\n";}
	// enable again all devices
	instrumentLabel_[instrIdx]->setEnabled(true);
	sensorPart_[instrIdx]->setEnabled(true);
	checkBoxHV_[instrIdx]->setEnabled(true);
	checkBoxImeas_[instrIdx]->setEnabled(true);
	gpibLabel_[instrIdx]->setEnabled(true);
	gpibPAD_[instrIdx]->setEnabled(true);
	setVolt_[instrIdx]->setEnabled(true);
	measCurr_[instrIdx]->setEnabled(true);
	setVLabel_[instrIdx]->setEnabled(true);
	measALabel_[instrIdx]->setEnabled(true);
	instrumentChan_[instrIdx]->setEnabled(true);
	chanLabel_[instrIdx]->setEnabled(true);
	setVolt_[instrIdx]->display(0);
	measCurr_[instrIdx]->display(0);
	instrumentChan_[instrIdx]->setValue(0);
	instrumentChan_[instrIdx]->setMaximum(99);
	instrumentChan_[instrIdx]->setMinimum(0);

	// return if device already not connected
	if(m_ConnInstr_[instrIdx]==NULL) return;

	if(gpibLabel_[instrIdx]->currentText().contains("GPIB")){
		PixGPIBDevice* dev = (PixGPIBDevice*)m_ConnInstr_[instrIdx];
		dev->setVoltage(0, 0.0);
		dev->setState(PixGPIBDevice::PGD_OFF);
		std::this_thread::sleep_for (std::chrono::milliseconds(100));
		delete dev;
	} else {
		PixRs232Device* dev = (PixRs232Device*)m_ConnInstr_[instrIdx];
		delete dev;
	}
	m_ConnInstr_[instrIdx] = NULL;
	gpibMessage_[instrIdx]->setText("");
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::disconnDev END\n";}
}


void IVscan::readDevThread() {
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::readDevThread\n";}
	while(m_thrRuns){
		readDevAll();
		QApplication::processEvents(); // update GUI
		std::this_thread::sleep_for (std::chrono::milliseconds(500));
	}
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::readDevThread END\n";}
}


void IVscan::readDevAll() {
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::readDevAll()\n";}
	for(uint i=0; i<idxConnInstr_.size(); i++){
		if(isHVsrc_[idxConnInstr_[i]]){
			if(gpibLabel_[idxConnInstr_[i]]->currentText().contains("GPIB")){
				PixGPIBDevice* dev = (PixGPIBDevice*) m_ConnInstr_[idxConnInstr_[i]];
				dev->measureVoltages();
				setVolt_[idxConnInstr_[i]]->display(dev->getVoltage(instrumentChan_[idxConnInstr_[i]]->value()));
			} else {
				PixRs232Device* dev = (PixRs232Device*) m_ConnInstr_[idxConnInstr_[i]];
				setVolt_[idxConnInstr_[i]]->display(dev->getVoltage(instrumentChan_[idxConnInstr_[i]]->value()));
			}
		}
		if(isImeter_[idxConnInstr_[i]]){
			if(gpibLabel_[idxConnInstr_[i]]->currentText().contains("GPIB")){
				PixGPIBDevice* dev = (PixGPIBDevice*) m_ConnInstr_[idxConnInstr_[i]];
				dev->measureCurrents();
				measCurr_[idxConnInstr_[i]]->display(dev->getCurrent(instrumentChan_[idxConnInstr_[i]]->value()));
			} else {
				PixRs232Device* dev = (PixRs232Device*) m_ConnInstr_[idxConnInstr_[i]];
				if(checkBoxImeas_[idxConnInstr_[i]]->text()=="C Meter") { // LCR meter
					double freq = capFreqBox->currentText().toDouble()*1.e3;
					double veff = boxCVeff->value();
					double circtype;
					int biastype;
					QString circtypestring = capCircuitBox->currentText();
					if(circtypestring == "Par")
						circtype=1;
					else if(circtypestring == "Ser")
						circtype=0;
					else circtype=2;
					QString biastypestring = capBiasBox->currentText();
					if(biastypestring == "EXT")
						biastype=2;
					else if(biastypestring == "INT")
						biastype=1;
					else biastype=0;
					//if(DEBUG){std::cout<<"In IVscan.cxx, IVscan::readDevAll(). i: "<<i<<"!\n";}//1
					measCurr_[idxConnInstr_[i]]->display(dev->getCapacity(instrumentChan_[idxConnInstr_[i]]->value(), freq, veff, circtype, biastype, 0)*1.e12);
				} else { // HV device
					measCurr_[idxConnInstr_[i]]->display(dev->getCurrent(instrumentChan_[idxConnInstr_[i]]->value()));
				}
			}
		}
	}
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::readDevAll() END\n";}
}


void IVscan::saveFileDialog() {
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::saveFileDialog()\n";}
	//fileNameLabel->setText(QFileDialog::getSaveFileName(this, tr("Select File Name"), "/work1/jlange/IVdata/IV_untitled"));
	//folderText->setText(QFileDialog::getSaveFileName(this, tr("Select File Name"), "/work1/jlange/IVdata/"));
	QFileDialog fdia(this, "Select data directory", folderText->text());
	fdia.setOption(QFileDialog::DontUseNativeDialog, true);
	fdia.setFileMode(QFileDialog::DirectoryOnly);
	if(fdia.exec() == QDialog::Accepted){
		QString path = fdia.selectedFiles().first();
		path.replace("\\", "/");
		folderText->setText(path+"/");
	}
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::saveFileDialog() END\n";}
}


void IVscan::fillTemperBox(QList<QString> tempTypes) {
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::fillTemperBox\n";}
	for(int i=0;i<tempTypes.size();i++){
		extTemperBox->addItem(tempTypes.at(i));
	}
	m_currT = -273.16;
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::fillTemperBox END\n";}
}


void IVscan::clearTemperBox(QList<QString> tempTypes) {
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::clearTemperBox\n";}
	for(int i=0;i<tempTypes.size();i++){
		int id = extTemperBox->findText(tempTypes.at(i));
		if(id>=0) extTemperBox->removeItem(id);
	}
	m_currT = -273.16;
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::clearTemperBox END\n";}
}


void IVscan::fillTempVal(QMap<QString, double> Tmap){
	if(DEBUG){std::cout<<"In IVscan.cxx IVscan::fillTempVal\n";}
	if(Tmap.find(extTemperBox->currentText())!=Tmap.end())
		m_currT = Tmap[extTemperBox->currentText()];
	if(Tmap.find("Chuck T(°C)")!=Tmap.end())
		m_ChuckT = Tmap["Chuck T(°C)"];
	if(Tmap.find("Ambient T(°C)")!=Tmap.end())
		m_AmbientT = Tmap["Ambient T(°C)"];
	if(Tmap.find("Rel. Humidity(%)")!=Tmap.end())
		m_RH = Tmap["Rel. Humidity(%)"];
	if (DEBUG){
		std::cout << "Current T = " << m_currT << "Chuck T = " << m_ChuckT << "Amb T = " << m_AmbientT << "RH = " << m_RH <<std::endl;
		std::cout<<"In IVscan.cxx IVscan::fillTempVal END\n";
	}
}
void IVscan::fillChillerTempVal(double chillerMeasuredT){
	m_chillerTmeas_tr = chillerMeasuredT;
	if (DEBUG){
		std::cout<<"Chiller meas. T (°C): "<<m_chillerTmeas_tr<<"\n";
		std::cout<<"In IVscan.cxx IVscan::fillChillerTempVal ran\n";
	}
}
