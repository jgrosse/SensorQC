#ifndef INTERLOCKCOND_H
#define INTERLOCKCOND_H

#include "ui_InterlockCond.h"
#include <QWidget>

class InterlockCond : public QWidget, public Ui::InterlockCond {

    Q_OBJECT

 public:
    InterlockCond(QWidget * parent = 0, Qt::WindowFlags flags = Qt::Window);
    ~InterlockCond();

    void addItem(QString name);
    void removeItem(QString name);
    QList<QString> listItems();
    bool checkState(QMap<QString, double> valList);
    void resetState();

 private:
    double m_valA, m_valB;

};

#endif // INTERLOCKCOND_H
