TEMPLATE = app

CONFIG += qt
QT += network
equals(QT_MAJOR_VERSION, 5) {
  QT+= widgets
}

include(../build-config.inc)
  
INCLUDEPATH += . ../include ../simpati ../PixGPIB ../PixRS232 ../InfluxDB

FORMS += Cycling.ui
FORMS += TempMon.ui
FORMS += DataLogging.ui
FORMS += Interlock.ui
FORMS += InterlockCond.ui

SOURCES += Cycling.cxx
SOURCES += TempMon.cxx
SOURCES += DataLogging.cxx
SOURCES += Interlock.cxx
SOURCES += InterlockCond.cxx
SOURCES += QRootApplication.cxx
SOURCES += ModuleQCCycling.cxx

HEADERS += Cycling.h
HEADERS += TempMon.h 
HEADERS += DataLogging.h 
HEADERS += Interlock.h 
HEADERS += InterlockCond.h 
HEADERS += QRootApplication.h

unix {
    DESTDIR =  .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lpthread -lboost_system
        QMAKE_LFLAGS  +=  $${system(root-config --libs)} -L../PixGPIB -lPixGPIB -L../PixRS232 -l PixRS232 -L../InfluxDB -l InfluxDB -L../simpati -l simpati
        QMAKE_RPATHDIR += ../PixGPIB ../PixRS232 ../InfluxDB ../simpati
}

win32 {
    DESTDIR =  ../bin
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
    QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
    QMAKE_LFLAGS_RELEASE = delayimp.lib simpati.lib
    QMAKE_LFLAGS_WINDOWS += /LIBPATH:../bin
}

