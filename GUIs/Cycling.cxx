#include <Cycling.h>
#include <QTimer>
#include <QFileDialog>
#include <QDateTime>

#include <simpati.h>

#include <math.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>

Cycling::Cycling(QWidget * parent, Qt::WindowFlags flags, bool modQC)
  : QWidget(parent, flags) {
  setupUi(this);
  m_timer = new QTimer(this);
  m_cycTimer = new QTimer(this);

  m_setVal[0] = setVal_0;
  m_setVal[1] = setVal_1;
  m_setVal[2] = setVal_2;
  m_setVal[3] = setVal_3;
  m_setVal[4] = setVal_4;
  m_measVal[0] = measVal_0;
  m_measVal[1] = measVal_1;
  m_measVal[2] = measVal_2;
  m_measVal[3] = measVal_3;
  m_measVal[4] = measVal_4;
  m_measLabels[0] = measLabel_0;
  m_measLabels[1] = measLabel_1;
  m_measLabels[2] = measLabel_2;
  m_measLabels[3] = measLabel_3;
  m_measLabels[4] = measLabel_4;

  // items for module QC only, hide by default
  if(modQC){
    // dynamically alter some pre-sets when in mod. QC mode
    preWarmBox->setValue(25.);
    preColdBox->setValue(20.);
    postWarmBox->setValue(25.);
    postColdBox->setValue(20.);
    preBottomButton->setChecked(true);
    finishMeasBox->hide();
    postTopButton_2->hide();
    postBottomButton_2->hide();
    measDoneButton->hide();
    dn2Box->setValue(14);
    checkRHBox->setChecked(true);
  } else {
    dn2Label->hide();
    dn2Box->hide();
    //dn2Spacer->hide();
    preRHLabel1->hide();
    preRHBox->hide();
    preRHLabel2->hide();
    preRHComboBox->hide();
    checkRHBox->hide();
    checkRHBox->setChecked(false);
  }

  m_chamber = NULL;
  m_extVals.clear();
  disconnDev();
  connect(startButton, SIGNAL(clicked()), this, SLOT(startCycling1()));
}

Cycling::~Cycling(){
  disconnDev();
}
void Cycling::connDev(){

  QList<QString> myTypes;

  m_chamber = new simpati(ipAddress->text().toStdString(), portBox->value(), idBox->value(), (uint)dn2Box->value());
  std::map<int, std::string> names = m_chamber->getNames();
  int k=0;
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    m_setVal[k]->show();
    m_measVal[k]->show();
    m_measLabels[k]->show();
    m_measLabels[k]->setText(it->second.c_str());
    myTypes.push_back("Chamber"+QString::number(idBox->value())+"_"+m_measLabels[k]->text());
    k++;
  }
  emit newCycDev(myTypes);

  m_timer->start(2000.);
  connect(m_timer, SIGNAL(timeout()), this, SLOT(readMeas()));

  startButton->setEnabled(true);

  connectButton->setText("Disconnect");
  disconnect(connectButton, SIGNAL(clicked()), this, SLOT(connDev()));
  connect(connectButton, SIGNAL(clicked()), this, SLOT(disconnDev()));
}
void Cycling::disconnDev(){
  m_timer->stop();
  disconnect(m_timer, SIGNAL(timeout()), this, SLOT(readMeas()));

  QList<QString> myTypes;

  if(m_chamber!=NULL) m_chamber->setEnabled(false);
  delete m_chamber; m_chamber=NULL;

  for(uint i=0;i<nitems;i++){
    m_setVal[i]->hide();
    m_measVal[i]->hide();
    m_measLabels[i]->hide();
    myTypes.push_back("Chamber"+QString::number(idBox->value())+"_"+m_measLabels[i]->text());
  }

  emit obsCycDev(myTypes);
  startButton->setEnabled(false);

  connectButton->setText("Connect");
  connect(connectButton, SIGNAL(clicked()), this, SLOT(connDev()));
  disconnect(connectButton, SIGNAL(clicked()), this, SLOT(disconnDev()));
}
void Cycling::readMeas(){
  if(m_chamber==0) return;
  QMap<QString, double> cymap;
  std::map<int, std::string> names = m_chamber->getNames();
  int k=0;
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    std::pair<double, double> vals = m_chamber->getVals()[it->first];
    m_setVal[k]->display(vals.first);
    m_measVal[k]->display(vals.second);
    cymap.insert("Chamber"+QString::number(idBox->value())+"_"+m_measLabels[k]->text(), vals.second);

    k++;
  }
  emit newCycVal(cymap);
}
void Cycling::startCycling1(){
  startButton->setText("Abort");
  startButton->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 0, 0);"));
  disconnect(startButton, SIGNAL(clicked()), this, SLOT(startCycling1()));
  connect(startButton, SIGNAL(clicked()), this, SLOT(stopCycling()));

  if(prepareBox->isChecked()){
    m_chamber->setTwarm(preWarmBox->value());
    m_chamber->setTcold(preColdBox->value());
    m_chamber->setCage(preTopButton->isChecked(), true);
    m_chamber->setEnabled(true);
    m_cycTimer->start(2000.);
    connect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling1()));
  } else
    startCycling2();
}
void Cycling::startCycling2(){
  m_chamber->startCyclingPrg(cycleProgBox->value());
  m_cycTimer->start(2000.);
  connect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling2()));
}
void Cycling::startCycling3(){
  if(finishBox->isChecked()){
    m_chamber->setTwarm(postWarmBox->value());
    m_chamber->setTcold(postColdBox->value());
    m_chamber->setEnabled(true);
    m_cycTimer->start(2000.);
    connect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling3()));
  } else{
    endCycling();
    m_chamber->setEnabled(false);
  }
}
void Cycling::monitorCycling1(){
  double Twarm=-9999., Tcold=-9999., RHext = -9999.;
  std::map<int, std::string> names = m_chamber->getNames();
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    std::pair<double, double> vals = m_chamber->getVals()[it->first];
    if(names[it->first].substr(0,10)=="Warmkammer") Twarm = vals.second;
    if(names[it->first].substr(0,10)=="Kaltkammer") Tcold = vals.second;
  }
  for(QMap<QString, double>::iterator it = m_extVals.begin(); it != m_extVals.end(); it++){
    if(it.key()==preRHComboBox->currentText()) RHext = it.value();
  }
  if(fabs(Twarm-preWarmBox->value())<tmatchPre->value() &&
     fabs(Tcold-preColdBox->value())<tmatchPre->value() &&
     (!checkRHBox->isChecked() || (RHext>0. && RHext<preRHBox->value())) ){
    m_cycTimer->stop();
    disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling1()));
    startCycling2();
  }
}
void Cycling::monitorCycling2(){
  if(m_chamber->getChamberStatus()==0){
    m_cycTimer->stop();
    disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling2()));
    startCycling3();
  }
}
void Cycling::monitorCycling3(){
  double Tcold=-9999.;//Twarm=-9999.
  std::map<int, std::string> names = m_chamber->getNames();
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    std::pair<double, double> vals = m_chamber->getVals()[it->first];
    //    if(names[it->first].substr(0,10)=="Warmkammer") Twarm = vals.second;
    if(names[it->first].substr(0,10)=="Kaltkammer") Tcold = vals.second;
  }
  if(fabs(Tcold-postColdBox->value())<tmatchPost->value()){
    m_cycTimer->stop();
    disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling3()));
    m_chamber->setCage(postTopButton->isChecked(), true);
    if(finishMeasBox->isChecked() && finishMeasBox->isEnabled()){
      measDoneButton->setEnabled(true);
      connect(measDoneButton, SIGNAL(clicked()), this, SLOT(endCycling()));
      emit readyForMeasurement();
    } else
      endCycling();
  }
}
void Cycling::endCycling(){
  if(m_chamber == NULL) return;
  measDoneButton->setEnabled(false);
  m_chamber->setEnabled(false);
  startButton->setText("Start");
  startButton->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 255, 0);"));
  disconnect(startButton, SIGNAL(clicked()), this, SLOT(stopCycling()));
  connect(startButton, SIGNAL(clicked()), this, SLOT(startCycling1()));
}
void Cycling::stopCycling(int opt){
  if(m_chamber == NULL) return;
  m_cycTimer->stop();
  disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling1()));
  disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling2()));
  disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling3()));
  m_chamber->stopCycling();
  switch(opt){
  case 0:
  default:
   //do nothing
   break;
  case 1:
    m_chamber->setCage(false, true);
   break;
  case 2:
    m_chamber->setCage(true, true);
   break;
  }
  endCycling();
}
void Cycling::newNames(QList<QString> nameList){
  // change drop-down menu content
  for(int i=0;i<nameList.size();i++){
    int itid = preRHComboBox->findText(nameList.at(i));
    if(itid<0) preRHComboBox->addItem(nameList.at(i));
  }
}
void Cycling::obsNames(QList<QString> nameList){
  // change drop-down menu content
  for(int i=0;i<nameList.size();i++){
    int itid =  preRHComboBox->findText(nameList.at(i));
    if(itid>=0) preRHComboBox->removeItem(itid);
  }
}
void Cycling::newVals(QMap<QString, double> valList){
  m_extVals = valList;
}
