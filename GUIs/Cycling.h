#ifndef CYCLING_H
#define CYCLING_H

#include "ui_Cycling.h"
#include <QWidget>

class QTimer;
class simpati;

class Cycling : public QWidget, public Ui::Cycling {

    Q_OBJECT

 public:
    Cycling(QWidget * parent = 0, Qt::WindowFlags flags = Qt::Window, bool modQC=false);
    ~Cycling();

 public slots:
    void connDev();
    void disconnDev();
    void readMeas();
    void startCycling1();
    void startCycling2();
    void startCycling3();
    void monitorCycling1();
    void monitorCycling2();
    void monitorCycling3();
    void stopCycling(){stopCycling(0);};
    void stopCycling(int);
    void endCycling();
    void newNames(QList<QString> nameList);
    void obsNames(QList<QString> nameList);
    void newVals(QMap<QString, double> valList);

 signals:
    void readyForMeasurement();
    void newCycDev(QList<QString>);
    void obsCycDev(QList<QString>);
    void newCycVal(QMap<QString, double>);

 private:
    QTimer *m_timer, *m_cycTimer;

    static const int nitems=5;
    QLCDNumber* m_setVal[nitems];
    QLCDNumber* m_measVal[nitems];
    QLabel* m_measLabels[nitems];
    QMap<QString, double> m_extVals;
    simpati *m_chamber;
};

#endif // CYCLING_H
