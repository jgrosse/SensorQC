TEMPLATE = app

CONFIG += qt
QT += network
equals(QT_MAJOR_VERSION, 5) {
  QT+= widgets
}

include(../build-config.inc)
  
INCLUDEPATH += . ../PixGPIB ../PixRS232 ../InfluxDB ../include

FORMS += TempMon.ui \
	 DataLogging.ui \
	 SuppControl.ui \
	 SuppControlBase.ui 

SOURCES += TempMon.cxx \
	   DataLogging.cxx \
	   SuppControl.cxx \
	   SuppControlBase.cxx \
	   MiniDCSMain.cxx \
	   QRootApplication.cxx

HEADERS += TempMon.h \
	   DataLogging.h \
   	   SuppControl.h \
   	   SuppControlBase.h \
	   QRootApplication.h

unix {
    DESTDIR =  .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lgpib -lpthread
        INCLUDEPATH += $${system(root-config --incdir)}
        QMAKE_LFLAGS  +=  $${system(root-config --libs)} -L../PixGPIB -lPixGPIB -L../PixRS232 -l PixRS232 -L../InfluxDB -l InfluxDB
        QMAKE_RPATHDIR += ../PixGPIB ../PixRS232 ../InfluxDB
}

win32 {
    DESTDIR =  ../bin
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
    QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
    INCLUDEPATH += $(ROOTSYS)/include
    QMAKE_LFLAGS_RELEASE = delayimp.lib
    QMAKE_LFLAGS_WINDOWS += /LIBPATH:../PixGPIB /LIBPATH:../bin /LIBPATH:$(ROOTSYS)/lib
    LIBS += PixGPIB.lib GPIB-32.obj
    LIBS += PixRS232.lib
    LIBS += libCore.lib
    LIBS += libCint.lib 
    LIBS += libRIO.lib 
    LIBS += libNet.lib 
    LIBS += libHist.lib 
    LIBS += libGraf.lib 
    LIBS += libGraf3d.lib 
    LIBS += libGpad.lib
    LIBS += libTree.lib
    LIBS += libRint.lib
    LIBS += libPostscript.lib
    LIBS += libMatrix.lib
    LIBS += libPhysics.lib
    LIBS += libMathCore.lib
}
