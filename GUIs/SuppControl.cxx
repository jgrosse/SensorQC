#include "SuppControl.h"
#include "SuppControlBase.h"

SuppControl::SuppControl(QWidget * parent, Qt::WindowFlags flags, bool isStandalone) : 
  QWidget(parent, flags), m_isStandalone(isStandalone) {
  setupUi(this);
  noDevChanged(1);
  connect(noDevBox, SIGNAL(valueChanged(int)), this, SLOT(noDevChanged(int)));
  connect(connectButton, SIGNAL(clicked()), this, SLOT(connectAllDev()));
}

SuppControl::~SuppControl(){
  disconnectAllDev();
  noDevChanged(1);
  delete (m_lvbases.at(0));
  m_lvbases.clear();
}
void SuppControl::noDevChanged(int noDev){
  if(noDev<0) return;
  if(noDev>(int)m_lvbases.size()){
    verticalLayout->removeItem(verticalSpacer);
    verticalLayout->removeItem(horizontalLayout);
    for(unsigned int i=m_lvbases.size(); i<(unsigned int)noDev;i++){
      SuppControlBase *lv = new SuppControlBase(this, Qt::SubWindow, m_isStandalone);
      lv->devGroupBox->setTitle("Device "+QString::number(i+1));
      lv->gpibSpinBox->setValue(lv->gpibSpinBox->value()+i);
      m_lvbases.push_back(lv);
      verticalLayout->addWidget(lv);
      connect(lv, SIGNAL(newSuppply(QList<QString>)), this, SIGNAL(newSuppply(QList<QString>)) );
      connect(lv, SIGNAL(obsSuppply(QList<QString>)), this, SIGNAL(obsSuppply(QList<QString>)) );
      connect(lv, SIGNAL(newSuppRdg(QMap<QString, double>)), this, SIGNAL(newSuppRdg(QMap<QString, double>)) );
    }
    verticalLayout->addItem(verticalSpacer);
    verticalLayout->addLayout(horizontalLayout);
  }
  if(noDev<(int)m_lvbases.size()){
    for(unsigned int i=m_lvbases.size()-1; i>=(unsigned int)noDev;i--){
      disconnect(m_lvbases.at(i), SIGNAL(newSuppply(QList<QString>)), this, SIGNAL(newSuppply(QList<QString>)) );
      disconnect(m_lvbases.at(i), SIGNAL(obsSuppply(QList<QString>)), this, SIGNAL(obsSuppply(QList<QString>)) );
      disconnect(m_lvbases.at(i), SIGNAL(newSuppRdg(QMap<QString, double>)), this, SIGNAL(newSuppRdg(QMap<QString, double>)) );
      delete (m_lvbases.at(i));
      m_lvbases.erase(m_lvbases.end()-1);
    }      
  }
}

void SuppControl::connectAllDev(){
  disconnect(connectButton, SIGNAL(clicked()), this, SLOT(connectAllDev()));

  for(std::vector<SuppControlBase*>::iterator it = m_lvbases.begin();
      it != m_lvbases.end(); it++)
    (*it)->connectDev();
  noDevBox->setEnabled(false);

  connectButton->setText("Disconnect");
  connect(connectButton, SIGNAL(clicked()), this, SLOT(disconnectAllDev()));
}
void SuppControl::disconnectAllDev(){
  disconnect(connectButton, SIGNAL(clicked()), this, SLOT(disconnectAllDev()));

  for(std::vector<SuppControlBase*>::iterator it = m_lvbases.begin();
      it != m_lvbases.end(); it++)
    (*it)->disconnectDev();
  noDevBox->setEnabled(true);

  connectButton->setText("Connect");
  connect(connectButton, SIGNAL(clicked()), this, SLOT(connectAllDev()));
}
