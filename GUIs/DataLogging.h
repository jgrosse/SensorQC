#ifndef DATALOGGING_H
#define DATALOGGING_H


#include "ui_DataLogging.h"
#include <QWidget>
#include <QMap>
#include <QString>
#include <QList>

class QTimer;
class QLCDNumber;
class TCanvas;
class TGraph;
class TMultiGraph;
class TLegend;
class InfluxDB;

class DataLogging : public QWidget, public Ui::DataLogging {

    Q_OBJECT

 public:
    DataLogging(QWidget * parent = 0, Qt::WindowFlags flags = Qt::Window);
    ~DataLogging();

 public slots:
    void browseLogFile();
    void writeLog();
    void plotResults(char *fname, int npts);
    void logChecked(int type);
    void obsValues(QList<QString> devList);
    void newValues(QMap<QString, double> valMap);
    void startDB();
    void stopDB();

 private:
    QTimer *m_logTimer;
    TCanvas *m_can;
    std::vector<TGraph*> m_gr;
    std::vector<TMultiGraph*> m_mg;
    std::vector<TLegend*> m_leg;
    QMap<QString, double> m_valList;

    InfluxDB *m_db = 0;
};

#endif //  DATALOGGING_H
