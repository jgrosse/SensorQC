#include "DataLogging.h"
#include <InfluxDB.h>

#include <QTimer>
#include <QFileDialog>
#include <QDateTime>
#include <QLCDNumber>

#include <TCanvas.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TLegend.h>
#include <TDatime.h>
#include <TAxis.h>
#include <TROOT.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>

DataLogging::DataLogging(QWidget * parent, Qt::WindowFlags flags)
  : QWidget(parent, flags), m_can(NULL){

  setupUi(this);

  m_logTimer = new QTimer(this);
  m_gr.clear();

  logChecked(0);

  connect(browseButton, SIGNAL(clicked()), this, SLOT(browseLogFile()) );
  connect(dbStartButton, SIGNAL(clicked()), this, SLOT(startDB()) );
  //  connect(logCheckBox, SIGNAL(clicked(bool)), this, SLOT(logChecked(bool)) );
  connect(logComboBox, SIGNAL(activated(int)), this, SLOT(logChecked(int)) );

}

DataLogging::~DataLogging(){
  stopDB();
  delete m_can;
  m_leg.clear();
  m_mg.clear();
  m_gr.clear();
}


void DataLogging::browseLogFile(){
  fileName->setText(QFileDialog::getSaveFileName(this, tr("Select File Name"), fileName->text()));
  logChecked(logComboBox->currentIndex());
	     //logCheckBox->isChecked());
}

void DataLogging::logChecked(int type){
  switch(type){
  case 0:
  default:
    fileName->hide();
    browseButton->hide();
    graphCheckBox->hide();
    graphSpinBox->hide();
    graphLabel->hide();
    dbNameLabel->hide();
    dbName->hide();
    dbMeasNameLabel->hide();
    dbMeasName->hide();
    dbHostLabel->hide();
    dbHostPort->hide();
    dbStartButton->hide();
    break;
  case 1:
    fileName->show();
    browseButton->show();
    graphCheckBox->show();
    graphSpinBox->show();
    graphLabel->show();
    dbNameLabel->hide();
    dbName->hide();
    dbMeasNameLabel->hide();
    dbMeasName->hide();
    dbHostLabel->hide();
    dbHostPort->hide();
    dbStartButton->hide();
    break;
  case 2:
    fileName->hide();
    browseButton->hide();
    graphCheckBox->hide();
    graphSpinBox->hide();
    graphLabel->hide();
    dbNameLabel->show();
    dbName->show();
    dbMeasNameLabel->show();
    dbMeasName->show();
    dbHostLabel->show();
    dbHostPort->show();
    dbStartButton->show();
    break;
  }

  if((type==1 && fileName->text()!="") || type==2){
    m_logTimer->start(1000*logIntervalBox->value());
    connect(m_logTimer, SIGNAL(timeout()), this, SLOT(writeLog()));
  } else {
    m_logTimer->stop();
    disconnect(m_logTimer, SIGNAL(timeout()), this, SLOT(writeLog()));
  }

}
void DataLogging::newValues(QMap<QString, double> valMap){
  for(QMap<QString, double>::iterator it = valMap.begin(); it != valMap.end(); it++){
//     if(fabs(it.value())<1.e-3)
//       printf("New val: %s\t%le\n", it.key().toLatin1().data(), it.value());
//     else
//       printf("New val: %s\t%lf\n", it.key().toLatin1().data(), it.value());
    m_valList[it.key()] = it.value();
  }
}
void DataLogging::obsValues(QList<QString> devList){
  for(int i=0;i<devList.size();i++)
    m_valList.remove(devList.at(i));
}
void DataLogging::writeLog(){

  if(m_valList.size()>0){
    
    if(logComboBox->currentIndex()==1){ // write to file

      FILE *out = fopen(fileName->text().toLatin1().data(),"a");
      
      fprintf(out,"%s", QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss").toLatin1().data());
      
      for(QMap<QString, double>::iterator it = m_valList.begin(); it != m_valList.end(); it++){
	if(fabs(it.value())<1.e-3)
	  fprintf(out,"\t%s\t%le", it.key().toLatin1().data(), it.value());
	else
	  fprintf(out,"\t%s\t%lf", it.key().toLatin1().data(), it.value());
      }
      
      fprintf(out,"\n");
      fclose(out);

      if(graphCheckBox->isChecked())
	plotResults(fileName->text().toLatin1().data(), graphSpinBox->value());

    } else if(logComboBox->currentIndex()==2 && m_db!=0){ // write to InfluxDB
      try{
	m_db->setTag("Sensor", "SensorQC-Logger");
	m_db->startMeasurement(dbMeasName->text().toStdString(), std::chrono::system_clock::now());
	// write all values that are covered into stream
	for(QMap<QString, double>::iterator it = m_valList.begin(); it != m_valList.end(); it++){
	  try{
	    m_db->setField(it.key().toStdString(), it.value());
	  } catch(...){}
	}
	m_db->recordPoint();
	m_db->endMeasurement();
      } catch(std::runtime_error &err){
	std::cerr << "Error writing to InfluxDB: " << std::string(err.what()) << std::endl;
      } catch(...){
	std::cerr << "Unknown error writing to InfluxDB" << std::endl;
      }
    }
  }

}
void DataLogging::plotResults(char *fname, int npts){

  TCanvas *tmpcan = (TCanvas*) gROOT->FindObject("logcan");
  if(m_can!=NULL && tmpcan==m_can){
    m_can->Show();
  } else if(tmpcan!=NULL && m_can==NULL) {
    m_can = tmpcan;
    m_can->Show();
    return;
  } else if(m_can==NULL || tmpcan==NULL)
    m_can = new TCanvas("logcan", "Logged Data", 800, 800);

  m_can->Clear();
  m_can->cd();

  FILE *in = fopen(fname,"r");
  char c;
  int lines = 0;
  while ((c = fgetc(in)) != EOF) {
    if (c == '\n') ++lines;
  }
  fclose(in);
  int ndiff = npts-lines;

  in = fopen(fname,"r");
  char line[2001];
  for(uint k=0; k<m_leg.size();k++) delete m_leg.at(k);
  m_leg.clear();
  for(uint k=0; k<m_mg.size();k++) delete m_mg.at(k);
  m_mg.clear();
  //for(uint k=0; k<m_gr.size();k++) delete m_gr.at(k);
  m_gr.clear();

  std::map<int, std::string> myUnits;
  std::map<std::string, uint> listUnits;

  while(fgets(line,2000,in)!=0){
    ndiff++;
    if(ndiff<0) continue;
    std::string date;
    std::string device;
    double Tdev;
    std::istringstream f(line);
    std::string s;
    int i=0;
    while (getline(f, s, '\t')) {
      if(i==0) date = s;
      else if (i%2){
	device = s;
	//std::cout << "Device = " << device << std::endl;
      }
      else{
	Tdev = (double)atof(s.c_str());
	//printf("%lf\n", Tdev);
	if((uint)(i/2)>m_gr.size()){
	  m_gr.push_back(new TGraph());
	  m_gr.at(i/2-1)->SetMarkerStyle(20);
	  m_gr.at(i/2-1)->SetMarkerSize(0.4);
	  int icol = i/2;
	  if(icol>=5) icol++; // avoid yellow
	  //printf("Created graph %d - %d; col %d\n", i/2-1, (int)m_gr.size(), icol);
	  m_gr.at(i/2-1)->SetMarkerColor(icol);
	  m_gr.at(i/2-1)->SetLineColor(icol);
	}
	if(m_gr.at(i/2-1)->GetN()==0){
	  std::string dlabel = device;
	  std::string unit = "";
	  std::string::size_type spos = device.find("(");
	  if(spos!=std::string::npos){
	    dlabel = device.substr(0,spos);
	    unit = device.substr(spos,device.size()-spos);
	  }
	  myUnits[i/2-1] = unit;
	  if(listUnits.find(unit)==listUnits.end()){
	    uint ic = listUnits.size();
	    listUnits[unit] = ic;
	    m_mg.push_back(new TMultiGraph ());
	    m_leg.push_back(new TLegend(.8, .9, .99, .95));
	  }
	  m_leg[listUnits[unit]]->AddEntry(m_gr.at(i/2-1), dlabel.c_str(), "p");
	  m_leg[listUnits[unit]]->SetY1(0.9-0.05*m_leg[listUnits[unit]]->GetNRows());
// 	  std::cout << "1) Item " << (i/2-1) << "(" << device << ")" << " with "<< listUnits[unit] 
// 	      << " has " << unit << std::endl;
	}
	TDatime dt(date.c_str());
	double mytime = (double) dt.Convert();
	//printf("%d: %lf - %lf\n", i/2, mytime-1547831723., Tdev);
	m_gr.at(i/2-1)->SetPoint(m_gr.at(i/2-1)->GetN(), mytime, Tdev);
      }
      i++;
    }
  }
  fclose(in);

  if(listUnits.size()>6)
    m_can->Divide(3,3);
  else if(listUnits.size()>4)
    m_can->Divide(3,2);
  else if(listUnits.size()>2)
    m_can->Divide(2,2);
  else
    m_can->Divide(1,listUnits.size());

  for(uint i=0; i<listUnits.size();i++){
    std::string unit = "";
    int gcid = 1;
    for(uint k=0; k<m_gr.size();k++) {
      if(listUnits[myUnits[k]]==i){
	unit = myUnits[k];
	m_gr.at(k)->SetMarkerColor(gcid);
	m_gr.at(k)->SetLineColor(gcid);
	m_mg[i]->Add(m_gr.at(k));
	gcid++;
	if(gcid==5) gcid++; // avoid yellow
      }
    }
    // std::cout << "2) Item " << i << " has " << unit << std::endl;
    if(unit!=""){
      m_can->cd(i+1);
      gPad->SetRightMargin(0.22);
      gPad->SetLeftMargin(0.12);
      m_mg[i]->Draw("AP");
      m_mg[i]->GetXaxis()->SetTimeDisplay(1);
      m_mg[i]->GetXaxis()->SetNdivisions(503);
      m_mg[i]->GetXaxis()->SetTimeFormat("%Y-%m-%d %H:%M");
      m_mg[i]->GetXaxis()->SetTimeOffset(0,"cet");
      m_mg[i]->GetXaxis()->SetTitle("Time");
      m_mg[i]->GetYaxis()->SetTitle(("Meas. val. "+unit).c_str());//"T (^{o}C) or RH (%)");
      m_leg[i]->Draw();
    }
  }

  m_can->Update();

}
void DataLogging::startDB(){
  if(dbName->text()!="" && dbHostPort->text()!=""){
    QString dbHost = dbHostPort->text().split(":").at(0);
    int dbPort = dbHostPort->text().split(":").at(1).toInt();
    m_db = new InfluxDB;
    m_db->init(dbHost.toStdString(), dbPort, dbName->text().toStdString());
    disconnect(dbStartButton, SIGNAL(clicked()), this, SLOT(startDB()) );
    connect(dbStartButton, SIGNAL(clicked()), this, SLOT(stopDB()) );
    dbStartButton->setText("Disconnect");
    dbNameLabel->setEnabled(0);
    dbName->setEnabled(0);
    dbMeasNameLabel->setEnabled(0);
    dbMeasName->setEnabled(0);
    dbHostLabel->setEnabled(0);
    dbHostPort->setEnabled(0);
  }

}

void DataLogging::stopDB(){
  delete m_db; m_db = 0;
  disconnect(dbStartButton, SIGNAL(clicked()), this, SLOT(stopDB()) );
  connect(dbStartButton, SIGNAL(clicked()), this, SLOT(startDB()) );
  dbStartButton->setText("Disconnect");
  dbNameLabel->setEnabled(1);
  dbName->setEnabled(1);
  dbMeasNameLabel->setEnabled(1);
  dbMeasName->setEnabled(1);
  dbHostLabel->setEnabled(1);
  dbHostPort->setEnabled(1);
  dbStartButton->setText("Connect");
}
