#include <Interlock.h>
#include <InterlockCond.h>

Interlock::Interlock(QWidget * parent, Qt::WindowFlags flags)
  : QWidget(parent, flags) {
  setupUi(this);
  m_active = 0;
  setNcond(nCondBox->value());
  connect(nCondBox, SIGNAL(valueChanged(int)), this, SLOT(setNcond(int)));
  connect(startButton, SIGNAL(clicked()), this, SLOT(setActive()));
}
Interlock::~Interlock(){
}
void Interlock::setActive(){
  switch(m_active){
  case 0:
  default:
    m_active = 1;
    startButton->setEnabled(false);
    nCondBox->setEnabled(false);
    stopCheckBox->setEnabled(false);
    moveCheckBox->setEnabled(false);
    warmButton->setEnabled(false);
    coldButton->setEnabled(false);
    startButton->setText("active");
    break;
  case 2:
    m_active = 0;
    for(uint k=0;k<m_conds.size(); k++)
      m_conds.at(k)->resetState();
    startButton->setEnabled(true);
    startButton->setText("Activate");
    nCondBox->setEnabled(true);
    stopCheckBox->setEnabled(true);
    moveCheckBox->setEnabled(stopCheckBox->isChecked());
    warmButton->setEnabled(moveCheckBox->isChecked());
    coldButton->setEnabled(moveCheckBox->isChecked());
    break;
  }
}
void Interlock::setNcond(int nc){
  if(nc<1) return; // impossible request
  QList<QString> nameList;
  // we should always have one item after init.: copy names to new items
  if(m_conds.size()>0) nameList = m_conds.at(0)->listItems();

  std::vector<InterlockCond*> temp_conds = m_conds;
  m_conds.clear();
  for(uint i=0;i<temp_conds.size(); i++){
    if(i<(uint)nc) m_conds.push_back(temp_conds.at(i));
    else {
      verticalLayout->removeWidget(temp_conds.at(i));
      delete temp_conds.at(i);
    }
  }

  verticalLayout->removeItem(verticalSpacer);
  verticalLayout->removeItem(horizontalLayout_2);
  for(int i=(int)m_conds.size();i<nc;i++) {
    InterlockCond *ic = new InterlockCond(this, Qt::SubWindow);
    verticalLayout->addWidget(ic);
    m_conds.push_back(ic);
    // add names as per old cond items
    for(int k=0;k<nameList.size();k++)
      ic->addItem(nameList.at(k));
  }
  verticalLayout->addItem(verticalSpacer);
  verticalLayout->addLayout(horizontalLayout_2);
}
void Interlock::newValues(QMap<QString, double> valList){
  // check interlock conditions
  if(m_active==1){
    for(uint k=0;k<m_conds.size(); k++){
      if(m_conds.at(k)->checkState(valList)){
	int stopCond = 0; // 0: no further action; 1: move cage to warm cabinet; 2: move cage to cold cabinet
	if(moveCheckBox->isChecked()) stopCond = 1+(int)coldButton->isChecked();
	m_active = 2;
	startButton->setEnabled(true);
	startButton->setText("Reset");
	if(stopCheckBox->isChecked()) emit interlockFired(stopCond);
	break;
      }
    }
  }
}
void Interlock::newNames(QList<QString> nameList){
  // set back to idle state
  m_active=2;
  setActive();
  // change drop-down menu content
  for(int i=0;i<nameList.size();i++){
    for(uint k=0;k<m_conds.size(); k++)
      m_conds.at(k)->addItem(nameList.at(i));
  }
}
void Interlock::obsNames(QList<QString> nameList){
  // set back to idle state
  m_active=2;
  setActive();
  // change drop-down menu content
  for(int i=0;i<nameList.size();i++){
    for(uint k=0;k<m_conds.size(); k++)
      m_conds.at(k)->removeItem(nameList.at(i));
  }
}
