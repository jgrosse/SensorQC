#include <QApplication>

#include <iostream>
#include <sstream>
#include <exception>
#include <string>
#include <chrono>

#include "ProberControl.h"

#include "GenericProber.h"
#include "VeloxRS232Prober.h"
#include "ProberBenchProber.h"

ProberControl::ProberControl(QWidget * parent, Qt::WindowFlags flags, bool showIV)
  : QWidget(parent, flags) {
  setupUi(this);
  m_prober = NULL;
  proberTypeSel();

  proberMessages->setText("");
  if(!showIV) checkBoxIVafterStep->hide();

  readLabel->setText("");
  if(1){
    readLabel->hide();
    sendEdit->hide();
    sendButton->hide();
  }
  labelMeasT->hide();
  labelSetT->hide();
  measTval->hide();
  setTval->hide();
  labelT->hide();
  spinBoxT->hide();
  setTbutton->hide();

  connect(connectButton, SIGNAL(clicked()), this, SLOT(connectProber()));
  connect(transferButton, SIGNAL(clicked()), this, SLOT(transfer()));
  connect(moveXYbutton, SIGNAL(clicked()), this, SLOT(moveXY()));
  connect(moveZbutton, SIGNAL(clicked()), this, SLOT(moveZ()));
  connect(setTbutton, SIGNAL(clicked()), this, SLOT(setT()));
  connect(scanXYbutton, SIGNAL(clicked()), this, SLOT(scanXY()));
  connect(proberSelection, SIGNAL(activated(int)), this, SLOT(proberTypeSel()));
  connect(sendButton, SIGNAL(clicked()), this, SLOT(sendCommand()));
}
ProberControl::~ProberControl() {
  disconnectProber();
}

void ProberControl::connectProber() {
  if(proberSelection->currentText()=="Velox RS232 Prober"){
    try {    
      m_prober = new Suess::VeloxRS232Prober(comPortBox->value()-1+COM1, baudRateBox->value());
      std::cout << "Suess::VeloxRS232Prober connected!" << std::endl;
    }catch(Suess::Exception e){
      std::cerr << "Suess exception: " << e.text<< std::endl;
      std::cerr << "can't create prober instance."<< std::endl;
      m_prober=NULL;
      return;
    }catch(...){
      std::cerr << "Unknown exception while creating prober" << std::endl;
      std::cerr << "can't create prober instance."<< std::endl;
      m_prober=NULL;
      return;
    }
  } else if(proberSelection->currentText()=="ProberBench TCP Prober") {
    try {    
      m_prober = new Suess::ProberBenchProber(hostLineEdit->text().toLatin1().data(), portBox->value(), "prober", false, 1);
      std::cout << "Suess::ProberBenchProber connected!" << std::endl;
    }catch(Suess::Exception e){
      std::cerr << "Suess exception: " << e.text<< std::endl;
      std::cerr << "can't create prober instance."<< std::endl;
      m_prober=NULL;
      return;
    }catch(...){
      std::cerr << "Unknown exception while creating prober" << std::endl;
      std::cerr << "can't create prober instance."<< std::endl;
      m_prober=NULL;
      return;
    }
  } else
    std::cerr << "Unknown prober type selected" << std::endl;

  if(m_prober!=NULL) {
    disconnect(connectButton, SIGNAL(clicked()), this, SLOT(connectProber()));
    connect(connectButton, SIGNAL(clicked()), this, SLOT(disconnectProber()));
    connectButton->setText("disconnect");
    proberSelection->setEnabled(false);
    moveXYbutton->setEnabled(true);
    moveZbutton->setEnabled(true);
    scanXYbutton->setEnabled(true);
    readDev();
    connectTimerToReadProberPosition(true);
  }
}
void ProberControl::disconnectProber() {
  connectTimerToReadProberPosition(false);
  delete m_prober; m_prober=NULL;
  disconnect(connectButton, SIGNAL(clicked()), this, SLOT(disconnectProber()));
  connect(connectButton, SIGNAL(clicked()), this, SLOT(connectProber()));
  connectButton->setText("connect");
  proberSelection->setEnabled(true);
  moveXYbutton->setEnabled(false);
  moveZbutton->setEnabled(false);
  scanXYbutton->setEnabled(false);
  labelMeasT->hide();
  labelSetT->hide();
  measTval->hide();
  setTval->hide();
  labelT->hide();
  spinBoxT->hide();
  setTbutton->hide();
}

void ProberControl::readDevThread() {
  while(m_thrRuns){
    readDev();
    QApplication::processEvents(); // update GUI
    std::this_thread::sleep_for (std::chrono::milliseconds(500));  
  }
}
float ProberControl::get_tr() {
  return m_chillerMeasuredTemp_tr;
}
void ProberControl::readDev() {
  if(m_prober==NULL) return;
  char posRef='H';
  if(posRefBox->currentText()=="zero") posRef='Z';
  else if(posRefBox->currentText()=="center") posRef='C';
  try {    
    Suess::ReadChuckPositionResponse pos(m_prober->ReadChuckPosition('Y', posRef, 'D'));
    positionX->display(pos.r_x);
    positionY->display(pos.r_y);
    positionZ->display(pos.r_z);
    //Suess::ReadChuckStatusResponse s(m_prober->ReadChuckStatus());
    //heightLabel->setText(s.r_height.c_str());

    try{
      Suess::ReadChuckActualTempResponse tr = m_prober->ReadChuckActualTemp();
      if(tr.r_sstatus == "unknown"){
	labelMeasT->hide();
	labelSetT->hide();
	measTval->hide();
	setTval->hide();
	labelT->hide();
	spinBoxT->hide();
	setTbutton->hide();
       } else {
	labelMeasT->show();
	labelSetT->show();
	measTval->show();
	setTval->show();
	labelT->show();
	spinBoxT->show();
	setTbutton->show();
	measTval->display(tr.r_temp);
	m_chillerMeasuredTemp_tr=tr.r_temp;
	emit newChillerTempVal(m_chillerMeasuredTemp_tr);
	Suess::ReadChuckSetTempResponse tsr = m_prober->ReadChuckSetTemp();
	setTval->display(tsr.r_temp);
      }
    }catch(...){
    }
  }catch(Suess::Exception e){
    std::cerr << "Suess exception: " << e.text<< std::endl;
    std::cerr << "will remove prober instance."<< std::endl;
    disconnectProber();
    return;
  }catch(...){
    std::cerr << "Unknown exception while reading from prober" << std::endl;
    std::cerr << "will remove prober instance."<< std::endl;
    disconnectProber();
    return;
  }
}

void ProberControl::transfer() {
  spinBoxX->setValue(positionX->value());
  spinBoxY->setValue(positionY->value());
  spinBoxZ->setValue(positionZ->value());
  spinBoxT->setValue(setTval->value());
}

void ProberControl::moveXY(){
  proberMessages->setText("");
  moveXYbutton->setEnabled(false);
  moveZbutton->setEnabled(false);
  scanXYbutton->setEnabled(false); 
  QApplication::processEvents();
  char posRef='H';
  if(posRefBox->currentText()=="zero") posRef='Z';
  else if(posRefBox->currentText()=="center") posRef='C';
  try{
    m_prober->MoveChuckWait(spinBoxX->value(), spinBoxY->value(),posRef);
  }catch(Suess::Exception e){
    //std::cerr << "Suess exception in prober MoveChuckWait: " << e.text<< std::endl;
    std::string msg(e.text.c_str());
    unsigned long int pos1 = msg.find("Parser error: \"");
    unsigned long int pos2 = msg.find("--");
    if(pos1!=std::string::npos && pos2!=std::string::npos)
      proberMessages->setText(msg.substr(pos1+15,pos2-pos1-15).c_str());
    else
      proberMessages->setText("Error moving chuck");
  } catch(std::exception& s){
    //std::cerr << "Std-lib exception  in prober MoveChuckWait: \"" << s.what() << "\"" << std::endl;
    proberMessages->setText("Error moving chuck");
  } catch(...){
    //std::cerr << "Exception in prober MoveChuckWait!" << std::endl;
    proberMessages->setText("Error moving chuck");
  }
  moveXYbutton->setEnabled(true);
  moveZbutton->setEnabled(true);
  scanXYbutton->setEnabled(true);
}

void ProberControl::moveZ(){
  proberMessages->setText("");
  moveXYbutton->setEnabled(false);
  moveZbutton->setEnabled(false);
  scanXYbutton->setEnabled(false);
  QApplication::processEvents();
  char posRef='H';
  if(posRefBox->currentText()=="zero") posRef='Z';
  else if(posRefBox->currentText()=="center") posRef='C';
  try{
    m_prober->MoveChuckZWait(spinBoxZ->value(), posRef);
  }catch(Suess::Exception e){
    //std::cerr << "Suess exception in prober MoveChuckWait: " << e.text<< std::endl;
    std::string msg(e.text.c_str());
    unsigned long int pos1 = msg.find("Parser error: \"");
    unsigned long int pos2 = msg.find("--");
    if(pos1!=std::string::npos && pos2!=std::string::npos)
      proberMessages->setText(msg.substr(pos1+15,pos2-pos1-15).c_str());
    else
      proberMessages->setText("Error moving chuck");
  } catch(std::exception& s){
    //std::cerr << "Std-lib exception  in prober MoveChuckWait: \"" << s.what() << "\"" << std::endl;
    proberMessages->setText("Error moving chuck");
  } catch(...){
    //std::cerr << "Exception in prober MoveChuckWait!" << std::endl;
    proberMessages->setText("Error moving chuck");
  }
  moveXYbutton->setEnabled(true);
  moveZbutton->setEnabled(true);
  scanXYbutton->setEnabled(true);
}

void ProberControl::setT(){
  proberMessages->setText("");
  setTbutton->setEnabled(false);
  QApplication::processEvents();
  try{
    std::stringstream cmdopt;
    cmdopt << spinBoxT->value() << " C";
    m_prober->clientCommand("HeatChuck", cmdopt.str());
  }catch(Suess::Exception e){
    //std::cerr << "Suess exception in prober MoveChuckWait: " << e.text<< std::endl;
    std::string msg(e.text.c_str());
    unsigned long int pos1 = msg.find("Parser error: \"");
    unsigned long int pos2 = msg.find("--");
    if(pos1!=std::string::npos && pos2!=std::string::npos)
      proberMessages->setText(msg.substr(pos1+15,pos2-pos1-15).c_str());
    else
      proberMessages->setText("Error setting temperature");
  } catch(std::exception& s){
    //std::cerr << "Std-lib exception  in prober MoveChuckWait: \"" << s.what() << "\"" << std::endl;
    proberMessages->setText("Error setting temperature");
  } catch(...){
    //std::cerr << "Exception in prober MoveChuckWait!" << std::endl;
    proberMessages->setText("Error setting temperature");
  }
  setTbutton->setEnabled(true);
}

void ProberControl::scanXY(){
  std::cout << "Start XY scan!" << std::endl;
  proberMessages->setText("");
  connectTimerToReadProberPosition(false);

  moveXYbutton->setEnabled(false);
  moveZbutton->setEnabled(false);
  scanXYbutton->setEnabled(false);
  posRefBox->setEnabled(false);
  char posRef='H';
  if(posRefBox->currentText()=="zero") posRef='Z';
  else if(posRefBox->currentText()=="center") posRef='C';
  QApplication::processEvents();
  // read current position
  Suess::ReadChuckPositionResponse pos(m_prober->ReadChuckPosition('Y', posRef, 'D'));
  double x0 = pos.r_x, y0 = pos.r_y;
  double xStepSize = spinBoxXstepSize->value();
  double yStepSize = spinBoxYstepSize->value();
  // start scan
  for(int iY=0; iY<spinBoxYsteps->value(); iY++){
    for(int iX=0; iX<spinBoxXsteps->value(); iX++){
      stepX->display(iX);
      stepY->display(iY);
      relX->display(iX*xStepSize);
      relY->display(iY*yStepSize);
      QApplication::processEvents();
      try{
	m_prober->MoveChuckWait(x0 + iX*xStepSize, y0 + iY*yStepSize, posRef);
      }catch(Suess::Exception e){
	std::string msg(e.text.c_str());
	unsigned long int pos1 = msg.find("Parser error: \"");
	unsigned long int pos2 = msg.find("--");
	if(pos1!=std::string::npos && pos2!=std::string::npos)
	  proberMessages->setText(msg.substr(pos1+15,pos2-pos1-15).c_str());
	else
	  proberMessages->setText("Error moving chuck");
      } catch(std::exception& s){
	proberMessages->setText("Error moving chuck");
      } catch(...){
	proberMessages->setText("Error moving chuck");
      }
      readDev();
      QApplication::processEvents();
      std::this_thread::sleep_for (std::chrono::milliseconds((int)spinBoxWaitTime->value()));
      // currently, scan code if duplicated in IVandXYscan -> trigger IV scan from the following signal?
      if(checkBoxIVafterStep->isChecked()) runIV();
    }
  }
  // move back to original position
  m_prober->MoveChuckWait(x0, y0);
  // enable again buttons
  moveXYbutton->setEnabled(true);
  moveZbutton->setEnabled(true);
  scanXYbutton->setEnabled(true);
  posRefBox->setEnabled(true);

  connectTimerToReadProberPosition(true);

  std::cout << "XY scan finished!" << std::endl;
}
void ProberControl::proberTypeSel(){
  if(proberSelection->currentText()=="Velox RS232 Prober"){
    hostLabel->hide();
    hostLineEdit->hide();
    portLabel->hide();
    portBox->hide();
    comLabel->show();
    comPortBox->show();
    baudLabel->show();
    baudRateBox->show();
  } else if(proberSelection->currentText()=="ProberBench TCP Prober") {
    hostLabel->show();
    hostLineEdit->show();
    portLabel->show();
    portBox->show();
    comLabel->hide();
    comPortBox->hide();
    baudLabel->hide();
    baudRateBox->hide();
  } else {
    hostLabel->hide();
    hostLineEdit->hide();
    portLabel->hide();
    portBox->hide();
    comLabel->hide();
    comPortBox->hide();
    baudLabel->hide();
    baudRateBox->hide();
  }
}
void ProberControl::sendCommand(){
  std::string cmd, recv;
  cmd = sendEdit->text().toLatin1().data();
  try{
    recv = m_prober->clientCommand(cmd,"");
  }catch(Suess::Exception e){
    recv = e.text.c_str();
  } catch(...){
    recv = "Error sending command";
  }
  readLabel->setText(recv.c_str());
}
// HeatChuck [x.y] C
void ProberControl::connectTimerToReadProberPosition(bool connectOrNot){
  if(connectOrNot){
    // start backround reading thread
    m_thrRuns = true;
    m_thread = std::thread(&ProberControl::readDevThread, this);
  } else {
    // stop background reading thread
    m_thrRuns = false;
    if(m_thread.joinable()) m_thread.join();
  }
}
