#include "SuppControlBase.h"
#include <QCoreApplication>

#include "PixGPIBDevice.h"
#include "PixRs232Device.h"
#include <math.h> 

SuppControlBase::SuppControlBase(QWidget * parent, Qt::WindowFlags flags, bool isStandalone)
  : QWidget(parent, flags) { 
  setupUi(this);
  devMsgLabel->setText("");
  vDisp.push_back(voltsLcd_1);vDisp.push_back(voltsLcd_2);vDisp.push_back(voltsLcd_3);
  aDisp.push_back(ampsLcd_1);aDisp.push_back(ampsLcd_2);aDisp.push_back(ampsLcd_3);
  vLabel.push_back(voltsLabel_1);vLabel.push_back(voltsLabel_2);vLabel.push_back(voltsLabel_3);
  aLabel.push_back(ampsLabel_1);aLabel.push_back(ampsLabel_2);aLabel.push_back(ampsLabel_3);
  vSet.push_back(voltsSpinBox_1);vSet.push_back(voltsSpinBox_2);vSet.push_back(voltsSpinBox_3);
  aSet.push_back(currLimSpinBox_1);aSet.push_back(currLimSpinBox_2);aSet.push_back(currLimSpinBox_3);
  vSlabel.push_back(setVoltsLabel_1);vSlabel.push_back(setVoltsLabel_2);vSlabel.push_back(setVoltsLabel_3);
  aSlabel.push_back(setAmpsLabel_1);aSlabel.push_back(setAmpsLabel_2);aSlabel.push_back(setAmpsLabel_3);

  if(isStandalone) deviceDescription->hide();

  m_dev = NULL;
  typeChg(devTypeBox->currentText());
  setChan(0);

  connect(devTypeBox, SIGNAL(activated(QString)), this, SLOT(typeChg(QString)));
  connect(gpibSpinBox, SIGNAL(valueChanged(int)), this, SLOT(padChanged(int)));
}
void SuppControlBase::typeChg(QString label){
  if(label=="GPIB"){
    idLabel->clear();
    idLabel->addItem("PAD ");
    gpibSpinBox->setMaximum(99);
  } else if(label=="RS232"){
    idLabel->clear();
    idLabel->addItem("COM ");
    idLabel->addItem("USB ");
    gpibSpinBox->setMaximum(29);
  } else {
    std::cerr << "Unhandled device type: " << std::string(label.toLatin1().data()) << std::endl;
    idLabel->clear();
  }
  padChanged(gpibSpinBox->value());
}
void SuppControlBase::padChanged(int pad){
  if(deviceDescription->text().left(5)=="GPIB_" || deviceDescription->text().left(6)=="RS232_" ||
     deviceDescription->text().isEmpty())
    deviceDescription->setText(devTypeBox->currentText()+"_"+QString::number(pad));
}
void SuppControlBase::setChan(int nChan){
  m_nChan = nChan;
  if(m_nChan>(int)vDisp.size()) m_nChan = (int)vDisp.size();
  for(int i=m_nChan;i<(int)vDisp.size();i++) {
    vDisp.at(i)->hide();
    aDisp.at(i)->hide();
    vLabel.at(i)->hide();
    aLabel.at(i)->hide();
    vSet.at(i)->hide();
    aSet.at(i)->hide();
    vSlabel.at(i)->hide();
    aSlabel.at(i)->hide();
  }
  for(int i=0;i<m_nChan;i++) {
    vDisp.at(i)->show();
    aDisp.at(i)->show();
    vLabel.at(i)->show();
    aLabel.at(i)->show();
    vSet.at(i)->show();
    aSet.at(i)->show();
    vSlabel.at(i)->show();
    aSlabel.at(i)->show();
  }
}
void SuppControlBase::connectDev(){
  if(m_dev!=NULL) disconnect();
  if(devTypeBox->currentText()=="GPIB"){
    PixGPIBDevice *dev = new PixGPIBDevice(0, gpibSpinBox->value(), 1, false);
    m_dev = dev;
    if(dev->getStatus()==PixGPIBDevice::PGD_ERROR){
      delete dev; m_dev=NULL;
      devMsgLabel->setText("ERROR connecting to device");
      return;
    }
    if(dev->getDeviceFunction()!=SUPPLY_LV && dev->getDeviceFunction()!=SUPPLY_HV){
      delete dev; m_dev=NULL;
      devMsgLabel->setText("ERROR: device is not a (known) supply");
      return;
    }
    devMsgLabel->setText("Conn. device is "+QString(dev->getDescription())+
			 " with "+
			 QString::number(dev->getDeviceNumberChannels())+
			 " ch.");
    setChan(dev->getDeviceNumberChannels());
    for(int i=0;i<m_nChan;i++){
      aLabel.at(i)->setCurrentText((dev->getDeviceFunction()==SUPPLY_HV)?"µA":"A");
      aSlabel.at(i)->setCurrentText((dev->getDeviceFunction()==SUPPLY_HV)?"µA":"A");
    }
  } else if(devTypeBox->currentText()=="RS232"){
    int iPort = gpibSpinBox->value()-1;
    if(idLabel->currentText()=="USB ")
      iPort += PixRs232Device::USB1;
    else
      iPort += PixRs232Device::COM1;
    PixRs232Device *dev = new PixRs232Device((PixRs232Device::Portids)(iPort));
    m_dev = dev;
    if(dev->getStatus()==PixRs232Device::COM_ERROR){
      delete dev; m_dev=NULL;
      devMsgLabel->setText("ERROR connecting to device");
      return;
    }
    if(dev->getDeviceFunction()!=SUPPLY_LV && dev->getDeviceFunction()!=SUPPLY_HV){
      delete dev; m_dev=NULL;
      devMsgLabel->setText("ERROR: device is not a (known) supply");
      return;
    }
    devMsgLabel->setText("Conn. device is "+QString(dev->getDescription())+
			 " with "+
			 QString::number(dev->getDeviceNumberChannels())+
			 " ch.");
    setChan(dev->getDeviceNumberChannels());
    for(int i=0;i<m_nChan;i++){
      aLabel.at(i)->setCurrentText((dev->getDeviceFunction()==SUPPLY_HV)?"µA":"A");
      aSlabel.at(i)->setCurrentText((dev->getDeviceFunction()==SUPPLY_HV)?"µA":"A");
    }
  }

  devTypeBox->setEnabled(false);
  gpibSpinBox->setEnabled(false);
  onoffButton->setEnabled(true);
  deviceDescription->setEnabled(false);
  idLabel->setEnabled(false);
  turnOff(false);
  m_thrRuns = true;;
  m_thread = std::thread(&SuppControlBase::readChansThr, this);
  connect(updateButton, SIGNAL(clicked()), this, SLOT(updateIV()));
  QList<QString> myTypes;
  for(int i=0;i<m_nChan;i++) {
    myTypes.push_back(deviceDescription->text()+"_Ch"+QString::number(i)+"_V ("+vLabel.at(i)->text()+")");       
    myTypes.push_back(deviceDescription->text()+"_Ch"+QString::number(i)+"_I ("+aLabel.at(i)->currentText()+")");
  }
  QCoreApplication::processEvents();
  emit newSuppply(myTypes);
  QCoreApplication::processEvents();
}

void SuppControlBase::disconnectDev(){
  turnOff(true);
  m_thrRuns = false;
  if(m_thread.joinable()) m_thread.join();
  if(devTypeBox->currentText()=="GPIB")
    delete ((PixGPIBDevice*)m_dev);
  else if(devTypeBox->currentText()=="RS232")
    delete ((PixRs232Device*)m_dev);
  m_dev=NULL;
  devMsgLabel->setText("");
  setChan(0);
  devTypeBox->setEnabled(true);
  gpibSpinBox->setEnabled(true);
  onoffButton->setEnabled(false);
  updateButton->setEnabled(false);
  deviceDescription->setEnabled(true);
  idLabel->setEnabled(true);
  disconnect(updateButton, SIGNAL(clicked()), this, SLOT(updateIV()));
  QList<QString> myTypes;
  for(int i=0;i<m_nChan;i++) {
    myTypes.push_back(deviceDescription->text()+"_Ch"+QString::number(i)+"_V ("+vLabel.at(i)->text()+")");
    myTypes.push_back(deviceDescription->text()+"_Ch"+QString::number(i)+"_I ("+aLabel.at(i)->currentText()+")");
  }
  QCoreApplication::processEvents();
  emit obsSuppply(myTypes);
  QCoreApplication::processEvents();
}
void SuppControlBase::turnOn(){
  if(m_dev==NULL) return;
  onoffButton->setEnabled(false);
  updateButton->setEnabled(false);
  QCoreApplication::processEvents();
  m_mutex.lock();
  disconnect(onoffButton, SIGNAL(clicked()), this, SLOT(turnOn()));
  for(int i=0;i<m_nChan;i++) {
    double lval = aSet.at(i)->value();
    if(aSlabel.at(i)->currentText()=="µA") lval *= 1.e-6;
    if(devTypeBox->currentText()=="GPIB"){
      PixGPIBDevice * dev = (PixGPIBDevice*)m_dev;
      devMsgLabel->setText("Conn. device is "+QString(dev->getDescription())+
			   " with "+
			   QString::number(dev->getDeviceNumberChannels())+
			   " ch.");
      dev->setVoltage(i, vSet.at(i)->value());
      dev->setCurrentLimit(i, lval);
      aDisp.at(i)->setDigitCount((dev->getDeviceFunction()==SUPPLY_HV)?8:6);
      dev->setState(PixGPIBDevice::PGD_ON);
    } else if(devTypeBox->currentText()=="RS232") {
      PixRs232Device *dev = (PixRs232Device*)m_dev;
      devMsgLabel->setText("Conn. device is "+QString(dev->getDescription())+
			   " with "+
			   QString::number(dev->getDeviceNumberChannels())+
			   " ch.");
      aDisp.at(i)->setDigitCount((dev->getDeviceFunction()==SUPPLY_HV)?8:6);
      dev->setCurrentLimit(i, 0.);
      dev->setVoltage(i, vSet.at(i)->value());
      dev->setPowerStatus(PixRs232Device::COM_ON);
    }
    QCoreApplication::processEvents();
    std::this_thread::sleep_for (std::chrono::milliseconds(100));  
  }

  bool inRamp = true;
  while(inRamp){
    inRamp = false;
    for(int i=0;i<m_nChan;i++) {
      if(vSet.at(i)->value()!=0.){
	bool haveTrip = readChans(true);
	inRamp |= !haveTrip && (fabs(vDisp.at(i)->value()/vSet.at(i)->value()-1.)>0.1) // wait until at target +- 10%
	             && (fabs(aDisp.at(i)->value()/aSet.at(i)->value()-1.)>0.05); // or until current limit was reached 
      }
    }
    QCoreApplication::processEvents();
    std::this_thread::sleep_for (std::chrono::milliseconds(250));  
  }
  if(devTypeBox->currentText()=="RS232") {
    PixRs232Device *dev = (PixRs232Device*)m_dev;
    for(int i=0;i<m_nChan;i++) {
      double lval = aSet.at(i)->value();
      if(aSlabel.at(i)->currentText()=="µA") lval *= 1.e-6;
      dev->setCurrentLimit(i, lval);
    }
  }

  m_mutex.unlock();
  updateButton->setEnabled(true);
  onoffButton->setEnabled(true);
  onoffButton->setText("ON");
  onoffButton->setStyleSheet("QPushButton {background-color: orange}");
  QCoreApplication::processEvents();
  connect(onoffButton, SIGNAL(clicked()), this, SLOT(turnOff()));
}
void SuppControlBase::turnOff(){
  turnOff(true);
}
void SuppControlBase::turnOff(bool wait){
  if(m_dev==NULL) return;
  onoffButton->setEnabled(false);
  updateButton->setEnabled(false);
  QCoreApplication::processEvents();
  m_mutex.lock();
  disconnect(onoffButton, SIGNAL(clicked()), this, SLOT(turnOff()));
  for(int i=0;i<m_nChan;i++) {
    if(devTypeBox->currentText()=="GPIB"){
      PixGPIBDevice * dev = (PixGPIBDevice*)m_dev;
      dev->setVoltage(i, 0.);
      dev->setState(PixGPIBDevice::PGD_OFF);
    } else if(devTypeBox->currentText()=="RS232") {
      PixRs232Device *dev = (PixRs232Device*)m_dev;
      dev->setCurrentLimit(i, 0.);
      dev->setVoltage(i, 0.);
      dev->setPowerStatus(PixRs232Device::COM_OFF);
    }
    QCoreApplication::processEvents();
    std::this_thread::sleep_for (std::chrono::milliseconds(100));  
  }

  bool inRamp = true;
  while(inRamp && wait){
    inRamp = false;
    for(int i=0;i<m_nChan;i++) {
      if(vSet.at(i)->value()!=0.){
	bool haveTrip = readChans(true);
	inRamp |= !haveTrip && (fabs(vDisp.at(i)->value()/vSet.at(i)->value())>0.1) // wait until at target +- 10%
	             && (fabs(aDisp.at(i)->value()/aSet.at(i)->value()-1.)>0.05); // or until current limit was reached 
      }
    }
    QCoreApplication::processEvents();
    std::this_thread::sleep_for (std::chrono::milliseconds(250));  
  }

  m_mutex.unlock();
  onoffButton->setEnabled(true);
  onoffButton->setText("OFF");
  onoffButton->setStyleSheet("QPushButton {background-color: lightgrey}");
  connect(onoffButton, SIGNAL(clicked()), this, SLOT(turnOn()));
}

QString convDispl(double value, int digs){
  char cstrg[50], fstrg[20];
  if(digs>0)
    sprintf(fstrg, "%%.%dlf", digs);
  else
    sprintf(fstrg, "%%.%dle", -digs);
  sprintf(cstrg, fstrg, value);
  return QString(cstrg);
}
void SuppControlBase::readChansThr(){
  while(m_thrRuns){
    m_mutex.lock();
    bool haveTrip = readChans(false);
    m_mutex.unlock();
    if(haveTrip) turnOff(false);
    QMap<QString, double> valmap;
    for(int i=0;i<m_nChan;i++) {
      valmap.insert(deviceDescription->text()+"_Ch"+QString::number(i)+"_V ("+vLabel.at(i)->text()+")"       , vDisp.at(i)->value());
      valmap.insert(deviceDescription->text()+"_Ch"+QString::number(i)+"_I ("+aLabel.at(i)->currentText()+")", aDisp.at(i)->value());
    }
    setNewRdgEvent *nre = new setNewRdgEvent(valmap);
    QCoreApplication::postEvent(this, nre);

    std::this_thread::sleep_for (std::chrono::milliseconds(500));    
  }
}
bool SuppControlBase::readChans(bool publish){
  if(devTypeBox->currentText()=="GPIB"){
    PixGPIBDevice * dev = (PixGPIBDevice*)m_dev;
    dev->measureVoltages();
    dev->measureCurrents();
    for(int i=0;i<m_nChan;i++) {
      vDisp.at(i)->display(convDispl(dev->getVoltage(i),(dev->getDeviceFunction()==SUPPLY_HV)?1:2));
      int digis = 3;
      double ival = dev->getCurrent(i);
      if(aLabel.at(i)->currentText()=="µA") ival *= 1.e6;
      if(fabs(ival)<5.e-3) digis=-1;
      aDisp.at(i)->display(convDispl(ival,digis));
    }
    if(dev->getStatus()==PixGPIBDevice::PGD_ERROR){
      std::string errMsg;
      dev->getError(errMsg);
      devMsgLabel->setText(("GPIB error\n"+errMsg).c_str());
    }
  } else if(devTypeBox->currentText()=="RS232") {
    PixRs232Device *dev = (PixRs232Device*)m_dev;
    for(int i=0;i<m_nChan;i++) {
      PixRs232Device::DevicePowerStatus ps = dev->getPowerStatus(i);
      if(ps==PixRs232Device::COM_LIM){ // supply tripped -> turn off in GUI
	devMsgLabel->setText("Supply tripped -> turning off");
	return true;
      }
      else if(ps==PixRs232Device::COM_OFF && onoffButton->text()=="ON"){
	devMsgLabel->setText("Inconsistent on/off state: check front panel switch!");
	return true;
      }
      float val = dev->getVoltage(i);
      if(val>-999999.)
	vDisp.at(i)->display(convDispl(val,(dev->getDeviceFunction()==SUPPLY_HV)?1:2));
      else
	std::cerr << "Error reading voltage for RS232 device on "<< idLabel->currentText().toStdString() << gpibSpinBox->value() << std::endl;
      QCoreApplication::processEvents();
      int digis = 3;
      double ival = dev->getCurrent(i);
      if(ival>-999999.){
	if(aLabel.at(i)->currentText()=="µA") ival *= 1.e6;
	if(fabs(ival)<5.e-3) digis=-1;
	aDisp.at(i)->display(convDispl(ival,digis));
      } else
	std::cerr << "Error reading current for RS232 device on "<< idLabel->currentText().toStdString() << gpibSpinBox->value() << std::endl;
      QCoreApplication::processEvents();
    }
    if(dev->getStatus()!=PixRs232Device::COM_OK){
      std::string errMsg;
      dev->getError(errMsg);
      std::cerr << errMsg << std::endl;
      if(dev->getStatus()!=PixRs232Device::COM_ERROR)
	devMsgLabel->setText("RS232 warning: check std.err printout");
      else {
	devMsgLabel->setText("RS232 error: check std.err printout - attempting re-connection");
	// attempt re-connection:
	delete dev;
	std::this_thread::sleep_for (std::chrono::milliseconds(500));  
	int iPort = gpibSpinBox->value()-1;
	if(idLabel->currentText()=="USB ")
	  iPort += PixRs232Device::USB1;
	else
	  iPort += PixRs232Device::COM1;
	PixRs232Device *dev = new PixRs232Device((PixRs232Device::Portids)(iPort));
	dev = new PixRs232Device((PixRs232Device::Portids)(iPort));
	m_dev = dev;
	if(dev->getStatus()==PixRs232Device::COM_ERROR){
	  // give up
	  devMsgLabel->setText("RS232 error: check std.err printout - re-connection unsuccessful");
	  return true;
	}
      }
    } else 
      devMsgLabel->setText("Conn. device is "+QString(dev->getDescription())+
			   " with "+
			   QString::number(dev->getDeviceNumberChannels())+
			   " ch.");
  }

  if(publish){ // must be able to exclude this when run from thread other than main thread (QT doesn't like signal emission then!)
    QMap<QString, double> valmap;
    for(int i=0;i<m_nChan;i++) {
      valmap.insert(deviceDescription->text()+"_Ch"+QString::number(i)+"_V ("+vLabel.at(i)->text()+")"       , vDisp.at(i)->value());
      valmap.insert(deviceDescription->text()+"_Ch"+QString::number(i)+"_I ("+aLabel.at(i)->currentText()+")", aDisp.at(i)->value());
    }
    emit newSuppRdg(valmap);
  }
  return false;
}
void SuppControlBase::updateIV(){
  m_mutex.lock();
  for(int i=0;i<m_nChan;i++) {
    double lval = aSet.at(i)->value();
    if(aSlabel.at(i)->currentText()=="µA") lval *= 1.e-6;
    if(devTypeBox->currentText()=="GPIB"){
      PixGPIBDevice * dev = (PixGPIBDevice*)m_dev;
      dev->setCurrentLimit(i, lval);
      dev->setVoltage(i, vSet.at(i)->value());
    } else if(devTypeBox->currentText()=="RS232") {
      PixRs232Device *dev = (PixRs232Device*)m_dev;
      dev->setCurrentLimit(i, 0.);
      dev->setVoltage(i, vSet.at(i)->value());
    }
  }
  bool inRamp = true;
  while(inRamp){
    inRamp = false;
    for(int i=0;i<m_nChan;i++) {
      if(vSet.at(i)->value()!=0.){
	bool haveTrip = readChans(true);
	inRamp |= !haveTrip && (fabs(vDisp.at(i)->value()/vSet.at(i)->value()-1.)>0.1) // wait until at target +- 10%
	             && (fabs(aDisp.at(i)->value()/aSet.at(i)->value()-1.)>0.05); // or until current limit was reached 
      }
    }
    QCoreApplication::processEvents();
    std::this_thread::sleep_for (std::chrono::milliseconds(250));  
  }
  if(devTypeBox->currentText()=="RS232") {
    PixRs232Device *dev = (PixRs232Device*)m_dev;
    for(int i=0;i<m_nChan;i++) {
      double lval = aSet.at(i)->value();
      if(aSlabel.at(i)->currentText()=="µA") lval *= 1.e-6;
      dev->setCurrentLimit(i, lval);
    }
  }
  m_mutex.unlock();
}
void SuppControlBase::customEvent( QEvent * event )
{
  if(event==0){
    std::cerr << "SuppControlBase::customEvent called with a NULL-pointer";
    return;
  }
  
  if(event->type()==2001){
    QMap<QString, double> valmap = dynamic_cast<setNewRdgEvent*>(event)->getVals();
    emit newSuppRdg(valmap);
  }
}
