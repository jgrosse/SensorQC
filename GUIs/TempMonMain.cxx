#include "TempMon.h"
#include "DataLogging.h"
#include "QRootApplication.h"
#include <QMainWindow>
#include <QDockWidget>

#include <iostream>
#include <sstream>
#include <exception>

int main( int argc, char** argv )
{
  bool cycling = false;
  std::string cfgfile="";
  for(int i=1; i<argc; i++){
    if(strcmp(argv[i], "-h")==0){
      std::cout << "Usage:" << std::endl;
      std::cout << "TempMon [-c] [-f file]" << std::endl;
      std::cout << "   -c start in mode with 2nd set of ambient sensors" << std::endl;
      std::cout << "   -f file loads pre-set of channels etc. and calibration constants of sensors" << std::endl;
      return 0;
    }
    if(strcmp(argv[i], "-c")==0) cycling = true;
    if(strcmp(argv[i], "-f")==0 && i<(argc-1)){
      cfgfile = argv[i+1];
      i++;
    }
  }

  // start root and QT application
  QRootApplication app( argc, argv);

  // create main data viewer window
  QMainWindow *Win = new QMainWindow(0);
  Win->setWindowTitle("Temperature Monitoring");

  QDockWidget* dockWidget_1 = new QDockWidget(Win);
  dockWidget_1->setObjectName(QStringLiteral("dockWidget_1"));
  dockWidget_1->setWindowTitle("Temperature Devices");
  QDockWidget* dockWidget_2 = new QDockWidget(Win);
  dockWidget_2->setObjectName(QStringLiteral("dockWidget_2"));
  dockWidget_2->setWindowTitle("Data Logging");

  QString hname = qgetenv("HOSTNAME");
  hname = hname.split(".").at(0);
  TempMon *tm = new TempMon(Win, Qt::Window, cycling, cfgfile);
  dockWidget_1->setWidget(tm);
  DataLogging *dl = new DataLogging(Win, Qt::Window);
  dockWidget_2->setWidget(dl);
  // dynamically alter some pre-sets in logger GUI
  if(cycling) dl->dbHostPort->setText("192.168.1.100:8086");
  dl->dbMeasName->setText(dl->dbMeasName->text()+"_"+hname);
  QWidget::connect(tm, SIGNAL(newTempVal(QMap<QString, double>)), dl, SLOT(newValues(QMap<QString, double>)) );
  QWidget::connect(tm, SIGNAL(obsTempDev(QList<QString>)), dl, SLOT(obsValues(QList<QString>)) );

  Win->addDockWidget(Qt::LeftDockWidgetArea,  dockWidget_1);
  Win->addDockWidget(Qt::LeftDockWidgetArea,  dockWidget_2);
  Win->show();

  // executing our application
  int ret = 0;
  std::stringstream msg;
  try{
    app.startTimer();
    ret  = app.exec();
  } catch(std::exception& s){
    msg << "Std-lib exception \"";
    msg << s.what();
  } catch(...){
    msg << "Unknown exception \"";
  }
  if(msg.str()!="")
    std::cerr << msg.str() << "\" not caught during execution of main window." << std::endl;
  // cleaning up
  delete Win;

  return ret;
}
